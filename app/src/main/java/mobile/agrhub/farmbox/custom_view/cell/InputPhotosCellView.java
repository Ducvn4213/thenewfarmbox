package mobile.agrhub.farmbox.custom_view.cell;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.dmcbig.mediapicker.PickerActivity;
import com.dmcbig.mediapicker.PickerConfig;
import com.dmcbig.mediapicker.entity.Media;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.adapters.InputPhotoCellAdapter;
import mobile.agrhub.farmbox.custom_view.layout.ExpandableHeightGridView;
import mobile.agrhub.farmbox.service.model.Photo;

public class InputPhotosCellView extends BaseCellView {
    public static final int REQUEST_CHOOSE_PHOTO_CODE = 901;


    private ImageButton mAddPhotos;
    private ExpandableHeightGridView mPhotoView;
    private InputPhotoCellAdapter mAdapter;

    private List<Photo> mPhotos;

    private InputPhotosCellListener listener;

    @Override
    protected int layoutView() {
        return R.layout.layout_input_photos_cell;
    }

    @Override
    public boolean isEmpty() {
        return mPhotos == null || mPhotos.isEmpty();
    }

    public InputPhotosCellView(Context context) {
        super(context);
    }

    public InputPhotosCellView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void bindingControls() {
        super.bindingControls();
        mAddPhotos = (ImageButton) findViewById(R.id.ib_add_photo);
        mAddPhotos.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), PickerActivity.class);
                intent.putExtra(PickerConfig.SELECT_MODE, PickerConfig.PICKER_IMAGE);//default image and video (Optional)
                long maxSize = 188743680L;//long long long
                intent.putExtra(PickerConfig.MAX_SELECT_SIZE, maxSize); //default 180MB (Optional)
                intent.putExtra(PickerConfig.MAX_SELECT_COUNT, 10);  //default 40 (Optional)

                if (listener != null) {
                    Log.d("PHOTO", listener.getActivity().toString());
                    listener.getActivity().startActivityForResult(intent, REQUEST_CHOOSE_PHOTO_CODE);
                }
            }
        });


        mPhotoView = (ExpandableHeightGridView) findViewById(R.id.gv_photos);
        // diary photos
        mPhotos = new ArrayList<Photo>();
        mAdapter = new InputPhotoCellAdapter(getContext(), mPhotos);
        mPhotoView.setAdapter(mAdapter);
        mPhotoView.setExpanded(true);
    }

    public void setListener(InputPhotosCellListener listener) {
        this.listener = listener;
    }

    public void addMediaList(List<Media> mediaList) {
        if (null != mediaList && mediaList.size() > 0) {
            for (Media item : mediaList) {
                addMediaItem(item, false);
            }

            mAdapter.notifyDataSetChanged();
        }
    }

    public void addMediaItem(Media media, boolean needRefresh) {
        boolean existed = false;
        if (null != mPhotos && mPhotos.size() > 0) {
            for (Photo item : mPhotos) {
                if (item.isSame(media)) {
                    existed = true;
                    break;
                }
            }
        }
        if (!existed) {
            Photo photo = new Photo(media);
            mPhotos.add(photo);
        }

        if (needRefresh) {
            mAdapter.notifyDataSetChanged();
        }
    }

    public void addPhoto(Photo photo, boolean needRefresh) {
        mPhotos.add(photo);

        if (needRefresh) {
            mAdapter.notifyDataSetChanged();
        }
    }

    public List<Photo> getVal() {
        return mPhotos;
    }

    public Media getMedia() {
        if (mPhotos != null && mPhotos.size() > 0) {
            for (Photo item : mPhotos) {
                if (!item.isOnServer && item.media != null) {
                    return item.media;
                }
            }
        }
        return null;
    }

    public List<Media> getMediaList() {
        List<Media> mediaList = new ArrayList<>();
        if (mPhotos != null && mPhotos.size() > 0) {
            for (Photo item : mPhotos) {
                if (!item.isOnServer && item.media != null) {
                    mediaList.add(item.media);
                }
            }
        }
        return mediaList;
    }

    public String getOnlinePhoto() {
        if (mPhotos != null && mPhotos.size() > 0) {
            for (Photo item : mPhotos) {
                if (item.isOnServer && item.uri != null && !item.uri.isEmpty()) {
                    return item.uri;
                }
            }
        }
        return null;
    }

    public int size() {
        return mPhotos == null ? 0 : mPhotos.size();
    }

    public Photo get(int idx) {
        return mPhotos == null ? null : mPhotos.get(idx);
    }

    public interface InputPhotosCellListener {
        Activity getActivity();
    }
}
