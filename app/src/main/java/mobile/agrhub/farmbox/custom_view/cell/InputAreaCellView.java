package mobile.agrhub.farmbox.custom_view.cell;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

import mobile.agrhub.farmbox.R;

public class InputAreaCellView extends BaseCellView {

    private EditText mVal;

    @Override
    protected int layoutView() {
        return R.layout.layout_input_area_cell;
    }

    @Override
    public boolean isEmpty() {
        return getVal().isEmpty();
    }

    public InputAreaCellView(Context context) {
        super(context);
    }

    public InputAreaCellView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void bindingControls() {
        super.bindingControls();
        mVal = (EditText) findViewById(R.id.et_value);
    }

    public void setValue(String val, String placeHolder) {
        mVal.setText(val);
        mVal.setHint(placeHolder);
    }

    public String getVal() {
        return mVal.getText().toString();
    }
}
