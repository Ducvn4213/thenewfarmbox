package mobile.agrhub.farmbox.custom_view.dairy;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class DiaryPhotoItemView extends RelativeLayout {
    public DiaryPhotoItemView(Context context) {
        super(context);
    }

    public DiaryPhotoItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DiaryPhotoItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec); // This is the key that will make the height equivalent to its width
    }
}
