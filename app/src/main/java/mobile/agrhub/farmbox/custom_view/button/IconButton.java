package mobile.agrhub.farmbox.custom_view.button;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import mobile.agrhub.farmbox.R;

public class IconButton extends LinearLayout {
    public IconButton(Context context) {
        super(context);
    }

    public IconButton(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public IconButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        TypedArray a = getContext().obtainStyledAttributes(
                attrs,
                R.styleable.IconButton);

        //Don't forget this
        a.recycle();
    }
}
