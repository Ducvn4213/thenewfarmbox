package mobile.agrhub.farmbox.custom_view.segment_control;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import mobile.agrhub.farmbox.R;

public class FourSegmentedControl extends LinearLayout {

    private Button mSegmentedA;
    private Button mSegmentedB;
    private Button mSegmentedC;
    private Button mSegmentedD;

    private Context mContext;

    private int selected = 0;
    private int colorIndex = 0;

    private SegmentedControl.OnSegmentedChangedListener onSegmentedChangedListener;

    public void setOnSegmentedChangedListener(SegmentedControl.OnSegmentedChangedListener onSegmentedChangedListener) {
        this.onSegmentedChangedListener = onSegmentedChangedListener;
    }

    public FourSegmentedControl(Context context) {
        this(context, null);
    }

    public FourSegmentedControl(final Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_four_segmented, this, true);

        bindingControls();
        setupControlEvents();
        init();
    }

    void bindingControls() {
        mSegmentedA = (Button) findViewById(R.id.btn_segmented_a);
        mSegmentedB = (Button) findViewById(R.id.btn_segmented_b);
        mSegmentedC = (Button) findViewById(R.id.btn_segmented_c);
        mSegmentedD = (Button) findViewById(R.id.btn_segmented_d);
    }

    void setupControlEvents() {
        mSegmentedA.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                selected = 0;
                onSegmentedChangedListener.onSegmentedChanged(0);
                updateUI();
            }
        });

        mSegmentedB.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                selected = 1;
                onSegmentedChangedListener.onSegmentedChanged(1);
                updateUI();
            }
        });

        mSegmentedC.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                selected = 2;
                onSegmentedChangedListener.onSegmentedChanged(2);
                updateUI();
            }
        });

        mSegmentedD.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                selected = 3;
                onSegmentedChangedListener.onSegmentedChanged(3);
                updateUI();
            }
        });
    }

    void init() {
        updateUI();
    }

    void updateUI() {
        if (selected == 0) {
            mSegmentedA.setTextColor(Color.WHITE);
            mSegmentedA.setBackground(getFillDrawable(0));

            mSegmentedB.setTextColor(getMainColor());
            mSegmentedB.setBackground(getNormalDrawable(1));
            mSegmentedC.setTextColor(getMainColor());
            mSegmentedC.setBackground(getNormalDrawable(1));
            mSegmentedD.setTextColor(getMainColor());
            mSegmentedD.setBackground(getNormalDrawable(2));
        } else if (selected == 1) {
            mSegmentedB.setTextColor(Color.WHITE);
            mSegmentedB.setBackground(getFillDrawable(1));

            mSegmentedA.setTextColor(getMainColor());
            mSegmentedA.setBackground(getNormalDrawable(0));
            mSegmentedC.setTextColor(getMainColor());
            mSegmentedC.setBackground(getNormalDrawable(1));
            mSegmentedD.setTextColor(getMainColor());
            mSegmentedD.setBackground(getNormalDrawable(2));
        } else if (selected == 2) {
            mSegmentedC.setTextColor(Color.WHITE);
            mSegmentedC.setBackground(getFillDrawable(1));

            mSegmentedB.setTextColor(getMainColor());
            mSegmentedB.setBackground(getNormalDrawable(1));
            mSegmentedA.setTextColor(getMainColor());
            mSegmentedA.setBackground(getNormalDrawable(0));
            mSegmentedD.setTextColor(getMainColor());
            mSegmentedD.setBackground(getNormalDrawable(2));
        } else if (selected == 3) {
            mSegmentedD.setTextColor(Color.WHITE);
            mSegmentedD.setBackground(getFillDrawable(2));

            mSegmentedB.setTextColor(getMainColor());
            mSegmentedB.setBackground(getNormalDrawable(1));
            mSegmentedC.setTextColor(getMainColor());
            mSegmentedC.setBackground(getNormalDrawable(1));
            mSegmentedA.setTextColor(getMainColor());
            mSegmentedA.setBackground(getNormalDrawable(0));
        }
    }

    public void setSelected(int index) {
        selected = index;
        updateUI();
    }

    public int getSelected() {
        return selected;
    }

    public void setTextA(String data) {
        mSegmentedA.setText(data);
    }

    public void setTextB(String data) {
        mSegmentedB.setText(data);
    }

    public void setTextC(String data) {
        mSegmentedC.setText(data);
    }

    public void setTextD(String data) {
        mSegmentedD.setText(data);
    }

    public void setColorIndex(int index) {
        colorIndex = index;
        updateUI();
    }

    private int getMainColor() {
        switch (colorIndex) {
            case 1:
                return ContextCompat.getColor(mContext, R.color.color1);
            case 2:
                return ContextCompat.getColor(mContext, R.color.color2);
            case 3:
                return ContextCompat.getColor(mContext, R.color.color3);
            case 4:
                return ContextCompat.getColor(mContext, R.color.color4);
            case 5:
                return ContextCompat.getColor(mContext, R.color.color5);
        }

        return 0;
    }

    private Drawable getFillDrawable(int pos) {
        if (pos == 0) {
            switch (colorIndex) {
                case 1:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_left_bottom_fill_color1);
                case 2:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_left_bottom_fill_color2);
                case 3:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_left_bottom_fill_color3);
                case 4:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_left_bottom_fill_color4);
                case 5:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_left_bottom_fill_color5);
            }
        } else if (pos == 1) {
            switch (colorIndex) {
                case 1:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_square_fill_color1);
                case 2:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_square_fill_color2);
                case 3:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_square_fill_color3);
                case 4:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_square_fill_color4);
                case 5:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_square_fill_color5);
            }
        } else {
            switch (colorIndex) {
                case 1:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_right_bottom_fill_color1);
                case 2:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_right_bottom_fill_color2);
                case 3:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_right_bottom_fill_color3);
                case 4:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_right_bottom_fill_color4);
                case 5:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_right_bottom_fill_color5);
            }
        }

        return null;
    }

    private Drawable getNormalDrawable(int pos) {
        if (pos == 0) {
            switch (colorIndex) {
                case 1:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_left_bottom_color1);
                case 2:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_left_bottom_color2);
                case 3:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_left_bottom_color3);
                case 4:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_left_bottom_color4);
                case 5:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_left_bottom_color5);
            }
        } else if (pos == 1) {
            switch (colorIndex) {
                case 1:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_square_color1);
                case 2:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_square_color2);
                case 3:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_square_color3);
                case 4:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_square_color4);
                case 5:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_square_color5);
            }
        } else {
            switch (colorIndex) {
                case 1:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_right_bottom_color1);
                case 2:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_right_bottom_color2);
                case 3:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_right_bottom_color3);
                case 4:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_right_bottom_color4);
                case 5:
                    return ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_right_bottom_color5);
            }
        }

        return null;
    }
}
