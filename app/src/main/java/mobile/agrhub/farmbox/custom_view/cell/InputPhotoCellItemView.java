package mobile.agrhub.farmbox.custom_view.cell;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class InputPhotoCellItemView extends RelativeLayout {
    public InputPhotoCellItemView(Context context) {
        super(context);
    }

    public InputPhotoCellItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public InputPhotoCellItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec); // This is the key that will make the height equivalent to its width
    }
}
