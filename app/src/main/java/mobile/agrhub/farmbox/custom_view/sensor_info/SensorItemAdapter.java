package mobile.agrhub.farmbox.custom_view.sensor_info;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.Device;
import mobile.agrhub.farmbox.utils.Utils;

public class SensorItemAdapter extends RecyclerView.Adapter<SensorItemAdapter.MyViewHolder> {

    private List<Device> deviceList;
    private long farmType;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private SensorItemView sensorItemView;
        private LinearLayout noSensorView;

        public MyViewHolder(View view) {
            super(view);
            sensorItemView = (SensorItemView) view.findViewById(R.id.siv_sensor_item);
            noSensorView = (LinearLayout) view.findViewById(R.id.ll_no_sensor);
        }
    }

    public SensorItemAdapter(List<Device> farmsList) {
        farmType = 0;
        setDeviceList(deviceList, farmType);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_sensor_item_holder, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.sensorItemView.setBackgroundColor(Utils.getRandomColor(position));
        Device data = this.deviceList.get(position);
        if (data == null) {
            holder.sensorItemView.setVisibility(View.GONE);
            holder.noSensorView.setVisibility(View.VISIBLE);
        } else {
            holder.sensorItemView.setVisibility(View.VISIBLE);
            holder.noSensorView.setVisibility(View.GONE);
            holder.sensorItemView.updateUI(this.deviceList.get(position), this.farmType);
        }
    }

    @Override
    public int getItemCount() {
        return deviceList.size();
    }

    public void setDeviceList(List<Device> data, long farmType) {
        this.farmType = farmType;
        if (this.deviceList == null) {
            this.deviceList = new ArrayList<>();
        }
        this.deviceList.clear();
        if (data != null) {
            Collections.sort(data, new Comparator<Device>() {
                @Override
                public int compare(Device o1, Device o2) {
                    if (o1.device_type > o2.device_type) {
                        return 1;
                    } else if (o1.device_type < o2.device_type) {
                        return -1;
                    } else {
                        return o1.device_id < o2.device_id ? 1 : -1;
                    }
                }
            });

            this.deviceList.addAll(data);
        }

        if (deviceList.size() == 0) {
            deviceList.add(null);
        }
    }
}