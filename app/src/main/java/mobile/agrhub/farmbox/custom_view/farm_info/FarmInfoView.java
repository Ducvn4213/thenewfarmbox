package mobile.agrhub.farmbox.custom_view.farm_info;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.activities.edit.EditFarmActivity;
import mobile.agrhub.farmbox.service.model.Farm;
import mobile.agrhub.farmbox.utils.ResourceUtils;

public class FarmInfoView extends LinearLayout {

    private TextView mTitle;
    private TextView mName;
    private TextView mType;
    private TextView mAddress;
    private TextView mAcreages;
    private ImageButton mEdit;

    public FarmInfoView(Context context) {
        this(context, null);
    }

    public FarmInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_farm_info, this, true);

        bindingControls();
        configControlEvents();
    }

    private void bindingControls() {
        mTitle = (TextView) findViewById(R.id.tv_title);
        mName = (TextView) findViewById(R.id.tv_name);
        mType = (TextView) findViewById(R.id.tv_type);
        mAddress = (TextView) findViewById(R.id.tv_address);
        mAcreages = (TextView) findViewById(R.id.tv_acreages);
        mEdit = (ImageButton) findViewById(R.id.ib_edit);
    }

    private void configControlEvents() {
        mEdit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), EditFarmActivity.class);
                getContext().startActivity(intent);
            }
        });
    }

    public void updateUI(Farm data) {
        if (data == null) {
            mTitle.setText(R.string.farm_info_title);
            mName.setText("---");
            mType.setText("---");
            mAddress.setText("---");
            mAcreages.setText("---");
            mEdit.setVisibility(GONE);
            return;
        }

        if (Farm.STORAGE == data.farm_type) {
            mTitle.setText(R.string.storage_info_title);
        } else if (Farm.CATTLE_HOUSE == data.farm_type) {
            mTitle.setText(R.string.cattle_house_info_title);
        } else {
            mTitle.setText(R.string.farm_info_title);
        }
        mEdit.setVisibility(VISIBLE);
        mName.setText(data.farm_name);
        mType.setText(ResourceUtils.getFarmTypeName(data.farm_type));
        mAddress.setText(data.farm_address);
        mAcreages.setText(data.farm_acreage + "");
    }
}
