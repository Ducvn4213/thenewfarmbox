package mobile.agrhub.farmbox.custom_view.setting_item;


import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.activities.setting.SettingCronTimeActivity;
import mobile.agrhub.farmbox.activities.setting.SettingFormulaActivity;
import mobile.agrhub.farmbox.activities.setting.SettingMinMaxActivity;
import mobile.agrhub.farmbox.activities.setting.SettingOnOffTimeActivity;
import mobile.agrhub.farmbox.activities.setting.SettingSingleActivity;
import mobile.agrhub.farmbox.activities.setting.SettingTimeLoopActivity;

public class SettingItemView extends LinearLayout {

    public static enum TYPE {
        MINMAX,
        ONOFF,
        SINGLE,
        FORMULA,
        CRON,
        TIME_LOOP
    }

    private TextView mName;
    private TextView mValue;
    private TYPE mType;

    private String f0, f1, f2, f3;
    private String farmID, factoryID;

    public SettingItemView(Context context) {
        this(context, null);
    }

    public SettingItemView(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_setting_item, this, true);

        bindingControls();
    }

    private void bindingControls() {
        mName = (TextView) findViewById(R.id.tv_name);
        mValue = (TextView) findViewById(R.id.tv_value);
    }

    public void setName(String data) {
        mName.setText(data);
    }

    public void setValue(String data) {
        mValue.setText(data);
    }

    public void setBasicMetaData(TYPE type, String farmID, String factoryID) {
        this.mType = type;
        this.farmID = farmID;
        this.factoryID = factoryID;
    }

    public void setMetaData(String f0, String f1, String f2, String f3) {
        this.f0 = f0;
        this.f1 = f1;
        this.f2 = f2;
        this.f3 = f3;
    }

    public Intent getIntentToChangeSetting(Context context) {
        Intent intent = null;
        if (mType == TYPE.MINMAX) {
            intent = new Intent(context, SettingMinMaxActivity.class);
            intent.putExtra("min", f2);
            intent.putExtra("max", f3);
        } else if (mType == TYPE.ONOFF) {
            intent = new Intent(context, SettingOnOffTimeActivity.class);
            intent.putExtra("start", f2);
            intent.putExtra("stop", f3);
        } else if (mType == TYPE.SINGLE) {
            intent = new Intent(context, SettingSingleActivity.class);
            intent.putExtra("value", f2);
        } else if (mType == TYPE.FORMULA) {
            intent = new Intent(context, SettingFormulaActivity.class);
            intent.putExtra("A", f0);
            intent.putExtra("B", f1);
            intent.putExtra("C", f2);

            intent.putExtra("farmID", farmID);
            intent.putExtra("factoryID", factoryID);

            return intent;
        } else if (mType == TYPE.CRON) {
            intent = new Intent(context, SettingCronTimeActivity.class);
            intent.putExtra("data", f0);

            intent.putExtra("farmID", farmID);
            intent.putExtra("factoryID", factoryID);

            return intent;
        } else if (mType == TYPE.TIME_LOOP) {
            intent = new Intent(context, SettingTimeLoopActivity.class);
            intent.putExtra("run_time", f2);
            intent.putExtra("stop_time", f3);
        }

        intent.putExtra("field", f0);
        intent.putExtra("name", f1);
        intent.putExtra("farmID", farmID);
        intent.putExtra("factoryID", factoryID);
        return intent;
    }

    public String getSettingValue() {
        return f2;
    }

    public String getFarmID() {
        return farmID;
    }

    public String getFactoryID() {
        return factoryID;
    }
}
