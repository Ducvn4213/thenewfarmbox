package mobile.agrhub.farmbox.custom_view.cell;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.utils.Utils;

public class InputCellView extends BaseCellView {

    private EditText mVal;

    @Override
    protected int layoutView() {
        return R.layout.layout_input_cell;
    }

    @Override
    public boolean isEmpty() {
        return getVal().isEmpty();
    }

    public InputCellView(Context context) {
        super(context);
    }

    public InputCellView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void bindingControls() {
        super.bindingControls();
        mVal = (EditText) findViewById(R.id.et_value);
    }

    public void setInputType(int inputType) {
        mVal.setInputType(inputType);
    }

    public void setValue(String val, String placeHolder) {
        mVal.setText(val);
        mVal.setHint(placeHolder);
    }

    public String getVal() {
        return mVal.getText().toString();
    }

    public int getIntVal() {
        int val = 0;
        try {
            val = Utils.str2Int(getVal());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return val;
    }

    public long getLongVal() {
        long val = 0;
        try {
            val = Utils.str2Long(getVal());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return val;
    }

    public float getFloatVal() {
        float val = 0;
        try {
            val = Utils.str2Float(getVal());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return val;
    }
}
