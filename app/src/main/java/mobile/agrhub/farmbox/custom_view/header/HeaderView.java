package mobile.agrhub.farmbox.custom_view.header;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.WeatherManager;
import mobile.agrhub.farmbox.service.model.UnitItem;
import mobile.agrhub.farmbox.service.model.User;
import mobile.agrhub.farmbox.service.model.Weather;
import mobile.agrhub.farmbox.utils.Utils;

public class HeaderView extends LinearLayout {

    @BindView(R.id.iv_background)
    ImageView mBackground;
    @BindView(R.id.iv_sun_or_moon)
    ImageView mSunOrMoon;
    @BindView(R.id.sv_search)
    SearchView mSearchView;
    @BindView(R.id.tv_temperature)
    TextView mTemp;
    @BindView(R.id.tv_wind)
    TextView mWind;
    @BindView(R.id.tv_humidity)
    TextView mHumi;

    @BindView(R.id.iv_wind)
    ImageView mWindImage;
    @BindView(R.id.iv_humidity)
    ImageView mHumidityImage;
    @BindView(R.id.v_separator)
    View mSeparator;

    public HeaderView(Context context) {
        this(context, null);
    }

    public HeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_header_view, this, true);

        ButterKnife.bind(this);
    }

    private void updateUI(String phone) {
        if (phone.startsWith("+91")) {
            updateUI_Panim();
        } else {
            updateUI();
        }
    }

    public void updateUIWithoutData() {
        User user = FarmBoxAppController.getInstance().getUser();
        if (user != null) {
            String phone = user.user_phone;
            if (phone.startsWith("+91")) {
                updateUI_Panim();
            }
        }
    }

    private void updateUI_Panim() {
        Picasso.with(getContext()).load(R.drawable.the_pamin_logo).into(mBackground);
        mBackground.setBackgroundColor(getResources().getColor(android.R.color.white));

        setElementVisible(INVISIBLE);
    }

    private void updateUI() {
        setElementVisible(VISIBLE);

        WeatherManager weatherManager = FarmBoxAppController.getInstance().getWeatherManager();
        if (weatherManager.getWeatherInfo() != null) {
            Weather weather = weatherManager.getWeatherInfo();
            FarmBoxAppController app = FarmBoxAppController.getInstance();
            mTemp.setText(app.getTemperatureValue(Math.round(weather.data.temp), UnitItem.TEMPERATURE_F_VALUE) + app.getTemperatureUnit());
            mWind.setText(Math.round(weather.data.wind) + "km");
            mHumi.setText(Math.round(weather.data.humi * 100) + "%");

            int hour = Integer.parseInt(Utils.getDate(weather.data.time * 1000, "HH"));

            if (hour > 5 && hour < 18) {
                setUIForDay();
                mSunOrMoon.setImageResource(R.mipmap.ic_sun);
            } else {
                setUIForNight();
                mSunOrMoon.setImageResource(R.mipmap.ic_moon);
            }
        }
    }

    private void setElementVisible(int value) {
        mSunOrMoon.setVisibility(value);
        mTemp.setVisibility(value);
        mWind.setVisibility(value);
        mHumi.setVisibility(value);
//        mSearchView.setVisibility(value);
        mWindImage.setVisibility(value);
        mHumidityImage.setVisibility(value);
        mSeparator.setVisibility(value);
    }

    public void setWeatherData(Weather data) {
        User user = FarmBoxAppController.getInstance().getUser();
        updateUI(user.user_phone);
    }

    private void setUIForNight() {
        Picasso.with(getContext()).load(R.drawable.farm_night).into(mBackground);
    }

    private void setUIForDay() {
        Picasso.with(getContext()).load(R.drawable.farm_day).into(mBackground);
    }
}
