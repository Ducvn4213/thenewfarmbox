package mobile.agrhub.farmbox.custom_view.sensor_info;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.activities.sensor.ChartActivity;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.model.Sensor;

public class SensorItemChildView extends LinearLayout {

    private RelativeLayout container;
    private TextView valueTextView;
    private TextView unitTextView;
    private TextView nameTextView;

    private Sensor data;

    public SensorItemChildView(Context context) {
        this(context, null);
    }

    public SensorItemChildView(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_sensor_child_item, this, true);

        bindingControls();
        setupEvent();
    }

    private void bindingControls() {
        valueTextView = (TextView) findViewById(R.id.tv_value);
        unitTextView = (TextView) findViewById(R.id.tv_unit);
        nameTextView = (TextView) findViewById(R.id.tv_name);
        container = (RelativeLayout) findViewById(R.id.rl_container);
    }

    private void setupEvent() {
        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                long farmType = 0;
                FarmBoxAppController app = FarmBoxAppController.getInstance();
                if (app.getSelectedFarm() != null) {
                    farmType = app.getSelectedFarm().farm_type;
                }

                Intent intent = new Intent(getContext(), ChartActivity.class);
                intent.putExtra("id", data.sensor_id);
                intent.putExtra("type", data.sensor_type);
                intent.putExtra("farmType", farmType);
                getContext().startActivity(intent);
            }
        });
    }

    public void updateUI(Sensor data, long deviceType) {
        this.data = data;

        valueTextView.setText(data.displayValue());
        unitTextView.setText(data.displayUnit((int)deviceType));
        nameTextView.setText(data.shortName(getContext()));

//        if (FBHelper.getInstance().currentPresentSensor != null && data.sensor_id == FBHelper.getInstance().currentPresentSensor.sensor_id) {
//            container.setBackgroundResource(R.drawable.sensor_child_item_round_selected);
//        }
//        else {
//            container.setBackgroundResource(R.drawable.sensor_child_item_round);
//        }
    }

    public void makeSelected() {
//        try {
//            if (FBHelper.getInstance().currentPresentSensor == data || FBHelper.getInstance().currentPresentSensor.sensor_id == data.sensor_id) {
//                FBHelper.getInstance().currentPresentSensor = null;
//                return;
//            }
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        FBHelper.getInstance().currentPresentSensor = data;
    }
}
