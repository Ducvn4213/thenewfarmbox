package mobile.agrhub.farmbox.custom_view.farm_hub;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.FarmBasic;

public class FarmHubView extends LinearLayout {

    private RecyclerView mHolder;
    private FarmHubAdapter mAdapter;
    private FarmHubAdapter.FarmHubItemSelectedListener onFarmHubItemSelectedListener;

    public FarmHubView(Context context) {
        this(context, null);
    }

    public FarmHubView(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_farm_hub, this, true);

        bindingControls();
        initData();
    }

    private void bindingControls() {
        mHolder = (RecyclerView) findViewById(R.id.rv_farm_hub);
    }

    private void initData() {
        mAdapter = new FarmHubAdapter(getContext(), new ArrayList<FarmBasic>());
        mAdapter.setOnItemSelectedListener(onItemSelectedListener);
        mAdapter.setSelected(0);
        mHolder.setAdapter(mAdapter);
    }

    private FarmHubAdapter.FarmHubItemSelectedListener onItemSelectedListener = new FarmHubAdapter.FarmHubItemSelectedListener() {
        @Override
        public void onFarmHubItemSelected(int index, FarmBasic data) {
            if (data != null) {
                mAdapter.setSelected(index);
            }

            if (onFarmHubItemSelectedListener != null) {
                onFarmHubItemSelectedListener.onFarmHubItemSelected(index, data);
            }
        }
    };

    public void setOnFarmHubItemSelectedListener(FarmHubAdapter.FarmHubItemSelectedListener listener) {
        this.onFarmHubItemSelectedListener = listener;
    }

    public void updateUI(List<FarmBasic> data) {
        mAdapter.setFarmsList(data);
        mAdapter.notifyDataSetChanged();
    }

    public int getSelectedFarm() {
        return mAdapter.getSelected();
    }

    public void setSelectedFarm(int idx) {
        mAdapter.setSelected(idx);
    }
}
