package mobile.agrhub.farmbox.custom_view.cell;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import mobile.agrhub.farmbox.R;

public class InputChooseOptionCellView extends BaseCellView {
    public static final long NONE = -1;

    private TextView mVal;
    private long selectedVal = InputChooseOptionCellView.NONE;

    @Override
    protected int layoutView() {
        return R.layout.layout_input_choose_option_cell;
    }

    @Override
    public boolean isEmpty() {
        return selectedVal == InputChooseOptionCellView.NONE;
    }

    public InputChooseOptionCellView(Context context) {
        super(context);
    }

    public InputChooseOptionCellView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void bindingControls() {
        super.bindingControls();
        mVal = (TextView) findViewById(R.id.et_value);
    }

    public void setValue(String val, long selectedVal) {
        mVal.setText(val);
        this.selectedVal = selectedVal;
    }

    public long getVal() {
        return selectedVal;
    }
}
