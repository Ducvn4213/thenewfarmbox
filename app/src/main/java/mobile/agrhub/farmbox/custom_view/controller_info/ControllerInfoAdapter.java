package mobile.agrhub.farmbox.custom_view.controller_info;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.custom_view.controller_info.ControllerItemView.ControllerItemView;
import mobile.agrhub.farmbox.service.model.Controller;
import mobile.agrhub.farmbox.service.model.Device;
import mobile.agrhub.farmbox.service.model.Farm;
import mobile.agrhub.farmbox.utils.ResourceUtils;

import static mobile.agrhub.farmbox.utils.ResourceUtils.ARIATEC_HYDROPONIC_CONTROLLER;
import static mobile.agrhub.farmbox.utils.ResourceUtils.BROADLINK_SP3_SMART_PLUG;
import static mobile.agrhub.farmbox.utils.ResourceUtils.FIOT_SMART_TANK_CONTROLLER;

public class ControllerInfoAdapter extends RecyclerView.Adapter<ControllerInfoAdapter.MyViewHolder> {

    private Context mContext;
    private List<Controller> controllerList;
    private List<Long> showRules = Arrays.asList(
            BROADLINK_SP3_SMART_PLUG,
            FIOT_SMART_TANK_CONTROLLER);
    private List<Integer> containerShowRules = Arrays.asList(Controller.TYPE_LIGHT, Controller.TYPE_WATER_PUMP, Controller.TYPE_FAN, Controller.TYPE_VAN, 10, 11);

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ControllerItemView controllerItem;

        public MyViewHolder(View view) {
            super(view);
            controllerItem = (ControllerItemView) view.findViewById(R.id.civ_controller_item);
        }
    }


    public ControllerInfoAdapter(Context context, Farm data) {
        this.mContext = context;
        setFarm(data);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_controller_item_holder, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Controller c = controllerList.get(position);
        holder.controllerItem.setData(c);
    }

    @Override
    public int getItemCount() {
        return controllerList.size();
    }

    public void setFarm(Farm data) {
        if (controllerList == null) {
            controllerList = new ArrayList<>();
        }

        controllerList.clear();

        if (data == null || data.devices == null || data.devices.size() == 0) {
            controllerList.add(null);
        } else {
            for (Device d : data.devices) {
                if (d.is_active && d.controllers != null && !d.controllers.isEmpty()) {
                    if (d.device_name == FIOT_SMART_TANK_CONTROLLER) {
                        for (Controller c : d.controllers) {
                            if (c.isActive) {
                                c.deviceID = d.device_id;
                                if (containerShowRules.contains(c.controller_type)) {
                                    c.editable = false;
                                    controllerList.add(c);
                                }
                            }
                        }
                    } else if (d.device_name == ARIATEC_HYDROPONIC_CONTROLLER) {
                            for (Controller c : d.controllers) {
                                if (c.isActive) {
                                    c.deviceID = d.device_id;
                                    if (containerShowRules.contains(c.controller_type)) {
                                        c.editable = false;
                                        controllerList.add(c);
                                    }
                                }
                            }
                    } else if (Device.CONTROLLER == d.device_type) {
                        if (d.device_name == ResourceUtils.STORAGE_CONTROLLER) {
                            Controller c;
                            for (int i = 0; i < d.controllers.size(); i++) {
                                c = d.controllers.get(i);
                                if (c.isActive) {
                                    c.deviceID = d.device_id;
                                    c.customerName = d.device_custom_name + " (" + mContext.getString(R.string.channel_number, i + 1) + ")";
                                    c.editable = true;
                                    controllerList.add(c);
                                }
                            }
                        } else {
                            for (Controller c : d.controllers) {
                                if (c.isActive) {
                                    c.deviceID = d.device_id;
                                    c.customerName = d.device_custom_name;
                                    c.editable = true;
                                    controllerList.add(c);
                                }
                            }
                        }
                    }
                }
            }

            if (controllerList.size() == 0) {
                controllerList.add(null);
            }
        }

        notifyDataSetChanged();
    }
}