package mobile.agrhub.farmbox.custom_view.layout;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

/**
 * Created by lecong on 3/27/18.
 */

public class SquareGridView extends GridView {
    public SquareGridView(Context context) {
        super(context);
    }

    public SquareGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareGridView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec); // This is the key that will make the height equivalent to its width
    }
}
