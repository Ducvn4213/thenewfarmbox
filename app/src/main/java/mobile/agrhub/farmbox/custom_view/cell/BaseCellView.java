package mobile.agrhub.farmbox.custom_view.cell;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import mobile.agrhub.farmbox.R;

public abstract class BaseCellView extends LinearLayout {

    private TextView mTitle;
    private View mSeparator;

    protected abstract int layoutView();
    public abstract boolean isEmpty();

    public BaseCellView(Context context) {
        this(context, null);
    }

    public BaseCellView(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(this.layoutView(), this, true);

        bindingControls();
    }

    protected void bindingControls() {
        mTitle = (TextView) findViewById(R.id.tv_title);
        mSeparator = (View) findViewById(R.id.v_separator);
    }

    public void setTitle(String title) {
        mTitle.setText(title);
    }

    public void setTitle(int stringRes) {
        mTitle.setText(stringRes);
    }

    public String getTitle() {
        return mTitle.getText().toString();
    }

    public void setSeparatorVisibility(int visibility) {
        if (mSeparator != null) {
            mSeparator.setVisibility(visibility);
        }
    }
}
