package mobile.agrhub.farmbox.custom_view.controller_info;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.Farm;
import mobile.agrhub.farmbox.utils.Utils;

public class ControllerInfoView extends LinearLayout {

    ControllerInfoAdapter mAdapter;
    RecyclerView mHolder;
    ImageButton mAdd;

    public ControllerInfoView(Context context) {
        this(context, null);
    }

    public ControllerInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_controller_info, this, true);

        bindingControls();
        initData();
    }

    private void bindingControls() {
        mHolder = (RecyclerView) findViewById(R.id.rv_controller_list);
        mAdd = (ImageButton) findViewById(R.id.ib_add);
    }

    private void initData() {
        mAdapter = new ControllerInfoAdapter(getContext(),null);
        mHolder.setAdapter(mAdapter);
    }

    public void setOnAddControllerListener(OnClickListener listener) {
        mAdd.setOnClickListener(listener);
    }

    public void updateUI(Farm data) {
        mAdapter.setFarm(data);
        mAdapter.notifyDataSetChanged();
        Utils.setRecycleViewHeightBasedOnChildren(getContext(), mHolder, 40);
    }
}
