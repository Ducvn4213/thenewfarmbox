package mobile.agrhub.farmbox.custom_view.layout;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.LinearLayout;

/**
 * Created by lecong on 3/27/18.
 */

public class CameraThumbnailView extends LinearLayout {
    public CameraThumbnailView(Context context) {
        super(context);
    }

    public CameraThumbnailView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CameraThumbnailView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Log.d("CameraThumbnailView", "Width: " + widthMeasureSpec + " | Height: " + Math.round((widthMeasureSpec * 3) / 4));
        super.onMeasure(widthMeasureSpec, Math.round((widthMeasureSpec * 3) / 4)); // This is the key that will make the height equivalent to its width
    }
}
