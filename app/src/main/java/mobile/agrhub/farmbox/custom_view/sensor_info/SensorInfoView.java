package mobile.agrhub.farmbox.custom_view.sensor_info;

import android.content.Context;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.kingfisher.easyviewindicator.RecyclerViewIndicator;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.UtilAPI;
import mobile.agrhub.farmbox.service.model.Device;
import mobile.agrhub.farmbox.service.model.Farm;
import mobile.agrhub.farmbox.service.model.SensorChartPointItem;

public class SensorInfoView extends LinearLayout {

    public static final int DiaryType_UTM = 0;
    public static final int DiaryType_Hourly = 1;
    public static final int DiaryType_Daily = 2;
    public static final int DiaryType_Weekly = 3;
    public static final int DiaryType_Monthly = 4;

    private RecyclerView mRecycleView;
    private SensorItemAdapter mAdapter;
    private ImageButton mAdd;
    private ProgressBar mProgressBar;
    private RecyclerViewIndicator circleIndicator;

    private long currentSensorIDShown = -1;
    private List<SensorChartPointItem> lastedSensorData;
    private boolean isNewOne;
    private int currentDairy = 0;

    UtilAPI utilAPI = APIHelper.getUtilAPI();

    public SensorInfoView(Context context) {
        this(context, null);
    }

    public SensorInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_sensor_info, this, true);

        bindingControls();
        initData();
    }

    private void bindingControls() {
        mRecycleView = (RecyclerView) findViewById(R.id.rv_sensor_list);
        mAdd = (ImageButton) findViewById(R.id.ib_add);
        mProgressBar = (ProgressBar) findViewById(R.id.pb_progress);
        circleIndicator = (RecyclerViewIndicator) findViewById(R.id.circleIndicator);
    }

    private void initData() {
//        mDairySegment.setTextA(getContext().getString(R.string.dairy_utm));
//        mDairySegment.setTextB(getContext().getString(R.string.dairy_hourly));
//        mDairySegment.setTextC(getContext().getString(R.string.dairy_daily));
//        mDairySegment.setTextD(getContext().getString(R.string.dairy_monthly));
//        mDairySegment.setColorIndex(1);
//        mDairySegment.setSelected(0);

        mAdapter = new SensorItemAdapter(new ArrayList<Device>());
        mRecycleView.setAdapter(mAdapter);

        PagerSnapHelper helper = new PagerSnapHelper();
        helper.attachToRecyclerView(mRecycleView);

        circleIndicator.setRecyclerView(mRecycleView);

//        mLineChartView.setPinchZoom(false);
//        mLineChartView.setDescription(null);
//        mLineChartView.getAxisRight().setEnabled(false);
//        mLineChartView.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
//        mLineChartView.getXAxis().setValueFormatter(this);

//        mAdapter.setOnRequestUpdateUI(new SensorItemView.OnRequestUpdateUI() {
//            @Override
//            public void onRequestUpdateUI() {
//                mAdapter.notifyDataSetChanged();
//
//                if (FBHelper.getInstance().currentPresentSensor != null) {
//                    showChart();
//                }
//                else {
//                    hideChart();
//                }
//            }
//        });

//        mDairySegment.setOnSegmentedChangedListener(new SegmentedControl.OnSegmentedChangedListener() {
//            @Override
//            public void onSegmentedChanged(int index) {
//                currentDairy = index;
//                updateChartInfo();
//            }
//        });
    }

    public void setOnAddSensorListener(OnClickListener listener) {
        mAdd.setOnClickListener(listener);
    }

    public void updateUI(Farm data) {
        List<Device> devices = new ArrayList<>();
        if (data != null) {
            for (Device d : data.devices) {
                if (d.is_active
                        && d.sensors != null
                        && d.sensors.size() > 0) {
                    devices.add(d);
                }
            }
        }

        mAdapter.setDeviceList(devices, data == null ? 0 : data.farm_type);
        mAdapter.notifyDataSetChanged();
        circleIndicator.forceUpdateItemCount();

//        if (FBHelper.getInstance().currentPresentSensor != null) {
//            showChart();
//        }
//        else {
//            hideChart();
//        }
    }

//    private void showChart() {
//        updateChartInfo();
//        Utils.init(getContext());
//        ValueAnimator anim = ValueAnimator.ofFloat(mChartContainer.getMeasuredHeight(), Utils.convertDpToPixel(260));
//        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//            @Override
//            public void onAnimationUpdate(ValueAnimator valueAnimator) {
//                float val = (float) valueAnimator.getAnimatedValue();
//                ViewGroup.LayoutParams layoutParams = mChartContainer.getLayoutParams();
//                layoutParams.height = (int) val;
//                mChartContainer.setLayoutParams(layoutParams);
//            }
//        });
//        anim.setDuration(200);
//        anim.start();
//    }
//
//    private void hideChart() {
//        Utils.init(getContext());
//        ValueAnimator anim = ValueAnimator.ofFloat(mChartContainer.getMeasuredHeight(), Utils.convertDpToPixel(0));
//        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//            @Override
//            public void onAnimationUpdate(ValueAnimator valueAnimator) {
//                float val = (float) valueAnimator.getAnimatedValue();
//                ViewGroup.LayoutParams layoutParams = mChartContainer.getLayoutParams();
//                layoutParams.height = (int) val;
//                mChartContainer.setLayoutParams(layoutParams);
//            }
//        });
//        anim.setDuration(200);
//        anim.start();
//    }
//
//    private void updateChartInfo() {
//        currentSensorIDShown = FBHelper.getInstance().currentPresentSensor.sensor_id;
//        isNewOne = FBHelper.getInstance().currentPresentSensor.sensor_id != currentSensorIDShown;
//        if (isNewOne) {
//            showLoadingChart();
//        }
//
//        try {
//            utilAPI.getSensorChartData(String.valueOf(currentSensorIDShown), String.valueOf(currentDairy), new APIHelper.Callback<ChartDataResponse>() {
//                @Override
//                public void onSuccess(final ChartDataResponse data) {
//                    Handler handler = new Handler(Looper.getMainLooper());
//                    handler.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            hideLoadingChart();
//                            int removed = 0;
//                            if (currentDairy == 4) {
//                                while (data.data.size() > 10) {
//                                    data.data.remove(1 + removed);
//                                    removed += 1;
//                                }
//                            } else {
//                                while (data.data.size() > 10) {
//                                    data.data.remove(0);
//                                }
//                            }
//
//                            updateDateForChart(isNewOne, data);
//                        }
//                    });
//                }
//
//                @Override
//                public void onFail(String error) {
//                    Handler handler = new Handler(Looper.getMainLooper());
//                    handler.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            hideLoadingChart();
//                        }
//                    });
//                }
//            });
//        }
//        catch (FarmBoxApp.FarmBoxNotInitException ex) {

//            //TODO: show a popup inform that the farm box is not init
//        }
//    }
//
//    private void showLoadingChart() {
//        mProgressBar.setVisibility(VISIBLE);
//        mLineChartView.setVisibility(GONE);
//    }
//
//    private void hideLoadingChart() {
//        mProgressBar.setVisibility(GONE);
//        mLineChartView.setVisibility(VISIBLE);
//    }
//
//    void updateDateForChart(boolean isNewOne, ChartDataResponse _data) {
//        if (_data == null || _data.data == null) {
//            return;
//        }
//
//        ArrayList<Entry> values = new ArrayList<Entry>();
//
//        lastedSensorData = _data.data;
//        float theIndex = 0;
//        for (SensorChartPointItem item : _data.data) {
//            values.add(new Entry((float) theIndex, new Float(item.getValue())));
//            theIndex++;
//        }
//
//        int theColor = getColor();
//
//        LineDataSet set1;
//        if (isNewOne == false && mLineChartView.getData() != null && mLineChartView.getData().getDataSetCount() > 0) {
//            set1 = (LineDataSet) mLineChartView.getData().getDataSetByIndex(0);
//            set1.setLabel(getContext().getString(ResourceUtils.getSensorNameFromType((int) FBHelper.getInstance().currentPresentSensor.sensor_type)));
//            set1.setValues(values);
//            if(isNewOne) {
//                set1.setColor(theColor);
//                set1.setCircleColor(theColor);
//                set1.setFillColor(theColor);
//            }
//            mLineChartView.getData().notifyDataChanged();
//            mLineChartView.notifyDataSetChanged();
//        } else {
//            set1 = new LineDataSet(values, getContext().getString(ResourceUtils.getSensorNameFromType((int) FBHelper.getInstance().currentPresentSensor.sensor_type)));
//
//            set1.setAxisDependency(YAxis.AxisDependency.LEFT);
//
//            set1.setHighlightEnabled(false);
//            set1.setDrawCircleHole(true);
//            set1.setDrawValues(true);
//            set1.setDrawFilled(true);
//
//            set1.setLineWidth(1);
//            set1.setCircleRadius(5);
//            set1.setFillAlpha(65);
//            set1.setColor(theColor);
//            set1.setCircleColor(theColor);
//            set1.setFillColor(theColor);
//            set1.setCircleHoleRadius(3);
//
//            set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
//            set1.setCubicIntensity(0.2f);
//
//            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
//            dataSets.add(set1);
//            LineData data = new LineData(dataSets);
//
//            mLineChartView.setData(data);
//        }
//
//        mLineChartView.invalidate();
//    }
//
//    public int getColor() {
//        double theWidth = mRecycleView.getMeasuredWidth();
//        int theIndex = mRecycleView.computeHorizontalScrollOffset() / (int) theWidth;
//        if (isNewOne) {
//            mDairySegment.setColorIndex(theIndex + 1);
//        }
//        return mobile.agrhub.farmbox.utils.Utils.getRandomColor(theIndex);
//    }
//
//    @Override
//    public String getFormattedValue(float value, AxisBase axis) {
//        long dateValue = lastedSensorData.get((int) value).diary_date;
//        if (mDairySegment.getSelected() > 1) {
//            return mobile.agrhub.farmbox.utils.Utils.getDate(dateValue, "dd:MMM");
//        }
//        return mobile.agrhub.farmbox.utils.Utils.getDate(dateValue, "HH:mm");
//    }
}
