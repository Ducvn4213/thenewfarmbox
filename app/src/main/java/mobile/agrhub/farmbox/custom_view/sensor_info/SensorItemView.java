package mobile.agrhub.farmbox.custom_view.sensor_info;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.Device;
import mobile.agrhub.farmbox.service.model.Sensor;
import mobile.agrhub.farmbox.utils.SensorUtil;
import mobile.agrhub.farmbox.utils.Utils;

public class SensorItemView extends LinearLayout {
    @BindView(R.id.sicv_sensor_item_1)
    SensorItemChildView sensorView1;
    @BindView(R.id.sicv_sensor_item_2)
    SensorItemChildView sensorView2;
    @BindView(R.id.sicv_sensor_item_3)
    SensorItemChildView sensorView3;
    @BindView(R.id.sicv_sensor_item_4)
    SensorItemChildView sensorView4;
    @BindView(R.id.sicv_sensor_item_5)
    SensorItemChildView sensorView5;

    @BindView(R.id.tv_updated_time)
    TextView updatedTimeTextView;
    @BindView(R.id.tv_name)
    TextView nameTextView;
    @BindView(R.id.tv_battery)
    TextView batteryTextView;

    private Device data;
    private long farmType;

    public SensorItemView(Context context) {
        this(context, null);
    }

    public SensorItemView(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_sensor_item, this, true);

        ButterKnife.bind(this);
    }

    private List<SensorItemChildView> getSensorViewList() {
        return Arrays.asList(sensorView1, sensorView2, sensorView3, sensorView4, sensorView5);
    }

    public void updateUI(Device data, long farmType) {
        this.data = data;
        this.farmType = farmType;

        disableAllSensor();
        int processingSensorIndex = 0;

        List<Sensor> sensorList = new ArrayList<>();
        sensorList.addAll(this.data.sensors);
        Collections.sort(sensorList, new Comparator<Sensor>() {
            @Override
            public int compare(Sensor o1, Sensor o2) {
                return o1.sensor_type < o2.sensor_type ? 1 : -1;
            }
        });

        for (Sensor s : sensorList) {
            if (s.sensor_type != 12 && s.sensor_type != 15 && s.sensor_type != 16) {
                updateValueForSensor(processingSensorIndex, s, this.data.device_name);
                processingSensorIndex++;
            }
        }

        nameTextView.setText(this.data.getDisplayName());

        String batteryString = SensorUtil.getBatteryValue(this.data.sensors);
        batteryTextView.setText(batteryString);

        String updatedTimeString = Utils.getMarginTimeString(getContext(), this.data.device_updated_date);
        updatedTimeTextView.setText(updatedTimeString);
    }

    private void updateValueForSensor(int index, Sensor s, long deviceType) {
        List<SensorItemChildView> itemChildViewList = getSensorViewList();
        if (index < itemChildViewList.size()) {
            SensorItemChildView itemChildView = itemChildViewList.get(index);
            itemChildView.setVisibility(VISIBLE);
            itemChildView.updateUI(s, deviceType);
        }
    }

    private void disableAllSensor() {
        List<SensorItemChildView> itemChildViewList = getSensorViewList();
        for (SensorItemChildView item : itemChildViewList) {
            item.setVisibility(INVISIBLE);
        }
    }
}
