package mobile.agrhub.farmbox.custom_view.farm_hub;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.FarmBasic;

public class FarmHubAdapter extends RecyclerView.Adapter<FarmHubAdapter.MyViewHolder> {

    public static interface FarmHubItemSelectedListener {
        void onFarmHubItemSelected(int index, FarmBasic data);
    }

    private Context context;
    private List<FarmBasic> farmsList;
    private int selectedIndex = 0;
    private FarmHubItemSelectedListener onItemSelectedListener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public ImageView image;
        public ImageView imageAdd;
        private ImageView imageSelected;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.tv_name);
            image = (ImageView) view.findViewById(R.id.iv_image);
            imageAdd = (ImageView) view.findViewById(R.id.iv_image_add);
            imageSelected = (ImageView) view.findViewById(R.id.iv_image_selected);
        }

        public void setSelected() {
            imageSelected.setVisibility(View.VISIBLE);
        }

        public void removeSelected() {
            imageSelected.setVisibility(View.GONE);
        }
    }


    public FarmHubAdapter(Context context, List<FarmBasic> farmsList) {
        setFarmsList(farmsList);
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_farm_hub_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final FarmBasic f = farmsList.get(position);
        if (f == null) {
            holder.image.setVisibility(View.GONE);
            holder.imageAdd.setVisibility(View.VISIBLE);
            holder.name.setText(R.string.farm_hub_add_farm);
        } else {
            holder.image.setVisibility(View.VISIBLE);
            holder.imageAdd.setVisibility(View.GONE);
            holder.name.setText(f.farm_name);
            holder.image.setImageResource(getImageByPosition(position));

//            if (f.season != null) {
//                Plant plant = FBService.getInstance().getPlantByID(f.season.plant_id);
//                List<String> images = new Gson().fromJson(plant.plant_photos, new TypeToken<List<String>>(){}.getType());
//                Picasso.with(context).load(images.get(0)).transform(new Utils.CircleTransform()).into(holder.image);
//            }
        }

        if (position == selectedIndex) {
            holder.setSelected();
        } else {
            holder.removeSelected();
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemSelectedListener != null) {
                    onItemSelectedListener.onFarmHubItemSelected(position, f);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return farmsList.size();
    }

    private int getImageByPosition(int pos) {
        int theIndex = pos % 4;
        switch (theIndex) {
            case 0:
                return R.drawable.ic_farm_1;
            case 1:
                return R.drawable.ic_farm_2;
            case 2:
                return R.drawable.ic_farm_3;
            case 3:
                return R.drawable.ic_farm_4;
        }

        return R.drawable.ic_farm_4;
    }

    public void setOnItemSelectedListener(FarmHubItemSelectedListener listener) {
        this.onItemSelectedListener = listener;
    }

    public void setFarmsList(List<FarmBasic> data) {
        if (this.farmsList == null) {
            this.farmsList = new ArrayList<>();
        }
        this.farmsList.clear();
        this.farmsList.addAll(data);
        this.farmsList.add(null);

        if (this.farmsList.size() <= 1) {
            selectedIndex = 0;
        } else if (this.selectedIndex >= (this.farmsList.size() - 1)) {
            selectedIndex = 0;
        }
    }

    public void setSelected(int index) {
        selectedIndex = index;
        notifyDataSetChanged();
    }

    public int getSelected() {
        return selectedIndex;
    }
}