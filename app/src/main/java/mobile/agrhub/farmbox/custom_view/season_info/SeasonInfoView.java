package mobile.agrhub.farmbox.custom_view.season_info;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.PlantAPI;
import mobile.agrhub.farmbox.service.model.Farm;
import mobile.agrhub.farmbox.utils.Utils;

public class SeasonInfoView extends LinearLayout {

    private TextView mPlantName;
    private TextView mStartDate;
    private TextView mPlantAge;
    private ImageButton mAdd;

    PlantAPI plantAPI = APIHelper.getPlantAPI();

    public SeasonInfoView(Context context) {
        this(context, null);
    }

    public SeasonInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_season_info, this, true);

        bindingControls();
    }

    private void bindingControls() {
        mPlantName = (TextView) findViewById(R.id.tv_plant_name);
        mStartDate = (TextView) findViewById(R.id.tv_start_date);
        mPlantAge = (TextView) findViewById(R.id.tv_plant_age);
        mAdd = (ImageButton) findViewById(R.id.ib_add);
    }

    public void setOnAddEditSeasonClick(OnClickListener listener) {
        mAdd.setOnClickListener(listener);
    }

    public void updateUI(Farm data) {
        if (data == null) {
            mPlantName.setText("---");
            mStartDate.setText("---");
            mPlantAge.setText("---");
            return;
        }

        try {
            mPlantName.setText(Utils.getValueFromMultiLanguageString(data.season.plant.plant_name));
        } catch (Exception ex) {
            mPlantName.setText("---");
        }

        try {
            mStartDate.setText(Utils.getDate(data.season.season_start_date, "dd-MM-yyyy"));
        } catch (Exception ex) {
            mStartDate.setText("---");
        }

        try {
            mPlantAge.setText(data.season.season_age_of_plant + "");
        } catch (Exception ex) {
            mPlantAge.setText("---");
        }
    }
}
