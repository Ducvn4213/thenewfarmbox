package mobile.agrhub.farmbox.custom_view.cell;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.KeyValue;
import mobile.agrhub.farmbox.utils.Utils;

public class InputWithUnitCellView extends BaseCellView implements AdapterView.OnItemSelectedListener {

    private EditText mVal;
    private Spinner mUnit;
    private ArrayAdapter<String> adapter;
    private List<KeyValue> unitData;
    private long selectedUnit = -1;

    @Override
    protected int layoutView() {
        return R.layout.layout_input_with_unit_cell;
    }

    @Override
    public boolean isEmpty() {
        return getVal().isEmpty() || getFloatVal() == 0f;
    }

    public boolean isUnitEmpty() {
        return selectedUnit == -1;
    }

    public InputWithUnitCellView(Context context) {
        super(context);
    }

    public InputWithUnitCellView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void bindingControls() {
        super.bindingControls();
        mVal = (EditText) findViewById(R.id.et_value);
        mUnit = (Spinner) findViewById(R.id.spn_unit);

        unitData = new ArrayList<>();

        updateUnitData();
    }

    public void setValue(String val, String placeHolder) {
        mVal.setText(val);
        mVal.setHint(placeHolder);
    }

    public void setValue(float val, String placeHolder) {
        mVal.setText(Utils.f2s(val));
        mVal.setHint(placeHolder);
    }

    private void updateUnitData() {
        List<String> tmpData = new ArrayList<>();
        int selectedIdx = 0;
        for (int i = 0; i < this.unitData.size(); i++) {
            tmpData.add(unitData.get(i).value);
            if (this.selectedUnit == unitData.get(i).id) {
                selectedIdx = i;
            }
        }

        if (adapter == null) {
            adapter = new ArrayAdapter<String>(getContext(), R.layout.layout_input_spinner, tmpData);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mUnit.setAdapter(adapter);
            mUnit.setSelection(selectedIdx);
            mUnit.setOnItemSelectedListener(this);
        } else {
            adapter.clear();
            adapter.addAll(tmpData);
            adapter.notifyDataSetChanged();
            mUnit.setSelection(selectedIdx);
        }
    }

    public void setUnitData(List<KeyValue> unitData, long selectedUnit) {
        this.unitData = unitData;
        this.selectedUnit = selectedUnit;

        updateUnitData();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        for (int i = 0; i < this.unitData.size(); i++) {
            if (i == position) {
                selectedUnit = this.unitData.get(i).id;
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public String getVal() {
        return mVal.getText().toString();
    }

    public int getIntVal() {
        int val = 0;
        try {
            val = Utils.str2Int(getVal());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return val;
    }

    public long getLongVal() {
        long val = 0;
        try {
            val = Utils.str2Long(getVal());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return val;
    }

    public float getFloatVal() {
        float val = 0;
        try {
            val = Utils.str2Float(getVal());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return val;
    }

    public int getUnitVal() {
        return (int)selectedUnit;
    }

    public void setEnabled(boolean disable) {
        mVal.setFocusable(disable);
        mVal.setEnabled(disable);

        mUnit.setFocusable(disable);
        mUnit.setEnabled(disable);
    }
}
