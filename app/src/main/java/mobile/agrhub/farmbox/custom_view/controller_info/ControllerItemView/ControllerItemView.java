package mobile.agrhub.farmbox.custom_view.controller_info.ControllerItemView;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.activities.edit.EditControllerActivity;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.UtilAPI;
import mobile.agrhub.farmbox.service.model.Controller;
import mobile.agrhub.farmbox.utils.ResourceUtils;

public class ControllerItemView extends LinearLayout {

    private TextView mName;
    private Switch mToggle;
    private ProgressBar mProgress;
    private Controller mData;
    private View mCover;
    private ImageView mEdit;

    UtilAPI utilAPI = APIHelper.getUtilAPI();

    public ControllerItemView(Context context) {
        this(context, null);
    }

    public ControllerItemView(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_controller_item, this, true);

        bindingControls();
        setupControlEvents();
    }

    private void bindingControls() {
        mName = (TextView) findViewById(R.id.tv_name);
        mToggle = (Switch) findViewById(R.id.sw_toggle);
        mProgress = (ProgressBar) findViewById(R.id.pb_progress);
        mCover = findViewById(R.id.view_cover);
        mEdit = (ImageView) findViewById(R.id.iv_edit);
    }

    private void setChecked(Boolean value) {
        mToggle.setOnCheckedChangeListener(null);
        mToggle.setChecked(value);
        setupControlEvents();
    }

    private void setupControlEvents() {
        mToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, final boolean b) {
                mProgress.setVisibility(View.VISIBLE);
                mCover.setVisibility(VISIBLE);

                utilAPI.toggleController(mData.controller_id + "", b, new APIHelper.Callback<Controller>() {
                    @Override
                    public void onSuccess(Controller data) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            public void run() {
                                mProgress.setVisibility(View.INVISIBLE);
                                mToggle.setChecked(b);
                                mCover.setVisibility(GONE);
                            }
                        });
                    }

                    @Override
                    public void onFail(int errorCode, String error) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            public void run() {
                                mProgress.setVisibility(View.INVISIBLE);
                                mToggle.setChecked(!b);
                                mCover.setVisibility(GONE);
                            }
                        });
                    }
                });
            }
        });

        mName.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleEdit();
            }
        });

        mEdit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleEdit();
                Intent intent = new Intent(getContext(), EditControllerActivity.class);
                intent.putExtra("id", mData.deviceID);
                intent.putExtra("name", mData.customerName);
                getContext().startActivity(intent);
            }
        });
    }

    private void toggleEdit() {
//        if (mData != null && mData.editable) {
//            if (FBHelper.getInstance().currentOpenEditControllerIDs.contains(mData.controller_id)) {
//                FBHelper.getInstance().currentOpenEditControllerIDs.remove(mData.controller_id);
//                mName.animate().translationX(0);
//            }
//            else {
//                FBHelper.getInstance().currentOpenEditControllerIDs.add(mData.controller_id);
//                mName.animate().translationX(Utils.getPixelFromDP(40, getContext()));
//            }
//        }
    }

    private void updateUI() {
        if (mData == null) {
            mName.setText(R.string.controller_info_no_device);
            mToggle.setVisibility(View.GONE);
        } else {
            int controllerName = getNameFromController(mData);
            if (controllerName > 0) {
                mName.setText(controllerName);
            } else {
                mName.setText(mData.customerName);
            }
            setChecked(mData.controller_is_on);
            mToggle.setVisibility(View.VISIBLE);

//            if (FBHelper.getInstance().currentOpenEditControllerIDs.contains(mData.controller_id) && mData.editable) {
//                mName.setX(Utils.getPixelFromDP(40, getContext()));
//            }
//            else {
//                mName.setX(0);
//            }
        }
    }

    private int getNameFromController(Controller c) {
        if (c.customerName == null || c.customerName.trim().isEmpty()) {
            return ResourceUtils.getControllerNameByType(c.controller_type);
        }

        return 0;
    }

    public void setData(Controller data) {
        mData = data;
        updateUI();
    }
}