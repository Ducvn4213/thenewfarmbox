package mobile.agrhub.farmbox.fragments.intro;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import mobile.agrhub.farmbox.R;

public class IntroContentTwoFragment extends Fragment {

    View containerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        containerView = inflater.inflate(R.layout.intro_content_fragment_two, container, false);
        return containerView;
    }
}
