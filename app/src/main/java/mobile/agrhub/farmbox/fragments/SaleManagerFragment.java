package mobile.agrhub.farmbox.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.adapters.SaleManagerTabAdapter;
import mobile.agrhub.farmbox.utils.BaseFragment;

public class SaleManagerFragment extends BaseFragment {
    private static final String TAG = "SaleManagerFragment";

    private View view;
    private TabLayout diaryTabLayout;
    private ViewPager diaryViewPager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_sale_manager, container, false);

        diaryTabLayout = (TabLayout) view.findViewById(R.id.tl_diary);
        diaryViewPager = (ViewPager) view.findViewById(R.id.vp_diary);

        diaryViewPager.setAdapter(new SaleManagerTabAdapter(getActivity().getSupportFragmentManager(), getActivity()));

        diaryTabLayout.setupWithViewPager(diaryViewPager);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public String getTitle(Context context) {
        return context.getString(R.string.sale_manager_title);
    }
}
