package mobile.agrhub.farmbox.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.activities.farm.product.AddFarmProductActivity;
import mobile.agrhub.farmbox.activities.farm.product.DetailFarmProductActivity;
import mobile.agrhub.farmbox.adapters.product.FarmProductAdapter;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.model.FarmProduct;
import mobile.agrhub.farmbox.service.model.User;
import mobile.agrhub.farmbox.service.model.response.GetFarmProductResponse;
import mobile.agrhub.farmbox.utils.BaseFragment;
import mobile.agrhub.farmbox.utils.Constants;

public class FarmProductFragment extends BaseFragment implements FarmProductAdapter.OnClickListener {
    private static final String TAG = "FarmProductFragment";

    private View view;
    private GridView gvProducts;
    private FarmProductAdapter listAdapter;
    private LinearLayout mEmptyView;
    private TextView mEmptyMessage;
    private Button mAddProduct;
    private FloatingActionButton mAddBtn;

    private List<FarmProduct> products;

    int page = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_farm_product, container, false);

        gvProducts = (GridView) view.findViewById(R.id.gv_farm_product);
        mEmptyView = (LinearLayout) view.findViewById(android.R.id.empty);
        mEmptyMessage = (TextView) view.findViewById(R.id.tv_empty_message);
        mAddProduct = (Button) view.findViewById(R.id.btn_add_product);
        mAddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FarmProductFragment.this.getContext(), AddFarmProductActivity.class);
                FarmProductFragment.this.startActivity(intent);
            }
        });

        mAddBtn = (FloatingActionButton) view.findViewById(R.id.fb_add);
        mAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FarmProductFragment.this.getContext(), AddFarmProductActivity.class);
                FarmProductFragment.this.startActivity(intent);
            }
        });

        products = new ArrayList<FarmProduct>();
        listAdapter = new FarmProductAdapter(getActivity(), products, this);
        gvProducts.setAdapter(listAdapter);
        gvProducts.setEmptyView(mEmptyView);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        User user = FarmBoxAppController.getInstance().getUser();
        if (user == null) {
            return;
        }

        APIHelper.getSaleAPI().getProducts(page, user.userId, true, new APIHelper.Callback<GetFarmProductResponse>() {
            @Override
            public void onSuccess(GetFarmProductResponse data) {
                if (data == null) {
                    products.clear();
                } else {
                    products = data.farmProducts;
                    FragmentActivity activity = FarmProductFragment.this.getActivity();
                    if (activity != null) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (FarmProductFragment.this != null) {
                                    FarmProductFragment.this.hideLoading();
                                    FarmProductFragment.this.refreshListView();
                                }
                            }
                        });
                    }
                }
            }

            @Override
            public void onFail(int errorCode, String error) {

            }
        });
    }

    private void refreshListView() {
        if (null != gvProducts) {
            if (null == listAdapter) {
                listAdapter = new FarmProductAdapter(getActivity(), products, this);
                gvProducts.setAdapter(listAdapter);
            } else {
                listAdapter.update(products);
                listAdapter.notifyDataSetChanged();
            }
            mEmptyMessage.setText(getString(R.string.product_empty_message));
            mAddProduct.setVisibility(View.VISIBLE);
            gvProducts.smoothScrollToPosition(0);
        }
    }

    @Override
    public void onProductClicked(FarmProduct product) {
        Gson gson = new Gson();
        String productStr = gson.toJson(product);

        Intent intent = new Intent(getActivity(), DetailFarmProductActivity.class);
        intent.putExtra(Constants.FARM_PRODUCT_INFO_KEY, productStr);

        startActivity(intent);
    }

    @Override
    public void onMinusClicked(String productId) {

    }

    @Override
    public void onProductAmountClicked(long productId, int amount) {

    }

    @Override
    public void onPlusClicked(String productId) {

    }

    @Override
    public void onAddToCartClicked(long productId) {

    }
}
