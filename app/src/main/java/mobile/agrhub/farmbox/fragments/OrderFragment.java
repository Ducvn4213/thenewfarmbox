package mobile.agrhub.farmbox.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.activities.farm.order.AddProductToCartActivity;
import mobile.agrhub.farmbox.activities.farm.order.AssignTraceabilityCodeActivity;
import mobile.agrhub.farmbox.activities.farm.order.DetailFarmOrderActivity;
import mobile.agrhub.farmbox.adapters.order.OrderAdapter;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.model.FarmOrder;
import mobile.agrhub.farmbox.service.model.KeyValue;
import mobile.agrhub.farmbox.service.model.User;
import mobile.agrhub.farmbox.service.model.response.GetFarmOrderResponse;
import mobile.agrhub.farmbox.utils.BaseFragment;
import mobile.agrhub.farmbox.utils.Constants;

public class OrderFragment extends BaseFragment implements OrderAdapter.OnClickListener, AdapterView.OnItemSelectedListener {
    private static final String TAG = "OrderFragment";

    private View view;
    private Spinner mState;
    private ListView listView;
    private OrderAdapter listAdapter;
    private LinearLayout mEmptyView;
    private TextView mEmptyMessage;
    private Button mAddOrder;
    private FloatingActionButton mAddBtn;

    private List<KeyValue> stateData;
    private List<FarmOrder> orders;

    int page = 0;
    Long userId = null;
    Long customerId = null;
    Integer state = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_order, container, false);

        stateData = new ArrayList<>();
        KeyValue item;
        item = new KeyValue(-99, getString(R.string.all_stage));
        stateData.add(item);

        item = new KeyValue(FarmOrder.STATE_NEW, getString(R.string.order_new));
        stateData.add(item);

        item = new KeyValue(FarmOrder.STATE_PROCESS, getString(R.string.order_process));
        stateData.add(item);

        item = new KeyValue(FarmOrder.STATE_DELIVERY, getString(R.string.order_delivery));
        stateData.add(item);

        item = new KeyValue(FarmOrder.STATE_FINISH, getString(R.string.order_finish));
        stateData.add(item);

        item = new KeyValue(FarmOrder.STATE_CANCEL, getString(R.string.order_cancel));
        stateData.add(item);

        mState = view.findViewById(R.id.spn_state);

        List<String> tmpData = new ArrayList<>();
        for (int i = 0; i < this.stateData.size(); i++) {
            tmpData.add(stateData.get(i).value);
        }
        ArrayAdapter adapter = new ArrayAdapter<String>(getContext(), R.layout.layout_input_spinner, tmpData);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mState.setAdapter(adapter);
        mState.setSelection(0);
        mState.setOnItemSelectedListener(this);

        User user = FarmBoxAppController.getInstance().getUser();
        if (user != null) {
            userId = user.userId;
        }

        listView = (ListView) view.findViewById(R.id.list_view);
        mEmptyView = (LinearLayout) view.findViewById(android.R.id.empty);
        mEmptyMessage = (TextView) view.findViewById(R.id.tv_empty_message);
        mAddOrder = (Button) view.findViewById(R.id.btn_add_order);
        mAddOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderFragment.this.getContext(), AddProductToCartActivity.class);
                OrderFragment.this.startActivity(intent);
            }
        });

        mAddBtn = (FloatingActionButton) view.findViewById(R.id.fb_add);
        mAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderFragment.this.getContext(), AddProductToCartActivity.class);
                OrderFragment.this.startActivity(intent);
            }
        });

        orders = new ArrayList<>();
        listAdapter = new OrderAdapter(getActivity(), orders, this);
        listView.setAdapter(listAdapter);
        listView.setEmptyView(mEmptyView);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        loadData();
    }

    private void loadData() {
        APIHelper.getSaleAPI().getOrders(page, userId, customerId, state, new APIHelper.Callback<GetFarmOrderResponse>() {
            @Override
            public void onSuccess(GetFarmOrderResponse data) {
                if (data == null) {
                    orders.clear();
                } else {
                    orders = data.orders;
                }
                FragmentActivity activity = OrderFragment.this.getActivity();
                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            OrderFragment activity = OrderFragment.this;
                            if (activity != null) {
                                activity.hideLoading();
                                activity.refreshListView();
                            }
                        }
                    });
                }
            }

            @Override
            public void onFail(int errorCode, String error) {
                FragmentActivity activity = OrderFragment.this.getActivity();
                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            OrderFragment activity = OrderFragment.this;
                            if (activity != null) {
                                activity.hideLoading();
                            }
                        }
                    });
                }
            }
        });
    }

    private void refreshListView() {
        if (null != listView) {
            if (null == listAdapter) {
                listAdapter = new OrderAdapter(getActivity(), orders, this);
                listView.setAdapter(listAdapter);
            } else {
                listAdapter.update(orders);
                listAdapter.notifyDataSetChanged();
            }
            mEmptyMessage.setText(getString(R.string.product_empty_message));
            mAddOrder.setVisibility(View.VISIBLE);
            listView.smoothScrollToPosition(0);
        }
    }

    @Override
    public void onSelectOrder(FarmOrder order) {
        if (order == null) {
            return;
        }
        Intent intent = new Intent(getContext(), DetailFarmOrderActivity.class);
        intent.putExtra(Constants.ORDER_ID_KEY, order.orderId);
        getContext().startActivity(intent);
    }

    @Override
    public void onCancelOrderClicked(final FarmOrder order) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext(), R.style.AppTheme_Dialog_Alert);
        alertDialog.setMessage(getString(R.string.confirm_cancel_order));

        alertDialog.setPositiveButton(getString(R.string.button_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                changeOrderStatus(order.orderId, FarmOrder.STATE_CANCEL);
            }
        });

        alertDialog.setNegativeButton(getString(R.string.button_no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    @Override
    public void onReturnOrderClicked(final FarmOrder order) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext(), R.style.AppTheme_Dialog_Alert);
        alertDialog.setMessage(getString(R.string.confirm_return_order));

        alertDialog.setPositiveButton(getString(R.string.button_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                changeOrderStatus(order.orderId, FarmOrder.STATE_RETURN);
            }
        });

        alertDialog.setNegativeButton(getString(R.string.button_no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    @Override
    public void onAcceptOrderClicked(FarmOrder order) {
        changeOrderStatus(order.orderId, FarmOrder.STATE_PROCESS);
    }

    @Override
    public void onAssignTraceabilityCodeClicked(FarmOrder order) {
        Gson gson = new Gson();

        Intent intent = new Intent(getContext(), AssignTraceabilityCodeActivity.class);
        intent.putExtra(Constants.ORDER_INFO_KEY, gson.toJson(order));
        getContext().startActivity(intent);
    }

    @Override
    public void onDeliveryOrderClicked(FarmOrder order) {
        changeOrderStatus(order.orderId, FarmOrder.STATE_DELIVERY);
    }

    @Override
    public void onFinishOrderClicked(FarmOrder order) {
        changeOrderStatus(order.orderId, FarmOrder.STATE_FINISH);
    }

    private void changeOrderStatus(long orderId, int status) {
        showLoading();
        APIHelper.getSaleAPI().updateOrderField(orderId, Constants.FIELD_ORDER_STATE, String.valueOf(status), new APIHelper.Callback<FarmOrder>() {
            @Override
            public void onSuccess(FarmOrder data) {
                FragmentActivity activity = OrderFragment.this.getActivity();
                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            OrderFragment activity = OrderFragment.this;
                            if (activity != null) {
                                activity.loadData();
                            }
                        }
                    });
                }
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                FragmentActivity activity = OrderFragment.this.getActivity();
                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            OrderFragment activity = OrderFragment.this;
                            if (activity != null) {
                                activity.hideLoading();
                                activity.handleError(errorCode, error);
                            }
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position == 0) {
            if (state != null) {
                state = null;
                loadData();
            }
        } else {
            KeyValue data = stateData.get(position);
            if (state == null || (state != null && state.longValue() != data.id)) {
                state = (int) data.id;

                loadData();
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
