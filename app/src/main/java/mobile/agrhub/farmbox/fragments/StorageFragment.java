package mobile.agrhub.farmbox.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.StoreAPI;
import mobile.agrhub.farmbox.service.model.FileStorage;
import mobile.agrhub.farmbox.utils.BaseFragment;

public class StorageFragment extends BaseFragment {
    private static final int SPAN_SIZE = 2;

    private List<FileStorage> fileList = new ArrayList<FileStorage>();

    private RecyclerView mRecycler;

    private View view;

    private StoreAPI storeAPI = APIHelper.getStoreAPI();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_storage, container, false);


        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void reloadData() {

    }
}
