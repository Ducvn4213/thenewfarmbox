package mobile.agrhub.farmbox.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.github.mikephil.charting.utils.Utils;
import com.nirhart.parallaxscroll.views.ParallaxScrollView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.activities.TabActivity;
import mobile.agrhub.farmbox.activities.add.AddByQRCodeActivity;
import mobile.agrhub.farmbox.activities.add.AddFarmActivity;
import mobile.agrhub.farmbox.activities.camera.PlayCameraActivity;
import mobile.agrhub.farmbox.activities.edit.EditSeasonActivity;
import mobile.agrhub.farmbox.activities.registration.PhoneRegistrationActivity;
import mobile.agrhub.farmbox.adapters.CameraAdapter;
import mobile.agrhub.farmbox.custom_view.controller_info.ControllerInfoView;
import mobile.agrhub.farmbox.custom_view.farm_hub.FarmHubAdapter;
import mobile.agrhub.farmbox.custom_view.farm_hub.FarmHubView;
import mobile.agrhub.farmbox.custom_view.farm_info.FarmInfoView;
import mobile.agrhub.farmbox.custom_view.header.HeaderView;
import mobile.agrhub.farmbox.custom_view.season_info.SeasonInfoView;
import mobile.agrhub.farmbox.custom_view.sensor_info.SensorInfoView;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.WeatherManager;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.FarmAPI;
import mobile.agrhub.farmbox.service.interfaces.UtilAPI;
import mobile.agrhub.farmbox.service.model.Camera;
import mobile.agrhub.farmbox.service.model.Farm;
import mobile.agrhub.farmbox.service.model.FarmBasic;
import mobile.agrhub.farmbox.service.model.Weather;
import mobile.agrhub.farmbox.service.model.response.GetFarmBasicResponse;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.BaseFragment;
import mobile.agrhub.farmbox.utils.Constants;

public class MainFragment extends BaseFragment {
    private static final int DELAYED_TIME = 30 * 1000;

    private View view;

    @BindView(R.id.hv_header)
    HeaderView headerView;
    @BindView(R.id.fb_farm_hub)
    FarmHubView farmHubView;
    @BindView(R.id.fi_farm_info)
    FarmInfoView farmInfoView;
    @BindView(R.id.si_season_info)
    SeasonInfoView seasonInfoView;
    @BindView(R.id.ssi_sensor_info)
    SensorInfoView sensorInfoView;
    @BindView(R.id.ci_controller_info)
    ControllerInfoView controllerInfoView;
    @BindView(R.id.rcv_camera_list)
    RecyclerView mCameraListView;
    @BindView(R.id.psv_scroll_view)
    ParallaxScrollView parallaxScrollView;

    UtilAPI utilAPI = APIHelper.getUtilAPI();
    FarmAPI farmAPI = APIHelper.getFarmAPI();

    private boolean windowIsInBackground = true;

    private CameraAdapter mCameraAdapter;

    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            Activity activity = getActivity();
            if (activity != null) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (windowIsInBackground) {
                            return;
                        }
                        loadFarmData();
                    }
                });
            }
            handler.postDelayed(runnable, DELAYED_TIME);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);

        ButterKnife.bind(this, view);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int margin = Math.round(10 * getContext().getResources().getDisplayMetrics().density);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        mCameraListView.setLayoutManager(layoutManager);
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(mCameraListView);
        mCameraAdapter = new CameraAdapter(getContext(), displayMetrics.widthPixels - margin);
        mCameraListView.setAdapter(mCameraAdapter);

        setupControlEvents();

        windowIsInBackground = false;

        return view;
    }

    private void setupControlEvents() {
        parallaxScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                Utils.init(getContext());
                //MainActivity.animateNavBarWithOffset(Utils.convertDpToPixel(180), mMainScrollView.getScrollY());
            }
        });

        seasonInfoView.setOnAddEditSeasonClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Farm farm = FarmBoxAppController.getInstance().getSelectedFarm();
                if (farm == null) {
                    showErrorDialog(getString(R.string.add_season_farm_missing));
                } else {
                    Intent intent = new Intent(getActivity(), EditSeasonActivity.class);
                    intent.putExtra("id", farm.farm_id);
                    startActivity(intent);
                }
            }
        });

        sensorInfoView.setOnAddSensorListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Farm farm = FarmBoxAppController.getInstance().getSelectedFarm();
                if (farm == null) {
                    showErrorDialog(getString(R.string.add_device_farm_missing));
                } else {
                    Intent intent = new Intent(getActivity(), AddByQRCodeActivity.class);
                    intent.putExtra("id", farm.farm_id);
                    startActivity(intent);
                }
            }
        });

        controllerInfoView.setOnAddControllerListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Farm farm = FarmBoxAppController.getInstance().getSelectedFarm();
                if (farm == null) {
                    showErrorDialog(getString(R.string.add_device_farm_missing));
                } else {
                    Intent intent = new Intent(getActivity(), AddByQRCodeActivity.class);
                    intent.putExtra("id", farm.farm_id);
                    startActivity(intent);
                }
            }
        });

        mCameraAdapter.setListener(new CameraAdapter.OnClickListener() {
            @Override
            public void onSelectCamera(Camera camera, int position) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.CAMERA_INFO_KEY, camera);

                Intent intent = new Intent(getActivity(), PlayCameraActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        farmHubView.setOnFarmHubItemSelectedListener(onFarmHubItemSelectedListener);

        windowIsInBackground = false;

        loadData();

        handler.postDelayed(runnable, DELAYED_TIME);
    }

    @Override
    public void onPause() {
        super.onPause();

        windowIsInBackground = true;
        handler.removeCallbacks(runnable);
    }

    public void loadData() {
        showLoading();

        FarmBoxAppController app = FarmBoxAppController.getInstance();
        if (app.isNeedLoadFarmList() || app.getFarmList() == null || app.getFarmList().size() == 0) {
            farmAPI.getAllFarmBasic(false, new APIHelper.Callback<GetFarmBasicResponse>() {
                @Override
                public void onSuccess(GetFarmBasicResponse data) {
                    FarmBoxAppController app = FarmBoxAppController.getInstance();
                    app.setNeedLoadFarmList(false);

                    List<FarmBasic> farmList = new ArrayList<>();
                    if (null != data && null != data.farms && data.farms.size() > 0) {
                        for (FarmBasic farm : data.farms) {
                            if (farm.is_active) {
                                farmList.add(farm);
                            }
                        }
                    }
                    app.setFarmList(farmList);
                    Activity activity = getActivity();
                    if (activity != null) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loadFarmData();
                            }
                        });
                    }
                }

                @Override
                public void onFail(final int errorCode, final String error) {
                    Activity activity = getActivity();
                    if (activity != null) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideLoading();
                                AlertDialog dialog = new AlertDialog.Builder(getContext(), R.style.AppTheme_Dialog_Alert)
                                        .setTitle(R.string.button_error)
                                        .setMessage(R.string.fatal_error_message)
                                        .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();

                                                Intent intent = new Intent(getContext(), PhoneRegistrationActivity.class);
                                                startActivity(intent);
                                                getActivity().finish();

                                                FarmBoxAppController app = FarmBoxAppController.getInstance();
                                                app.logout();
                                            }
                                        }).create();
                                dialog.show();
                            }
                        });
                    }
                }
            });
        } else {
            loadFarmData();
        }
    }

    private void loadFarmData() {
        FarmBoxAppController app = FarmBoxAppController.getInstance();
        if (null == app.getFarmList() || app.getFarmList().size() == 0) {
            hideLoading();
            return;
        }
        if (app.getSelectedFarmIndex() < 0 || app.getSelectedFarmIndex() >= app.getFarmList().size()) {
            app.setSelectedFarmIndex(0);
        }

        FarmBasic selectedFarm = app.getFarmList().get(app.getSelectedFarmIndex());

        farmAPI.getFarmByID(selectedFarm.farm_id, new APIHelper.Callback<Farm>() {
            @Override
            public void onSuccess(Farm data) {
                FarmBoxAppController app = FarmBoxAppController.getInstance();
                app.setSelectedFarm(data);
                Activity activity = getActivity();
                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();
                            updateUI();
                        }
                    });
                }
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                Activity activity = getActivity();
                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();
                        }
                    });
                }
            }
        });
    }

    private void updateUI() {
        FarmBoxAppController app = FarmBoxAppController.getInstance();
        if (app.getUser() == null) {
            return;
        }
        List<FarmBasic> farmList = app.getFarmList();

        if (windowIsInBackground == true || farmList.size() == 0) {
            controllerInfoView.updateUI(null);
            sensorInfoView.updateUI(null);
            farmInfoView.updateUI(null);
            headerView.updateUIWithoutData();
            updateWeatherInfo(null);
            return;
        }

        Farm farm = app.getSelectedFarm();
        if (null != farm) {
            setActivityTitle(farm.farm_name);
        } else {
            setActivityTitle(getString(R.string.app_name));
        }
        farmHubView.updateUI(farmList);
        if (null != farm) {
            FarmBasic farmBasic;
            for (int i = 0; i < farmList.size(); i++) {
                farmBasic = farmList.get(i);
                if (farmBasic.farm_id == farm.farm_id) {
                    farmHubView.setSelectedFarm(i);
                    break;
                }
            }
        }

        if (farm != null) {
            farmInfoView.updateUI(farm);
            if (Farm.GENERAL == farm.farm_type
                    || Farm.STORAGE == farm.farm_type
                    || Farm.CATTLE_HOUSE == farm.farm_type) {
                seasonInfoView.setVisibility(View.GONE);
            } else {
                seasonInfoView.setVisibility(View.VISIBLE);
                seasonInfoView.updateUI(farm);
            }
            sensorInfoView.updateUI(farm);
            controllerInfoView.updateUI(farm);
            mCameraAdapter.setData(farm, farm.camera);
            // cameraInfoView.updateUI(FBHelper.getInstance().currentSelectedFarm.camera);
            updateWeatherInfo(farm);
        }
    }

    private void updateWeatherInfo(Farm farm) {
        if (windowIsInBackground == true) {
            return;
        }

        String latitude = "0";
        String longitude = "0";

        if (farm == null) {
            this.getCurrentLocationCallback = _getCurrentLocationCallback;
            getCurrentLocationAndContinue();
        } else {
            latitude = String.valueOf(farm.farm_latitude);
            longitude = String.valueOf(farm.farm_longitude);

            updateWeatherInfo(latitude, longitude);
        }
    }

    private void updateWeatherInfo(String latitude, String longitude) {
        WeatherManager weatherManager = FarmBoxAppController.getInstance().getWeatherManager();
        if (weatherManager.needLoadWeather()) {
            weatherManager.setLoadingWeather(true);
            utilAPI.getWeatherInfo(latitude, longitude, new APIHelper.Callback<Weather>() {
                @Override
                public void onSuccess(final Weather data) {
                    if (windowIsInBackground == true) {
                        return;
                    }

                    FragmentActivity activity = MainFragment.this.getActivity();
                    if (null != activity) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                WeatherManager weatherManager = FarmBoxAppController.getInstance().getWeatherManager();
                                weatherManager.setLoadingWeather(false);

                                weatherManager.setWeatherInfo(data);
                                headerView.setWeatherData(data);
                            }
                        });
                    }
                }

                @Override
                public void onFail(int errorCode, String error) {
                    WeatherManager weatherManager = FarmBoxAppController.getInstance().getWeatherManager();
                    weatherManager.setLoadingWeather(false);
                }
            });
        } else {
            headerView.setWeatherData(weatherManager.getWeatherInfo());
        }
    }

    BaseActivity.GetCurrentLocationCallback _getCurrentLocationCallback = new BaseActivity.GetCurrentLocationCallback() {
        @Override
        public void afterGotCurrentLocation(double lat, double lon) {
            String latitude = String.valueOf(lat);
            String longitude = String.valueOf(lon);

            updateWeatherInfo(latitude, longitude);
        }

        @Override
        public void afterGetCurrentLocationFail() {

        }
    };

    FarmHubAdapter.FarmHubItemSelectedListener onFarmHubItemSelectedListener = new FarmHubAdapter.FarmHubItemSelectedListener() {
        @Override
        public void onFarmHubItemSelected(int index, FarmBasic data) {
            if (data == null) {
                Intent intent = new Intent(getActivity(), AddFarmActivity.class);
                intent.putExtra("disable_add_device", 1);
                startActivity(intent);
                return;
            }

            FarmBoxAppController app = FarmBoxAppController.getInstance();
            if (index != app.getSelectedFarmIndex()) {
                app.setSelectedFarmIndex(index);
                loadFarmData();
            }
        }
    };

    private void setActivityTitle(String title) {
        Activity tabActivity = this.getActivity();
        if (null != tabActivity && tabActivity instanceof TabActivity) {
            ((TabActivity) tabActivity).setTitle(title);
        }
    }
}
