package mobile.agrhub.farmbox.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.activities.TabActivity;
import mobile.agrhub.farmbox.adapters.DiaryTabAdapter;
import mobile.agrhub.farmbox.utils.BaseFragment;

public class DiaryFragment extends BaseFragment implements TabLayout.OnTabSelectedListener {
    private static final String TAG = "DiaryFragment";
    public static final int PRODUCTIVITY_POSITION = 0;
    public static final int CERTIFICATE_POSITION = 1;

    private View view;
    public TabLayout diaryTabLayout;
    private ViewPager diaryViewPager;
    private DiaryTabAdapter tabAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_diary, container, false);

        diaryTabLayout = (TabLayout) view.findViewById(R.id.tl_diary);
        diaryViewPager = (ViewPager) view.findViewById(R.id.vp_diary);

        tabAdapter = new DiaryTabAdapter(getActivity().getSupportFragmentManager(), getActivity());
        diaryViewPager.setAdapter(tabAdapter);

        diaryTabLayout.setupWithViewPager(diaryViewPager);
        diaryTabLayout.addOnTabSelectedListener(this);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        TabActivity activity = (TabActivity)getActivity();
        activity.didTabChanged();
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public boolean hasFilter() {
        if (diaryTabLayout != null && diaryTabLayout.getSelectedTabPosition() == PRODUCTIVITY_POSITION) {
            ProductivityFragment fragment = tabAdapter.getProductivityFragment();
            if (fragment != null) {
                return fragment.hasFilter();
            }
        }
        return false;
    }

    public String getTitle(Context context) {
        if (diaryTabLayout != null && diaryTabLayout.getSelectedTabPosition() == PRODUCTIVITY_POSITION) {
            return context.getString(R.string.productivity_title);
        } else if (diaryTabLayout != null && diaryTabLayout.getSelectedTabPosition() == CERTIFICATE_POSITION) {
            return context.getString(R.string.certificate_title);
        } else {
            return context.getString(R.string.diary_title);
        }
    }

    public void setFilter(Bundle bundle) {
        if (diaryTabLayout != null && diaryTabLayout.getSelectedTabPosition() == PRODUCTIVITY_POSITION) {
            ProductivityFragment fragment = tabAdapter.getProductivityFragment();
            if (fragment != null) {
                fragment.setFilter(bundle);
            }
        }
    }

    public Bundle getFilterBundle() {
        if (diaryTabLayout != null && diaryTabLayout.getSelectedTabPosition() == PRODUCTIVITY_POSITION) {
            ProductivityFragment fragment = tabAdapter.getProductivityFragment();
            if (fragment != null) {
                return fragment.getFilterBundle();
            }
        }
        return null;
    }
}
