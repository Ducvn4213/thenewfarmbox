package mobile.agrhub.farmbox.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.nostra13.universalimageloader.core.ImageLoader;

import mobile.agrhub.farmbox.BuildConfig;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.activities.TabActivity;
import mobile.agrhub.farmbox.activities.account.EditAccountActivity;
import mobile.agrhub.farmbox.activities.common.ScanQRCodeActivity;
import mobile.agrhub.farmbox.activities.registration.PhoneRegistrationActivity;
import mobile.agrhub.farmbox.activities.setting.SettingUnitActivity;
import mobile.agrhub.farmbox.activities.setting.SettingYourFarmsActivity;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.UtilAPI;
import mobile.agrhub.farmbox.service.model.User;
import mobile.agrhub.farmbox.service.network.Config;
import mobile.agrhub.farmbox.utils.BaseFragment;
import mobile.agrhub.farmbox.utils.Constants;
import mobile.agrhub.farmbox.utils.Utils;

public class SettingsFragment extends BaseFragment {
    private static final String TAG = "SettingsFragment";

    View view;

    ImageView mAvatar;
    TextView mUserName;
    TextView mPhoneNumber;
    ImageButton mEditProfile;
    LinearLayout mFarmsSetting;
    Switch mNotification;
    LinearLayout mUnitSetting;
    LinearLayout mScanQRCode;
    LinearLayout mLanguageView;
    TextView mAppLanguage;
    TextView mAppVersion;
    TextView mAppUpdateDate;
    LinearLayout mLogout;
    ImageLoader mImageLoader = ImageLoader.getInstance();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_settings, container, false);

        bindingControls();
        setupControlEvents();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        initData();
    }

    private void bindingControls() {
        mAvatar = (ImageView) view.findViewById(R.id.iv_avatar);
        mUserName = (TextView) view.findViewById(R.id.tv_user_name);
        mPhoneNumber = (TextView) view.findViewById(R.id.tv_phone);
        mEditProfile = (ImageButton) view.findViewById(R.id.ib_edit_profile);
        mFarmsSetting = (LinearLayout) view.findViewById(R.id.ll_farm_setting);
        mNotification = (Switch) view.findViewById(R.id.sw_notification);
        mUnitSetting = (LinearLayout) view.findViewById(R.id.ll_unit_setting);
        mScanQRCode =  (LinearLayout) view.findViewById(R.id.ll_scan_qr_code);
        mLanguageView = (LinearLayout) view.findViewById(R.id.ll_language);
        mAppLanguage = (TextView) view.findViewById(R.id.tv_app_language);
        mAppVersion = (TextView) view.findViewById(R.id.tv_app_version);
        mAppUpdateDate = (TextView) view.findViewById(R.id.tv_app_update_date);
        mLogout = (LinearLayout) view.findViewById(R.id.ll_logout);
    }

    private void initData() {
        FarmBoxAppController app = FarmBoxAppController.getInstance();
        User user = FarmBoxAppController.getInstance().getUser();
        if (null != user) {
            mUserName.setText(user.user_first_name + " " + user.user_last_name);

            if (app.isDemoMode()) {
                mPhoneNumber.setText(R.string.phone_number_has_hidden);
            } else {
                mPhoneNumber.setText(user.user_phone);
            }
            if (null != user.user_avatar && !user.user_avatar.isEmpty()) {
                mImageLoader.displayImage(user.user_avatar, mAvatar);
            } else {
                mAvatar.setImageResource(R.drawable.ic_no_avatar_gray_360dp);
            }
        } else {
            mUserName.setText(R.string.unknown);
            mPhoneNumber.setText("");
            mAvatar.setImageResource(R.drawable.ic_no_avatar_gray_360dp);
        }

        // check exist notification setting
        int notificationVal = 0;
        if (app.isDemoMode()) {
            mNotification.setEnabled(false);
        } else {
            if (!Utils.existPreference(getContext(), Constants.NOTIFICATION_SETTING_KEY + user.user_phone)) {
                Utils.savePreference(getContext(), Constants.NOTIFICATION_SETTING_KEY + user.user_phone, 1);
            }
            notificationVal = Utils.getIntPreference(getContext(), Constants.NOTIFICATION_SETTING_KEY + user.user_phone);
        }
        if (notificationVal > 0) {
            mNotification.setChecked(true);
        } else {
            mNotification.setChecked(false);
        }

        String lang = app.getLanguage();
        if ("vi".equals(lang)) {
            mAppLanguage.setText(getContext().getString(R.string.language_popup_vietnamese));
        } else {
            mAppLanguage.setText(getContext().getString(R.string.language_popup_english));
        }
        mAppVersion.setText(BuildConfig.VERSION_NAME);
    }

    private void setupControlEvents() {
        mEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FarmBoxAppController app = FarmBoxAppController.getInstance();
                if (app.isDemoMode()) {
                    Utils.showDialog(getContext(), getString(R.string.demo_mode), getString(R.string.demo_mode_message));
                } else {
                    Intent intent = new Intent(getContext(), EditAccountActivity.class);
                    startActivity(intent);
                }
            }
        });

        mFarmsSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), SettingYourFarmsActivity.class);
                startActivity(intent);
            }
        });

        mNotification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                User user = FarmBoxAppController.getInstance().getUser();
                int notificationVal = Utils.getIntPreference(getContext(), Constants.NOTIFICATION_SETTING_KEY + user.user_phone);
                boolean isVal = notificationVal > 0;
                if (isVal != isChecked) {
                    if (isChecked) {
                        subscribeNotification();
                    } else {
                        unsubscribeNotification();
                    }
                }
            }
        });

        mUnitSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), SettingUnitActivity.class);
                startActivity(intent);
            }
        });

        mScanQRCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ScanQRCodeActivity.class);
                startActivity(intent);
            }
        });

        mLanguageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLanguageDialog();
            }
        });

        mLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestLogout();
            }
        });
    }

    private void requestLogout() {
        final AlertDialog dialog = new AlertDialog.Builder(getContext(), R.style.AppTheme_Dialog_Alert)
                .setTitle(R.string.app_name)
                .setMessage(R.string.logout_message)
                .setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        doLogout();
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    private void doLogout() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        if (refreshedToken != null && !refreshedToken.isEmpty()) {
            UtilAPI utilAPI = APIHelper.getUtilAPI();
            utilAPI.unsubscribeNotification(refreshedToken, new APIHelper.Callback<String>() {
                @Override
                public void onSuccess(String data) {
                    Log.d(TAG, data);
                }

                @Override
                public void onFail(int errorCode, String error) {
                    Log.d(TAG, error);
                }
            });
        }

        FarmBoxAppController app = FarmBoxAppController.getInstance();

        Utils.savePreference(getContext(), Config.USER_TOKEN_KEY, "");
        Intent intent = new Intent(getContext(), PhoneRegistrationActivity.class);
        startActivity(intent);
        getActivity().finish();

        app.logout();
    }

    private void showLanguageDialog() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_option_language);

        TextView english = (TextView) dialog.findViewById(R.id.tv_english);
        TextView vietnamese = (TextView) dialog.findViewById(R.id.tv_vietnamese);
        TextView cancel = (TextView) dialog.findViewById(R.id.tv_cancel);

        english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                SettingsFragment.this.setLanguage("en");
            }
        });

        vietnamese.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                SettingsFragment.this.setLanguage("vi");
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void setLanguage(String lang) {
        FarmBoxAppController.getInstance().setLanguage(lang);

        if (getActivity() instanceof TabActivity) {
            TabActivity activity = (TabActivity) getActivity();
            activity.recreate();
//
//            FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
//            transaction.replace(R.id.frame_layout, new SettingsFragment());
//            transaction.commit();
//
//            // apply menu
//            activity.didTabChanged();
        }
    }

    private void subscribeNotification() {
        FarmBoxAppController app = FarmBoxAppController.getInstance();
        if (app.isDemoMode()) {
            Utils.showDialog(getContext(), getString(R.string.demo_mode), getString(R.string.demo_mode_message));
            return;
        }

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        if (null == refreshedToken || refreshedToken.isEmpty()) {
            return;
        }

        UtilAPI utilAPI = APIHelper.getUtilAPI();
        utilAPI.subscribeNotification(refreshedToken, new APIHelper.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                Activity activity = getActivity();
                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            User user = FarmBoxAppController.getInstance().getUser();
                            Utils.savePreference(getContext(), Constants.NOTIFICATION_SETTING_KEY + user.user_phone, 1);
                            mNotification.setChecked(true);
                        }
                    });
                }
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                Activity activity = getActivity();
                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            handleError(errorCode, error);
                            resetNotificationSetting();
                        }
                    });
                }
            }
        });
    }

    private void unsubscribeNotification() {
        FarmBoxAppController app = FarmBoxAppController.getInstance();
        if (app.isDemoMode()) {
            Utils.showDialog(getContext(), getString(R.string.demo_mode), getString(R.string.demo_mode_message));
            return;
        }


        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        UtilAPI utilAPI = APIHelper.getUtilAPI();
        utilAPI.unsubscribeNotification(refreshedToken, new APIHelper.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                Activity activity = getActivity();
                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            User user = FarmBoxAppController.getInstance().getUser();
                            Utils.savePreference(getContext(), Constants.NOTIFICATION_SETTING_KEY + user.user_phone, 0);
                            mNotification.setChecked(false);
                        }
                    });
                }
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                Activity activity = getActivity();
                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            handleError(errorCode, error);
                            resetNotificationSetting();
                        }
                    });
                }
            }
        });
    }

    private void resetNotificationSetting() {
        User user = FarmBoxAppController.getInstance().getUser();
        int notificationVal = Utils.getIntPreference(getContext(), Constants.NOTIFICATION_SETTING_KEY + user.user_phone);
        if (notificationVal > 0) {
            mNotification.setChecked(true);
        } else {
            mNotification.setChecked(false);
        }
    }
}
