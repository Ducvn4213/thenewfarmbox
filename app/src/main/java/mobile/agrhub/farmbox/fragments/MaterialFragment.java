package mobile.agrhub.farmbox.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.activities.dairy.AddDiaryActivity;
import mobile.agrhub.farmbox.adapters.DiaryListAdapter;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.model.Diary;
import mobile.agrhub.farmbox.service.model.response.GetDiariesResponse;
import mobile.agrhub.farmbox.utils.BaseFragment;

public class MaterialFragment extends BaseFragment {
    private static final String TAG = "DiaryFragment";

    private View view;
    private ListView listView;
    private LinearLayout mEmptyView;
    private TextView mEmptyMessage;
    private Button mAddDiary;
    private DiaryListAdapter listAdapter;
    private List<Diary> diaries;
    private String mSelectedFarmId = "";
    private int mSelectedFromMonth;
    private int mSelectedFromYear;
    private int mSelectedFromDate;
    private int mSelectedToYear;
    private int mSelectedToMonth;
    private int mSelectedToDate;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_productivity, container, false);

        listView = (ListView) view.findViewById(R.id.feed_list);
        mEmptyView = (LinearLayout) view.findViewById(android.R.id.empty);
        mEmptyMessage = (TextView) view.findViewById(R.id.tv_empty_message);
        mAddDiary = (Button) view.findViewById(R.id.btn_write_a_diary);
        mAddDiary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MaterialFragment.this.getContext(), AddDiaryActivity.class);
                MaterialFragment.this.startActivity(intent);
            }
        });

        diaries = new ArrayList<Diary>();
        listAdapter = new DiaryListAdapter(this.getActivity(), diaries);
        listView.setAdapter(listAdapter);
        listView.setEmptyView(mEmptyView);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        String startTimeStr = "";
        if (mSelectedFromYear > 0 && mSelectedFromMonth > 0 && mSelectedFromDate > 0) {
            Calendar startTime = Calendar.getInstance();
            startTime.set(mSelectedFromYear, mSelectedToMonth, mSelectedFromDate, 0, 0, 0);
            startTimeStr = String.valueOf(startTime.getTimeInMillis());
        }

        String endTimeStr = "";
        if (mSelectedToYear > 0 && mSelectedToMonth > 0 && mSelectedToDate > 0) {
            Calendar endTime = Calendar.getInstance();
            endTime.set(mSelectedToYear, mSelectedToMonth, mSelectedToDate, 59, 59, 59);
            endTimeStr = String.valueOf(endTime.getTimeInMillis());
        }

        APIHelper.getGapAPI().getDiary(mSelectedFarmId, startTimeStr, endTimeStr, new APIHelper.Callback<GetDiariesResponse>() {
            @Override
            public void onSuccess(GetDiariesResponse data) {
                if (data == null) {
                    diaries.clear();
                } else {
                    diaries = data.diaries;
                    FragmentActivity activity = MaterialFragment.this.getActivity();
                    if (activity != null) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (MaterialFragment.this != null) {
                                    MaterialFragment.this.hideLoading();
                                    MaterialFragment.this.refreshListView();
                                }
                            }
                        });
                    }
                }
            }

            @Override
            public void onFail(int errorCode, String error) {

            }
        });
    }

    private void refreshListView() {
        if (null != listView) {
            if (null == listAdapter) {
                listAdapter = new DiaryListAdapter(getContext(), diaries);
                listView.setAdapter(listAdapter);
            } else {
                listAdapter.setData(diaries);
                listAdapter.notifyDataSetChanged();
            }
            if (hasFilter()) {
                mEmptyMessage.setText(getString(R.string.diary_filter_empty));
                mAddDiary.setVisibility(View.GONE);
            } else {
                mEmptyMessage.setText(getString(R.string.diary_empty));
                mAddDiary.setVisibility(View.VISIBLE);
            }
            listView.smoothScrollToPosition(0);
        }
    }

    public String getmSelectedFarmId() {
        return mSelectedFarmId;
    }

    public void setmSelectedFarmId(String mSelectedFarmId) {
        this.mSelectedFarmId = mSelectedFarmId;
    }

    public int getmSelectedFromMonth() {
        return mSelectedFromMonth;
    }

    public void setmSelectedFromMonth(int mSelectedFromMonth) {
        this.mSelectedFromMonth = mSelectedFromMonth;
    }

    public int getmSelectedFromYear() {
        return mSelectedFromYear;
    }

    public void setmSelectedFromYear(int mSelectedFromYear) {
        this.mSelectedFromYear = mSelectedFromYear;
    }

    public int getmSelectedFromDate() {
        return mSelectedFromDate;
    }

    public void setmSelectedFromDate(int mSelectedFromDate) {
        this.mSelectedFromDate = mSelectedFromDate;
    }

    public int getmSelectedToYear() {
        return mSelectedToYear;
    }

    public void setmSelectedToYear(int mSelectedToYear) {
        this.mSelectedToYear = mSelectedToYear;
    }

    public int getmSelectedToMonth() {
        return mSelectedToMonth;
    }

    public void setmSelectedToMonth(int mSelectedToMonth) {
        this.mSelectedToMonth = mSelectedToMonth;
    }

    public int getmSelectedToDate() {
        return mSelectedToDate;
    }

    public void setmSelectedToDate(int mSelectedToDate) {
        this.mSelectedToDate = mSelectedToDate;
    }

    public boolean hasFilter() {
        if (this.mSelectedFarmId != null && !this.mSelectedFarmId.isEmpty()) {
            return true;
        }

        if (this.mSelectedFromYear > 0 && this.mSelectedFromMonth > 0 && this.mSelectedFromDate > 0) {
            return true;
        }

        if (this.mSelectedToYear > 0 && this.mSelectedToMonth > 0 && this.mSelectedToDate > 0) {
            return true;
        }

        return false;
    }
}
