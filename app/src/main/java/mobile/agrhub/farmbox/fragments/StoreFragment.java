package mobile.agrhub.farmbox.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.codewaves.stickyheadergrid.StickyHeaderGridLayoutManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.activities.TabActivity;
import mobile.agrhub.farmbox.activities.store.ProductInfoActivity;
import mobile.agrhub.farmbox.adapters.ProductAdapter;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.StoreAPI;
import mobile.agrhub.farmbox.service.model.Product;
import mobile.agrhub.farmbox.service.model.ProductCategory;
import mobile.agrhub.farmbox.service.model.response.GetProductCategoryResponse;
import mobile.agrhub.farmbox.service.model.response.GetProductResponse;
import mobile.agrhub.farmbox.utils.BaseFragment;

public class StoreFragment extends BaseFragment implements ProductAdapter.OnProductClickListener {
    private static final int SPAN_SIZE = 2;

    private List<ProductCategory> categoryList = new ArrayList<ProductCategory>();
    private Map<String, List<Product>> productMap = new HashMap<String, List<Product>>();

    private RecyclerView mRecycler;
    private StickyHeaderGridLayoutManager mLayoutManager;
    private ProductAdapter adapter;

    private View view;

    private StoreAPI storeAPI = APIHelper.getStoreAPI();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_store, container, false);

        // Setup recycler
        mRecycler = (RecyclerView) view.findViewById(R.id.recycler);
        mLayoutManager = new StickyHeaderGridLayoutManager(SPAN_SIZE);
//        mLayoutManager.setHeaderBottomOverlapMargin(getResources().getDimensionPixelSize(R.dimen.header_shadow_size));
        mLayoutManager.setHeaderBottomOverlapMargin(1);
        mLayoutManager.setSpanSizeLookup(new StickyHeaderGridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int section, int position) {
                return 1;
            }
        });

        // Workaround RecyclerView limitation when playing remove animations. RecyclerView always
        // puts the removed item on the top of other views and it will be drawn above sticky header.
        // The only way to fix this, abandon remove animations :(
        mRecycler.setItemAnimator(new DefaultItemAnimator() {
            @Override
            public boolean animateRemove(RecyclerView.ViewHolder holder) {
                dispatchRemoveFinished(holder);
                return false;
            }
        });
        mRecycler.setLayoutManager(mLayoutManager);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        getProductCategoryList();
    }

    private void getProductCategoryList() {
        storeAPI.getProductCategoryList(new APIHelper.Callback<GetProductCategoryResponse>() {
            @Override
            public void onSuccess(GetProductCategoryResponse data) {
                categoryList.clear();
                productMap.clear();
                if (data != null) {
                    if (data.productCategories != null && data.productCategories.size() > 0) {
                        for (ProductCategory category : data.productCategories) {
                            if ("FB-KITS".equals(category.categoryCode)) {
                                categoryList.add(0, category);
                            } else {
                                categoryList.add(category);
                            }
                        }
                    }

                    String categoryId;
                    List<Product> productList;
                    for (ProductCategory category : categoryList) {
                        categoryId = String.valueOf(category.categoryId);
                        productList = new ArrayList<Product>();

                        productMap.put(categoryId, productList);
                    }

                    getProductList();
                }
            }

            @Override
            public void onFail(int errorCode, String error) {

            }
        });
    }

    private void getProductList() {
        storeAPI.getProductList(new APIHelper.Callback<GetProductResponse>() {
            @Override
            public void onSuccess(GetProductResponse data) {
                if (data != null) {
                    Gson gson = new Gson();
                    ProductCategory category;
                    String categoryId;
                    List<Product> productList;
                    for (Product product : data.products) {
                        if (product.productAvatar == null || product.productAvatar.isEmpty()) {
                            product.productImageList = new ArrayList<String>();
                        } else {
                            product.productImageList = gson.fromJson(product.productAvatar, new TypeToken<List<String>>() {
                            }.getType());
                        }

                        category = product.category;
                        if (category != null) {
                            categoryId = String.valueOf(category.categoryId);
                            productList = productMap.get(categoryId);
                            productList.add(product);

                            productMap.put(categoryId, productList);
                        }
                    }
                    FragmentActivity activity = StoreFragment.this.getActivity();
                    if (activity != null) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                StoreFragment.this.hideLoading();
                                StoreFragment.this.refreshListView();
                            }
                        });
                    }
                }
            }

            @Override
            public void onFail(int errorCode, String error) {

            }
        });
    }

    private void refreshListView() {
        if (null != mRecycler) {
            if (null == adapter) {
                adapter = new ProductAdapter(categoryList, productMap, StoreFragment.this);
                mRecycler.setAdapter(adapter);
            } else {
                adapter.setData(categoryList, productMap);
                adapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onSelectProduct(Product product) {
        Gson gson = new Gson();
        String productJsonInfo = gson.toJson(product);

        Intent intent = new Intent(StoreFragment.this.getActivity(), ProductInfoActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("SELECTED_PRODUCT_INFO", productJsonInfo);
        intent.putExtras(bundle); //Put your id to your next Intent

        startActivity(intent);
    }

    @Override
    public void addToCartProduct(Product product) {
        FarmBoxAppController app = FarmBoxAppController.getInstance();
        app.addProduct(product, 1);

        Activity tabActivity = this.getActivity();
        if (tabActivity instanceof TabActivity) {
            ((TabActivity) tabActivity).updateAlertIcon();
        }

        Toast.makeText(StoreFragment.this.getContext(), R.string.added_product_to_cart, Toast.LENGTH_SHORT).show();
    }
}
