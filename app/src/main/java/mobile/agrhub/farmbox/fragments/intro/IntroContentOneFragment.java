package mobile.agrhub.farmbox.fragments.intro;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.dev_hub.DevHub;

public class IntroContentOneFragment extends Fragment {

    View containerView;
    TextView devHint;

    int devHit = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        containerView = inflater.inflate(R.layout.intro_content_fragment_one, container, false);

        devHint = (TextView) containerView.findViewById(R.id.tv_dev_hint);

        devHint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDevHintPressed();
            }
        });

        return containerView;
    }

    public void onDevHintPressed() {
        if (devHit == 5) {
            devHit = -1;
            showConfirmPasswordDialog();
            return;
        }

        devHit++;

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                devHit = 0;
            }
        }, 2000);
    }

    private void showConfirmPasswordDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.AppTheme_Dialog_Alert);
//        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Enter passcode");

        // Set up the input
        final EditText tokenInput = new EditText(getActivity());

        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        tokenInput.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        tokenInput.setGravity(Gravity.CENTER);
        builder.setView(tokenInput);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String token = tokenInput.getText().toString();
                if ("075031".equals(token)) {
                    openDevHub();
                } else {
                    dialog.cancel();
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }


    private void openDevHub() {
        Intent intent = new Intent(getActivity(), DevHub.class);
        startActivity(intent);
        getActivity().finish();
    }
}
