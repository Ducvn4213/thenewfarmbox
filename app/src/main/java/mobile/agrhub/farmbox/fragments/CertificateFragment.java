package mobile.agrhub.farmbox.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.activities.certificate.AddCertificateActivity;
import mobile.agrhub.farmbox.activities.certificate.DetailCertificateActivity;
import mobile.agrhub.farmbox.adapters.gap.CertificateAdapter;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.model.GapCertificate;
import mobile.agrhub.farmbox.service.model.response.GetCertificateResponse;
import mobile.agrhub.farmbox.utils.BaseFragment;
import mobile.agrhub.farmbox.utils.Constants;

public class CertificateFragment extends BaseFragment implements CertificateAdapter.OnClickListener {
    private static final String TAG = "CertificateFragment";

    private View view;
    @BindView(R.id.list_view)
    ListView listView;
    private CertificateAdapter listAdapter;
    @BindView(android.R.id.empty)
    LinearLayout mEmptyView;
    @BindView(R.id.tv_empty_message)
    TextView mEmptyMessage;
    @BindView(R.id.btn_add_certificate)
    Button mAddCertificate;
    @BindView(R.id.fb_add)
    FloatingActionButton mAddBtn;

    private List<GapCertificate> certificates;

    int page = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_certificate, container, false);
        ButterKnife.bind(this, view);

        mAddCertificate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CertificateFragment.this.getContext(), AddCertificateActivity.class);
                CertificateFragment.this.startActivity(intent);
            }
        });

        mAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CertificateFragment.this.getContext(), AddCertificateActivity.class);
                CertificateFragment.this.startActivity(intent);
            }
        });

        certificates = new ArrayList<>();
        listAdapter = new CertificateAdapter(getContext(), certificates, this);
        listView.setAdapter(listAdapter);
        listView.setEmptyView(mEmptyView);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        APIHelper.getGapAPI().getCertificates(new APIHelper.Callback<GetCertificateResponse>() {
            @Override
            public void onSuccess(GetCertificateResponse data) {
                if (data == null) {
                    certificates.clear();
                } else {
                    certificates = data.certificates;
                }
                FragmentActivity activity = CertificateFragment.this.getActivity();
                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (CertificateFragment.this != null) {
                                CertificateFragment.this.hideLoading();
                                CertificateFragment.this.refreshListView();
                            }
                        }
                    });
                }
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                FragmentActivity activity = CertificateFragment.this.getActivity();
                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (CertificateFragment.this != null) {
                                CertificateFragment.this.hideLoading();
                                CertificateFragment.this.handleError(errorCode, error);
                            }
                        }
                    });
                }
            }
        });
    }

    private void refreshListView() {
        if (null != listView) {
            if (null == listAdapter) {
                listAdapter = new CertificateAdapter(getContext(), certificates, this);
                listView.setAdapter(listAdapter);
            } else {
                listAdapter.setData(certificates);
                listAdapter.notifyDataSetChanged();
            }
            mEmptyMessage.setText(getString(R.string.certificate_empty_message));
            mAddCertificate.setVisibility(View.VISIBLE);
            listView.smoothScrollToPosition(0);
        }
    }

    @Override
    public void onLearnMoreClicked(GapCertificate certificate) {
        Gson gson = new Gson();
        String data = gson.toJson(certificate);

        Intent intent = new Intent(getContext(), DetailCertificateActivity.class);
        intent.putExtra(Constants.GAP_CERTIFICATE_INFO_KEY, data);
        startActivity(intent);
    }

    @Override
    public void onUpdateClicked(GapCertificate certificate) {
        Gson gson = new Gson();
        String data = gson.toJson(certificate);

        Intent intent = new Intent(getContext(), AddCertificateActivity.class);
        intent.putExtra(Constants.GAP_CERTIFICATE_INFO_KEY, data);
        startActivity(intent);
    }
}
