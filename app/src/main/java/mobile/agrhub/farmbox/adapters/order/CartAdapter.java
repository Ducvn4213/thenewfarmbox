package mobile.agrhub.farmbox.adapters.order;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.FarmCart;
import mobile.agrhub.farmbox.service.model.FarmProduct;
import mobile.agrhub.farmbox.utils.Utils;

/**
 * Created by lecong on 3/27/18.
 */

public class CartAdapter extends BaseAdapter implements View.OnClickListener {

    private int mode = 0; // 0: default; 1: add to cart; 2: assign QR Code
    private Context mContext;
    private LayoutInflater inflater;
    private List<FarmCart> carts;
    private OnClickListener listener;
    ImageLoader imageLoader = ImageLoader.getInstance();

    // Constructor
    public CartAdapter(Activity context, List<FarmCart> carts, OnClickListener listener) {
        this.mode = 1;
        this.mContext = context;
        this.inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.carts = carts;
        this.listener = listener;
    }

    public CartAdapter(Activity context, List<FarmCart> carts, int mode, OnClickListener listener) {
        this.mode = mode;
        this.mContext = context;
        this.inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.carts = carts;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return carts.size();
    }

    @Override
    public Object getItem(int position) {
        return this.carts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = inflater.inflate(R.layout.layout_farm_product_item, null);

        FarmCart cart = (FarmCart) getItem(position);
        long productId = cart.cartProduct.productId;
        FarmProduct product =  cart.cartProduct;

        CardView container = convertView.findViewById(R.id.cv_container);
        container.setTag(String.valueOf(productId));
        container.setOnClickListener(this);

        ImageView productImageView = convertView.findViewById(R.id.iv_product_image);
        productImageView.setBackgroundResource(R.color.colorBackground);
        if (product == null) {
            productImageView.setImageResource(R.drawable.ic_photo_gray_24dp);
        } else {
            String avatar = product.getDisplayAvatar();
            if (avatar == null || avatar.isEmpty()) {
                productImageView.setImageResource(R.drawable.ic_photo_gray_24dp);
            } else {
                if (imageLoader == null) {
                    imageLoader = ImageLoader.getInstance();
                }
                imageLoader.displayImage(avatar, productImageView);
            }
        }

        TextView productPriceView = convertView.findViewById(R.id.tv_product_price);
        TextView productNameView = convertView.findViewById(R.id.tv_product_name);
        TextView productWeightView = convertView.findViewById(R.id.tv_product_weight);
        LinearLayout assignQRCodeView = convertView.findViewById(R.id.ll_assign_qr_code_remain);
        TextView productQuantityView = convertView.findViewById(R.id.tv_product_quantity);
        TextView productRemainView = convertView.findViewById(R.id.tv_product_remain);

        if (product != null) {
            productPriceView.setText(product.displayPrice());
            productNameView.setText(product.productName);
            productWeightView.setText(product.displayWeight(mContext));
            productQuantityView.setText(cart.displayQuantity());
            productRemainView.setText(cart.displayRemain());
        }

        View separator = convertView.findViewById(R.id.v_separator);
        LinearLayout productAmountView = convertView.findViewById(R.id.ll_product_amount);
        LinearLayout addToCartView = convertView.findViewById(R.id.ll_add_to_cart);
        LinearLayout assignQRCodeControlView = convertView.findViewById(R.id.ll_assign_qr_code_control_view);

        if (mode == 0) {
            productPriceView.setVisibility(View.VISIBLE);
            separator.setVisibility(View.GONE);
            productAmountView.setVisibility(View.GONE);
            addToCartView.setVisibility(View.GONE);
            assignQRCodeView.setVisibility(View.GONE);
            assignQRCodeControlView.setVisibility(View.GONE);
        } else if (mode == 1) {
            productPriceView.setVisibility(View.VISIBLE);
            if (cart.cartItemQuantity <= 0) {
                separator.setVisibility(View.VISIBLE);
                productAmountView.setVisibility(View.GONE);
                addToCartView.setVisibility(View.VISIBLE);
            } else {
                separator.setVisibility(View.VISIBLE);
                productAmountView.setVisibility(View.VISIBLE);
                addToCartView.setVisibility(View.GONE);
            }
            assignQRCodeView.setVisibility(View.GONE);
            assignQRCodeControlView.setVisibility(View.GONE);
        } else if (mode == 2) {
            productPriceView.setVisibility(View.GONE);
            productAmountView.setVisibility(View.GONE);
            addToCartView.setVisibility(View.GONE);
            assignQRCodeView.setVisibility(View.VISIBLE);
            assignQRCodeControlView.setVisibility(View.VISIBLE);
        }

        ImageButton minusAmountBtn = convertView.findViewById(R.id.ib_amount_minus);
        minusAmountBtn.setTag(String.valueOf(productId));
        minusAmountBtn.setOnClickListener(this);

        TextView amountView = convertView.findViewById(R.id.tv_product_amount);
        amountView.setTag(String.valueOf(productId));
        amountView.setText(Utils.i2s(cart.cartItemQuantity));
        amountView.setOnClickListener(this);

        ImageButton plusAmountBtn = convertView.findViewById(R.id.ib_amount_plus);
        plusAmountBtn.setTag(String.valueOf(productId));
        plusAmountBtn.setOnClickListener(this);

        addToCartView.setTag(String.valueOf(productId));
        addToCartView.setOnClickListener(this);

        Button scanBtn = convertView.findViewById(R.id.btn_scan);
        scanBtn.setTag(String.valueOf(productId));
        scanBtn.setOnClickListener(this);

        return convertView;
    }

    public void update(List<FarmCart> carts) {
        this.carts = carts;
        notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        Object tagObj = v.getTag();
        if (tagObj != null && tagObj instanceof String) {
            long productId = 0;

            String productIdStr = (String) tagObj;
            try {
                productId = Utils.str2Long(productIdStr);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (v.getId() == R.id.ib_amount_minus) {
                addToCart(productId, 1, -1);
                listener.onMinusClicked(productIdStr);
            } else if (v.getId() == R.id.ib_amount_plus) {
                addToCart(productId, 1, 1);
                listener.onPlusClicked(productIdStr);
            } else if (v.getId() == R.id.tv_product_amount) {
                listener.onProductAmountClicked(productId, getAmount(productId));
            } else if (v.getId() == R.id.ll_add_to_cart) {
                addToCart(productId, 1, 0);
                listener.onAddToCartClicked(productId);
            } else if (v.getId() == R.id.btn_scan) {
                listener.onAssignQRCodeToCartClicked(getCart(productId));
            }
        }
    }

    private int getAmount(long id) {
        int amount = 0;
        if (carts != null && carts.size() > 0) {
            for (FarmCart item : carts) {
                if (id == item.cartProduct.productId) {
                    amount = item.cartItemQuantity;
                    break;
                }
            }
        }
        return amount;
    }

    public void addToCart(long id, int amount, int type) {
        if (carts != null && carts.size() > 0) {
            FarmCart item;
            for (int i = 0; i < carts.size(); i++) {
                item = carts.get(i);
                if (id == item.cartProduct.productId) {
                    if (type == 0) {
                        item.cartItemQuantity = amount;
                    } else {
                        item.cartItemQuantity += (amount * type);
                        if (item.cartItemQuantity < 0) {
                            item.cartItemQuantity = 0;
                        }
                    }

                    break;
                }
            }
        }
    }

    public int totalItem() {
        int totalItem = 0;
        if (carts != null && carts.size() > 0) {
            for (FarmCart item : carts) {
                totalItem += item.cartItemQuantity;
            }
        }
        return totalItem;
    }

    public int totalPrice() {
        int total = 0;

        if (carts != null && carts.size() > 0) {
            int price = 0;
            for (FarmCart item : carts) {
                if (item.cartProduct != null) {
                    price = 0;
                    try {
                        price = Utils.str2Int(item.cartProduct.productPrice);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    total += item.cartItemQuantity * price;
                }
            }
        }

        return total;
    }

    private FarmCart getCart(long productId) {
        FarmCart cart = null;
        if (carts != null && carts.size() > 0) {
            for (FarmCart item : carts) {
                if (item.cartProduct != null && productId == item.cartProduct.productId) {
                    cart = item;
                    break;
                }
            }
        }
        return cart;
    }

    public int totalRemain() {
        int total = 0;
        if (carts != null && carts.size() > 0) {
            for (FarmCart item : carts) {
                total += item.getRemain();
            }
        }
        return total;
    }

    public interface OnClickListener {
        void onMinusClicked(String productId);
        void onProductAmountClicked(long productId, int amount);
        void onPlusClicked(String productId);
        void onAddToCartClicked(long productId);
        void onAssignQRCodeToCartClicked(FarmCart cart);
    }
}
