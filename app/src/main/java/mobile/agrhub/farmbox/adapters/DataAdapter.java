package mobile.agrhub.farmbox.adapters;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.DataItem;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private List<DataItem> datas;
    private Activity mActivity;
    private DataItemListener listener;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View view) {
            super(view);

            bindingControls(view);
            setupControlEvents();
        }

        private DataItem mData;
        private LinearLayout mContent;
        private TextView mTitle;
        private TextView mValue;
        private ImageView mDetailArrow;

        private void bindingControls(View v) {
            mContent = (LinearLayout) v.findViewById(R.id.ll_content_view);
            mTitle = (TextView) v.findViewById(R.id.tv_title);
            mValue = (TextView) v.findViewById(R.id.tv_value);
            mDetailArrow = (ImageView) v.findViewById(R.id.iv_detail_arrow);
        }

        private void setupControlEvents() {
            mContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(mData);
                }
            });
        }

        private void updateUI() {
            mTitle.setText(mData.getTitle());
            if (mData.getDisplayValue() == null || mData.getDisplayValue().isEmpty()) {
                SpannableString spanString = new SpannableString(mData.getPlaceholder());
                spanString.setSpan(new StyleSpan(Typeface.ITALIC), 0, spanString.length(), 0);
                mValue.setText(spanString);
            } else {
                mValue.setText(mData.getDisplayValue());
            }
        }

        public void setData(DataItem data) {
            mData = data;
            updateUI();
        }
    }

    public interface DataItemListener {
        void onClick(DataItem item);
    }


    public DataAdapter(Activity activity, List<DataItem> data, DataItemListener listener) {
        this.mActivity = activity;
        this.listener = listener;
        this.datas = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_data_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        DataItem item = datas.get(position);
        holder.setData(item);
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }
}
