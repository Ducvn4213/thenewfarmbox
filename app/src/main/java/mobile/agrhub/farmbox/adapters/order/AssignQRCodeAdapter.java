package mobile.agrhub.farmbox.adapters.order;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.SerialPackage;
import mobile.agrhub.farmbox.utils.Utils;

public class AssignQRCodeAdapter extends BaseAdapter implements View.OnClickListener {

    private Context context;
    private LayoutInflater inflater;
    private List<SerialPackage> packageList;
    private OnClickListener listener;

    public AssignQRCodeAdapter(Context context, List<SerialPackage> packageList, OnClickListener listener) {
        this.context = context;
        this.packageList = packageList;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return this.packageList.size();
    }

    @Override
    public Object getItem(int location) {
        return this.packageList.get(location);
    }

    public void setData(List<SerialPackage> packageList) {
        this.packageList = packageList;
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.layout_assign_qr_code_item, null);

        TextView fromTitle = convertView.findViewById(R.id.tv_from_title);
        TextView toTitle = convertView.findViewById(R.id.tv_to_title);
        TextView fromVal = convertView.findViewById(R.id.tv_from_val);
        TextView toVal = convertView.findViewById(R.id.tv_to_val);
        TextView quantity = convertView.findViewById(R.id.tv_quantity);
        ImageButton removeBtn = convertView.findViewById(R.id.ib_remove);

        SerialPackage serial = this.packageList.get(position);
        if (serial.type == SerialPackage.TYPE_CODE) {
            fromTitle.setText(context.getString(R.string.qr_code));
            toTitle.setVisibility(View.GONE);
            fromVal.setText(serial.code);
            toVal.setVisibility(View.GONE);
            quantity.setText(Utils.i2s(serial.quantity));
        } else {
            fromTitle.setText(context.getString(R.string.from));
            fromVal.setText(serial.fromSerial);
            toTitle.setVisibility(View.VISIBLE);
            toVal.setVisibility(View.VISIBLE);
            toVal.setText(serial.toSerial);
            quantity.setText(Utils.i2s(serial.quantity));
        }

        removeBtn.setTag(String.valueOf(position));
        removeBtn.setOnClickListener(this);

        return convertView;
    }

    @Override
    public void onClick(View v) {
        if (v.getTag() != null && v.getTag() instanceof String) {
            int position = -1;
            try {
                position = Utils.str2Int((String)v.getTag());
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (position >= 0 && position < this.packageList.size()) {
                listener.onRemoveSerialPackage(this.packageList.get(position));
            }
        }
    }

    public interface OnClickListener {
        void onRemoveSerialPackage(SerialPackage serial);
    }
}
