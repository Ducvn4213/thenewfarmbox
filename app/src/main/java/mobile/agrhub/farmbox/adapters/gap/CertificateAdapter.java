package mobile.agrhub.farmbox.adapters.gap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.GapCertificate;

public class CertificateAdapter extends BaseAdapter implements View.OnClickListener {

    private Context context;
    private LayoutInflater inflater;
    private List<GapCertificate> certificates;
    private OnClickListener listener;
    ImageLoader imageLoader;

    public CertificateAdapter(Context context, List<GapCertificate> certificates, OnClickListener listener) {
        this.context = context;
        this.certificates = certificates;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return certificates.size();
    }

    @Override
    public Object getItem(int location) {
        return certificates.get(location);
    }

    public void setData(List<GapCertificate> certificates) {
        this.certificates = certificates;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.layout_gap_certificate_item, null);

        if (imageLoader == null)
            imageLoader = ImageLoader.getInstance();

        ImageView photoView = (ImageView) convertView.findViewById(R.id.iv_photo);
        TextView titleView = (TextView) convertView.findViewById(R.id.tv_title);
        TextView descriptionView = (TextView) convertView.findViewById(R.id.tv_description);
        TextView stateView = (TextView) convertView.findViewById(R.id.tv_state);
        Button learnMore = (Button) convertView.findViewById(R.id.btn_learn_more);
        Button update = (Button) convertView.findViewById(R.id.btn_update);

        GapCertificate certificate = (GapCertificate)getItem(position);
        String photoUrl = "";
        if (certificate.gapCertificateMedia != null && certificate.gapCertificateMedia.length > 0) {
            for (String url : certificate.gapCertificateMedia) {
                if (!url.isEmpty()) {
                    photoUrl = url;
                    break;
                }
            }
        }
        if (!photoUrl.isEmpty()) {
            imageLoader.displayImage(photoUrl, photoView);
        }
        titleView.setText(certificate.gapCertificateName);
        descriptionView.setText(context.getString(R.string.certificate_by_name, certificate.gapCertificateIssuer));
        switch (certificate.gapCertificateState) {
            case GapCertificate.STATE_REJECTED:
                stateView.setText(R.string.rejected);
                break;
            case GapCertificate.STATE_REWORK:
                stateView.setText(R.string.rework);
                break;
            case GapCertificate.STATE_APPROVED:
                stateView.setText(R.string.approved);
                break;
            default:
                stateView.setText(R.string.reviewing);
                break;
        }

        learnMore.setTag(String.valueOf(certificate.gapCertificateId));
        learnMore.setOnClickListener(this);

        update.setTag(String.valueOf(certificate.gapCertificateId));
        update.setOnClickListener(this);

        return convertView;
    }

    @Override
    public void onClick(View v) {
        if (v.getTag() != null && v.getTag() instanceof String) {
            long id = Long.parseLong((String)v.getTag());

            GapCertificate certificate = null;
            for (GapCertificate item: certificates) {
                if (item.gapCertificateId == id) {
                    certificate = item;
                    break;
                }
            }

            if (certificate != null) {
                if (R.id.btn_learn_more == v.getId()) {
                    this.listener.onLearnMoreClicked(certificate);
                } else if (R.id.btn_update == v.getId()) {
                    this.listener.onUpdateClicked(certificate);
                }
            }
        }
    }

    public interface OnClickListener {
        void onLearnMoreClicked(GapCertificate certificate);
        void onUpdateClicked(GapCertificate certificate);
    }
}
