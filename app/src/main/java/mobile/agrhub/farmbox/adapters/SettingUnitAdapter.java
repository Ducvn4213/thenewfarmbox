package mobile.agrhub.farmbox.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.model.UnitItem;
import mobile.agrhub.farmbox.service.model.UnitOptionItem;
import mobile.agrhub.farmbox.service.model.User;
import mobile.agrhub.farmbox.utils.Constants;
import mobile.agrhub.farmbox.utils.Utils;

public class SettingUnitAdapter extends RecyclerView.Adapter<SettingUnitAdapter.ViewHolder> {
    private Activity mActivity;
    private List<UnitItem> unitList;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View view) {
            super(view);

            bindingControls(view);
            setupControlEvents();
        }

        private TextView mName;
        private RadioGroup mRadioGroup;
        private UnitItem mData;

        private void bindingControls(View v) {
            mName = (TextView) v.findViewById(R.id.tv_name);
            mRadioGroup = (RadioGroup) v.findViewById(R.id.rdg_options);
        }

        private void setupControlEvents() {
            mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                    // If the radiobutton that has changed in check state is now checked...
                    if (checkedRadioButton.isChecked()) {
                        UnitOptionItem item = (UnitOptionItem)checkedRadioButton.getTag();
                        Log.d("savePreference", "Key: " + item.getKey());
                        Log.d("savePreference", "Value: " + item.getValue());
                        Utils.savePreference(mActivity, item.getKey(), item.getValue());

                        FarmBoxAppController app = FarmBoxAppController.getInstance();
                        User user = app.getUser();
                        if (item.getKey().startsWith(Constants.SETTING_TEMPERATURE_UNIT_KEY)) {
                            user.setTemperatureUnit(item.getValue());
                        } else if (item.getKey().startsWith(Constants.SETTING_NUTRITION_UNIT_KEY)) {
                            user.setNutritionUnit(item.getValue());
                        }
                        app.setUser(user, app.getAccessToken());
                    }
                }
            });
        }

        private void updateUI(int position) {
            mName.setText(mData.getTitle());
            if (mData.getOptions() != null) {
                RadioButton radioOptionItem;
                UnitOptionItem item;

                LinearLayout.LayoutParams layoutParam = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.MATCH_PARENT
                );

                int checkedId = -1;
                int checkboxId;
                for (int i = 0; i < mData.getOptions().size(); i++) {
                    checkboxId = position * 100 + i;

                    item = mData.getOptions().get(i);
                    radioOptionItem = new RadioButton(mActivity);
                    radioOptionItem.setText(item.getTitle());
                    radioOptionItem.setId(checkboxId);
                    if (mData.getValue().equals(item.getValue())) {
                        checkedId = checkboxId;
                    }
                    radioOptionItem.setTag(item);
                    radioOptionItem.setLayoutParams(layoutParam);
                    radioOptionItem.setTextColor(mActivity.getResources().getColor(R.color.gray_text_view));

                    mRadioGroup.addView(radioOptionItem);
                }
                mRadioGroup.check(checkedId);
            }
        }

        public void setData(int position, UnitItem data) {
            mData = data;
            updateUI(position);
        }
    }


    public SettingUnitAdapter(Activity activity, List<UnitItem> data) {
        this.mActivity = activity;
        if (data == null) {
            this.unitList = new ArrayList<>();
        } else {
            this.unitList = data;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_setting_unit_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        UnitItem item = this.unitList.get(position);
        holder.setData(position, item);
    }

    @Override
    public int getItemCount() {
        return this.unitList.size();
    }
}
