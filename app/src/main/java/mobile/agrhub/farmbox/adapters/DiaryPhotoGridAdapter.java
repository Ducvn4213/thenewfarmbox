package mobile.agrhub.farmbox.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.dmcbig.mediapicker.entity.Media;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageSize;

import java.util.List;

import mobile.agrhub.farmbox.R;

/**
 * Created by lecong on 3/26/18.
 */

public class DiaryPhotoGridAdapter extends BaseAdapter implements View.OnClickListener {
    private Activity mContext;
    private LayoutInflater inflater;
    private List<Media> mImagesList;
    private ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance

    // Constructor
    public DiaryPhotoGridAdapter(Activity context, List<Media> items) {
        this.mContext = context;
        this.mImagesList = items;
    }

    @Override
    public int getCount() {
        return mImagesList.size();
    }

    @Override
    public Object getItem(int position) {
        return mImagesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.layout_diary_photo_item, null);

        if (imageLoader == null)
            imageLoader = ImageLoader.getInstance();

        ImageView imageView = (ImageView) convertView.findViewById(R.id.iv_image);
        imageLoader.displayImage("file://" + mImagesList.get(position).path, imageView, new ImageSize(60, 60));
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        ImageView imageButton = (ImageView) convertView.findViewById(R.id.ib_remove);
        imageButton.setVisibility(View.VISIBLE);
        imageButton.setTag(1000 + position);
        imageButton.setOnClickListener(this);
        return convertView;
    }

    @Override
    public void onClick(View view) {
        Object tagObj = view.getTag();
        if (tagObj != null && tagObj instanceof Integer) {
            int tag = Integer.valueOf(tagObj.toString()) - 1000;
            if (tag < mImagesList.size()) {
                mImagesList.remove(tag);

                notifyDataSetChanged();
            }
        }
    }

    public void updateData(List<Media> mediaList) {
        mImagesList = mediaList;
        notifyDataSetChanged();
    }

}
