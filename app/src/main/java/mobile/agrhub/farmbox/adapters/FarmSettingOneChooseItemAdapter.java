package mobile.agrhub.farmbox.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.FarmSettingDetailData;

public class FarmSettingOneChooseItemAdapter extends RecyclerView.Adapter<FarmSettingOneChooseItemAdapter.ViewHolder> {
    private String selectedVal;
    private List<FarmSettingDetailData> options;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View view) {
            super(view);

            bindingControls(view);
            setupControlEvents(view);
        }

        private FarmSettingDetailData mData;
        private RadioButton mOption;
        private TextView mDescription;

        private void bindingControls(View v) {
            mOption = (RadioButton) v.findViewById(R.id.rb_option);
            mDescription = (TextView) v.findViewById(R.id.tv_description);
        }

        private void setupControlEvents(View view) {
            mOption.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked && selectedVal != null && !selectedVal.equals(mData.getValue())) {
                        selectedVal = mData.getValue();
                        notifyDataSetChanged();
                    }
                }
            });
        }

        private void updateUI() {
            mOption.setText(mData.getName());
            if (selectedVal != null && selectedVal.equals(mData.getValue())) {
                mOption.setChecked(true);
            } else {
                mOption.setChecked(false);
            }
            if (mData.getDescription() == null || mData.getDescription().isEmpty()) {
                mDescription.setVisibility(View.GONE);
            } else {
                mDescription.setVisibility(View.VISIBLE);
                mDescription.setText(mData.getDescription());
            }
        }

        public void setData(FarmSettingDetailData data) {
            mData = data;
            updateUI();
        }
    }


    public FarmSettingOneChooseItemAdapter(Activity activity, List<FarmSettingDetailData> data, String selectedVal) {
        if (data == null) {
            this.options = new ArrayList<>();
        } else {
            this.options = data;
        }
        this.selectedVal = selectedVal;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_farm_setting_one_choose_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        FarmSettingDetailData settingItem = options.get(position);
        holder.setData(settingItem);
    }

    @Override
    public int getItemCount() {
        return options.size();
    }

    public String getSelectedVal() {
        return selectedVal;
    }
}
