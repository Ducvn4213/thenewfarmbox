package mobile.agrhub.farmbox.adapters.order;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.FarmCart;

/**
 * Created by lecong on 3/27/18.
 */

public class OrderCartAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater inflater;
    private List<FarmCart> carts;
    ImageLoader imageLoader = ImageLoader.getInstance();

    // Constructor
    public OrderCartAdapter(Context context, List<FarmCart> carts) {
        this.mContext = context;
        this.inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.carts = carts;
    }

    @Override
    public int getCount() {
        return carts.size();
    }

    @Override
    public Object getItem(int position) {
        return this.carts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = inflater.inflate(R.layout.layout_farm_order_cart_item, null);

        CircleImageView productAvatar = convertView.findViewById(R.id.ci_product_avatar);
        TextView productNameView = convertView.findViewById(R.id.tv_product_name);
        TextView productWeightView = convertView.findViewById(R.id.tv_product_weight);
        TextView productPriceView = convertView.findViewById(R.id.tv_product_price);

        FarmCart cart = (FarmCart) getItem(position);
        if (cart.cartProduct != null) {
            String avatar = cart.cartProduct.getDisplayAvatar();
            if (avatar == null || avatar.isEmpty()) {
                productAvatar.setImageResource(R.drawable.ic_photo_gray_24dp);
            } else {
                if (imageLoader == null) {
                    imageLoader = ImageLoader.getInstance();
                }
                imageLoader.displayImage(avatar, productAvatar);
            }

            productNameView.setText(cart.cartProduct.productName);
            productWeightView.setText(cart.cartProduct.displayWeight(mContext, cart.cartItemQuantity));
            productPriceView.setText(cart.cartProduct.displayPrice());
        }

        return convertView;
    }

    public void update(List<FarmCart> carts) {
        this.carts = carts;
        notifyDataSetChanged();
    }

    public int getTotalItem() {
        return carts.size();
    }

    public int getTotalPrice() {
        int total = 0;
        if (carts != null && carts.size() > 0) {
            for (FarmCart item : carts) {
                if (item.cartProduct != null) {
                    total += (item.cartItemQuantity * item.cartProduct.price());
                }
            }
        }
        return total;
    }
}
