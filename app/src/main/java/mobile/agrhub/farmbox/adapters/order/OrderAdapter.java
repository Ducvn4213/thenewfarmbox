package mobile.agrhub.farmbox.adapters.order;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.custom_view.layout.ExpandableHeightListView;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.model.FarmOrder;
import mobile.agrhub.farmbox.service.model.User;
import mobile.agrhub.farmbox.utils.Utils;

/**
 * Created by lecong on 3/27/18.
 */

public class OrderAdapter extends BaseAdapter implements View.OnClickListener {

    private Context mContext;
    private LayoutInflater inflater;
    private List<FarmOrder> orders;
    private OnClickListener listener;
    User user = FarmBoxAppController.getInstance().getUser();
    ImageLoader imageLoader = ImageLoader.getInstance();

    // Constructor
    public OrderAdapter(Context context, List<FarmOrder> orders, OnClickListener listener) {
        this.mContext = context;
        this.inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.orders = orders;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return orders.size();
    }

    @Override
    public Object getItem(int position) {
        return this.orders.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = inflater.inflate(R.layout.layout_farm_order_item, null);

        FarmOrder order = (FarmOrder) getItem(position);

        CardView container = convertView.findViewById(R.id.cv_container);
        container.setTag(String.valueOf(order.orderId));
        container.setOnClickListener(this);

        CircleImageView customerAvatar = convertView.findViewById(R.id.ci_customer_avatar);
        TextView customerNameView = convertView.findViewById(R.id.tv_customer_name);
        TextView customerStatusView = convertView.findViewById(R.id.tv_customer_status);
        TextView orderCodeView = convertView.findViewById(R.id.tv_order_code);
        TextView orderStatusView = convertView.findViewById(R.id.tv_order_status);
        ExpandableHeightListView cartView = convertView.findViewById(R.id.lv_cart);
        TextView totalItemView = convertView.findViewById(R.id.tv_product_total_item);
        TextView totalPriceView = convertView.findViewById(R.id.tv_product_total_price);
        LinearLayout controlView = convertView.findViewById(R.id.ll_control_view);
        LinearLayout cancelOrderView = convertView.findViewById(R.id.ll_cancel_order_view);
        Button cancelOrderBtn = convertView.findViewById(R.id.btn_cancel_order);
        LinearLayout returnOrderView = convertView.findViewById(R.id.ll_return_order_view);
        Button returnOrderBtn = convertView.findViewById(R.id.btn_return_order);
        LinearLayout acceptOrderView = convertView.findViewById(R.id.ll_accept_order_view);
        Button acceptOrderBtn = convertView.findViewById(R.id.btn_accept_order);
        LinearLayout assignCodeView = convertView.findViewById(R.id.ll_assign_traceability_code_view);
        Button assignCodeBtn = convertView.findViewById(R.id.btn_assign_traceability_code);
        LinearLayout deliveryCodeView = convertView.findViewById(R.id.ll_delivery_order_view);
        Button deliveryCodeBtn = convertView.findViewById(R.id.btn_delivery_order);
        LinearLayout finishCodeView = convertView.findViewById(R.id.ll_finish_order_view);
        Button finishCodeBtn = convertView.findViewById(R.id.btn_finish_order);


        if (user == null || user.user_avatar == null || user.user_avatar.isEmpty()) {
            customerAvatar.setImageResource(R.drawable.ic_no_avatar_gray_360dp);
            customerNameView.setText("");
        } else {
            if (imageLoader == null) {
                imageLoader = ImageLoader.getInstance();
            }
            imageLoader.displayImage(user.user_avatar, customerAvatar);

            customerNameView.setText(user.getDisplayName());
        }

        customerStatusView.setText(Utils.getDate(order.orderUpdatedDate, "yyyy/MM/dd HH:mm"));

        orderCodeView.setText(order.displayOrderNumber());
        orderStatusView.setText(mContext.getString(R.string.status) + ": " + order.displayStatus(mContext));

        OrderCartAdapter adapter = new OrderCartAdapter(mContext, order.carts);
        cartView.setExpanded(true);
        cartView.setAdapter(adapter);

        totalItemView.setText(order.displayTotalItem());
        totalPriceView.setText(order.displaySubTotalPrice());

        if (order.canControl()) {
            controlView.setVisibility(View.VISIBLE);
            switch (order.orderState) {
                case FarmOrder.STATE_NEW:
                    cancelOrderView.setVisibility(View.VISIBLE);
                    returnOrderView.setVisibility(View.GONE);
                    acceptOrderView.setVisibility(View.VISIBLE);
                    assignCodeView.setVisibility(View.VISIBLE);
                    deliveryCodeView.setVisibility(View.GONE);
                    finishCodeView.setVisibility(View.GONE);
                    break;
                case FarmOrder.STATE_PROCESS:
                    cancelOrderView.setVisibility(View.VISIBLE);
                    returnOrderView.setVisibility(View.GONE);
                    acceptOrderView.setVisibility(View.GONE);
                    assignCodeView.setVisibility(View.VISIBLE);
                    deliveryCodeView.setVisibility(View.VISIBLE);
                    finishCodeView.setVisibility(View.GONE);
                    break;
                case FarmOrder.STATE_DELIVERY:
                    cancelOrderView.setVisibility(View.GONE);
                    returnOrderView.setVisibility(View.VISIBLE);
                    acceptOrderView.setVisibility(View.GONE);
                    assignCodeView.setVisibility(View.GONE);
                    deliveryCodeView.setVisibility(View.GONE);
                    finishCodeView.setVisibility(View.VISIBLE);
                    break;
                case FarmOrder.STATE_RETURN:
                    cancelOrderView.setVisibility(View.VISIBLE);
                    returnOrderView.setVisibility(View.GONE);
                    acceptOrderView.setVisibility(View.GONE);
                    assignCodeView.setVisibility(View.GONE);
                    deliveryCodeView.setVisibility(View.VISIBLE);
                    finishCodeView.setVisibility(View.VISIBLE);
                    break;
            }
        } else {
            controlView.setVisibility(View.GONE);
        }
        cancelOrderBtn.setTag(String.valueOf(order.orderId));
        cancelOrderBtn.setOnClickListener(this);

        returnOrderBtn.setTag(String.valueOf(order.orderId));
        returnOrderBtn.setOnClickListener(this);

        acceptOrderBtn.setTag(String.valueOf(order.orderId));
        acceptOrderBtn.setOnClickListener(this);

        assignCodeBtn.setTag(String.valueOf(order.orderId));
        assignCodeBtn.setOnClickListener(this);

        deliveryCodeBtn.setTag(String.valueOf(order.orderId));
        deliveryCodeBtn.setOnClickListener(this);

        finishCodeBtn.setTag(String.valueOf(order.orderId));
        finishCodeBtn.setOnClickListener(this);

        return convertView;
    }

    public void update(List<FarmOrder> orders) {
        this.orders = orders;
        notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        Object tagObj = v.getTag();
        if (tagObj != null && tagObj instanceof String) {
            long orderId = 0;
            try {
                orderId = Utils.str2Long((String) tagObj);
            } catch (Exception e) {
                e.printStackTrace();
            }

            FarmOrder order = null;
            if (orders != null && orders.size() > 0) {
                for (FarmOrder item : orders) {
                    if (item.orderId == orderId) {
                        order = item;
                        break;
                    }
                }
            }

            if (order != null) {
                switch (v.getId()) {
                    case R.id.btn_cancel_order:
                        listener.onCancelOrderClicked(order);
                        break;
                    case R.id.btn_return_order:
                        listener.onReturnOrderClicked(order);
                        break;
                    case R.id.btn_accept_order:
                        listener.onAcceptOrderClicked(order);
                        break;
                    case R.id.btn_assign_traceability_code:
                        listener.onAssignTraceabilityCodeClicked(order);
                        break;
                    case R.id.btn_delivery_order:
                        listener.onDeliveryOrderClicked(order);
                        break;
                    case R.id.btn_finish_order:
                        listener.onFinishOrderClicked(order);
                        break;
                    default:
                        listener.onSelectOrder(order);
                        break;
                }
            }
        }
    }

    private int getAmount(long id) {
        int amount = 0;
//        if (orders != null && orders.size() > 0) {
//            for (FarmCart item : carts) {
//                if (id == item.cartProduct.productId) {
//                    amount = item.cartItemQuantity;
//                    break;
//                }
//            }
//        }
        return amount;
    }

    public void addToCart(long id, int amount, int type) {
//        if (carts != null && carts.size() > 0) {
//            FarmCart item;
//            for (int i = 0; i < carts.size(); i++) {
//                item = carts.get(i);
//                if (id == item.cartProduct.productId) {
//                    if (type == 0) {
//                        item.cartItemQuantity = amount;
//                    } else {
//                        item.cartItemQuantity += (amount * type);
//                        if (item.cartItemQuantity < 0) {
//                            item.cartItemQuantity = 0;
//                        }
//                    }
//
//                    break;
//                }
//            }
//        }
    }

    public int totalItem() {
        int totalItem = 0;
//        if (carts != null && carts.size() > 0) {
//            for (FarmCart item : carts) {
//                totalItem += item.cartItemQuantity;
//            }
//        }
        return totalItem;
    }

    public int totalPrice() {
        int total = 0;

//        if (carts != null && carts.size() > 0) {
//            int price = 0;
//            for (FarmCart item : carts) {
//                if (item.cartProduct != null) {
//                    price = 0;
//                    try {
//                        price = Utils.str2Int(item.cartProduct.productPrice);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    total += item.cartItemQuantity * price;
//                }
//            }
//        }

        return total;
    }

    public interface OnClickListener {
        void onSelectOrder(FarmOrder order);
        void onCancelOrderClicked(FarmOrder order);
        void onReturnOrderClicked(FarmOrder order);
        void onAcceptOrderClicked(FarmOrder order);
        void onAssignTraceabilityCodeClicked(FarmOrder order);
        void onDeliveryOrderClicked(FarmOrder order);
        void onFinishOrderClicked(FarmOrder order);
    }
}
