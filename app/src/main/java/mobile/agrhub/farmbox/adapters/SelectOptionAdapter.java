package mobile.agrhub.farmbox.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.OptionItem;

public class SelectOptionAdapter extends RecyclerView.Adapter<SelectOptionAdapter.ViewHolder> {
    private List<OptionItem> options;
    private Activity mActivity;
    private SelectOptionListener listener;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View view) {
            super(view);

            bindingControls(view);
            setupControlEvents();
        }

        private OptionItem mData;
        private LinearLayout mContent;
        private TextView mTitle;
        private ImageView mChecked;

        private void bindingControls(View v) {
            mContent = (LinearLayout) v.findViewById(R.id.ll_content_view);
            mTitle = (TextView) v.findViewById(R.id.tv_title);
            mChecked = (ImageView) v.findViewById(R.id.iv_checked);
        }

        private void setupControlEvents() {
            mContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!mData.isChecked()) {
                        for (OptionItem item : SelectOptionAdapter.this.options) {
                            item.setChecked(false);
                        }

                        mData.setChecked(true);
                    }
                    SelectOptionAdapter.this.notifyDataSetChanged();
                    listener.onClick(mData);
                }
            });
        }

        private void updateUI() {
            mTitle.setText(mData.getValue());
            if (mData.isChecked()) {
                mChecked.setVisibility(View.VISIBLE);
            } else {
                mChecked.setVisibility(View.INVISIBLE);
            }
        }

        public void setData(OptionItem data) {
            mData = data;
            updateUI();
        }
    }

    public interface SelectOptionListener {
        void onClick(OptionItem item);
    }


    public SelectOptionAdapter(Activity activity, List<OptionItem> data, SelectOptionListener listener) {
        this.mActivity = activity;
        this.listener = listener;
        this.options = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_select_option_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        OptionItem item = options.get(position);
        holder.setData(item);
    }

    @Override
    public int getItemCount() {
        return options.size();
    }
}
