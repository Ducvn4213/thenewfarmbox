package mobile.agrhub.farmbox.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.Device;

public class SettingDeviceAdapter extends RecyclerView.Adapter<SettingDeviceAdapter.ViewHolder> {
    private Context mContext;
    private List<Device> mDeviceList;
    private CallBackListener mListener;

    public class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout mContentView;
        private TextView mName;
        private TextView mDeviceAddress;
        private Switch mToggle;

        public ViewHolder(View view) {
            super(view);

            bindingControls(view);
        }

        private void bindingControls(View v) {
            mContentView = (LinearLayout) v.findViewById(R.id.ll_content_view);
            mName = (TextView) v.findViewById(R.id.tv_name);
            mDeviceAddress = (TextView) v.findViewById(R.id.tv_device_address);
            mToggle = (Switch) v.findViewById(R.id.sw_toggle);
        }
    }


    public SettingDeviceAdapter(Context context) {
        this.mContext = context;
        this.mDeviceList = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_setting_farm_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Device device = mDeviceList.get(position);
        holder.mName.setText(device.getDisplayName());
        holder.mDeviceAddress.setText(device.device_mac_address);

        holder.mToggle.setChecked(device.is_active);
        holder.mToggle.setTag(device.is_active ? "1" : "0");
        holder.mToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String tag = (String)buttonView.getTag();
                boolean oldVal = tag != null && "1".equals(tag);
                if (oldVal != isChecked) {
                    if (null != mListener) {
                        mListener.onChanged(String.valueOf(device.device_id), isChecked);
                    }
                }
            }
        });
        holder.mContentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onSelected(device);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDeviceList.size();
    }

    public void setData(List<Device> deviceList) {
        this.mDeviceList = deviceList;
        notifyDataSetChanged();
    }

    public void setListener(CallBackListener listener) {
        mListener = listener;
    }

    public interface CallBackListener {
        void onChanged(String deviceId, boolean val);
        void onSelected(Device device);
    }
}
