package mobile.agrhub.farmbox.adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.volokh.danylo.hashtaghelper.HashTagHelper;

import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.GapAPI;
import mobile.agrhub.farmbox.service.model.Diary;
import mobile.agrhub.farmbox.service.model.User;
import mobile.agrhub.farmbox.utils.Utils;

public class DiaryListAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater;
    private List<Diary> feedItems;
    private User user;
    private String updateDateFormat;
    private Gson gson;
    ImageLoader imageLoader = ImageLoader.getInstance();
    private GapAPI gapAPI = APIHelper.getGapAPI();
    private FarmBoxAppController app = FarmBoxAppController.getInstance();

    public DiaryListAdapter(Context context, List<Diary> feedItems) {
        this.context = context;
        this.feedItems = feedItems;
        try {
            this.user = FarmBoxAppController.getInstance().getUser();
        } catch (Exception ex) {
        }
        this.gson = new Gson();

        updateDateFormat = context.getString(R.string.diary_item_update_format);
    }

    @Override
    public int getCount() {
        return feedItems.size();
    }

    @Override
    public Object getItem(int location) {
        return feedItems.get(location);
    }

    public void setData(List<Diary> data) {
        this.feedItems = data;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.layout_diary_item, null);

        if (imageLoader == null)
            imageLoader = ImageLoader.getInstance();

        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView timestamp = (TextView) convertView
                .findViewById(R.id.timestamp);
        TextView statusMsg = (TextView) convertView.findViewById(R.id.txtStatusMsg);
        ImageView profilePic = (ImageView) convertView
                .findViewById(R.id.profilePic);
        AppCompatImageView feedImageView1 = (AppCompatImageView) convertView
                .findViewById(R.id.feedImage1);

        LinearLayout feedImage2Layout = (LinearLayout) convertView.findViewById(R.id.feedImage2Layout);
        ImageView feedImageView21 = (ImageView) convertView.findViewById(R.id.feedImage21);
        ImageView feedImageView22 = (ImageView) convertView.findViewById(R.id.feedImage22);

        LinearLayout feedImage3Layout = (LinearLayout) convertView.findViewById(R.id.feedImage3Layout);
        ImageView feedImageView31 = (ImageView) convertView.findViewById(R.id.feedImage31);
        ImageView feedImageView32 = (ImageView) convertView.findViewById(R.id.feedImage32);
        ImageView feedImageView33 = (ImageView) convertView.findViewById(R.id.feedImage33);

        LinearLayout feedImage4Layout = (LinearLayout) convertView.findViewById(R.id.feedImage4Layout);
        ImageView feedImageView41 = (ImageView) convertView.findViewById(R.id.feedImage41);
        ImageView feedImageView42 = (ImageView) convertView.findViewById(R.id.feedImage42);
        ImageView feedImageView43 = (ImageView) convertView.findViewById(R.id.feedImage43);
        ImageView feedImageView44 = (ImageView) convertView.findViewById(R.id.feedImage44);

        LinearLayout feedImage5Layout = (LinearLayout) convertView.findViewById(R.id.feedImage5Layout);
        ImageView feedImageView51 = (ImageView) convertView.findViewById(R.id.feedImage51);
        ImageView feedImageView52 = (ImageView) convertView.findViewById(R.id.feedImage52);
        ImageView feedImageView53 = (ImageView) convertView.findViewById(R.id.feedImage53);
        ImageView feedImageView54 = (ImageView) convertView.findViewById(R.id.feedImage54);
        ImageView feedImageView55 = (ImageView) convertView.findViewById(R.id.feedImage55);
        TextView overImageNumber = (TextView) convertView.findViewById(R.id.tv_over_image_number);

        Diary item = feedItems.get(position);

        String fullName = "";
        if (null != user) {
            fullName = user.user_first_name + " " + user.user_last_name;
        }
        String activities = gapAPI.getGapTasksName(item.gapDiaryTasks);
        String farmName = app.getFarmName(item.farmId);
        String title1 = context.getString(R.string.diary_item_name, fullName, activities, farmName);
        name.setText(Html.fromHtml(title1));

        if (item.gapDiaryDate > 0) {
            timestamp.setText(Utils.getDate(item.gapDiaryDate, updateDateFormat));
        } else {
            timestamp.setText(Utils.getDate(item.gapDiaryUpdatedDate, updateDateFormat));
        }

        // Check for empty status message
        if (!TextUtils.isEmpty(item.gapDiaryDescription)) {
            statusMsg.setVisibility(View.VISIBLE);
            statusMsg.setText(item.gapDiaryDescription);

            HashTagHelper mTextHashTagHelper = HashTagHelper.Creator.create(context.getResources().getColor(R.color.colorPrimary), null);
            mTextHashTagHelper.handle(statusMsg);
        } else {
            // status is empty, remove from view
            statusMsg.setVisibility(View.GONE);
        }

        // user profile pic
        if (null != this.user && null != this.user.user_avatar && !this.user.user_avatar.isEmpty()) {
            imageLoader.displayImage(this.user.user_avatar, profilePic);
        } else {
            profilePic.setImageResource(R.drawable.ic_no_avatar_gray_360dp);
        }

        // Feed image
        List<String> imageList = gson.fromJson(item.gapDiaryMedias, new TypeToken<List<String>>() {
        }.getType());
        if (imageList != null && imageList.size() > 0) {
            if (1 == imageList.size()) {
                feedImage2Layout.setVisibility(View.GONE);
                feedImage3Layout.setVisibility(View.GONE);
                feedImage4Layout.setVisibility(View.GONE);
                feedImage5Layout.setVisibility(View.GONE);

                feedImageView1.setVisibility(View.VISIBLE);
                imageLoader.displayImage(imageList.get(0), feedImageView1);
            } else if (2 == imageList.size()) {
                feedImageView1.setVisibility(View.GONE);
                feedImage3Layout.setVisibility(View.GONE);
                feedImage4Layout.setVisibility(View.GONE);
                feedImage5Layout.setVisibility(View.GONE);

                feedImage2Layout.setVisibility(View.VISIBLE);
                imageLoader.displayImage(imageList.get(0), feedImageView21);
                imageLoader.displayImage(imageList.get(1), feedImageView22);
            } else if (3 == imageList.size()) {
                feedImageView1.setVisibility(View.GONE);
                feedImage2Layout.setVisibility(View.GONE);
                feedImage4Layout.setVisibility(View.GONE);
                feedImage5Layout.setVisibility(View.GONE);

                feedImage3Layout.setVisibility(View.VISIBLE);
                imageLoader.displayImage(imageList.get(0), feedImageView31);
                imageLoader.displayImage(imageList.get(1), feedImageView32);
                imageLoader.displayImage(imageList.get(2), feedImageView33);
            } else if (4 == imageList.size()) {
                feedImageView1.setVisibility(View.GONE);
                feedImage2Layout.setVisibility(View.GONE);
                feedImage3Layout.setVisibility(View.GONE);
                feedImage5Layout.setVisibility(View.GONE);

                feedImage4Layout.setVisibility(View.VISIBLE);
                imageLoader.displayImage(imageList.get(0), feedImageView41);
                imageLoader.displayImage(imageList.get(1), feedImageView42);
                imageLoader.displayImage(imageList.get(2), feedImageView43);
                imageLoader.displayImage(imageList.get(3), feedImageView44);
            } else {
                feedImageView1.setVisibility(View.GONE);
                feedImage2Layout.setVisibility(View.GONE);
                feedImage3Layout.setVisibility(View.GONE);
                feedImage4Layout.setVisibility(View.GONE);

                feedImage5Layout.setVisibility(View.VISIBLE);
                imageLoader.displayImage(imageList.get(0), feedImageView51);
                imageLoader.displayImage(imageList.get(1), feedImageView52);
                imageLoader.displayImage(imageList.get(2), feedImageView53);
                imageLoader.displayImage(imageList.get(3), feedImageView54);
                imageLoader.displayImage(imageList.get(4), feedImageView55);

                if (imageList.size() > 5) {
                    overImageNumber.setVisibility(View.VISIBLE);
                    overImageNumber.setText("+" + (imageList.size() - 5));
                } else {
                    overImageNumber.setVisibility(View.GONE);
                }
            }

        } else {
            feedImageView1.setVisibility(View.GONE);
            feedImage2Layout.setVisibility(View.GONE);
            feedImage3Layout.setVisibility(View.GONE);
            feedImage4Layout.setVisibility(View.GONE);
            feedImage5Layout.setVisibility(View.GONE);
        }

        return convertView;
    }
}
