package mobile.agrhub.farmbox.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import mobile.agrhub.farmbox.fragments.intro.IntroContentFourFragment;
import mobile.agrhub.farmbox.fragments.intro.IntroContentOneFragment;
import mobile.agrhub.farmbox.fragments.intro.IntroContentThreeFragment;
import mobile.agrhub.farmbox.fragments.intro.IntroContentTwoFragment;

public class IntroAdapter extends FragmentStatePagerAdapter {

    public IntroAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new IntroContentOneFragment();
            case 1:
                return new IntroContentTwoFragment();
            case 2:
                return new IntroContentThreeFragment();
            case 3:
                return new IntroContentFourFragment();
            default:
                break;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }
}

