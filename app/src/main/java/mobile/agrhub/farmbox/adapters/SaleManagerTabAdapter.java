package mobile.agrhub.farmbox.adapters;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.fragments.FarmProductFragment;
import mobile.agrhub.farmbox.fragments.OrderFragment;

public class SaleManagerTabAdapter extends FragmentStatePagerAdapter {
    private Activity activity;
    private OrderFragment orderFragment;
    private FarmProductFragment productFragment;


    public SaleManagerTabAdapter(FragmentManager fm) {
        super(fm);
    }

    public SaleManagerTabAdapter(FragmentManager fm, Activity activity) {
        super(fm);
        this.activity = activity;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 1:
                if (productFragment == null) {
                    productFragment = new FarmProductFragment();
                }
                return productFragment;
            default:
                if (orderFragment == null) {
                    orderFragment = new OrderFragment();
                }
                return orderFragment;
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 1:
                return activity.getString(R.string.product_tab_title);
            default:
                return activity.getString(R.string.order_tab_title);
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
