package mobile.agrhub.farmbox.adapters;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ss.com.bannerslider.adapters.SliderAdapter;
import ss.com.bannerslider.viewholder.ImageSlideViewHolder;

/**
 * Created by lecong on 3/27/18.
 */

public class ProductImageAdapter extends SliderAdapter {
    private List<String> imageList;
    ImageLoader imageLoader = ImageLoader.getInstance();

    public ProductImageAdapter(String productImageJson) {
        if (productImageJson == null || productImageJson.isEmpty()) {
            imageList = new ArrayList<String>();
        } else {
            Gson gson = new Gson();
            imageList = gson.fromJson(productImageJson, new TypeToken<List<String>>() {
            }.getType());
        }
    }

    public ProductImageAdapter(String[] images) {
        if (images == null || images.length <= 0) {
            imageList = new ArrayList<String>();
        } else {
            imageList = Arrays.asList(images);
        }
    }

    @Override
    public int getItemCount() {
        return imageList.size();
    }

    @Override
    public void onBindImageSlide(int position, ImageSlideViewHolder viewHolder) {
        String imageUrl = imageList.get(position);
        if (imageLoader == null) {
            imageLoader = ImageLoader.getInstance();
        }

        imageLoader.displayImage(imageUrl, viewHolder.imageView);
    }
}
