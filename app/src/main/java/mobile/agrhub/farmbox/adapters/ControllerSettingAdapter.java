package mobile.agrhub.farmbox.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.Controller;

public class ControllerSettingAdapter extends RecyclerView.Adapter<ControllerSettingAdapter.ViewHolder> {
    private List<Controller> controllers;
    private Activity mActivity;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View view) {
            super(view);

            bindingControls(view);
            setupControlEvents();
        }

        private Controller mData;
        private TextView mTitle;
        private Switch mActive;

        private void bindingControls(View v) {
            mTitle = (TextView) v.findViewById(R.id.setting_title);
            mActive = (Switch) v.findViewById(R.id.sw_active);
        }

        private void setupControlEvents() {
            mActive.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    mData.isActive = b;
                }
            });
        }

        private void updateUI(int position) {
            mTitle.setText(mActivity.getString(R.string.channel_number, position + 1));
            mActive.setChecked(mData.isActive);
        }

        public void setData(Controller data, int position) {
            mData = data;
            updateUI(position);
        }
    }


    public ControllerSettingAdapter(Activity activity, List<Controller> data) {
        this.mActivity = activity;
        setData(data);
    }

    public void setData(List<Controller> data) {
        if (data == null) {
            this.controllers = new ArrayList<>();
        } else {
            this.controllers = data;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_controller_setting_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Controller item = controllers.get(position);
        holder.setData(item, position);
    }

    @Override
    public int getItemCount() {
        return controllers.size();
    }
}
