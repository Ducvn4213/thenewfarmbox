package mobile.agrhub.farmbox.adapters;

import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codewaves.stickyheadergrid.StickyHeaderGridAdapter;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;
import java.util.Map;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.Product;
import mobile.agrhub.farmbox.service.model.ProductCategory;
import mobile.agrhub.farmbox.utils.Utils;

/**
 * Created by lecong on 3/27/18.
 */

public class ProductAdapter extends StickyHeaderGridAdapter implements View.OnClickListener {

    private List<ProductCategory> categoryList;
    private Map<String, List<Product>> productMap;
    private OnProductClickListener listener;
    ImageLoader imageLoader = ImageLoader.getInstance();

    public ProductAdapter(List<ProductCategory> categoryList, Map<String, List<Product>> productMap, OnProductClickListener listener) {
        this.categoryList = categoryList;
        this.productMap = productMap;
        this.listener = listener;
    }

    public void setData(List<ProductCategory> categoryList, Map<String, List<Product>> productMap) {
        this.categoryList = categoryList;
        this.productMap = productMap;
    }

    @Override
    public int getSectionCount() {
        return categoryList.size();
    }

    @Override
    public int getSectionItemCount(int section) {
        if (section < categoryList.size()) {
            ProductCategory category = categoryList.get(section);
            String categoryId = String.valueOf(category.categoryId);
            if (productMap.containsKey(categoryId)) {
                List<Product> productList = productMap.get(categoryId);
                return productList.size();
            }
        }

        return 0;
    }

    @Override
    public HeaderViewHolder onCreateHeaderViewHolder(ViewGroup parent, int headerType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_product_header, parent, false);
        return new MyHeaderViewHolder(view);
    }

    @Override
    public ItemViewHolder onCreateItemViewHolder(ViewGroup parent, int itemType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_product_item, parent, false);
        return new MyItemViewHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(HeaderViewHolder viewHolder, int section) {
        final MyHeaderViewHolder holder = (MyHeaderViewHolder) viewHolder;
        if (section < categoryList.size()) {
            ProductCategory category = categoryList.get(section);
            holder.labelView.setText(Utils.getValueFromMultiLanguageString(category.categoryName));
        } else {
            holder.labelView.setText("");
        }
    }

    @Override
    public void onBindItemViewHolder(ItemViewHolder viewHolder, final int section, final int position) {
        final MyItemViewHolder holder = (MyItemViewHolder) viewHolder;
        holder.labelView.setText("");

        if (section < categoryList.size()) {
            ProductCategory category = categoryList.get(section);
            String categoryId = String.valueOf(category.categoryId);
            if (productMap.containsKey(categoryId)) {
                List<Product> productList = productMap.get(categoryId);
                if (position < productList.size()) {
                    Product product = productList.get(position);
                    holder.labelView.setText(Utils.getValueFromMultiLanguageString(product.productName));

                    long discount = Utils.getLongFromMultiLanguageString(product.productDiscount);

                    long originPrice = Utils.getLongFromMultiLanguageString(product.productPrice);
                    if (discount > 0) {
                        holder.originPriceContainerView.setVisibility(View.VISIBLE);
                        holder.originPriceView.setText(Utils.formatPrice(originPrice));
                        holder.originPriceView.setPaintFlags(holder.originPriceView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                        holder.discountPercentView.setText("↓" + discount + "%");

                        long currentPrice = originPrice - Math.round((originPrice * discount) / 100.0);
                        holder.discountPriceView.setText(Utils.formatPrice(currentPrice));
                    } else {
                        holder.originPriceContainerView.setVisibility(View.GONE);

                        holder.discountPriceView.setText(Utils.formatPrice(originPrice));
                    }

                    if (product.productImageList.size() == 0) {
                        holder.imageView.setVisibility(View.GONE);
                    } else {
                        if (imageLoader == null) {
                            imageLoader = ImageLoader.getInstance();
                        }

                        holder.imageView.setVisibility(View.VISIBLE);
                        imageLoader.displayImage(product.productImageList.get(0), holder.imageView);
                    }

                    holder.productLayout.setTag(product);
                    holder.productLayout.setOnClickListener(this);

                    holder.addToCartButton.setTag(product);
                    holder.addToCartButton.setOnClickListener(this);
                }
            }
        }
    }

    @Override
    public void onClick(View view) {
        Object obj = view.getTag();
        if (obj != null && obj instanceof Product) {
            Product product = (Product) obj;

            if (view instanceof LinearLayout) {
                listener.onSelectProduct(product);
            } else if (view instanceof ImageButton) {
                listener.addToCartProduct(product);
            }
        }
    }

    public interface OnProductClickListener {
        void onSelectProduct(Product product);

        void addToCartProduct(Product product);
    }

    public static class MyHeaderViewHolder extends HeaderViewHolder {
        TextView labelView;

        MyHeaderViewHolder(View itemView) {
            super(itemView);
            labelView = (TextView) itemView.findViewById(R.id.label);
        }
    }

    public static class MyItemViewHolder extends ItemViewHolder {
        LinearLayout productLayout;
        TextView labelView;
        ImageView imageView;
        LinearLayout originPriceContainerView;
        TextView originPriceView;
        TextView discountPercentView;
        TextView discountPriceView;
        ImageButton addToCartButton;

        MyItemViewHolder(View itemView) {
            super(itemView);
            productLayout = (LinearLayout) itemView.findViewById(R.id.ll_product_layout);
            labelView = (TextView) itemView.findViewById(R.id.tv_product_name);
            imageView = (ImageView) itemView.findViewById(R.id.iv_product_image);
            originPriceContainerView = (LinearLayout) itemView.findViewById(R.id.ll_origin_price);
            originPriceView = (TextView) itemView.findViewById(R.id.tv_origin_price);
            discountPercentView = (TextView) itemView.findViewById(R.id.tv_discount_percent);
            discountPriceView = (TextView) itemView.findViewById(R.id.tv_discount_price);
            addToCartButton = (ImageButton) itemView.findViewById(R.id.ib_add_to_cart);
        }
    }
}
