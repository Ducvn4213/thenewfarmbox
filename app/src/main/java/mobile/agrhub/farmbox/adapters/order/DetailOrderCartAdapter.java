package mobile.agrhub.farmbox.adapters.order;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.FarmCart;
import mobile.agrhub.farmbox.utils.Utils;

/**
 * Created by lecong on 3/27/18.
 */

public class DetailOrderCartAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater inflater;
    private List<FarmCart> carts;
    ImageLoader imageLoader = ImageLoader.getInstance();

    // Constructor
    public DetailOrderCartAdapter(Context context, List<FarmCart> carts) {
        this.mContext = context;
        this.inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.carts = carts;
    }

    @Override
    public int getCount() {
        return carts.size();
    }

    @Override
    public Object getItem(int position) {
        return this.carts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = inflater.inflate(R.layout.layout_farm_order_detail_cart_item, null);

        TextView numberView = convertView.findViewById(R.id.tv_number);
        CircleImageView productAvatar = convertView.findViewById(R.id.ci_product_avatar);
        TextView productNameView = convertView.findViewById(R.id.tv_product_name);
        TextView productPriceView = convertView.findViewById(R.id.tv_product_price);
        TextView productQuantityView = convertView.findViewById(R.id.tv_product_quantity);
        TextView productTotalView = convertView.findViewById(R.id.tv_product_total);

        numberView.setText(String.format("%02d", position + 1));
        FarmCart cart = (FarmCart) getItem(position);
        if (cart.cartProduct != null) {
            String avatar = cart.cartProduct.getDisplayAvatar();
            if (avatar == null || avatar.isEmpty()) {
                productAvatar.setImageResource(R.drawable.ic_photo_gray_24dp);
            } else {
                if (imageLoader == null) {
                    imageLoader = ImageLoader.getInstance();
                }
                imageLoader.displayImage(avatar, productAvatar);
            }

            productNameView.setText(cart.cartProduct.productName);
            productPriceView.setText(cart.cartProduct.displayPrice());
            productQuantityView.setText(Utils.i2s(cart.cartItemQuantity));
            productTotalView.setText(Utils.l2s(cart.cartProduct.price() * cart.cartItemQuantity));
        }

        return convertView;
    }

    public void update(List<FarmCart> carts) {
        this.carts = carts;
        notifyDataSetChanged();
    }

    public int getTotalItem() {
        int total = 0;
        if (carts != null && carts.size() > 0) {
            for (FarmCart item : carts) {
                total += item.cartItemQuantity;
            }
        }
        return total;
    }

    public int getTotalPrice() {
        int total = 0;
        if (carts != null && carts.size() > 0) {
            for (FarmCart item : carts) {
                if (item.cartProduct != null) {
                    total += (item.cartItemQuantity * item.cartProduct.price());
                }
            }
        }
        return total;
    }
}
