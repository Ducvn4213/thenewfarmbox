package mobile.agrhub.farmbox.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.model.FarmSettingDetailItem;
import mobile.agrhub.farmbox.service.model.UnitItem;

public class FarmSettingItemAdapter extends RecyclerView.Adapter<FarmSettingItemAdapter.ViewHolder> {
    private Activity mAtivity;
    private List<FarmSettingDetailItem> settings;
    private FarmSettingAdapter.SettingItemClickListener listener;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ViewHolder(View view) {
            super(view);

            bindingControls(view);
            setupControlEvents(view);
        }

        private FarmSettingDetailItem mData;
        private int mSettingPosition;
        private TextView mTitle;
        private TextView mValue;
        private Switch mSwitchValue;
        private ImageButton mCopy;
        private ImageView mDetailArrow;

        private void bindingControls(View v) {
            mTitle = (TextView) v.findViewById(R.id.tv_title);
            mValue = (TextView) v.findViewById(R.id.tv_value);
            mSwitchValue = (Switch) v.findViewById(R.id.sw_value);
            mCopy = (ImageButton) v.findViewById(R.id.ib_copy);
            mDetailArrow = (ImageView) v.findViewById(R.id.iv_detail_arrow);
        }

        private void setupControlEvents(View view) {
            view.setOnClickListener(this);

            mSwitchValue.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if ("true".equals(mData.getValue())) {
                        if (!b) {
                            listener.onChangeProfileSession(mData, b);
                        }
                    } else if ("false".equals(mData.getValue())) {
                        if (b) {
                            listener.onChangeProfileSession(mData, b);
                        }
                    }
                }
            });

            mCopy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onCopyProfileSession(mData);
                }
            });
        }

        private void updateUI() {
            if (UnitItem.TEMPERATURE_DOT_C_VALUE.equals(mData.getUnit())) {
                FarmBoxAppController app = FarmBoxAppController.getInstance();
                if (app.isTemperatureFUnit()) {
                    String title = mData.getName();
                    title = title.replace(mAtivity.getString(R.string.temperature_unit_c), mAtivity.getString(R.string.temperature_unit_f));
                    mTitle.setText(title);

                    long min = app.getTemperatureValue(mData.getMinVal(), UnitItem.TEMPERATURE_C_VALUE);
                    long max = app.getTemperatureValue(mData.getMaxVal(), UnitItem.TEMPERATURE_C_VALUE);

                    mValue.setText(min + " / " + max);
                    mValue.setVisibility(View.VISIBLE);
                    mSwitchValue.setVisibility(View.GONE);
                    mCopy.setVisibility(View.GONE);
                    mDetailArrow.setVisibility(View.VISIBLE);
                } else {
                    mTitle.setText(mData.getName());
                    mValue.setText(mData.getValue());
                    mValue.setVisibility(View.VISIBLE);
                    mSwitchValue.setVisibility(View.GONE);
                    mCopy.setVisibility(View.GONE);
                    mDetailArrow.setVisibility(View.VISIBLE);
                }
            } else if (UnitItem.NUTRITION_MILLISIEMENS_VALUE.equals(mData.getUnit())) {
                FarmBoxAppController app = FarmBoxAppController.getInstance();
                if (app.isNutritionPPMUnit()) {
                    String title = mData.getName();
                    title = title.replace(mAtivity.getString(R.string.nutrition_unit_ms_cm), mAtivity.getString(R.string.nutrition_unit_ppm));
                    mTitle.setText(title);

                    double min = app.getNutritionValue(mData.getMinDoubleVal(), UnitItem.NUTRITION_MILLISIEMENS_VALUE);
                    double max = app.getNutritionValue(mData.getMaxDoubleVal(), UnitItem.NUTRITION_MILLISIEMENS_VALUE);

                    mValue.setText(min + " / " + max);
                    mValue.setVisibility(View.VISIBLE);
                    mSwitchValue.setVisibility(View.GONE);
                    mCopy.setVisibility(View.GONE);
                    mDetailArrow.setVisibility(View.VISIBLE);
                } else {
                    mTitle.setText(mData.getName());
                    mValue.setText(mData.getValue());
                    mValue.setVisibility(View.VISIBLE);
                    mSwitchValue.setVisibility(View.GONE);
                    mCopy.setVisibility(View.GONE);
                    mDetailArrow.setVisibility(View.VISIBLE);
                }
            } else if ("session".equals(mData.getType())) {
                mTitle.setText(mData.getName());
                mTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 40);
                mValue.setVisibility(View.GONE);
                if (mSettingPosition == 0) {
                    mSwitchValue.setVisibility(View.GONE);
                } else {
                    mSwitchValue.setVisibility(View.VISIBLE);
                    if ("true".equals(mData.getValue())) {
                        if (!mSwitchValue.isChecked()) {
                            mSwitchValue.setChecked(true);
                        }
                    } else {
                        if (mSwitchValue.isChecked()) {
                            mSwitchValue.setChecked(false);
                        }
                    }
                }
                mCopy.setVisibility(View.VISIBLE);
                mDetailArrow.setVisibility(View.GONE);
            } else {
                mTitle.setText(mData.getName());
                mTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 11);
                mValue.setText(mData.getValue());
                mValue.setVisibility(View.VISIBLE);
                mSwitchValue.setVisibility(View.GONE);
                mCopy.setVisibility(View.GONE);
                mDetailArrow.setVisibility(View.VISIBLE);
            }
        }

        public void setData(FarmSettingDetailItem data, int position) {
            mData = data;
            mSettingPosition = position;
            updateUI();
        }


        @Override
        public void onClick(View v) {
            listener.onClick(mData, mSettingPosition);
        }
    }


    public FarmSettingItemAdapter(Activity activity, List<FarmSettingDetailItem> data, FarmSettingAdapter.SettingItemClickListener listener) {
        this.mAtivity = activity;
        if (data == null) {
            this.settings = new ArrayList<>();
        } else {
            this.settings = data;
        }
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_farm_setting_sub_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        FarmSettingDetailItem settingItem = settings.get(position);
        holder.setData(settingItem, position);
    }

    @Override
    public int getItemCount() {
        return settings.size();
    }

}
