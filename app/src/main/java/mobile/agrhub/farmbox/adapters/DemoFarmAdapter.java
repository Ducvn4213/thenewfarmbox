package mobile.agrhub.farmbox.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.activities.DemoFarmActivity;

/**
 * Created by lecong on 3/27/18.
 */

public class DemoFarmAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater inflater;
    private List<DemoFarmActivity.FarmInfo> mDataList;

    // Constructor
    public DemoFarmAdapter(Context context, List<DemoFarmActivity.FarmInfo> items) {
        this.mContext = context;
        this.mDataList = items;
        this.inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = inflater.inflate(R.layout.layout_demo_farm_item, null);

        TextView titleView = (TextView) convertView.findViewById(R.id.tv_title);
        TextView descriptionView = (TextView) convertView.findViewById(R.id.tv_description);

        DemoFarmActivity.FarmInfo farmInfo = mDataList.get(position);
        titleView.setText(farmInfo.getName());
        descriptionView.setText(farmInfo.getAddress());

        return convertView;
    }

}
