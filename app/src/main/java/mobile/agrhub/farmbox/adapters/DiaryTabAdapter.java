package mobile.agrhub.farmbox.adapters;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.fragments.CertificateFragment;
import mobile.agrhub.farmbox.fragments.FarmProductFragment;
import mobile.agrhub.farmbox.fragments.MaterialFragment;
import mobile.agrhub.farmbox.fragments.ProductivityFragment;

public class DiaryTabAdapter extends FragmentStatePagerAdapter {
    private Activity activity;
    private ProductivityFragment productivityFragment;
    private FarmProductFragment productFragment;
    private MaterialFragment materialFragment;
    private CertificateFragment certificateFragment;


    public DiaryTabAdapter(FragmentManager fm) {
        super(fm);
    }

    public DiaryTabAdapter(FragmentManager fm, Activity activity) {
        super(fm);
        this.activity = activity;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 1:
                if (certificateFragment == null) {
                    certificateFragment = new CertificateFragment();
                }
                return certificateFragment;
            default:
                if (productivityFragment == null) {
                    productivityFragment = new ProductivityFragment();
                }
                return productivityFragment;
        }
//        switch (position) {
//            case 1:
//                if (productFragment == null) {
//                    productFragment = new FarmProductFragment();
//                }
//                return productFragment;
//            case 2:
//                if (materialFragment == null) {
//                    materialFragment = new MaterialFragment();
//                }
//                return materialFragment;
//            case 3:
//                if (ceritificateFragment == null) {
//                    ceritificateFragment = new CeritificateFragment();
//                }
//                return ceritificateFragment;
//            default:
//                if (productivityFragment == null) {
//                    productivityFragment = new ProductivityFragment();
//                }
//                return productivityFragment;
//        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 1:
                return activity.getString(R.string.certificate_tab_title);
            default:
                return activity.getString(R.string.productivity_tab_title);
        }
//        switch (position) {
//            case 1:
//                return activity.getString(R.string.sale_tab_title);
//            case 2:
//                return activity.getString(R.string.material_tab_title);
//            case 3:
//                return activity.getString(R.string.certificate_tab_title);
//            default:
//                return activity.getString(R.string.productivity_tab_title);
//        }
    }

    @Override
    public int getCount() {
        return 2; //4;
    }

    public ProductivityFragment getProductivityFragment() {
        return productivityFragment;
    }
}
