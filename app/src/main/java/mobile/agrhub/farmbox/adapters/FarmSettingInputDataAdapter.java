package mobile.agrhub.farmbox.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.FarmSettingDetailData;
import mobile.agrhub.farmbox.service.model.FarmSettingDetailItem;

public class FarmSettingInputDataAdapter extends RecyclerView.Adapter<FarmSettingInputDataAdapter.ViewHolder> {
    private FarmSettingDetailItem mSettingItem;
    private List<FarmSettingDetailData> mNutritionList;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View view) {
            super(view);

            bindingControls(view);
            setupControlEvents(view);
        }

        private FarmSettingDetailItem mSettingItem;
        private FarmSettingDetailData mData;
        private TextView mTitle;
        private EditText mValue;
        private TextView mUnit;

        private void bindingControls(View v) {
            mTitle = (TextView) v.findViewById(R.id.tv_title);
            mValue = (EditText) v.findViewById(R.id.et_value);
            mUnit = (TextView) v.findViewById(R.id.tv_unit);
        }

        private void setupControlEvents(View view) {
        }

        private void updateUI() {
            mTitle.setText(mData.getName());
            mValue.setText(mData.getValue());
            mUnit.setText(mSettingItem.getUnit());
        }

        public void setData(FarmSettingDetailItem settingItem, FarmSettingDetailData data) {
            mSettingItem = settingItem;
            mData = data;
            updateUI();
        }

        public String getVal() {
            return mValue.getText().toString().trim();
        }
    }


    public FarmSettingInputDataAdapter(Activity activity, FarmSettingDetailItem data) {
        this.mSettingItem = data;
        if (data == null || data.getData() == null) {

            this.mNutritionList = new ArrayList<>();
        } else {
            this.mNutritionList = data.getData();
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_farm_setting_input_data_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        FarmSettingDetailData settingItem = mNutritionList.get(position);
        holder.setData(this.mSettingItem, settingItem);
    }

    @Override
    public int getItemCount() {
        return mNutritionList.size();
    }
}
