package mobile.agrhub.farmbox.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.FarmSettingDetailItem;
import mobile.agrhub.farmbox.service.model.FarmSettingItem;

public class FarmProfileSessionAdapter extends RecyclerView.Adapter<FarmProfileSessionAdapter.ViewHolder> {
    private List<FarmSettingItem> settings;
    private Activity mActivity;
    private SettingItemClickListener listener;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View view) {
            super(view);

            bindingControls(view);
            setupControlEvents();
        }

        private FarmSettingItem mData;
        private TextView mTitle;
        private ImageButton mButtonEdit;
        private RecyclerView mSettings;
        private FarmSettingItemAdapter mSettingItemAdapter;

        private void bindingControls(View v) {
            mTitle = (TextView) v.findViewById(R.id.setting_title);
            mButtonEdit = (ImageButton) v.findViewById(R.id.ib_edit);
            mSettings = (RecyclerView) v.findViewById(R.id.rv_settings);
        }

        private void setupControlEvents() {
        }

        private void updateUI() {
            mTitle.setText(mData.getName());

            boolean needShowAddBtn = false;
            List<FarmSettingDetailItem> settings = mData.getSettings();
            if (settings != null && settings.size() > 0) {
                for (FarmSettingDetailItem item : settings) {
                    if ("session".equals(item.getType())) {
                        needShowAddBtn = true;
                        break;
                    }
                }
            }
            if (needShowAddBtn) {
                mButtonEdit.setVisibility(View.VISIBLE);
                mButtonEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onProfileSessionEditPress(mData.getSettings());
                    }
                });
            } else {
                mButtonEdit.setVisibility(View.GONE);
            }
//            mSettingItemAdapter = new FarmSettingItemAdapter(mActivity, mData.getSettings(), listener);
//            mSettings.setAdapter(mSettingItemAdapter);
        }

        public void setData(FarmSettingItem data) {
            mData = data;
            updateUI();
        }
    }

    public interface SettingItemClickListener {
        void onProfileSessionEditPress(List<FarmSettingDetailItem> settings);
        void onClick(FarmSettingDetailItem setting);
    }


    public FarmProfileSessionAdapter(Activity activity, List<FarmSettingItem> data, SettingItemClickListener listener) {
        this.mActivity = activity;
        this.listener = listener;
        setData(data);
    }

    public void setData(List<FarmSettingItem> data) {
        if (data == null) {
            this.settings = new ArrayList<>();
        } else {
            List<FarmSettingItem> settingList = new ArrayList<>();
            for (FarmSettingItem item : data) {
                if (item.getSettings() != null && item.getSettings().size() > 0) {
                    settingList.add(item);
                }
            }
            this.settings = settingList;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_farm_setting_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        FarmSettingItem settingItem = settings.get(position);
        holder.setData(settingItem);
    }

    @Override
    public int getItemCount() {
        return settings.size();
    }
}
