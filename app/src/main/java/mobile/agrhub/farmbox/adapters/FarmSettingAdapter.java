package mobile.agrhub.farmbox.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.FarmSettingDetailItem;
import mobile.agrhub.farmbox.service.model.FarmSettingItem;

public class FarmSettingAdapter extends RecyclerView.Adapter<FarmSettingAdapter.ViewHolder> {
    private List<FarmSettingItem> settings;
    private Activity mActivity;
    private SettingItemClickListener listener;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View view) {
            super(view);

            bindingControls(view);
            setupControlEvents();
        }

        private FarmSettingItem mData;
        private TextView mTitle;
        private RecyclerView mSettings;
        private FarmSettingItemAdapter mSettingItemAdapter;

        private void bindingControls(View v) {
            mTitle = (TextView) v.findViewById(R.id.setting_title);
            mSettings = (RecyclerView) v.findViewById(R.id.rv_settings);
        }

        private void setupControlEvents() {
        }

        private void updateUI() {
            mTitle.setText(mData.getName());
            mSettingItemAdapter = new FarmSettingItemAdapter(mActivity, mData.getSettings(), listener);
            mSettings.setAdapter(mSettingItemAdapter);
        }

        public void setData(FarmSettingItem data) {
            mData = data;
            updateUI();
        }
    }

    public interface SettingItemClickListener {
        void onClick(FarmSettingDetailItem setting, int position);
        void onChangeProfileSession(FarmSettingDetailItem setting, boolean val);
        void onCopyProfileSession(FarmSettingDetailItem setting);
    }


    public FarmSettingAdapter(Activity activity, List<FarmSettingItem> data, SettingItemClickListener listener) {
        this.mActivity = activity;
        this.listener = listener;
        setData(data);
    }

    public void setData(List<FarmSettingItem> data) {
        if (data == null) {
            this.settings = new ArrayList<>();
        } else {
            List<FarmSettingItem> settingList = new ArrayList<>();
            for (FarmSettingItem item : data) {
                if (item.getSettings() != null && item.getSettings().size() > 0) {
                    settingList.add(item);
                }
            }
            this.settings = settingList;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_farm_setting_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        FarmSettingItem settingItem = settings.get(position);
        holder.setData(settingItem);
    }

    @Override
    public int getItemCount() {
        return settings.size();
    }
}
