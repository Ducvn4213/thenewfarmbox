package mobile.agrhub.farmbox.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.Camera;
import mobile.agrhub.farmbox.service.model.Device;
import mobile.agrhub.farmbox.service.model.Farm;

public class CameraAdapter extends RecyclerView.Adapter<CameraAdapter.ViewHolder> {
    private Context mContext;
    private int mScreenWidth = 0;
    private List<Camera> mCameraList;
    private OnClickListener mListener;
    private ImageLoader imageLoader = ImageLoader.getInstance();

    public CameraAdapter(Context context, int screenWidth) {
        mContext = context;
        mScreenWidth = screenWidth;
        mCameraList = new ArrayList<>();
    }

    public void setData(Farm farm, List<Camera> cameraList) {
        if (cameraList == null) {
            mCameraList.clear();
        } else {
            List<Camera> tmp = new ArrayList<>();
            for (Camera camera : cameraList) {
                for (Device device : farm.devices) {
                    if (camera.getDeviceId() == device.device_id) {
                        if (device.is_active) {
                            camera.setCameraName(device.getDisplayName());
                            tmp.add(camera);
                        }
                        break;
                    }
                }
            }
            if (tmp.size() > 0) {
                Collections.sort(tmp, new Comparator<Camera>() {
                    @Override
                    public int compare(Camera o1, Camera o2) {
                        if (o1.getCameraId() >= o2.getCameraId()) {
                            return 1;
                        } else {
                            return -1;
                        }
                    }
                });
            }

            mCameraList = tmp;
        }
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_camera_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        TableRow.LayoutParams params;
        if (getItemCount() == 1) {
            params = new TableRow.LayoutParams(mScreenWidth, Math.round((mScreenWidth * 3) / 4)); // (width, height)
        } else {
            int width = Math.round((mScreenWidth * 80) / 100);
            params = new TableRow.LayoutParams(width, Math.round((width * 3) / 4)); // (width, height)

            int margin = Math.round(5 * mContext.getResources().getDisplayMetrics().density);
            if (position == 0) {
                params.setMargins(0, 0, margin, 0);
            } else if (position == (getItemCount() - 1)) {
                params.setMargins(margin, 0, 0, 0);
            } else {
                params.setMargins(margin, 0, margin, 0);
            }
        }
        holder.mContainer.setLayoutParams(params);

        final Camera camera = mCameraList.get(position);

        imageLoader.displayImage(camera.getCameraThumbnail(), holder.mThumbnail);
        holder.mTitle.setText(camera.getCameraName());
        holder.mContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onSelectCamera(camera, position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCameraList.size();
    }

    public void setListener(OnClickListener listener) {
        mListener = listener;
    }

    public interface OnClickListener {
        void onSelectCamera(Camera camera, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View view) {
            super(view);

            bindingControls(view);
        }

        private RelativeLayout mContainer;
        private ImageView mThumbnail;
        private ImageView mPlay;
        private TextView mTitle;

        private void bindingControls(View v) {
            mContainer = (RelativeLayout) v.findViewById(R.id.rl_container);
            mThumbnail = (ImageView) v.findViewById(R.id.iv_camera_thumbnail);
            mPlay = (ImageView) v.findViewById(R.id.ib_play);
            mTitle = (TextView) v.findViewById(R.id.tv_camera_name);
        }
    }
}
