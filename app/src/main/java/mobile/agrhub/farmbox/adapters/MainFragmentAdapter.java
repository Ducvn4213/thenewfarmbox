package mobile.agrhub.farmbox.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import mobile.agrhub.farmbox.activities.account.AccountActivity;
import mobile.agrhub.farmbox.fragments.DiaryFragment;
import mobile.agrhub.farmbox.fragments.MainFragment;
import mobile.agrhub.farmbox.fragments.SettingsFragment;
import mobile.agrhub.farmbox.fragments.StoreFragment;


public class MainFragmentAdapter extends FragmentStatePagerAdapter {
    public static final int NUMBER_OF_ITEM = 5;
    public static final int HOME_FRAGMENT_INDEX = 0;
    public static final int DIARY_FRAGMENT_INDEX = 1;
    public static final int STORE_FRAGMENT_INDEX = 2;
    public static final int ACCOUNT_FRAGMENT_INDEX = 3;
    public static final int SETTING_FRAGMENT_INDEX = 4;

    private MainFragment mainFragment;
    private DiaryFragment diaryFragment;
    private StoreFragment storeFragment;
    private AccountActivity accountFragment;
    private SettingsFragment settingsFragment;

    public MainFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case HOME_FRAGMENT_INDEX:
                return getMainFragment();
            case DIARY_FRAGMENT_INDEX:
                return getDiaryFragment();
            case STORE_FRAGMENT_INDEX:
                return getStoreFragment();
//            case ACCOUNT_FRAGMENT_INDEX:
//                return getAccountFragment();
            case SETTING_FRAGMENT_INDEX:
                return getSettingsFragment();
            default:
                break;
        }
        return null;
    }

    @Override
    public int getCount() {
        return NUMBER_OF_ITEM;
    }

    public MainFragment getMainFragment() {
        if (mainFragment == null) {
            mainFragment = new MainFragment();
        }
        return mainFragment;
    }

    public DiaryFragment getDiaryFragment() {
        if (diaryFragment == null) {
            diaryFragment = new DiaryFragment();
        }
        return diaryFragment;
    }

    public StoreFragment getStoreFragment() {
        if (storeFragment == null) {
            storeFragment = new StoreFragment();
        }
        return storeFragment;
    }

    public AccountActivity getAccountFragment() {
        if (accountFragment == null) {
            accountFragment = new AccountActivity();
        }
        return accountFragment;
    }

    public SettingsFragment getSettingsFragment() {
        if (settingsFragment == null) {
            settingsFragment = new SettingsFragment();
        }
        return settingsFragment;
    }
}