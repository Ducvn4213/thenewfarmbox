package mobile.agrhub.farmbox.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.GapTask;
import mobile.agrhub.farmbox.utils.Utils;

/**
 * Created by lecong on 3/27/18.
 */

public class ChooseGapTaskAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private LayoutInflater inflater;
    private List<GapTask> beanList;
    List<GapTask> mStringFilterList;
    ValueFilter valueFilter;

    public ChooseGapTaskAdapter(Context context, List<GapTask> beanList) {
        this.context = context;
        this.beanList = beanList;
        mStringFilterList = beanList;
    }

    @Override
    public int getCount() {
        return this.beanList.size();
    }

    @Override
    public Object getItem(int position) {
        return beanList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        // Contact to display
        GapTask gapTask = (GapTask) this.getItem(position);

        // The child views in each row.
        CheckBox checkBox;
        TextView textView;

        // Create a new row view
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.layout_single_checkbox_item, null);

            // Find the child views.
            textView = (TextView) convertView.findViewById(R.id.list_view_item_text);
            checkBox = (CheckBox) convertView.findViewById(R.id.list_view_item_checkbox);

            // Optimization: Tag the row with it's child views, so we don't
            // have to
            // call findViewById() later when we reuse the row.
            convertView.setTag(new GapTaskViewHolder(textView, checkBox));

            // If CheckBox is toggled, update the Contact it is tagged with.
            checkBox.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v;
                    GapTask gapTask = (GapTask) cb.getTag();
                    gapTask.isChecked = cb.isChecked();
                }
            });
        }
        // Reuse existing row view
        else {
            // Because we use a ViewHolder, we avoid having to call
            // findViewById().
            GapTaskViewHolder viewHolder = (GapTaskViewHolder) convertView.getTag();
            checkBox = viewHolder.getCheckBox();
            textView = viewHolder.getTextView();
        }

        // Tag the CheckBox with the Contact it is displaying, so that we
        // can
        // access the Contact in onClick() when the CheckBox is toggled.
        checkBox.setTag(gapTask);

        // Display Contact data
        checkBox.setChecked(gapTask.isChecked);
        textView.setText(Utils.getValueFromMultiLanguageString(gapTask.gapTaskTitle));

        return convertView;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private static class GapTaskViewHolder {
        private CheckBox checkBox;
        private TextView textView;

        public GapTaskViewHolder() {
        }

        public GapTaskViewHolder(TextView textView, CheckBox checkBox) {
            this.checkBox = checkBox;
            this.textView = textView;
        }

        public CheckBox getCheckBox() {
            return checkBox;
        }

        public void setCheckBox(CheckBox checkBox) {
            this.checkBox = checkBox;
        }

        public TextView getTextView() {
            return textView;
        }

        public void setTextView(TextView textView) {
            this.textView = textView;
        }
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                ArrayList<GapTask> filterList = new ArrayList<GapTask>();
                for (int i = 0; i < mStringFilterList.size(); i++) {
                    String title = Utils.getValueFromMultiLanguageString(mStringFilterList.get(i).gapTaskTitle);
                    if ((title.toUpperCase())
                            .contains(constraint.toString().toUpperCase())) {
                        filterList.add(mStringFilterList.get(i));
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = mStringFilterList.size();
                results.values = mStringFilterList;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            beanList = (ArrayList<GapTask>) results.values;
            notifyDataSetChanged();
        }

    }
}
