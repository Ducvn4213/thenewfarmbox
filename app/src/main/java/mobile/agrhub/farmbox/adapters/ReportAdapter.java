package mobile.agrhub.farmbox.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.FarmReport;
import mobile.agrhub.farmbox.utils.Utils;

public class ReportAdapter extends RecyclerView.Adapter<ReportAdapter.ViewHolder> {
    public static final String DATE_FORMAT = "dd/MM/yyyy";
    public static final String DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm";

    private List<FarmReport> datas;
    private Activity mActivity;
    private ReportItemListener listener;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View view) {
            super(view);

            bindingControls(view);
            setupControlEvents();
        }

        private FarmReport mData;
        private LinearLayout mContent;
        private TextView mTitle;
        private TextView mDescription;
        private ImageView mOpenIn;
        private ImageView mDetailArrow;

        private void bindingControls(View v) {
            mContent = (LinearLayout) v.findViewById(R.id.ll_content_view);
            mTitle = (TextView) v.findViewById(R.id.tv_title);
            mDescription = (TextView) v.findViewById(R.id.tv_description);
            mOpenIn = (ImageView) v.findViewById(R.id.iv_open_in);
            mDetailArrow = (ImageView) v.findViewById(R.id.iv_detail_arrow);
        }

        private void setupControlEvents() {
            mContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(mData);
                }
            });
            mOpenIn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onOpenInClick(mData);
                }
            });
        }

        private void updateUI() {
            String startDate = Utils.getDate(mData.getReportStartTime(), DATE_FORMAT);
            String endDate = Utils.getDate(mData.getReportEndTime(), DATE_FORMAT);
            String granularity = FarmReport.granularityName(mData.getReportGranularity());
            mTitle.setText(mActivity.getString(R.string.report_title_format, startDate, endDate, granularity));

            String createdDate = Utils.getDate(mData.getReportRegisteredDate(), DATE_TIME_FORMAT);
            mDescription.setText(mActivity.getString(R.string.report_description_format, createdDate));
        }

        public void setData(FarmReport data) {
            mData = data;
            updateUI();
        }
    }

    public interface ReportItemListener {
        void onClick(FarmReport item);
        void onOpenInClick(FarmReport item);
    }


    public ReportAdapter(Activity activity, List<FarmReport> data, ReportItemListener listener) {
        this.mActivity = activity;
        this.listener = listener;
        this.datas = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_report_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        FarmReport item = datas.get(position);
        holder.setData(item);
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public void setData(List<FarmReport> data) {
        this.datas = data;
        notifyDataSetChanged();
    }
}
