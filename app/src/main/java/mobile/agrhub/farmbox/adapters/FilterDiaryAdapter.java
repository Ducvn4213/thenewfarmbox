package mobile.agrhub.farmbox.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.List;

import mobile.agrhub.farmbox.R;

/**
 * Created by lecong on 3/27/18.
 */

public class FilterDiaryAdapter extends BaseAdapter implements View.OnClickListener {

    private Activity mContext;
    private LayoutInflater inflater;
    private List<FilterDiaryItem> mDataList;
    private OnClickListener listener;

    // Constructor
    public FilterDiaryAdapter(Activity context, List<FilterDiaryItem> items, OnClickListener listener) {
        this.mContext = context;
        this.mDataList = items;
        this.inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = inflater.inflate(R.layout.layout_filter_diary_item, null);

        RadioButton radioView = (RadioButton) convertView.findViewById(R.id.radio_filter_diary_item);
        TextView titleView = (TextView) convertView.findViewById(R.id.title_filter_diary_item);
        TextView valueView = (TextView) convertView.findViewById(R.id.value_filter_diary_item);
        ImageView clearBtn = (ImageView) convertView.findViewById(R.id.ib_clear_diary_date);

        LinearLayout.LayoutParams fullWidth = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,
                1
        );

        LinearLayout.LayoutParams radioWidth = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,
                9
        );

        LinearLayout.LayoutParams farmTitleWidth = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,
                1
        );

        LinearLayout.LayoutParams halfWidth = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,
                5
        );

        Resources resources = mContext.getResources();
        FilterDiaryItem item = (FilterDiaryItem) getItem(position);
        switch (item.type) {
            case HEADER:
                convertView.setBackgroundColor(resources.getColor(R.color.color5));

                titleView.setLayoutParams(fullWidth);
                titleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                titleView.setTextColor(resources.getColor(android.R.color.white));
                titleView.setTypeface(null, Typeface.BOLD);
                titleView.setText(item.title);

                radioView.setVisibility(View.GONE);
                titleView.setVisibility(View.VISIBLE);
                valueView.setVisibility(View.GONE);
                clearBtn.setVisibility(View.GONE);
                break;
            case FARM_ITEM:
                convertView.setBackgroundColor(resources.getColor(R.color.colorBackground));

                radioView.setLayoutParams(radioWidth);
                radioView.setChecked(item.checked);

                titleView.setLayoutParams(farmTitleWidth);
                titleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                titleView.setTextColor(resources.getColor(R.color.gray_text_view));
                titleView.setTypeface(null, Typeface.NORMAL);
                titleView.setText(item.title);

                radioView.setVisibility(View.VISIBLE);
                titleView.setVisibility(View.VISIBLE);
                valueView.setVisibility(View.GONE);
                clearBtn.setVisibility(View.GONE);
                break;
            case FROM_DATE:
            case TO_DATE:
                convertView.setBackgroundColor(resources.getColor(R.color.colorBackground));

                titleView.setLayoutParams(halfWidth);
                titleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                titleView.setTextColor(resources.getColor(R.color.gray_text_view));
                titleView.setTypeface(null, Typeface.NORMAL);
                titleView.setText(item.title);

                valueView.setLayoutParams(halfWidth);
                valueView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                valueView.setText(item.value);

                radioView.setVisibility(View.GONE);
                titleView.setVisibility(View.VISIBLE);
                valueView.setVisibility(View.VISIBLE);
                if (item.value == null || item.value.isEmpty()) {
                    clearBtn.setVisibility(View.GONE);
                } else {
                    clearBtn.setVisibility(View.VISIBLE);
                    clearBtn.setTag(item.type);
                    clearBtn.setOnClickListener(this);
                }
                break;
            case BUTTON:
                convertView.setBackgroundColor(resources.getColor(R.color.colorPrimary));

                titleView.setLayoutParams(fullWidth);
                titleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                titleView.setTextColor(resources.getColor(android.R.color.white));
                titleView.setTypeface(null, Typeface.BOLD);
                titleView.setText(item.title);
                titleView.setGravity(Gravity.CENTER);

                radioView.setVisibility(View.GONE);
                titleView.setVisibility(View.VISIBLE);
                valueView.setVisibility(View.GONE);
                clearBtn.setVisibility(View.GONE);
                break;
        }


        return convertView;
    }

    @Override
    public void onClick(View v) {
        if (v.getTag() != null && v.getTag() instanceof FilterDiaryItemType) {
            listener.clearValue((FilterDiaryItemType) v.getTag());
        }
    }

    public static class FilterDiaryItem {
        public FilterDiaryItemType type;
        public String title;
        public String value;
        public boolean checked;
    }

    public enum FilterDiaryItemType {
        HEADER, FARM_ITEM, FROM_DATE, TO_DATE, BUTTON
    }

    public interface OnClickListener {
        void clearValue(FilterDiaryItemType type);
    }
}
