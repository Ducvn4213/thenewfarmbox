package mobile.agrhub.farmbox.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageSize;

import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.Photo;

/**
 * Created by lecong on 3/26/18.
 */

public class InputPhotoCellAdapter extends BaseAdapter implements View.OnClickListener {
    private Context mContext;
    private LayoutInflater inflater;
    private List<Photo> mImagesList;
    private ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance

    // Constructor
    public InputPhotoCellAdapter(Context context, List<Photo> items) {
        this.mContext = context;
        this.mImagesList = items;
    }

    @Override
    public int getCount() {
        return mImagesList.size();
    }

    @Override
    public Object getItem(int position) {
        return mImagesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.layout_input_photo_cell_item, null);

        if (imageLoader == null)
            imageLoader = ImageLoader.getInstance();

        ImageView imageView = (ImageView) convertView.findViewById(R.id.iv_image);
        imageLoader.displayImage(mImagesList.get(position).uri, imageView, new ImageSize(60, 60));
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        ImageView imageButton = (ImageView) convertView.findViewById(R.id.ib_remove);
        imageButton.setVisibility(View.VISIBLE);
        imageButton.setTag(1000 + position);
        imageButton.setOnClickListener(this);
        return convertView;
    }

    @Override
    public void onClick(View view) {
        Object tagObj = view.getTag();
        if (tagObj != null && tagObj instanceof Integer) {
            int tag = Integer.valueOf(tagObj.toString()) - 1000;
            if (tag < mImagesList.size()) {
                mImagesList.remove(tag);

                notifyDataSetChanged();
            }
        }
    }

    public void updateData(List<Photo> mediaList) {
        mImagesList = mediaList;
        notifyDataSetChanged();
    }

}
