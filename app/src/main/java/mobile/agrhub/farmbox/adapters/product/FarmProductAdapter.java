package mobile.agrhub.farmbox.adapters.product;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.FarmCart;
import mobile.agrhub.farmbox.service.model.FarmProduct;
import mobile.agrhub.farmbox.utils.Utils;

/**
 * Created by lecong on 3/27/18.
 */

public class FarmProductAdapter extends BaseAdapter implements View.OnClickListener {

    private int mode = 0; // 0: default; 1: add to cart
    private Context mContext;
    private LayoutInflater inflater;
    private List<FarmProduct> products;
    private List<FarmCart> carts;
    private OnClickListener listener;
    ImageLoader imageLoader = ImageLoader.getInstance();

    // Constructor
    public FarmProductAdapter(Activity context, List<FarmProduct> products, List<FarmCart> carts, OnClickListener listener) {
        this.mode = 1;
        this.mContext = context;
        this.inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.products = products;
        this.carts = carts;
        this.listener = listener;
    }

    public FarmProductAdapter(Activity context, List<FarmProduct> products, OnClickListener listener) {
        this.mContext = context;
        this.products = products;
        this.listener = listener;
        this.inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int position) {
        return this.products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = inflater.inflate(R.layout.layout_farm_product_item, null);

        FarmProduct product = (FarmProduct) getItem(position);

        CardView container = convertView.findViewById(R.id.cv_container);
        container.setTag(String.valueOf(product.productId));
        container.setOnClickListener(this);

        ImageView productImageView = convertView.findViewById(R.id.iv_product_image);
        productImageView.setBackgroundResource(R.color.colorBackground);
        String avatar = product.getDisplayAvatar();
        if (avatar == null || avatar.isEmpty()) {
            productImageView.setImageResource(R.drawable.ic_photo_gray_24dp);
        } else {
            if (imageLoader == null) {
                imageLoader = ImageLoader.getInstance();
            }
            imageLoader.displayImage(avatar, productImageView);
        }

        TextView productPriceView = convertView.findViewById(R.id.tv_product_price);
        productPriceView.setText(product.displayPrice());

        TextView productNameView = convertView.findViewById(R.id.tv_product_name);
        productNameView.setText(product.productName);

        TextView productWeightView = convertView.findViewById(R.id.tv_product_weight);
        productWeightView.setText(product.displayWeight(mContext));

        View separator = convertView.findViewById(R.id.v_separator);
        LinearLayout productAmountView = convertView.findViewById(R.id.ll_product_amount);
        LinearLayout addToCartView = convertView.findViewById(R.id.ll_add_to_cart);
        LinearLayout assignQRCodeView = convertView.findViewById(R.id.ll_assign_qr_code_remain);
        LinearLayout assignQRCodeControlView = convertView.findViewById(R.id.ll_assign_qr_code_control_view);

        int amount = 0;
        if (mode == 0) {
            separator.setVisibility(View.GONE);
            productAmountView.setVisibility(View.GONE);
            addToCartView.setVisibility(View.GONE);
            assignQRCodeView.setVisibility(View.GONE);
            assignQRCodeControlView.setVisibility(View.GONE);
        } else if (mode == 1) {
            amount = getAmount(product.productId);
            if (amount <= 0) {
                separator.setVisibility(View.VISIBLE);
                productAmountView.setVisibility(View.GONE);
                addToCartView.setVisibility(View.VISIBLE);
            } else {
                separator.setVisibility(View.VISIBLE);
                productAmountView.setVisibility(View.VISIBLE);
                addToCartView.setVisibility(View.GONE);
            }
            assignQRCodeView.setVisibility(View.GONE);
            assignQRCodeControlView.setVisibility(View.GONE);
        }

        ImageButton minusAmountBtn = convertView.findViewById(R.id.ib_amount_minus);
        minusAmountBtn.setTag(String.valueOf(product.productId));
        minusAmountBtn.setOnClickListener(this);

        TextView amountView = convertView.findViewById(R.id.tv_product_amount);
        amountView.setTag(String.valueOf(product.productId));
        amountView.setText(Utils.i2s(amount));
        amountView.setOnClickListener(this);

        ImageButton plusAmountBtn = convertView.findViewById(R.id.ib_amount_plus);
        plusAmountBtn.setTag(String.valueOf(product.productId));
        plusAmountBtn.setOnClickListener(this);

        addToCartView.setTag(String.valueOf(product.productId));
        addToCartView.setOnClickListener(this);

        return convertView;
    }

    public void update(List<FarmProduct> products) {
        this.products = products;
        notifyDataSetChanged();
    }

    public void update(List<FarmProduct> products, List<FarmCart> carts) {
        this.products = products;
        this.carts = carts;
        notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        Object tagObj = v.getTag();
        if (tagObj != null && tagObj instanceof String) {
            long productId = 0;

            String productIdStr = (String) tagObj;
            try {
                productId = Utils.str2Long(productIdStr);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (v.getId() == R.id.cv_container) {
                for (FarmProduct item : products) {
                    if (item.productId == productId) {
                        listener.onProductClicked(item);
                        break;
                    }
                }
            } else if (v.getId() == R.id.ib_amount_minus) {
                addToCart(productId, 1, -1);
                listener.onMinusClicked(productIdStr);
            } else if (v.getId() == R.id.ib_amount_plus) {
                addToCart(productId, 1, 1);
                listener.onPlusClicked(productIdStr);
            } else if (v.getId() == R.id.tv_product_amount) {
                listener.onProductAmountClicked(productId, getAmount(productId));
            } else if (v.getId() == R.id.ll_add_to_cart) {
                addToCart(productId, 1, 0);
                listener.onAddToCartClicked(productId);
            }
        }
    }

    private int getAmount(long id) {
        int amount = 0;
        if (carts != null && carts.size() > 0) {
            for (FarmCart item : carts) {
                if (id == item.cartProduct.productId) {
                    amount = item.cartItemQuantity;
                    break;
                }
            }
        }
        return amount;
    }

    public void addToCart(long id, int amount, int type) {
        boolean existed = false;
        if (carts != null && carts.size() > 0) {
            FarmCart item;
            for (int i = 0; i < carts.size(); i++) {
                item = carts.get(i);
                if (id == item.cartProduct.productId) {
                    existed = true;

                    if (type == 0) {
                        item.cartItemQuantity = amount;
                    } else {
                        item.cartItemQuantity += (amount * type);
                        if (item.cartItemQuantity < 0) {
                            item.cartItemQuantity = 0;
                        }
                    }

                    break;
                }
            }
        }
        if (!existed) {
            if (type != 0) {
                amount *= type;
                if (amount < 0) {
                    amount = 0;
                }
            }
            FarmProduct product = null;
            for (FarmProduct item : products) {
                if (id == item.productId) {
                    product = item;
                    break;
                }
            }
            if (product != null) {
                FarmCart cart = new FarmCart(product, amount);
                carts.add(cart);
            }
        }
    }

    public int totalItem() {
        int totalItem = 0;
        if (carts != null && carts.size() > 0) {
            for (FarmCart item : carts) {
                totalItem += item.cartItemQuantity;
            }
        }
        return totalItem;
    }

    public interface OnClickListener {
        void onProductClicked(FarmProduct product);
        void onMinusClicked(String productId);
        void onProductAmountClicked(long productId, int amount);
        void onPlusClicked(String productId);
        void onAddToCartClicked(long productId);
    }
}
