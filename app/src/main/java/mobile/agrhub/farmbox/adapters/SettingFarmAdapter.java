package mobile.agrhub.farmbox.adapters;

import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.FarmAPI;
import mobile.agrhub.farmbox.service.model.FarmBasic;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class SettingFarmAdapter extends RecyclerView.Adapter<SettingFarmAdapter.MyViewHolder> {
    private List<FarmBasic> farmList;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public MyViewHolder(View view) {
            super(view);

            bindingControls(view);
            setupControlEvents();
        }

        private TextView mName;
        private Switch mToggle;
        private ProgressBar mProgress;
        private FarmBasic mData;
        private View mCover;

        private boolean humanInteract = true;

        FarmAPI farmAPI = APIHelper.getFarmAPI();

        private void bindingControls(View v) {
            mName = (TextView) v.findViewById(R.id.tv_name);
            mToggle = (Switch) v.findViewById(R.id.sw_toggle);
            mProgress = (ProgressBar) v.findViewById(R.id.pb_progress);
            mCover = v.findViewById(R.id.view_cover);
        }

        private void setChecked(Boolean value) {
            mToggle.setOnCheckedChangeListener(null);
            mToggle.setChecked(value);
            setupControlEvents();
        }

        private void setupControlEvents() {
            mToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, final boolean b) {
                    if (humanInteract == false) {
                        humanInteract = true;
                        return;
                    }

                    mProgress.setVisibility(VISIBLE);
                    mCover.setVisibility(VISIBLE);

                    final boolean newValue = !mData.is_active;
                    farmAPI.editFarmField(String.valueOf(mData.farm_id), "is_active", newValue ? "true" : "false", new APIHelper.Callback<String>() {
                        @Override
                        public void onSuccess(String data) {
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                public void run() {
                                    mData.is_active = newValue;
                                    updateGlobalData();
                                    mProgress.setVisibility(View.INVISIBLE);
                                    mCover.setVisibility(GONE);

                                    FarmBoxAppController.getInstance().setNeedLoadFarmList(true);
                                }
                            });
                        }

                        @Override
                        public void onFail(int errorCode, String error) {
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                public void run() {
                                    mProgress.setVisibility(View.INVISIBLE);
                                    humanInteract = false;
                                    mToggle.setChecked(!b);
                                    mCover.setVisibility(GONE);
                                }
                            });
                        }
                    });
                }
            });
        }

        private void updateUI() {
            mName.setText(mData.farm_name);
            setChecked(mData.is_active);
            mToggle.setVisibility(VISIBLE);
        }

        public void setData(FarmBasic data) {
            mData = data;
            updateUI();
        }

        private void updateGlobalData() {
            for (FarmBasic f : farmList) {
                if (f.farm_id == mData.farm_id) {
                    f.is_active = mData.is_active;
                }
            }
        }
    }


    public SettingFarmAdapter(List<FarmBasic> data) {
        if (data == null) {
            this.farmList = new ArrayList<>();
        } else {
            this.farmList = data;
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_setting_farm_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        FarmBasic farm = farmList.get(position);
        holder.setData(farm);
    }

    @Override
    public int getItemCount() {
        return farmList.size();
    }
}
