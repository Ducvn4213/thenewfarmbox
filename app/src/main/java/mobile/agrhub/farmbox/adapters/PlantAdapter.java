package mobile.agrhub.farmbox.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.PlantBasic;
import mobile.agrhub.farmbox.utils.Utils;

public class PlantAdapter extends RecyclerView.Adapter<PlantAdapter.PlantViewHolder> {

    public static interface PlantItemSelectedListener {
        void onPlantItemSelected(int index, PlantBasic data);
    }

    public static interface BackListener {
        void onBackPressed();
    }

//    public static interface OnSearchWithTextCallback {
//        void onSearchWithText(String text);
//    }

    private List<PlantBasic> plantList;
    private Context mContext;
    private PlantItemSelectedListener onClickListener;
    private BackListener onBackListener;
//    private OnSearchWithTextCallback onSearchWithTextCallback;

    public class PlantViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public ImageView image;
        public TextView scientificName;

        public ImageView backButton;
//        public SearchView searchView;

        private RelativeLayout mHeader;
        private LinearLayout mContent;

        public PlantViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.tv_name);
            image = (ImageView) view.findViewById(R.id.iv_image);
            scientificName = (TextView) view.findViewById(R.id.tv_scientific_name);
            backButton = (ImageView) view.findViewById(R.id.iv_back);

//            searchView = (SearchView) view.findViewById(R.id.sv_search);

            mHeader = (RelativeLayout) view.findViewById(R.id.rl_header);
            mContent = (LinearLayout) view.findViewById(R.id.ll_content);
        }

        public void hideHeader() {
            mHeader.setVisibility(View.GONE);
        }

        public void showHeader() {
            mHeader.setVisibility(View.VISIBLE);
        }

        public void setOnBackClick(View.OnClickListener listener) {
            backButton.setOnClickListener(listener);
        }

//        public void setOnSearchCallback(SearchView.OnQueryTextListener listener) {
//            searchView.setOnQueryTextListener(listener);
//        }

        public void setOnItemClick(View.OnClickListener listener) {
            mContent.setOnClickListener(listener);
        }
    }

    public PlantAdapter(Context context, List<PlantBasic> data) {
        this.mContext = context;
        this.plantList = data;
    }

    public void setData(List<PlantBasic> data) {
        this.plantList = data;
        notifyDataSetChanged();
    }

    public void appendData(List<PlantBasic> data) {
        int currentCount = this.plantList.size();
        this.plantList.addAll(data);
        notifyItemRangeInserted(currentCount, data.size());
    }

    @Override
    public PlantViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_plant_info, parent, false);

        return new PlantViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PlantViewHolder holder, final int position) {
        final PlantBasic p = plantList.get(position);

        holder.hideHeader();
        holder.name.setText(Utils.getValueFromMultiLanguageString(p.plant_name));
        holder.scientificName.setText(p.plant_scientific_name);

        if (position == 0) {
            holder.setOnBackClick(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackListener.onBackPressed();
                }
            });

//            holder.setOnSearchCallback(new SearchView.OnQueryTextListener() {
//                @Override
//                public boolean onQueryTextSubmit(String s) {
//                    return false;
//                }
//
//                @Override
//                public boolean onQueryTextChange(String s) {
//                    onSearchWithTextCallback.onSearchWithText(s);
//                    return false;
//                }
//            });
        }

        holder.setOnItemClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onPlantItemSelected(position, p);
            }
        });

        if (p.plant_photo == null || p.plant_photo.trim().isEmpty()) {
            return;
        }

        Picasso.with(mContext).load(p.plant_photo).transform(new Utils.CircleTransform()).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return plantList.size();
    }

    public void setOnItemSelectedListener(PlantItemSelectedListener listener) {
        this.onClickListener = listener;
    }

    public void setOnBackListener(BackListener listener) {
        this.onBackListener = listener;
    }
//    public void setOnSearchWithTextCallback(OnSearchWithTextCallback listener) {
//        this.onSearchWithTextCallback = listener;
//    }
}