package mobile.agrhub.farmbox.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import mobile.agrhub.farmbox.R;

public class PlantImageAdapter extends RecyclerView.Adapter<PlantImageAdapter.PlantImageViewHolder> {


    private List<String> plantImageList;
    private Context mContext;

    public class PlantImageViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;

        public PlantImageViewHolder(View view) {
            super(view);
            image = (ImageView) view.findViewById(R.id.iv_image);
        }
    }

    public PlantImageAdapter(Context context, List<String> data) {
        this.mContext = context;
        plantImageList = data;
    }

    @Override
    public PlantImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_plant_image, parent, false);

        return new PlantImageViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PlantImageViewHolder holder, final int position) {
        Picasso.with(mContext).load(plantImageList.get(position)).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return plantImageList.size();
    }
}