package mobile.agrhub.farmbox.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.model.Product;
import mobile.agrhub.farmbox.utils.Utils;

/**
 * Created by lecong on 3/27/18.
 */

public class CartAdapter extends BaseAdapter implements View.OnClickListener {

    private Context mContext;
    private LayoutInflater inflater;
    private FarmBoxAppController app;
    private OnClickListener listener;
    ImageLoader imageLoader = ImageLoader.getInstance();

    // Constructor
    public CartAdapter(Activity context, OnClickListener listener) {
        this.mContext = context;
        this.listener = listener;
        this.inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.app = FarmBoxAppController.getInstance();
    }

    @Override
    public int getCount() {
        return app.getCountProduct();
    }

    @Override
    public Object getItem(int position) {
        return app.getProductAtIndex(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = inflater.inflate(R.layout.layout_cart_item, null);

        Product product = (Product) getItem(position);

        ImageView productImageView = convertView.findViewById(R.id.iv_product_image);
        if (product.productImageList.size() == 0) {
            productImageView.setImageBitmap(null);
        } else {
            if (imageLoader == null) {
                imageLoader = ImageLoader.getInstance();
            }

            imageLoader.displayImage(product.productImageList.get(0), productImageView);
        }

        TextView productNameView = convertView.findViewById(R.id.tv_product_name);
        productNameView.setText(Utils.getValueFromMultiLanguageString(product.productName));


        TextView discountPriceView = convertView.findViewById(R.id.tv_discount_price);
        long discount = Utils.getLongFromMultiLanguageString(product.productDiscount);
        long originPrice = Utils.getLongFromMultiLanguageString(product.productPrice);
        if (discount > 0) {
            long currentPrice = originPrice - Math.round((originPrice * discount) / 100.0);
            discountPriceView.setText(Utils.formatPrice(currentPrice));
        } else {
            discountPriceView.setText(Utils.formatPrice(originPrice));
        }

        ImageButton minusAmountBtn = convertView.findViewById(R.id.ib_amount_minus);
        minusAmountBtn.setTag(String.valueOf(product.productId));
        minusAmountBtn.setOnClickListener(this);

        TextView amountView = convertView.findViewById(R.id.tv_product_amount);
        amountView.setText("" + app.getAmountOfProduct(product.productId));

        ImageButton plusAmountBtn = convertView.findViewById(R.id.ib_amount_plus);
        plusAmountBtn.setTag(String.valueOf(product.productId));
        plusAmountBtn.setOnClickListener(this);

        return convertView;
    }

    @Override
    public void onClick(View v) {
        Object tagObj = v.getTag();
        if (tagObj != null && tagObj instanceof String) {
            String productIdStr = (String) tagObj;

            if (v.getId() == R.id.ib_amount_minus) {
                listener.onMinusClicked(productIdStr);
            } else if (v.getId() == R.id.ib_amount_plus) {
                listener.onPlusClicked(productIdStr);
            }
        }
    }

    public interface OnClickListener {
        void onMinusClicked(String productId);

        void onPlusClicked(String productId);
    }
}
