package mobile.agrhub.farmbox.activities.add;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.adapters.ControllerSettingAdapter;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.UtilAPI;
import mobile.agrhub.farmbox.service.model.Controller;
import mobile.agrhub.farmbox.service.model.Device;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Constants;
import mobile.agrhub.farmbox.utils.ResourceUtils;

public class AddDeviceSetNameActivity extends BaseActivity {

    private long deviceName = -1;

    private EditText mName;
    private RecyclerView mControllerList;
    private Button mType;
    private Button mSet;
    private ImageView mBackground;
    private int mSelectedType;
    private Device deviceInfo;
    private ControllerSettingAdapter mAdapter;
    private int currentUpdateController = 0;

    UtilAPI utilAPI = APIHelper.getUtilAPI();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_device_set_name);

        bindingControls();
        setupControlEvents();
        initData();
    }

    @Override
    public void onBackPressed() {
        return;
    }

    private void initData() {
        Intent intent = getIntent();
        deviceName = intent.getLongExtra("deviceName", -1);

        String deviceInfoStr = intent.getStringExtra(Constants.DEVICE_INFO_KEY);
        if (deviceInfoStr != null && !deviceInfoStr.isEmpty()) {
            Gson gson = new Gson();
            try {
                deviceInfo = gson.fromJson(deviceInfoStr, Device.class);
            } catch (Exception e) {}
        }
        if (deviceInfo == null) {
            finish();
        }

        if (deviceInfo.device_name == ResourceUtils.STORAGE_CONTROLLER) {
            mControllerList.setVisibility(View.VISIBLE);
            mAdapter = new ControllerSettingAdapter(this, deviceInfo.controllers);
            mControllerList.setAdapter(mAdapter);
        } else {
            mControllerList.setVisibility(View.GONE);
        }

        if (deviceName != ResourceUtils.BROADLINK_SP3_SMART_PLUG
                && deviceName != ResourceUtils.TYPE_SENSE_PLUG
                && deviceName != ResourceUtils.KAMOER_DRIPPING_PRO) {
            mName.setHint(R.string.add_device_device_name);
            mType.setVisibility(View.GONE);
        } else {
            mName.setHint(R.string.add_device_device_name_optional);
            mType.setVisibility(View.VISIBLE);
        }

        updateBackground();
    }

    private void updateBackground() {
        int drawable = 0;

        if (deviceName == ResourceUtils.UNKNOWN_DEVICE_NAME) {
            drawable = R.drawable.general_device;
        } else if (deviceName == ResourceUtils.BROADLINK_SP3_SMART_PLUG) {
            drawable = R.drawable.broadlink_sp3_smart_plug;
        } else if (deviceName == ResourceUtils.SONOFF_SMART_PLUG) {
            drawable = R.drawable.general_device;
        } else if (deviceName == ResourceUtils.XIAOMI_SMART_PLUG) {
            drawable = R.drawable.general_device;
        } else if (deviceName == ResourceUtils.XIAOMI_PLANT_CARE_SENSOR) {
            drawable = R.drawable.xiaomi_plant_care_sensor;
        } else if (deviceName == ResourceUtils.AXEC_AIR_SENSOR) {
            drawable = R.drawable.axec_air_sensor;
        } else if (deviceName == ResourceUtils.TI_TAG_SENSOR) {
            drawable = R.drawable.general_device;
        } else if (deviceName == ResourceUtils.FIOT_SMART_TANK_CONTROLLER) {
            drawable = R.drawable.fiot_smart_tank_controller;
        } else if (deviceName == ResourceUtils.BROADLINK_RM3_SMART_REMOTE) {
            drawable = R.drawable.broadlink_rm3_smart_remote;
        } else if (deviceName == ResourceUtils.AGRHUB_SENSE_HUB) {
            drawable = R.drawable.agrhub_sensor_hub;
        } else if (deviceName == ResourceUtils.VIETTEL_CAMERA) {
            drawable = R.drawable.viettel_camera;
        } else if (deviceName == ResourceUtils.AGRHUB_SENSE_HUB_PRO) {
            drawable = R.drawable.general_device;
        } else if (deviceName == ResourceUtils.NRF51822_SENSOR) {
            drawable = R.drawable.general_device;
        } else if (deviceName == ResourceUtils.VGV_CAMERA) {
            drawable = R.drawable.viettel_camera;
        } else {
            drawable = R.drawable.general_device;
        }

        mBackground.setImageResource(drawable);
    }

    private void bindingControls() {
        mName = (EditText) findViewById(R.id.et_name);
        mControllerList = (RecyclerView) findViewById(R.id.rv_controller);
        mType = (Button) findViewById(R.id.btn_type);
        mSet = (Button) findViewById(R.id.btn_set);
        mBackground = (ImageView) findViewById(R.id.iv_background);
    }

    private void setupControlEvents() {
        mType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestChangeType();
            }
        });

        mSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestSetDeviceNameAndType();
            }
        });
    }

    private void requestSetDeviceNameAndType() {
        String name = mName.getText().toString();

        if (deviceName != ResourceUtils.BROADLINK_SP3_SMART_PLUG
                && deviceName != ResourceUtils.TYPE_SENSE_PLUG
                && deviceName != ResourceUtils.KAMOER_DRIPPING_PRO) {
            if (name == null || name.trim().isEmpty()) {
                showErrorDialog(getString(R.string.add_device_missing_device_name));
                return;
            }
        }

        showLoading();
        setDeviceName(name);
    }

    private void setDeviceName(String name) {
        final String deviceID = getIntent().getStringExtra("deviceID");

        utilAPI.editDeviceName(deviceID, name, new APIHelper.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                AddDeviceSetNameActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (deviceName == ResourceUtils.BROADLINK_SP3_SMART_PLUG
                                || deviceName == ResourceUtils.TYPE_SENSE_PLUG
                                || deviceName == ResourceUtils.KAMOER_DRIPPING_PRO) {
                            setDeviceType();
                        } else if (deviceName == ResourceUtils.STORAGE_CONTROLLER) {
                            currentUpdateController = 0;
                            disableController();
                        } else {
                            addDeviceCompleted();
                        }
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                AddDeviceSetNameActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }

    private void setDeviceType() {
        String type = "0";
        switch (mSelectedType) {
            case Controller.TYPE_SWITCH:
                type = "0";
                break;
            case Controller.TYPE_LIGHT:
                type = "1";
                break;
            case Controller.TYPE_WATER_PUMP:
                type = "2";
                break;
            case Controller.TYPE_MISTING_PUMP:
                type = "3";
                break;
            case Controller.TYPE_FAN:
                type = "4";
                break;
            case Controller.TYPE_AIR_CONDITIONER:
                type = "5";
                break;
            case Controller.TYPE_NUTRIENT_DOSING_PUMP:
                type = "7";
                break;
            case Controller.TYPE_VAN:
                type = "9";
                break;
            case Controller.TYPE_PH_DOWN_DOSING_PUMP:
                type = "17";
                break;
            case Controller.TYPE_PH_UP_DOSING_PUMP:
                type = "18";
                break;
        }

        String controllerID = getIntent().getStringExtra("controllerID");
        utilAPI.addController(controllerID, type, new APIHelper.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                AddDeviceSetNameActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        final AlertDialog dialog = new AlertDialog.Builder(AddDeviceSetNameActivity.this, R.style.AppTheme_Dialog_Alert)
                                .setTitle(R.string.app_name)
                                .setMessage(R.string.add_device_edit_name_success)
                                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        AddDeviceSetNameActivity.this.finish();
                                    }
                                }).create();
                        if (!dialog.isShowing()) {
                            dialog.show();
                        }
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                AddDeviceSetNameActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }

    private void requestChangeType() {
        final Dialog dialog = new Dialog(AddDeviceSetNameActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        if (deviceName == ResourceUtils.BROADLINK_SP3_SMART_PLUG ||
                deviceName == ResourceUtils.TYPE_SENSE_PLUG) {
            dialog.setContentView(R.layout.layout_add_device_choose_type);

            TextView switchElem = (TextView) dialog.findViewById(R.id.tv_switch);
            TextView light = (TextView) dialog.findViewById(R.id.tv_light);
            TextView waterPump = (TextView) dialog.findViewById(R.id.tv_water_pump);
            TextView mistingPump = (TextView) dialog.findViewById(R.id.tv_misting_pump);
            TextView fan = (TextView) dialog.findViewById(R.id.tv_fan);
            TextView nutrientDosingPump = (TextView) dialog.findViewById(R.id.tv_nutrient_dosing_pump);
            TextView phDownDosingPump = (TextView) dialog.findViewById(R.id.tv_ph_down_dosing_pump);
            TextView phUpDosingPump = (TextView) dialog.findViewById(R.id.tv_ph_up_dosing_pump);
//            TextView airConditioner = (TextView) dialog.findViewById(R.id.tv_air_conditioner);

            switchElem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onChooseType(Controller.TYPE_SWITCH);
                    dialog.dismiss();
                }
            });
            light.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onChooseType(Controller.TYPE_LIGHT);
                    dialog.dismiss();
                }
            });
            waterPump.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onChooseType(Controller.TYPE_WATER_PUMP);
                    dialog.dismiss();
                }
            });
            mistingPump.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onChooseType(Controller.TYPE_MISTING_PUMP);
                    dialog.dismiss();
                }
            });
            fan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onChooseType(Controller.TYPE_FAN);
                    dialog.dismiss();
                }
            });
            nutrientDosingPump.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onChooseType(Controller.TYPE_MISTING_PUMP);
                    dialog.dismiss();
                }
            });
            phDownDosingPump.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onChooseType(Controller.TYPE_PH_DOWN_DOSING_PUMP);
                    dialog.dismiss();
                }
            });
            phUpDosingPump.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onChooseType(Controller.TYPE_PH_UP_DOSING_PUMP);
                    dialog.dismiss();
                }
            });
//            airConditioner.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    onChooseType(CONTROLLER_TYPE.AirConditioner);
//                    dialog.dismiss();
//                }
//            });
        } else if (deviceName == ResourceUtils.KAMOER_DRIPPING_PRO) {
            dialog.setContentView(R.layout.layout_add_device_choose_pump_controller_type);

            TextView waterPump = (TextView) dialog.findViewById(R.id.tv_water_pump);
            TextView dosingPump = (TextView) dialog.findViewById(R.id.tv_dosing_pump);

            waterPump.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onChooseType(Controller.TYPE_WATER_PUMP);
                    dialog.dismiss();
                }
            });
            dosingPump.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onChooseType(Controller.TYPE_NUTRIENT_DOSING_PUMP);
                    dialog.dismiss();
                }
            });
        }

        dialog.show();
    }

    private void onChooseType(int type) {
        mSelectedType = type;

        switch (type) {
            case Controller.TYPE_SWITCH:
                mType.setText(R.string.add_device_device_switch);
                break;
            case Controller.TYPE_LIGHT:
                mType.setText(R.string.add_device_device_light);
                break;
            case Controller.TYPE_WATER_PUMP:
                mType.setText(R.string.add_device_device_water_pump);
                break;
            case Controller.TYPE_MISTING_PUMP:
                mType.setText(R.string.add_device_device_misting_pump);
                break;
            case Controller.TYPE_FAN:
                mType.setText(R.string.add_device_device_fan);
                break;
            case Controller.TYPE_NUTRIENT_DOSING_PUMP:
                mType.setText(R.string.add_device_device_dosing_pump);
                break;
            case Controller.TYPE_AIR_CONDITIONER:
                mType.setText(R.string.add_device_device_air_conditioner);
                break;
            case Controller.TYPE_PH_DOWN_DOSING_PUMP:
                mType.setText(R.string.add_device_device_ph_down_dosing_pump);
                break;
            case Controller.TYPE_PH_UP_DOSING_PUMP:
                mType.setText(R.string.add_device_device_ph_up_dosing_pump);
                break;
        }
    }

    private void disableController() {
        if (deviceInfo.controllers != null && currentUpdateController < deviceInfo.controllers.size()) {
            Controller controller = null;
            for (int i = currentUpdateController; i < deviceInfo.controllers.size(); i++) {
                controller = deviceInfo.controllers.get(i);
                if (!controller.isActive) {
                    currentUpdateController = i;

                    utilAPI.editController(controller.controller_id + "", Constants.FIELD_IS_ACTIVE, "false", new APIHelper.Callback<String>() {
                        @Override
                        public void onSuccess(String data) {
                            AddDeviceSetNameActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    currentUpdateController += 1;
                                    disableController();
                                }
                            });
                        }

                        @Override
                        public void onFail(final int errorCode, final String error) {
                            AddDeviceSetNameActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    hideLoading();
                                    handleError(errorCode, error);
                                }
                            });
                        }
                    });
                    break;
                }
            }
        } else {
            addDeviceCompleted();
        }
    }

    private void addDeviceCompleted() {
        hideLoading();
        final AlertDialog dialog = new AlertDialog.Builder(AddDeviceSetNameActivity.this, R.style.AppTheme_Dialog_Alert)
                .setTitle(R.string.app_name)
                .setMessage(R.string.add_device_edit_name_success)
                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();

                        finish();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }
}
