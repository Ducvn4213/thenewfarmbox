package mobile.agrhub.farmbox.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.adapters.DemoFarmAdapter;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.UserAPI;
import mobile.agrhub.farmbox.service.model.User;
import mobile.agrhub.farmbox.utils.BaseActivity;

public class DemoFarmActivity extends BaseActivity {

    private ListView mDemoFarmListView;

    private List<FarmInfo> mFarmList;
    private DemoFarmAdapter mAdapter;
    private UserAPI userAPI = APIHelper.getUserAPI();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_farm);

        mDemoFarmListView = (ListView) findViewById(R.id.lv_farm_list);

        FarmBoxAppController app = FarmBoxAppController.getInstance();

        mFarmList = new ArrayList<>();
        FarmInfo farm;

        farm = new FarmInfo();
        if ("vi".equals(app.getLanguage())) {
            farm.setName("Nông trại của anh Minh");
            farm.setAddress("Trảng Bom, Đồng Nai");
        } else {
            farm.setName("Mr. Minh's farm");
            farm.setAddress("Trang Bom, Dong Nai, Vietnam");
        }
        farm.setToken("ODZjNzI2NmE0ODkzODM3Mjc2NzFhYmM4YzMwOTdmMTA=");
        mFarmList.add(farm);

        farm = new FarmInfo();
        if ("vi".equals(app.getLanguage())) {
            farm.setName("Nông trại của AgrHub");
            farm.setAddress("Hồ Chí Minh");
        } else {
            farm.setName("AgrHub's farm");
            farm.setAddress("Ho Chi Minh, Vietnam");
        }
        farm.setToken("N2QzYTg4YzUyZjc2NmZiYmNiZTc3YjZjMjdiYmU5ZTA=");
        mFarmList.add(farm);

        farm = new FarmInfo();
        if ("vi".equals(app.getLanguage())) {
            farm.setName("Nông trại nấm rơm");
            farm.setAddress("Phú Khương, Bến Tre");
        } else {
            farm.setName("Mushrooms farm");
            farm.setAddress("Phu Khuong, Ben Tre, Vietnam");
        }
        farm.setToken("M2NhYWFkOGQ2NTg1YTYyZDZjNWQwMWU4ZDMxNzA0YmY=");
        mFarmList.add(farm);

        farm = new FarmInfo();
        if ("vi".equals(app.getLanguage())) {
            farm.setName("Nông trại thủy canh");
            farm.setAddress("Quận 1, Hồ Chí Minh");
        } else {
            farm.setName("Hydroponic farm");
            farm.setAddress("District 1, Ho Chi Minh, Vietnam");
        }
        farm.setToken("ZjQ4ZGMxMWVkMmY4ZGNjNmJlMmM5ZTdhYzJmZjI4N2Q=");
        mFarmList.add(farm);

        farm = new FarmInfo();
        if ("vi".equals(app.getLanguage())) {
            farm.setName("Nông trại dưa lưới");
            farm.setAddress("Ninh Kiều, Cần Thơ");
        } else {
            farm.setName("Melon farm");
            farm.setAddress("Ninh Kieu, Can Tho, Vietnam");
        }
        farm.setToken("ZDkxM2Y1ZmZiMTE3YmJmODM4NDc1YTdkOTU4NDE5ZmQ=");
        mFarmList.add(farm);

        mAdapter = new DemoFarmAdapter(this, mFarmList);
        mDemoFarmListView.setAdapter(mAdapter);

        mDemoFarmListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position >= 0 && position < mFarmList.size()) {
                    DemoFarmActivity.this.doDemoFarm(mFarmList.get(position));
                }
            }
        });
    }

    private void doDemoFarm(final FarmInfo farm) {
        FarmBoxAppController app = FarmBoxAppController.getInstance();
        app.setDemoMode(true);

        showLoading();
        userAPI.getUserInfo(this, farm.getToken(), new APIHelper.Callback<User>() {
            @Override
            public void onSuccess(User user) {
                FarmBoxAppController app = FarmBoxAppController.getInstance();
                user = app.loadSettings(DemoFarmActivity.this, user);
                app.setUser(user, farm.getToken());
                app.setNeedLoadFarmList(true);

                DemoFarmActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        Intent intent = new Intent(DemoFarmActivity.this, TabActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                DemoFarmActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }


    public class FarmInfo {
        private String name;
        private String address;
        private String token;

        public FarmInfo() {

        }

        public FarmInfo(String name, String address, String token) {
            this.name = name;
            this.address = address;
            this.token = token;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }
    }
}
