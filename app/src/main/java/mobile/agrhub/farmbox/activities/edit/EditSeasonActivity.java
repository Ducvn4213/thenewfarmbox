package mobile.agrhub.farmbox.activities.edit;


import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.activities.plant.PlantSelectorActivity;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.FarmAPI;
import mobile.agrhub.farmbox.service.interfaces.PlantAPI;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Utils;

public class EditSeasonActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener {

    private final int SELECT_PLANT_REQUEST_CODE = 999;

    Button mPlant;
    Button mStartDate;
    EditText mPlantAge;
    EditText mProductivity;
    Button mAdd;

    Calendar myCalendar = Calendar.getInstance();
    private long mStartDateByTM = 0;
    private long mSelectedPlantID = 0;

    PlantAPI plantAPI = APIHelper.getPlantAPI();
    FarmAPI farmAPI = APIHelper.getFarmAPI();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_season);

        bindingControls();
        setupControlEvents();
        configActionBar();
    }

    private void configActionBar() {
        getSupportActionBar().setTitle(R.string.season_info_title);
        Intent intent = getIntent();
        boolean isEnableBack = intent.getBooleanExtra("enableBack", true);

        if (isEnableBack) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        } else {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
        }
    }

    private void bindingControls() {
        mPlant = (Button) findViewById(R.id.btn_plant);
        mStartDate = (Button) findViewById(R.id.btn_start_date);
        mPlantAge = (EditText) findViewById(R.id.et_age_plant);
        mProductivity = (EditText) findViewById(R.id.et_productivity);
        mAdd = (Button) findViewById(R.id.btn_next);
    }

    private void setupControlEvents() {
        mPlant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditSeasonActivity.this, PlantSelectorActivity.class);
                startActivityForResult(intent, SELECT_PLANT_REQUEST_CODE);
            }
        });

        mStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(EditSeasonActivity.this, R.style.AppTheme_Dialog, EditSeasonActivity.this, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        mAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestEditSeason();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = getIntent();
        boolean isEnableBack = intent.getBooleanExtra("enableBack", true);

        if (isEnableBack) {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_PLANT_REQUEST_CODE && resultCode == RESULT_OK) {
            String name = data.getStringExtra("name");
            mSelectedPlantID = data.getLongExtra("id", 0);
            mPlant.setText(Utils.getValueFromMultiLanguageString(name));
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, month);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        this.mStartDateByTM = myCalendar.getTimeInMillis();
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);

        mStartDate.setText(sdf.format(myCalendar.getTime()));
    }

    private void requestEditSeason() {
        if (mSelectedPlantID == 0) {
            showErrorDialog(getString(R.string.edit_season_missing_plant));
            return;
        }

        if (mStartDateByTM == 0) {
            showErrorDialog(getString(R.string.edit_season_missing_start_date));
            return;
        }

        String age = mPlantAge.getText().toString();
        String productivity = mProductivity.getText().toString();

        if (age == null || age.trim().isEmpty()) {
            showErrorDialog(getString(R.string.edit_season_missing_age));
            return;
        }

        if (productivity == null || productivity.trim().isEmpty()) {
            showErrorDialog(getString(R.string.edit_season_missing_productivity));
            return;
        }

        long farmID = getIntent().getLongExtra("id", 0);

        showLoading();
        farmAPI.addSeason(farmID + "", mSelectedPlantID + "", mStartDateByTM + "", age, new APIHelper.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                EditSeasonActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        final AlertDialog dialog = new AlertDialog.Builder(EditSeasonActivity.this, R.style.AppTheme_Dialog_Alert)
                                .setTitle(R.string.app_name)
                                .setMessage(R.string.edit_season_success)
                                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();

                                        Intent intent = new Intent();
                                        setResult(RESULT_OK, intent);
                                        EditSeasonActivity.this.finish();
                                    }
                                }).create();
                        if (!dialog.isShowing()) {
                            dialog.show();
                        }
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                EditSeasonActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }
}
