package mobile.agrhub.farmbox.activities.add;


import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.FarmAPI;
import mobile.agrhub.farmbox.service.interfaces.UtilAPI;
import mobile.agrhub.farmbox.service.model.Device;
import mobile.agrhub.farmbox.service.model.Farm;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Constants;
import mobile.agrhub.farmbox.utils.GetQRCodeActivity;
import mobile.agrhub.farmbox.utils.ResourceUtils;

import static mobile.agrhub.farmbox.utils.Utils.PERMISSIONS_CAMERA_REQUEST;

public class AddByQRCodeActivity extends BaseActivity {

    private static int GET_QRCODE_REQUEST_CODE = 999;

    @BindView(R.id.iv_background)
    ImageView backgroundImageView;
    @BindView(R.id.iv_qrcode)
    ImageView theQRCodeImageVIew;
    @BindView(R.id.et_code)
    EditText codeEditText;
    @BindView(R.id.btn_activate)
    Button activateButton;

    private long addedControllerID;
    private long addedDeviceID;

    FarmAPI farmAPI = APIHelper.getFarmAPI();
    UtilAPI utilAPI = APIHelper.getUtilAPI();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_farm_by_qr);
        ButterKnife.bind(this);

        configUI();
        configActionBar();
        initGetLocationCallback();
    }

    @OnClick(R.id.iv_qrcode)
    void onQRCodeImageViewClick() {
        checkCameraPermission();
    }

    @OnClick(R.id.btn_activate)
    void onActivateButtonClick() {
        doActivate();
    }

    private void configActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void initGetLocationCallback() {
        setGetCurrentLocationCallback(new GetCurrentLocationCallback() {
            @Override
            public void afterGotCurrentLocation(double lat, double lon) {
                AddByQRCodeActivity.this.afterGotCurrentLocation(lat, lon);
            }

            @Override
            public void afterGetCurrentLocationFail() {
                AddByQRCodeActivity.this.afterGetCurrentLocationFail();
            }
        });
    }

    private void doActivate() {
        String code = codeEditText.getText().toString();

        if (code == null || code.isEmpty()) {
            int type = getIntent().getIntExtra("type", -1);

            if (type == Farm.ADD_DEVICE) {
                showErrorDialog(getString(R.string.add_device_serial_missing));
            } else {
                showErrorDialog(getString(R.string.add_container_serial_missing));
            }
            return;
        }

        int type = getIntent().getIntExtra("type", -1);

        if (type == Farm.CONTAINER) {
            getCurrentLocationAndContinue();
        } else {
            String farmID = FarmBoxAppController.getInstance().getSelectedFarmId() + "";
            showLoading();

            utilAPI.addDevice(farmID, code, new APIHelper.Callback<Device>() {
                @Override
                public void onSuccess(final Device data) {
                    AddByQRCodeActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();
                            FarmBoxAppController.getInstance().setNeedLoadFarmList(true);

                            Gson gson = new Gson();
                            String deviceInfoStr = gson.toJson(data);

                            Intent intent = new Intent(AddByQRCodeActivity.this, AddDeviceSetNameActivity.class);
                            intent.putExtra("deviceName", data.device_name);
                            intent.putExtra("deviceID", data.device_id + "");
                            if (data.device_name == ResourceUtils.BROADLINK_SP3_SMART_PLUG
                                    || data.device_name == ResourceUtils.TYPE_SENSE_PLUG
                                    || data.device_name == ResourceUtils.KAMOER_DRIPPING_PRO) {
                                intent.putExtra("controllerID", data.controllers.get(0).controller_id + "");
                            }
                            intent.putExtra(Constants.DEVICE_INFO_KEY, deviceInfoStr);

                            startActivity(intent);
                            finish();
                        }
                    });
                }

                @Override
                public void onFail(final int errorCode, final String error) {
                    AddByQRCodeActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();
                            handleError(errorCode, error);
                        }
                    });
                }
            });
        }
        return;
    }

//    private boolean isSensor(AddDeviceResponse data) {
//        try {
//            addedDeviceID = data.device_id;
//            if (data.device_name == ResourceUtils.BROADLINK_SP3_SMART_PLUG) {
//                addedControllerID = data.controllers.get(0).controller_id;
//                return false;
//            }
//
//            return true;
//        }
//        catch (Exception ex) {
//            ex.printStackTrace();
//        }
//
//
//        return true;
//    }

    private void afterGotCurrentLocation(double lat, double lon) {
        String code = codeEditText.getText().toString();
        showLoading();

        farmAPI.addContainer(code, lat + "", lon + "", new APIHelper.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                AddByQRCodeActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        final AlertDialog dialog = new AlertDialog.Builder(AddByQRCodeActivity.this, R.style.AppTheme_Dialog_Alert)
                                .setTitle(R.string.app_name)
                                .setMessage(R.string.add_container_success)
                                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        AddByQRCodeActivity.this.finish();
                                    }
                                }).create();
                        if (!dialog.isShowing()) {
                            dialog.show();
                        }
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                AddByQRCodeActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }

    private void afterGetCurrentLocationFail() {
        showErrorDialog(getString(R.string.dialog_message_get_location_fail));
    }

    private void configUI() {
        int type = getIntent().getIntExtra("type", -1);

        if (type == Farm.CONTAINER) {
            getSupportActionBar().setTitle(R.string.add_farm_container);
            backgroundImageView.setImageResource(R.drawable.add_farm_device_background);
        } else {
            getSupportActionBar().setTitle(R.string.add_farm_device);
            backgroundImageView.setImageResource(R.drawable.add_farm_container_background);
        }
    }

    private void doMoveOn() {
        Intent intent = new Intent(AddByQRCodeActivity.this, GetQRCodeActivity.class);
        AddByQRCodeActivity.this.startActivityForResult(intent, GET_QRCODE_REQUEST_CODE);
    }

    void checkCameraPermission() {
        List<String> permissions = new ArrayList<>();
        if (ContextCompat.checkSelfPermission(AddByQRCodeActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            doMoveOn();
        } else {
            permissions.add(Manifest.permission.CAMERA);
            String[] perArr = new String[permissions.size()];
            perArr = permissions.toArray(perArr);
            ActivityCompat.requestPermissions(AddByQRCodeActivity.this,
                    perArr,
                    PERMISSIONS_CAMERA_REQUEST);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSIONS_CAMERA_REQUEST) {
            if (grantResults.length > 0) {
                boolean isGrant = false;
                for (int gr : grantResults) {
                    if (gr == PackageManager.PERMISSION_GRANTED) {
                        isGrant = true;
                        break;
                    }
                }

                if (isGrant) {
                    doMoveOn();
                } else {
                    showErrorDialog(getString(R.string.dialog_message_missing_camera_permission));
                }
            } else {
                showErrorDialog(getString(R.string.dialog_message_missing_camera_permission));
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GET_QRCODE_REQUEST_CODE && resultCode == RESULT_OK) {
            codeEditText.setText(data.getStringExtra("result"));
        }
    }
}
