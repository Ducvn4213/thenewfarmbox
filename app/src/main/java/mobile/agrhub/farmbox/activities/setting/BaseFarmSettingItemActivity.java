package mobile.agrhub.farmbox.activities.setting;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.FarmAPI;
import mobile.agrhub.farmbox.service.model.FarmSettingDetailItem;
import mobile.agrhub.farmbox.service.model.UnitItem;
import mobile.agrhub.farmbox.service.network.Param;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Constants;

public class BaseFarmSettingItemActivity extends BaseActivity implements APIHelper.Callback {

    protected long farmId, factoryId;
    protected FarmSettingDetailItem item;

    protected FarmAPI farmAPI = APIHelper.getFarmAPI();

    protected int contentView() {
        return 0;
    }

    protected void initView() {

    }

    protected boolean showApply() {
        return true;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(this.contentView());
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        Intent intent = getIntent();
        farmId = intent.getLongExtra(Constants.FARM_ID_KEY, 0);
        factoryId = intent.getLongExtra(Constants.FACTORY_ID_KEY, 0);
        String settingInfo = intent.getStringExtra(Constants.SETTING_INFO_KEY);
        if (farmId <= 0 || settingInfo == null || settingInfo.isEmpty()) {
            finish();
            return;
        } else {
            Gson gson = new Gson();
            item = gson.fromJson(settingInfo, FarmSettingDetailItem.class);
            if (UnitItem.TEMPERATURE_DOT_C_VALUE.equals(item.getUnit())) {
                FarmBoxAppController app = FarmBoxAppController.getInstance();
                if (app.isTemperatureFUnit()) {
                    String title = item.getName();
                    title = title.replace(getString(R.string.temperature_unit_c), getString(R.string.temperature_unit_f));
                    setTitle(title);
                } else {
                    setTitle(item.getName());
                }
            } else if (UnitItem.NUTRITION_MILLISIEMENS_VALUE.equals(item.getUnit())) {
                FarmBoxAppController app = FarmBoxAppController.getInstance();
                if (app.isNutritionPPMUnit()) {
                    String title = item.getName();
                    title = title.replace(getString(R.string.nutrition_unit_ms_cm), getString(R.string.nutrition_unit_ppm));
                    setTitle(title);
                } else {
                    setTitle(item.getName());
                }
            } else {
                setTitle(item.getName());
            }
        }

        ButterKnife.bind(this);

        this.initView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (showApply()) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.farm_setting_item_menu, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_apply:
                applyChange();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected boolean validate() {
        return true;
    }

    protected List<Param> buildParam(List<Param> params) {
        return params;
    }

    protected void applyChange() {
        if (validate()) {
            List<Param> params = new ArrayList<>();
            params = buildParam(params);

            showLoading();
            farmAPI.saveFarmSetting(factoryId + "", farmId + "", params, this);
        }
    }

    @Override
    public void onSuccess(Object data) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideLoading();
                BaseFarmSettingItemActivity.this.closeAndReloadSetting();
            }
        });
    }

    @Override
    public void onFail(final int errorCode, final String error) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideLoading();
                handleError(errorCode, error);
            }
        });
    }

    public void closeAndReloadSetting() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
