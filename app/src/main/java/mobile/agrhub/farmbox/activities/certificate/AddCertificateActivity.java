package mobile.agrhub.farmbox.activities.certificate;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.dmcbig.mediapicker.PickerConfig;
import com.dmcbig.mediapicker.entity.Media;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.activities.common.SingleChooseActivity;
import mobile.agrhub.farmbox.custom_view.cell.InputChooseOptionCellView;
import mobile.agrhub.farmbox.custom_view.cell.InputPhotosCellView;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.model.FarmBasic;
import mobile.agrhub.farmbox.service.model.GapCertificate;
import mobile.agrhub.farmbox.service.model.OptionItem;
import mobile.agrhub.farmbox.service.model.Photo;
import mobile.agrhub.farmbox.service.model.UploadImageItem;
import mobile.agrhub.farmbox.service.network.Network;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Constants;
import mobile.agrhub.farmbox.utils.Utils;

public class AddCertificateActivity extends BaseActivity implements InputPhotosCellView.InputPhotosCellListener {
    private static final int REQUEST_SELECT_FARM = 204;

    @BindView(R.id.ic_farm_id)
    InputChooseOptionCellView mFarm;
    @BindView(R.id.ic_product_photos)
    InputPhotosCellView mPhotos;

    private GapCertificate certificate;
    private List<String> mediaUrlList;
    private int photoIdx;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_certificate);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        String productStr = intent.getStringExtra(Constants.GAP_CERTIFICATE_INFO_KEY);
        if (productStr != null && !productStr.isEmpty()) {
            Gson gson = new Gson();
            certificate = gson.fromJson(productStr, GapCertificate.class);
        }

        configActionBar();

        mFarm.setTitle(R.string.farm);
        if (certificate == null) {
            mFarm.setValue("", InputChooseOptionCellView.NONE);
        } else {
            FarmBoxAppController app = FarmBoxAppController.getInstance();
            List<FarmBasic> farms = app.getFarmList();

            String name = "";
            long val = InputChooseOptionCellView.NONE;
            if (farms != null && farms.size() > 0) {
                for (FarmBasic item : farms) {
                    if (item.farm_id == certificate.farmId) {
                        name = item.farm_name;
                        val = item.farm_id;
                        break;
                    }
                }
            }

            mFarm.setValue(name, val);
        }
        mFarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseFarm();
            }
        });

        mPhotos.setTitle(R.string.certificate_photos);
        if (certificate != null && certificate.gapCertificateMedia != null && certificate.gapCertificateMedia.length > 0) {
            Photo photo;
            for (String url : certificate.gapCertificateMedia) {
                photo = new Photo(url);
                mPhotos.addPhoto(photo, false);
            }
        }
        mPhotos.setListener(this);
    }

    private void configActionBar() {
        if (certificate == null) {
            getSupportActionBar().setTitle(R.string.add_certificate);
        } else {
            getSupportActionBar().setTitle(R.string.edit_certificate);
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.save_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.action_save) {
            if (validate()) {
                save();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == InputPhotosCellView.REQUEST_CHOOSE_PHOTO_CODE && resultCode == PickerConfig.RESULT_CODE) {
            List<Media> mediaList = data.getParcelableArrayListExtra(PickerConfig.EXTRA_RESULT);
            mPhotos.addMediaList(mediaList);
        } else if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_SELECT_FARM) {
                String selectedVal = data.getStringExtra(Constants.SELECTED_OPTION_KEY);
                if (selectedVal != null && !selectedVal.isEmpty()) {
                    long farmId = Long.parseLong(selectedVal);

                    FarmBoxAppController app = FarmBoxAppController.getInstance();
                    List<FarmBasic> farmList = app.getFarmList();
                    if (farmList != null && farmList.size() > 0) {
                        for (FarmBasic farm : farmList) {
                            if (farmId == farm.farm_id) {
                                mFarm.setValue(farm.farm_name, farm.farm_id);
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    private boolean validate() {
        if (mFarm.isEmpty()) {
            showErrorDialog(getString(R.string.field_is_required, mFarm.getTitle()));
            return false;
        }

        if (mPhotos.isEmpty()) {
            showErrorDialog(getString(R.string.field_is_required, mPhotos.getTitle()));
            return false;
        }

        return true;
    }

    private void save() {
        showLoading();

        photoIdx = -1;
        if (mediaUrlList == null) {
            mediaUrlList = new ArrayList<>();
        } else {
            mediaUrlList.clear();
        }

        uploadMedia();
    }

    private void uploadMedia() {
        photoIdx += 1;
        if (photoIdx < mPhotos.size()) {
            Photo photo = mPhotos.get(photoIdx);
            if (photo != null) {
                if (photo.isOnServer) {
                    mediaUrlList.add(photo.uri);

                    uploadMedia();
                } else {
                    APIHelper.getUtilAPI().uploadFile(photo.media, new APIHelper.Callback<UploadImageItem>() {
                        @Override
                        public void onSuccess(UploadImageItem data) {
                            mediaUrlList.add(data.file_url);

                            uploadMedia();
                        }

                        @Override
                        public void onFail(int errorCode, String error) {
                            if (errorCode == Network.DEMO_MODE_ERROR) {
                                AddCertificateActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Utils.showDialog(AddCertificateActivity.this, getString(R.string.demo_mode), getString(R.string.demo_mode_message));
                                    }
                                });
                            } else {
                                uploadMedia();
                            }
                        }
                    });
                }
            }
        } else {
            saveCertificate();
        }
    }

    private void saveCertificate() {
        if (certificate != null) {
            APIHelper.getGapAPI().updateCertificate(certificate.gapCertificateId, this.mediaUrlList, true, new APIHelper.Callback<GapCertificate>() {
                @Override
                public void onSuccess(GapCertificate data) {
                    AddCertificateActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();
                            AddCertificateActivity.this.finish();
                        }
                    });
                }

                @Override
                public void onFail(final int errorCode, final String error) {
                    AddCertificateActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();
                            handleError(errorCode, error);
                        }
                    });
                }
            });
        } else {
            APIHelper.getGapAPI().createCertificate(mFarm.getVal(), this.mediaUrlList, new APIHelper.Callback<GapCertificate>() {
                @Override
                public void onSuccess(GapCertificate data) {
                    AddCertificateActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();
                            AddCertificateActivity.this.finish();
                        }
                    });
                }

                @Override
                public void onFail(final int errorCode, final String error) {
                    AddCertificateActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();
                            handleError(errorCode, error);
                        }
                    });
                }
            });
        }
    }

    private void chooseFarm() {
        List<OptionItem> options = new ArrayList<>();
        OptionItem optionItem;

        FarmBoxAppController app = FarmBoxAppController.getInstance();
        if (app.getFarmList() != null && app.getFarmList().size() > 0) {
            for (FarmBasic farm : app.getFarmList()) {
                optionItem = new OptionItem();
                optionItem.setKey(farm.farm_id + "");
                optionItem.setValue(farm.farm_name);
                optionItem.setChecked(farm.farm_id == mFarm.getVal());

                options.add(optionItem);
            }
        }

        Gson gson = new Gson();
        Intent intent = new Intent(this, SingleChooseActivity.class);
        intent.putExtra(Constants.SCREEN_TITLE_KEY, getString(R.string.choose_farm));
        intent.putExtra(Constants.OPTION_LIST_KEY, gson.toJson(options));

        startActivityForResult(intent, REQUEST_SELECT_FARM);
    }

}
