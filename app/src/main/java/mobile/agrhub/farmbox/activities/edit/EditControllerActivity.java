package mobile.agrhub.farmbox.activities.edit;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.UtilAPI;
import mobile.agrhub.farmbox.utils.BaseActivity;

public class EditControllerActivity extends BaseActivity {

    EditText mName;
    Button mDone;

    UtilAPI utilAPI = APIHelper.getUtilAPI();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_device);

        bindingControls();
        setupControlEvent();
        initData();
    }

    private void bindingControls() {
        mName = (EditText) findViewById(R.id.et_name);
        mDone = (Button) findViewById(R.id.btn_set);
    }

    private void setupControlEvent() {
        mDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestEditName();
            }
        });
    }

    private void initData() {
        String name = getIntent().getStringExtra("name");

        if (name != null && !name.isEmpty()) {
            mName.setText(name);
        }
    }

    private void requestEditName() {
        long theID = getIntent().getLongExtra("id", -1);
        String newName = mName.getText().toString();


        showLoading();
        utilAPI.editDeviceName(String.valueOf(theID), newName, new APIHelper.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                EditControllerActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateCurrentData();
                        final AlertDialog dialog = new AlertDialog.Builder(EditControllerActivity.this, R.style.AppTheme_Dialog_Alert)
                                .setTitle(R.string.app_name)
                                .setMessage(R.string.edit_controller_success)
                                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        EditControllerActivity.this.finish();
                                    }
                                }).create();
                        if (!dialog.isShowing()) {
                            dialog.show();
                        }
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                EditControllerActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }

    private void updateCurrentData() {
        long theID = getIntent().getLongExtra("id", -1);

        //TODO: a lot of things
//        List<Farm> farmList = FBHelper.getInstance().currentGetFarmResponse.farms;
//        for (Farm f : farmList) {
//            for (Device d : f.devices) {
//                if (d.device_id == theID) {
//                    d.device_custom_name = mName.getText().toString();
//                    return;
//                }
//            }
//        }
    }
}
