package mobile.agrhub.farmbox.activities.plant;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.adapters.PlantAdapter;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.PlantAPI;
import mobile.agrhub.farmbox.service.model.PlantBasic;
import mobile.agrhub.farmbox.service.model.response.GetPlantBasicResponse;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.EndlessRecyclerViewScrollListener;
import mobile.agrhub.farmbox.utils.Utils;

public class PlantSelectorActivity extends BaseActivity {
    @BindView(R.id.container_view)
    LinearLayout mContainerView;

    @BindView(R.id.search_view)
    SearchView mSearchView;

    @BindView(R.id.rv_plant_list)
    RecyclerView mPlantList;

    int dataOffset = 0;
    String query = "";
    PlantAdapter mAdapter;
    EndlessRecyclerViewScrollListener scrollListener;

    PlantAPI plantAPI = APIHelper.getPlantAPI();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plant_selector);
        ButterKnife.bind(this);

        initData();
        configActionBar();
    }

    private void configActionBar() {
        getSupportActionBar().setTitle(R.string.plant_selector_title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void setupControlEvents() {
        mAdapter.setOnBackListener(new PlantAdapter.BackListener() {
            @Override
            public void onBackPressed() {
                PlantSelectorActivity.this.setResult(RESULT_CANCELED);
                PlantSelectorActivity.this.finish();
            }
        });

        mAdapter.setOnItemSelectedListener(new PlantAdapter.PlantItemSelectedListener() {
            @Override
            public void onPlantItemSelected(int index, PlantBasic data) {
                requestShowOptionPlant(data);
            }
        });

//        mAdapter.setOnSearchWithTextCallback(new PlantAdapter.OnSearchWithTextCallback() {
//            @Override
//            public void onSearchWithText(String text) {
//                if (text.isEmpty()) {
//                    mAdapter.setData(rootData);
//                    return;
//                }
//
//                mAdapter.setData(getPlantListWith(text));
//            }
//        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void initBaseData() {
        mSearchView.setActivated(true);
        mSearchView.setQueryHint(getString(R.string.plant_selector_search_hint));
        mSearchView.onActionViewExpanded();
        mSearchView.setIconified(false);
        mSearchView.clearFocus();
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                PlantSelectorActivity.this.dataOffset = 0;
                PlantSelectorActivity.this.query = query;
                PlantSelectorActivity.this.getPlantData();
                PlantSelectorActivity.this.mSearchView.clearFocus();

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.isEmpty()) {
                    PlantSelectorActivity.this.dataOffset = 0;
                    PlantSelectorActivity.this.query = "";
                    PlantSelectorActivity.this.getPlantData();

                    return true;
                }
                return false;
            }
        });

        mAdapter = new PlantAdapter(getApplicationContext(), new ArrayList<PlantBasic>());
        mPlantList.setAdapter(mAdapter);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mPlantList.getContext(), DividerItemDecoration.VERTICAL);
        mPlantList.addItemDecoration(dividerItemDecoration);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PlantSelectorActivity.this);
        mPlantList.setLayoutManager(linearLayoutManager);
        mPlantList.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                PlantSelectorActivity.this.loadMore();
            }
        });
    }

    private void initData() {
        this.initBaseData();
        this.getPlantData();
    }


    private void getPlantData() {
        showLoading();
        String language = FarmBoxAppController.getInstance().getLanguage();
        plantAPI.searchPlant(dataOffset, language, this.query, new APIHelper.Callback<GetPlantBasicResponse>() {
            @Override
            public void onSuccess(final GetPlantBasicResponse data) {
                PlantSelectorActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handlePlantData(data.plants);
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                PlantSelectorActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }

    private void loadMore() {
        showLoading();
        dataOffset += 1;
        getPlantData();
    }

    private void handlePlantData(List<PlantBasic> plantData) {
        if (null == this.query || this.query.isEmpty()) {
            if (null != this.mSearchView) {
                mSearchView.clearFocus();
            }

            if (null != this.mContainerView) {
                mContainerView.requestFocus();
            }
        }

        if (dataOffset == 0) {
            mAdapter.setData(plantData);
        } else {
            mAdapter.appendData(plantData);
        }

        setupControlEvents();
    }

    private void requestShowOptionPlant(final PlantBasic data) {
        final Dialog dialog = new Dialog(PlantSelectorActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_option_plant);

        TextView name = (TextView) dialog.findViewById(R.id.tv_name);
        TextView sname = (TextView) dialog.findViewById(R.id.tv_scientific_name);
        TextView viewDetail = (TextView) dialog.findViewById(R.id.tv_view_detail);
        TextView choose = (TextView) dialog.findViewById(R.id.tv_choose);
        TextView cancel = (TextView) dialog.findViewById(R.id.tv_cancel);

        name.setText(Utils.getValueFromMultiLanguageString(data.plant_name));
        sname.setText(data.plant_scientific_name);

        viewDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PlantSelectorActivity.this, PlantDetailActivity.class);
                intent.putExtra("id", data.plant_id);
                startActivity(intent);
                dialog.dismiss();
            }
        });

        choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("id", data.plant_id);
                intent.putExtra("name", data.plant_name);
                setResult(RESULT_OK, intent);
                dialog.dismiss();
                finish();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

}
