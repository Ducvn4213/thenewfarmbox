package mobile.agrhub.farmbox.activities.registration;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.activities.StartActivity;
import mobile.agrhub.farmbox.activities.TabActivity;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.UserAPI;
import mobile.agrhub.farmbox.service.model.User;
import mobile.agrhub.farmbox.service.model.response.VerifyCodeResponse;
import mobile.agrhub.farmbox.service.network.Config;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Utils;

public class PhoneRegistrationConfirmActivity extends BaseActivity {

    Button mContinue;
    TextView mPhone;
    EditText mCode;

    UserAPI userAPI = APIHelper.getUserAPI();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.phone_registration_confirm_activity);

        bindingControl();
        setupControlEvents();
        initData();
    }

    public void bindingControl() {
        mContinue = (Button) findViewById(R.id.btn_continue);
        mPhone = (TextView) findViewById(R.id.tv_phone);
        mCode = (EditText) findViewById(R.id.et_code);
    }

    public void setupControlEvents() {
        mContinue.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                checkCode();
            }
        });
    }

    public void initData() {
        String phone = getIntent().getStringExtra("phone");
        mPhone.setText(phone);
    }

    public void checkCode() {
        String code = mCode.getText().toString();
        if (code.isEmpty()) {
            String errorMessage = getString(R.string.phone_registration_error_empty_code);
            showErrorDialog(errorMessage);
            return;
        }

        showLoading();
        final String phone = mPhone.getText().toString();

        userAPI.verifyCode(PhoneRegistrationConfirmActivity.this, phone, code, new APIHelper.Callback<VerifyCodeResponse>() {
            @Override
            public void onSuccess(final VerifyCodeResponse data) {
                FarmBoxAppController app = FarmBoxAppController.getInstance();
                app.setUser(null, data.user_token);

                if (data.is_new_user) {
                    PhoneRegistrationConfirmActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            PhoneRegistrationConfirmActivity.this.gotoPersonalInformation(phone);
                        }
                    });
                } else {
                    PhoneRegistrationConfirmActivity.this.getUserInfo(data.user_token);
                }
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                PhoneRegistrationConfirmActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }

    private void getUserInfo(final String token) {
        userAPI.getUserInfo(this, token, new APIHelper.Callback<User>() {
            @Override
            public void onSuccess(User user) {
                Utils.savePreference(PhoneRegistrationConfirmActivity.this, Config.USER_TOKEN_KEY, token);

                FarmBoxAppController app = FarmBoxAppController.getInstance();
                user = app.loadSettings(PhoneRegistrationConfirmActivity.this, user);
                app.setUser(user, token);
                app.setNeedLoadFarmList(true);

                PhoneRegistrationConfirmActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        Intent intent = new Intent(PhoneRegistrationConfirmActivity.this, TabActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                PhoneRegistrationConfirmActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }

    public void gotoPersonalInformation(String phone) {
        hideLoading();
        Intent intent = new Intent(PhoneRegistrationConfirmActivity.this, PhoneRegistrationPersonalInformationActivity.class);
        intent.putExtra("user_phone", phone);
        startActivity(intent);
        finish();
    }

    public void gotoDashboard() {
        hideLoading();

        FarmBoxAppController app = FarmBoxAppController.getInstance();
        app.setDemoMode(false);

        Intent intent = new Intent(PhoneRegistrationConfirmActivity.this, StartActivity.class);
        startActivity(intent);
        finish();
    }
}
