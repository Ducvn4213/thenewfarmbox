package mobile.agrhub.farmbox.activities.setting;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.List;

import butterknife.BindView;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.adapters.SettingDeviceAdapter;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.DeviceAPI;
import mobile.agrhub.farmbox.service.interfaces.FarmAPI;
import mobile.agrhub.farmbox.service.model.Device;
import mobile.agrhub.farmbox.service.model.Farm;
import mobile.agrhub.farmbox.utils.Constants;

public class SettingDeviceActivity extends BaseFarmSettingItemActivity implements SettingDeviceAdapter.CallBackListener {
    private static final int REQUEST_CHANGE_DEVICE_INFO = 202;

    @BindView(R.id.rv_content)
    RecyclerView mRecycleView;
    @BindView(R.id.tv_message)
    TextView tvMessage;

    List<Device> deviceList;
    SettingDeviceAdapter mAdapter;

    @Override
    protected int contentView() {
        return R.layout.activity_setting_device;
    }

    @Override
    protected void initView() {
        super.initView();
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(SettingDeviceActivity.this, DividerItemDecoration.VERTICAL);
        mRecycleView.addItemDecoration(dividerItemDecoration);

        mAdapter = new SettingDeviceAdapter(this);
        mRecycleView.setAdapter(mAdapter);
        mAdapter.setListener(this);

        loadData();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CHANGE_DEVICE_INFO && resultCode == Activity.RESULT_OK) {
            loadData();
        }
    }

    @Override
    protected boolean showApply() {
        return false;
    }

    private void loadData() {
        showLoading();
        FarmAPI farmAPI = APIHelper.getFarmAPI();
        farmAPI.getFarmByID(farmId, new APIHelper.Callback<Farm>() {
            @Override
            public void onSuccess(final Farm data) {
                SettingDeviceActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        if (data != null && data.devices != null && data.devices.size() > 0) {
                            deviceList = data.devices;
                            refreshView();
                        } else {
                            onNoFarm();
                        }
                    }
                });
            }

            @Override
            public void onFail(int errorCode, String error) {
                SettingDeviceActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        onNoFarm();
                    }
                });
            }
        });
    }

    private void refreshView() {
        tvMessage.setText(R.string.device_setting_hint);

        mAdapter.setData(this.deviceList);
    }

    private void onNoFarm() {
        tvMessage.setText(R.string.device_setting_no_device);
    }

    @Override
    public void onChanged(String deviceId, boolean val) {
        showLoading();
        DeviceAPI deviceAPI = APIHelper.getDeviceAPI();
        deviceAPI.activeDevice(deviceId, val, new APIHelper.Callback<String>() {
            @Override
            public void onSuccess(final String data) {
                SettingDeviceActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                SettingDeviceActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);

                        mAdapter.notifyDataSetChanged();
                    }
                });
            }
        });
    }

    @Override
    public void onSelected(Device device) {
        Gson gson = new Gson();
        Intent intent = new Intent(this, SettingDeviceDetailActivity.class);
        intent.putExtra(Constants.DEVICE_INFO_KEY, gson.toJson(device));

        startActivityForResult(intent, REQUEST_CHANGE_DEVICE_INFO);
    }
}
