package mobile.agrhub.farmbox.activities.dairy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.adapters.ChooseGapTaskAdapter;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.GapAPI;
import mobile.agrhub.farmbox.service.model.GapTask;
import mobile.agrhub.farmbox.service.model.KeyValue;
import mobile.agrhub.farmbox.service.model.response.GetGapTaskResponse;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Utils;

public class ChooseDiaryActivityTypeActivity extends BaseActivity {

    SearchView mSearchView;
    ListView mListView;
    ChooseGapTaskAdapter gapTaskAdapter;
    List<GapTask> gapTasks;
    List<Long> selectedList;

    GapAPI gapAPI = APIHelper.getGapAPI();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_diary_activity_type);

        bindingControls();
        initData();
        setupControlEvents();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void bindingControls() {
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mSearchView = (SearchView) findViewById(R.id.search_view);
        mListView = (ListView) findViewById(R.id.listView);
    }

    private void initData() {
        showLoading();

        selectedList = new ArrayList<>();
        Bundle bundle = getIntent().getExtras();
        if (bundle.containsKey("SELECTED_ACTIVITY_TYPE_LIST")) {
            String selectedValue = bundle.getString("SELECTED_ACTIVITY_TYPE_LIST");
            if (selectedValue != null && !selectedValue.isEmpty()) {
                String[] idArray = selectedValue.split(";");
                for (String item : idArray) {
                    selectedList.add(Long.valueOf(item));
                }
            }
        }

        gapAPI.getGAPTaskList(new APIHelper.Callback<GetGapTaskResponse>() {
            @Override
            public void onSuccess(GetGapTaskResponse data) {
                gapTasks = data.gapTasks;
                if (selectedList.size() > 0) {
                    for (GapTask gapTask : gapTasks) {
                        if (selectedList.contains(Long.valueOf(gapTask.gapTaskId))) {
                            gapTask.isChecked = true;
                        } else {
                            gapTask.isChecked = false;
                        }
                    }
                }

                ChooseDiaryActivityTypeActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();

                        gapTaskAdapter = new ChooseGapTaskAdapter(ChooseDiaryActivityTypeActivity.this, gapTasks);
                        mListView.setAdapter(gapTaskAdapter);
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                ChooseDiaryActivityTypeActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });

    }

    private void setupControlEvents() {
        mSearchView.setActivated(true);
        mSearchView.setQueryHint(getString(R.string.add_activity_search_hint));
        mSearchView.onActionViewExpanded();
        mSearchView.setIconified(false);
        mSearchView.clearFocus();
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                ChooseDiaryActivityTypeActivity.this.gapTaskAdapter.getFilter().filter(newText);
                return false;
            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i < gapTasks.size()) {
                    GapTask gapTask = gapTasks.get(i);
                    gapTask.isChecked = !gapTask.isChecked;

                    ChooseDiaryActivityTypeActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            gapTaskAdapter.notifyDataSetChanged();
                        }
                    });
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.choose_diary_task_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_ok:
                dismissView();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void dismissView() {
        Gson gson = new Gson();

        KeyValue obj;
        List<KeyValue> selectedTasks = new ArrayList<KeyValue>();
        for (GapTask gapTask : gapTasks) {
            if (gapTask.isChecked) {
                obj = new KeyValue(gapTask.gapTaskId, Utils.getValueFromMultiLanguageString(gapTask.gapTaskTitle));
                selectedTasks.add(obj);
            }
        }

        // Prepare data intent
        Intent data = new Intent();
        data.putExtra("TAG", "CHOOSE_DIARY_ACTIVITY_TYPE");
        data.putExtra("SELECTED_VALUE", gson.toJson(selectedTasks));
        // Activity finished ok, return the data
        ChooseDiaryActivityTypeActivity.this.setResult(Activity.RESULT_OK, data);
        ChooseDiaryActivityTypeActivity.this.finish();

    }
}
