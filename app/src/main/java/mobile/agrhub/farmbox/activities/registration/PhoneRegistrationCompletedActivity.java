package mobile.agrhub.farmbox.activities.registration;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.activities.TabActivity;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.UserAPI;
import mobile.agrhub.farmbox.service.model.User;
import mobile.agrhub.farmbox.service.network.Config;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Utils;

public class PhoneRegistrationCompletedActivity extends BaseActivity {

    private final int PICK_IMAGE_REQUEST_CODE = 9999;

    String userPhone;
    String avatarUrl;
    ImageButton mCompletedImage;
    Button mContinue;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.phone_registration_completed_activity);

        Intent intent = getIntent();
        userPhone = intent.getStringExtra("user_phone");
        avatarUrl = intent.getStringExtra("user_avatar");
        if (userPhone == null || userPhone.isEmpty()) {
            finish();
        }

        bindingControl();
        setupControlEvents();
        initData();
    }

    public void bindingControl() {
        mCompletedImage = (ImageButton) findViewById(R.id.ib_completed_image);
        mContinue = (Button) findViewById(R.id.btn_continue);
    }

    public void setupControlEvents() {
        mContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    public void initData() {
        Bitmap defaultAvatar = BitmapFactory.decodeResource(PhoneRegistrationCompletedActivity.this.getResources(), R.drawable.profile);
        mCompletedImage.setImageBitmap(Utils.getCircleBitmap(defaultAvatar));
    }

    private void login() {
        showLoading();
        final String token = FarmBoxAppController.getInstance().getAccessToken();
        UserAPI userAPI = APIHelper.getUserAPI();
        userAPI.getUserInfo(this, token, new APIHelper.Callback<User>() {
            @Override
            public void onSuccess(User user) {
                Utils.savePreference(PhoneRegistrationCompletedActivity.this, Config.USER_TOKEN_KEY, token);

                FarmBoxAppController app = FarmBoxAppController.getInstance();
                user = app.loadSettings(PhoneRegistrationCompletedActivity.this, user);
                app.setUser(user, token);
                app.setNeedLoadFarmList(true);

                PhoneRegistrationCompletedActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        Intent intent = new Intent(PhoneRegistrationCompletedActivity.this, TabActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                PhoneRegistrationCompletedActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }
}
