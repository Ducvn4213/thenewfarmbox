package mobile.agrhub.farmbox.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import java.util.Locale;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.UserAPI;
import mobile.agrhub.farmbox.service.model.User;
import mobile.agrhub.farmbox.service.network.Config;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Utils;
import pl.droidsonroids.gif.GifImageView;

public class StartActivity extends BaseActivity {
//    Farm CongLN
//    private static final String DEVELOP_MODE_WITH_TOKEN = "OGEyYjg4NTBlYTQzYmYzNWE0Mjg3YTJiZjA4N2M0NmI=";
//    private static final String DEVELOP_MODE_WITH_TOKEN = "ZjA4ZDFjMmQ3MzBmYjBhNzk2ZjlmYTkxMzZjZWM0MWI=";

//    Farm Anh Minh
//    private static final String DEVELOP_MODE_WITH_TOKEN = "ODZjNzI2NmE0ODkzODM3Mjc2NzFhYmM4YzMwOTdmMTA=";

    private static final String DEVELOP_MODE_WITH_TOKEN = "";

    GifImageView mGifPlayer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FarmBoxAppController app = FarmBoxAppController.getInstance();
        String lang = Utils.getPreference(this, "FARM_BOX_LANG");
        if (null == lang || lang.isEmpty()) {
            lang = Locale.getDefault().getLanguage();
            app.setLanguage(lang);
        } else {
            String appLang = app.getLanguage();
            if (null == appLang || appLang.isEmpty()) {
                app.setLanguage(lang);

                String deviceLang = Locale.getDefault().getLanguage();
                if (!lang.equals(deviceLang)) {
                    recreate();
                    return;
                }
            } else if (!lang.equals(appLang)) {
                app.setLanguage(lang);
                recreate();
                return;
            }
        }

        if (DEVELOP_MODE_WITH_TOKEN.isEmpty()) {
            String lastToken = Utils.getPreference(StartActivity.this, Config.USER_TOKEN_KEY);
            if (lastToken == null || lastToken.isEmpty()) {
                Intent intent = new Intent(StartActivity.this, IntroActivity.class);
                startActivity(intent);
                finish();
                return;
            } else {
                setContentView(R.layout.activity_start);

                loginWith(lastToken);
            }
        } else {
            loginWith(DEVELOP_MODE_WITH_TOKEN);
        }
    }

    private void gotoMain() {
        mGifPlayer = (GifImageView) findViewById(R.id.giv_gif_player);
        mGifPlayer.animate().alpha(0).setDuration(300);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(StartActivity.this, TabActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        }, 300);
    }

    private void loginWith(final String token) {
        showLoading();
        UserAPI userAPI = APIHelper.getUserAPI();
        userAPI.getUserInfo(this, token, new APIHelper.Callback<User>() {
            @Override
            public void onSuccess(User user) {
                FarmBoxAppController app = FarmBoxAppController.getInstance();
                user = app.loadSettings(StartActivity.this, user);
                app.setUser(user, token);
                app.setNeedLoadFarmList(true);

                StartActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        Intent intent = new Intent(StartActivity.this, TabActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                StartActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }
}
