package mobile.agrhub.farmbox.activities.setting;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.adapters.ControllerSettingAdapter;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.DeviceAPI;
import mobile.agrhub.farmbox.service.interfaces.UtilAPI;
import mobile.agrhub.farmbox.service.model.Controller;
import mobile.agrhub.farmbox.service.model.Device;
import mobile.agrhub.farmbox.service.network.Param;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Constants;
import mobile.agrhub.farmbox.utils.ResourceUtils;

public class SettingDeviceDetailActivity extends BaseActivity implements APIHelper.Callback<String> {

    @BindView(R.id.et_device_name)
    EditText mDeviceName;
    @BindView(R.id.ll_control_type_view)
    LinearLayout mControlTypeView;
    @BindView(R.id.tv_device_control_type)
    TextView mDeviceControlType;
    @BindView(R.id.tv_device_type)
    TextView mDeviceType;
    @BindView(R.id.tv_device_address)
    TextView mDeviceIPAddress;
    @BindView(R.id.ll_controller_view)
    CardView mControllerView;
    @BindView(R.id.rv_controller)
    RecyclerView mControllerList;

    ControllerSettingAdapter mAdapter;
    private int currentUpdateController = 0;
    private int currentControllerType = -1;

    Device device;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_device_detail);
        ButterKnife.bind(this);

        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        Intent intent = getIntent();
        String deviceInfo = intent.getStringExtra(Constants.DEVICE_INFO_KEY);
        if (deviceInfo == null || deviceInfo.isEmpty()) {
            finish();
            return;
        } else {
            Gson gson = new Gson();
            device = gson.fromJson(deviceInfo, Device.class);
            mDeviceName.setText(device.getDisplayName());
            mDeviceType.setText(device.getTypeName());
            mDeviceIPAddress.setText(device.device_mac_address);

            if (device.device_name == ResourceUtils.BROADLINK_SP3_SMART_PLUG
                    || device.device_name == ResourceUtils.KAMOER_DRIPPING_PRO) {
                mControlTypeView.setVisibility(View.VISIBLE);
                mControllerView.setVisibility(View.GONE);
                currentControllerType = device.getControlType();
                mDeviceControlType.setText(Controller.typeName(currentControllerType));
                mDeviceControlType.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        requestChangeType();
                    }
                });
            } else if (device.device_name == ResourceUtils.STORAGE_CONTROLLER) {
                mControlTypeView.setVisibility(View.GONE);

                mControllerView.setVisibility(View.VISIBLE);
                mAdapter = new ControllerSettingAdapter(this, device.controllers);
                mControllerList.setAdapter(mAdapter);
            } else {
                mControlTypeView.setVisibility(View.GONE);
                mControllerView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.farm_setting_item_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_apply:
                applyChange();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void applyChange() {
        String deviceName = mDeviceName.getText().toString().trim();
        if (deviceName.isEmpty()) {
            showErrorDialog(getString(R.string.field_is_required, getString(R.string.device_name)));
            return;
        }

        List<Param> params = new ArrayList<>();
        params.add(new Param("device_name", "device_custom_name"));
        params.add(new Param("device_value", deviceName));

        DeviceAPI deviceAPI = APIHelper.getDeviceAPI();
        deviceAPI.edit(device.device_id + "", params, this);
    }

    @Override
    public void onSuccess(String data) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (device.device_name == ResourceUtils.BROADLINK_SP3_SMART_PLUG
                        || device.device_name == ResourceUtils.KAMOER_DRIPPING_PRO) {
                    updateDeviceType();
                } else if (device.device_name == ResourceUtils.STORAGE_CONTROLLER) {
                    currentUpdateController = 0;
                    updateController();
                } else {
                    hideLoading();
                    addDeviceCompleted();
                }
            }
        });
    }

    @Override
    public void onFail(final int errorCode, final String error) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideLoading();
                handleError(errorCode, error);
            }
        });
    }

    private void updateDeviceType() {
        if (device.controllers != null && device.controllers.size() > 0) {
            Controller controller = device.controllers.get(0);
            UtilAPI utilAPI = APIHelper.getUtilAPI();
            utilAPI.editController(controller.controller_id + "", Constants.FIELD_CONTROLLER_TYPE, currentControllerType + "", new APIHelper.Callback<String>() {
                @Override
                public void onSuccess(String data) {
                    SettingDeviceDetailActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            addDeviceCompleted();
                        }
                    });
                }

                @Override
                public void onFail(final int errorCode, final String error) {
                    SettingDeviceDetailActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();
                            handleError(errorCode, error);
                        }
                    });
                }
            });
        } else {
            addDeviceCompleted();
        }
    }

    private void updateController() {
        if (device.controllers != null && currentUpdateController < device.controllers.size()) {
            Controller controller = device.controllers.get(currentUpdateController);
            UtilAPI utilAPI = APIHelper.getUtilAPI();
            utilAPI.editController(controller.controller_id + "", Constants.FIELD_IS_ACTIVE, controller.isActive ? "true" : "false", new APIHelper.Callback<String>() {
                @Override
                public void onSuccess(String data) {
                    SettingDeviceDetailActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            currentUpdateController += 1;
                            updateController();
                        }
                    });
                }

                @Override
                public void onFail(final int errorCode, final String error) {
                    SettingDeviceDetailActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();
                            handleError(errorCode, error);
                        }
                    });
                }
            });
        } else {
            addDeviceCompleted();
        }
    }

    private void addDeviceCompleted() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    private void requestChangeType() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        if (device.device_name == ResourceUtils.BROADLINK_SP3_SMART_PLUG) {
            dialog.setContentView(R.layout.layout_add_device_choose_type);

            TextView switchElem = (TextView) dialog.findViewById(R.id.tv_switch);
            TextView light = (TextView) dialog.findViewById(R.id.tv_light);
            TextView waterPump = (TextView) dialog.findViewById(R.id.tv_water_pump);
            TextView mistingPump = (TextView) dialog.findViewById(R.id.tv_misting_pump);
            TextView fan = (TextView) dialog.findViewById(R.id.tv_fan);
            TextView nutrientDosingPump = (TextView) dialog.findViewById(R.id.tv_nutrient_dosing_pump);
            TextView phDownDosingPump = (TextView) dialog.findViewById(R.id.tv_ph_down_dosing_pump);
            TextView phUpDosingPump = (TextView) dialog.findViewById(R.id.tv_ph_up_dosing_pump);

//            TextView airConditioner = (TextView) dialog.findViewById(R.id.tv_air_conditioner);

            switchElem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onChooseType(Controller.TYPE_SWITCH);
                    dialog.dismiss();
                }
            });
            light.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onChooseType(Controller.TYPE_LIGHT);
                    dialog.dismiss();
                }
            });
            waterPump.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onChooseType(Controller.TYPE_WATER_PUMP);
                    dialog.dismiss();
                }
            });
            mistingPump.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onChooseType(Controller.TYPE_MISTING_PUMP);
                    dialog.dismiss();
                }
            });
            fan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onChooseType(Controller.TYPE_FAN);
                    dialog.dismiss();
                }
            });
            nutrientDosingPump.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onChooseType(Controller.TYPE_NUTRIENT_DOSING_PUMP);
                    dialog.dismiss();
                }
            });
            phDownDosingPump.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onChooseType(Controller.TYPE_PH_DOWN_DOSING_PUMP);
                    dialog.dismiss();
                }
            });
            phUpDosingPump.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onChooseType(Controller.TYPE_PH_UP_DOSING_PUMP);
                    dialog.dismiss();
                }
            });
//            airConditioner.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    onChooseType(CONTROLLER_TYPE.AirConditioner);
//                    dialog.dismiss();
//                }
//            });
        } else if (device.device_name == ResourceUtils.KAMOER_DRIPPING_PRO) {
            dialog.setContentView(R.layout.layout_add_device_choose_pump_controller_type);

            TextView waterPump = (TextView) dialog.findViewById(R.id.tv_water_pump);
            TextView dosingPump = (TextView) dialog.findViewById(R.id.tv_dosing_pump);

            waterPump.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onChooseType(Controller.TYPE_WATER_PUMP);
                    dialog.dismiss();
                }
            });
            dosingPump.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onChooseType(Controller.TYPE_NUTRIENT_DOSING_PUMP);
                    dialog.dismiss();
                }
            });
        }

        dialog.show();
    }

    private void onChooseType(int controlType) {
        currentControllerType = controlType;
        mDeviceControlType.setText(Controller.typeName(currentControllerType));
    }
}
