package mobile.agrhub.farmbox.activities.registration;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.UserAPI;
import mobile.agrhub.farmbox.service.model.User;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Utils;

public class PhoneRegistrationPersonalInformationActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener {

    private static final String DATE_PICKER_START_DIALOG_TAG = "DATE_PICKER_START_DIALOG_TAG";

    AppCompatSpinner genderSpinner;
    Button birthButton;
    Button continueButton;
    EditText firstNameEditText;
    EditText lastNameEditText;
    EditText emailEditText;
    String userPhone;

    Calendar myCalendar = Calendar.getInstance();
    long birthDayByTM;

    UserAPI userAPI = APIHelper.getUserAPI();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.phone_registration_personal_information_activity);

        Intent intent = getIntent();
        userPhone = intent.getStringExtra("user_phone");
        if (userPhone == null || userPhone.isEmpty()) {
            finish();
        }

        bindingControl();
        setupControlEvents();
        initData();
    }

    public void bindingControl() {
        genderSpinner = (AppCompatSpinner) findViewById(R.id.spn_gender);
        birthButton = (Button) findViewById(R.id.btn_birth);
        continueButton = (Button) findViewById(R.id.btn_continue);
        firstNameEditText = (EditText) findViewById(R.id.et_first_name);
        lastNameEditText = (EditText) findViewById(R.id.et_last_name);
        emailEditText = (EditText) findViewById(R.id.et_email);
    }

    public void setupControlEvents() {
        birthButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(PhoneRegistrationPersonalInformationActivity.this, R.style.AppTheme_Dialog, PhoneRegistrationPersonalInformationActivity.this, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        continueButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                applySetting();
            }
        });
    }

    public void initData() {
        List<String> genderList = new ArrayList<>();
        String[] items = getResources().getStringArray(R.array.array_gender);
        if (genderList.size() == 0) {
            Collections.addAll(genderList, items);
        }

        ArrayAdapter<String> pm_adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.layout_spinner_item,
                R.id.tv_item_text, genderList);
        pm_adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item);
        genderSpinner.setAdapter(pm_adapter);
    }

    public void applySetting() {
        String firstName = firstNameEditText.getText().toString();
        String lastName = lastNameEditText.getText().toString();
        String email = emailEditText.getText().toString();
        String birthDay = birthButton.getText().toString();
        int indexGender = genderSpinner.getSelectedItemPosition();

        if (firstName.isEmpty()) {
            showErrorDialog(getString(R.string.phone_registration_personal_information_error_first_name_empty));
            return;
        }

        if (lastName.isEmpty()) {
            showErrorDialog(getString(R.string.phone_registration_personal_information_error_last_name_empty));
            return;
        }

        if (!Utils.isValidEmail(email)) {
            showErrorDialog(getString(R.string.phone_registration_personal_information_error_email_empty));
            return;
        }

        if (birthDay.isEmpty()) {
            showErrorDialog(getString(R.string.phone_registration_personal_information_error_birth_empty));
            return;
        }

        if (indexGender == 0) {
            showErrorDialog(getString(R.string.phone_registration_personal_information_error_gender_empty));
            return;
        }

        showLoading();
        String gender = indexGender == 1 ? "1" : "0";
        userAPI.updateProfile(userPhone, email, firstName, lastName, String.valueOf(birthDayByTM), gender, null, new APIHelper.Callback<User>() {
            @Override
            public void onSuccess(final User data) {
                PhoneRegistrationPersonalInformationActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        gotoSetAvatar();
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                PhoneRegistrationPersonalInformationActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }

    public void gotoSetAvatar() {
        Intent intent = new Intent(PhoneRegistrationPersonalInformationActivity.this, PhoneRegistrationAvatarActivity.class);
        intent.putExtra("user_phone", userPhone);
        startActivity(intent);
        finish();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, month);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        this.birthDayByTM = myCalendar.getTimeInMillis();
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);

        birthButton.setText(sdf.format(myCalendar.getTime()));
    }
}
