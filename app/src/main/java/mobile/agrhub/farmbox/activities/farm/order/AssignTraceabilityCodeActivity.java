package mobile.agrhub.farmbox.activities.farm.order;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.GridView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.adapters.order.CartAdapter;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.model.FarmCart;
import mobile.agrhub.farmbox.service.model.FarmOrder;
import mobile.agrhub.farmbox.service.model.SerialPackage;
import mobile.agrhub.farmbox.service.model.User;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Constants;
import mobile.agrhub.farmbox.utils.Utils;

public class AssignTraceabilityCodeActivity extends BaseActivity implements CartAdapter.OnClickListener {
    private static final int REQUEST_SCAN_SERIAL_CODE = 998;

    @BindView(R.id.gv_cart_item)
    GridView gvCartItems;
    @BindView(R.id.tv_total_remain)
    TextView mTotalRemain;

    private CartAdapter mAdapter;
    private FarmOrder order;
    private List<PostData> postDataList;
    private int currentPostDataIndex;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign_traceability_code);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        String orderStr = intent.getStringExtra(Constants.ORDER_INFO_KEY);
        if (orderStr != null && !orderStr.isEmpty()) {
            Gson gson = new Gson();
            order = gson.fromJson(orderStr, FarmOrder.class);
        }

        configActionBar();

        mAdapter = new CartAdapter(this, order.carts, 2,this);
        gvCartItems.setAdapter(mAdapter);

        mTotalRemain.setText(getString(R.string.remain_number, Utils.i2s(mAdapter.totalRemain())));
    }

    private void configActionBar() {
        getSupportActionBar().setTitle(R.string.assign_traceability_code_title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.done_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        } else if (menuItem.getItemId() == R.id.action_done) {
            if (order.carts != null && order.carts.size() > 0) {
                boolean isValid = true;
                // validate
                for (FarmCart item : order.carts) {
                    if (!item.isCompleteAssignQRCode()) {
                        isValid = false;
                        showErrorDialog(getString(R.string.please_assign_qr_code_message, item.cartProduct.productName));
                        break;
                    }
                }
                if (isValid) {
                    showLoading();

                    if (postDataList == null) {
                        postDataList = new ArrayList<>();
                    } else {
                        postDataList.clear();
                    }

                    PostData postData;
                    for (FarmCart item : order.carts) {
                        postData = new PostData();
                        postData.type = PostData.TYPE_POST_FARM;
                        postData.id = item.cartId;
                        postData.data = item;
                        postDataList.add(postData);

                        if (item.serialList != null && item.serialList.size() > 0) {
                            for (SerialPackage serial : item.serialList) {
                                if (serial.type == SerialPackage.TYPE_CODE) {
                                    postData = new PostData();
                                    postData.type = PostData.TYPE_POST_QRCODE;
                                    postData.id = item.cartId;
                                    postData.data = serial;
                                    postDataList.add(postData);
                                } else {
                                    postData = new PostData();
                                    postData.type = PostData.TYPE_POST_SERIAL;
                                    postData.id = item.cartId;
                                    postData.data = serial;
                                    postDataList.add(postData);
                                }
                            }
                        }
                    }

                    postData = new PostData();
                    postData.type = PostData.TYPE_POST_STATUS;
                    postData.id = order.orderId;
                    postData.data = order;
                    postDataList.add(postData);

                    currentPostDataIndex = 0;
                    doAssignCart();
                }
            }
        }

        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_SCAN_SERIAL_CODE) {
                String dataStr = data.getStringExtra(Constants.CART_INFO_KEY);
                if (dataStr != null && !dataStr.isEmpty()) {
                    Gson gson = new Gson();

                    FarmCart cart = gson.fromJson(dataStr, FarmCart.class);
                    if (cart != null && order.carts != null && order.carts.size() > 0) {
                        FarmCart item;
                        for (int i = 0; i < order.carts.size(); i++) {
                            item = order.carts.get(i);
                            if (item.cartId == cart.cartId) {
                                order.carts.set(i, cart);
                                break;
                            }
                        }
                    }
                    refreshListView();
                }
            }
        }
    }

    private void refreshListView() {
        if (null != gvCartItems) {
            if (null == mAdapter) {
                mAdapter = new CartAdapter(this, order.carts, this);
                gvCartItems.setAdapter(mAdapter);
            } else {
                mAdapter.update(order.carts);
                mAdapter.notifyDataSetChanged();
            }
        }
        mTotalRemain.setText(getString(R.string.remain_number, Utils.i2s(mAdapter.totalRemain())));
    }

    private void doAssignCart() {
        if (currentPostDataIndex >= 0 && currentPostDataIndex < postDataList.size()) {
            PostData data = postDataList.get(currentPostDataIndex);
            if (data.type == PostData.TYPE_POST_FARM) {
                FarmCart cart = (FarmCart)data.data;
                APIHelper.getSaleAPI().updateCartField(data.id, Constants.FIELD_FARM_ID, String.valueOf(cart.farmId), new APIHelper.Callback<FarmCart>() {
                    @Override
                    public void onSuccess(FarmCart data) {
                        AssignTraceabilityCodeActivity activity = AssignTraceabilityCodeActivity.this;
                        if (activity != null) {
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    AssignTraceabilityCodeActivity activity = AssignTraceabilityCodeActivity.this;
                                    if (activity != null) {
                                        activity.currentPostDataIndex ++;
                                        activity.doAssignCart();
                                    }
                                }
                            });
                        }
                    }

                    @Override
                    public void onFail(final int errorCode, final String error) {
                        AssignTraceabilityCodeActivity activity = AssignTraceabilityCodeActivity.this;
                        if (activity != null) {
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    AssignTraceabilityCodeActivity activity = AssignTraceabilityCodeActivity.this;
                                    if (activity != null) {
                                        activity.hideLoading();
                                        activity.handleError(errorCode, error);
                                    }
                                }
                            });
                        }
                    }
                });
            } else if (data.type == PostData.TYPE_POST_QRCODE) {
                SerialPackage serial = (SerialPackage)data.data;
                APIHelper.getSaleAPI().createTrace(data.id, serial.code, new APIHelper.Callback<String>() {
                    @Override
                    public void onSuccess(String data) {
                        AssignTraceabilityCodeActivity activity = AssignTraceabilityCodeActivity.this;
                        if (activity != null) {
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    AssignTraceabilityCodeActivity activity = AssignTraceabilityCodeActivity.this;
                                    if (activity != null) {
                                        activity.currentPostDataIndex ++;
                                        activity.doAssignCart();
                                    }
                                }
                            });
                        }
                    }

                    @Override
                    public void onFail(final int errorCode, final String error) {
                        AssignTraceabilityCodeActivity activity = AssignTraceabilityCodeActivity.this;
                        if (activity != null) {
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    AssignTraceabilityCodeActivity activity = AssignTraceabilityCodeActivity.this;
                                    if (activity != null) {
                                        activity.hideLoading();
                                        activity.handleError(errorCode, error);
                                    }
                                }
                            });
                        }
                    }
                });
            } else if (data.type == PostData.TYPE_POST_SERIAL) {
                SerialPackage serial = (SerialPackage)data.data;
                String fromSerial = serial.fromSerial;
                String toSerial = serial.toSerial;
                User user = FarmBoxAppController.getInstance().getUser();
                // check is Mr Quyet
                if (user != null
                        && user.userId == Long.valueOf("6364673650720768")
                        && fromSerial.startsWith("183241038")) {
                    fromSerial = fromSerial.replace("183241038", "183092114");
                    toSerial = toSerial.replace("183241038", "183092114");
                }
                APIHelper.getSaleAPI().createTrace(data.id, fromSerial, toSerial, new APIHelper.Callback<String>() {
                    @Override
                    public void onSuccess(String data) {
                        AssignTraceabilityCodeActivity activity = AssignTraceabilityCodeActivity.this;
                        if (activity != null) {
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    AssignTraceabilityCodeActivity activity = AssignTraceabilityCodeActivity.this;
                                    if (activity != null) {
                                        activity.currentPostDataIndex ++;
                                        activity.doAssignCart();
                                    }
                                }
                            });
                        }
                    }

                    @Override
                    public void onFail(final int errorCode, final String error) {
                        AssignTraceabilityCodeActivity activity = AssignTraceabilityCodeActivity.this;
                        if (activity != null) {
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    AssignTraceabilityCodeActivity activity = AssignTraceabilityCodeActivity.this;
                                    if (activity != null) {
                                        activity.hideLoading();
                                        activity.handleError(errorCode, error);
                                    }
                                }
                            });
                        }
                    }
                });
            } else if (data.type == PostData.TYPE_POST_STATUS) {
                APIHelper.getSaleAPI().updateOrderField(data.id, Constants.FIELD_ORDER_STATE, String.valueOf(FarmOrder.STATE_DELIVERY), new APIHelper.Callback<FarmOrder>() {
                    @Override
                    public void onSuccess(FarmOrder data) {
                        AssignTraceabilityCodeActivity activity = AssignTraceabilityCodeActivity.this;
                        if (activity != null) {
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    AssignTraceabilityCodeActivity activity = AssignTraceabilityCodeActivity.this;
                                    if (activity != null) {
                                        activity.currentPostDataIndex ++;
                                        activity.doAssignCart();
                                    }
                                }
                            });
                        }
                    }

                    @Override
                    public void onFail(final int errorCode, final String error) {
                        AssignTraceabilityCodeActivity activity = AssignTraceabilityCodeActivity.this;
                        if (activity != null) {
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    AssignTraceabilityCodeActivity activity = AssignTraceabilityCodeActivity.this;
                                    if (activity != null) {
                                        activity.hideLoading();
                                        activity.handleError(errorCode, error);
                                    }
                                }
                            });
                        }
                    }
                });
            } else {
                currentPostDataIndex ++;
                doAssignCart();
            }
        } else {
            hideLoading();
            finish();
        }

    }

    @Override
    public void onMinusClicked(String productId) {

    }

    @Override
    public void onProductAmountClicked(long productId, int amount) {

    }

    @Override
    public void onPlusClicked(String productId) {

    }

    @Override
    public void onAddToCartClicked(long productId) {

    }

    @Override
    public void onAssignQRCodeToCartClicked(FarmCart cart) {
        Gson gson = new Gson();

        Intent intent = new Intent(this, AssignTraceabilityCodeDetailActivity.class);
        intent.putExtra(Constants.CART_INFO_KEY, gson.toJson(cart));
        startActivityForResult(intent, REQUEST_SCAN_SERIAL_CODE);
    }

    private class PostData {
        public static final int TYPE_POST_FARM = 0;
        public static final int TYPE_POST_QRCODE = 1;
        public static final int TYPE_POST_SERIAL = 2;
        public static final int TYPE_POST_STATUS = 3;

        public int type;
        public long id;
        public Object data;
    }
}
