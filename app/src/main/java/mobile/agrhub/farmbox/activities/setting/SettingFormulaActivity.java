package mobile.agrhub.farmbox.activities.setting;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.FarmAPI;
import mobile.agrhub.farmbox.service.network.Param;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Utils;

public class SettingFormulaActivity extends BaseActivity {
    EditText mA, mB, mC;
    Button mDone;

    FarmAPI farmAPI = APIHelper.getFarmAPI();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_formula);

        bindingControls();
        setupControlEvents();
        initData();
        configActionBar();
    }

    private void configActionBar() {
        getSupportActionBar().setTitle(R.string.setting_nutri_formula_name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void bindingControls() {
        mDone = (Button) findViewById(R.id.btn_set);
        mA = (EditText) findViewById(R.id.et_formula_a);
        mB = (EditText) findViewById(R.id.et_formula_b);
        mC = (EditText) findViewById(R.id.et_formula_c);
    }

    private void setupControlEvents() {
        mDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestDone();
            }
        });
    }

    private void initData() {
        Intent intent = getIntent();

        String field = intent.getStringExtra("field");
        String name = intent.getStringExtra("name");
        String valueA = intent.getStringExtra("A");
        String valueB = intent.getStringExtra("B");
        String valueC = intent.getStringExtra("C");

        if (valueA.equalsIgnoreCase("-1")) {
            mA.setHint("--");
        }
        else {
            mA.setText(valueA);
        }

        if (valueB.equalsIgnoreCase("-1")) {
            mB.setHint("--");
        }
        else {
            mB.setText(valueB);
        }

        if (valueC.equalsIgnoreCase("-1")) {
            mC.setHint("--");
        }
        else {
            mC.setText(valueC);
        }
    }

    private void requestDone() {
        String valueA = mA.getText().toString();
        String valueB = mB.getText().toString();
        String valueC = mC.getText().toString();

        int valueAInt = 0;
        int valueBInt = 0;
        int valueCInt = 0;
        try {
            valueAInt = Utils.str2Int(valueA);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            valueBInt = Utils.str2Int(valueC);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            valueCInt = Utils.str2Int(valueC);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (valueAInt > 100 || valueBInt > 100 || valueCInt > 100) {
            showErrorDialog(getString(R.string.setting_formula_wrong_input));
            return;
        }

        if((valueAInt + valueBInt + valueCInt) != 100) {
            showErrorDialog(getString(R.string.setting_formula_over_value));
            return;
        }

        String farmID = getIntent().getStringExtra("farmID");
        String factoryID = getIntent().getStringExtra("factoryID");

        List<Param> params = new ArrayList<>();
        params.add(new Param("factory_ec_formula_a", valueA));
        params.add(new Param("factory_ec_formula_b", valueB));
        params.add(new Param("factory_ec_formula_c", valueC));

        showLoading();
        farmAPI.saveFarmSetting(factoryID, farmID, params, new APIHelper.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                SettingFormulaActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        final AlertDialog dialog = new AlertDialog.Builder(SettingFormulaActivity.this, R.style.AppTheme_Dialog_Alert)
                                .setTitle(R.string.app_name)
                                .setMessage(R.string.setting_save_success)
                                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        SettingFormulaActivity.this.finish();
                                    }
                                }).create();
                        if (!dialog.isShowing()) {
                            dialog.show();
                        }
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                SettingFormulaActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }
}
