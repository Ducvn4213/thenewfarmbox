package mobile.agrhub.farmbox.activities.plant;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.adapters.PlantImageAdapter;
import mobile.agrhub.farmbox.custom_view.segment_control.SegmentedControl;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.PlantAPI;
import mobile.agrhub.farmbox.service.model.Plant;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.ResourceUtils;
import mobile.agrhub.farmbox.utils.Utils;

public class PlantDetailActivity extends BaseActivity {

    PlantImageAdapter mAdapter;

    @BindView(R.id.rv_plant_images)
    RecyclerView mPlantImages;
    @BindView(R.id.sc_plant_detail)
    SegmentedControl mSegmented;

    @BindView(R.id.ll_general)
    LinearLayout mGeneralContainer;
    @BindView(R.id.ll_growing)
    LinearLayout mGrowingContainer;

    @BindView(R.id.tv_common_name)
    TextView commonName;
    @BindView(R.id.tv_scientist_name)
    TextView scientistName;
    @BindView(R.id.tv_family)
    TextView family;
    @BindView(R.id.tv_information)
    TextView information;
    @BindView(R.id.tv_type)
    TextView type;
    @BindView(R.id.tv_life_cycle)
    TextView lifeCycle;
    @BindView(R.id.tv_bloom_color)
    TextView bloomColor;
    @BindView(R.id.tv_leaf_color)
    TextView leafColor;
    @BindView(R.id.tv_bloom_season)
    TextView bloomSeason;
    @BindView(R.id.tv_shape)
    TextView shape;

    @BindView(R.id.tv_seedling)
    TextView seedling;
    @BindView(R.id.tv_soil_water)
    TextView soilWater;
    @BindView(R.id.tv_pruning)
    TextView pruning;

    @BindView(R.id.iv_sun_1)
    ImageView sun1;
    @BindView(R.id.iv_sun_2)
    ImageView sun2;
    @BindView(R.id.iv_sun_3)
    ImageView sun3;
    @BindView(R.id.iv_sun_4)
    ImageView sun4;
    @BindView(R.id.iv_sun_5)
    ImageView sun5;

    @BindView(R.id.iv_water_1)
    ImageView water1;
    @BindView(R.id.iv_water_2)
    ImageView water2;
    @BindView(R.id.iv_water_3)
    ImageView water3;
    @BindView(R.id.iv_water_4)
    ImageView water4;
    @BindView(R.id.iv_water_5)
    ImageView water5;

    @BindView(R.id.iv_crap_1)
    ImageView crap1;
    @BindView(R.id.iv_crap_2)
    ImageView crap2;
    @BindView(R.id.iv_crap_3)
    ImageView crap3;
    @BindView(R.id.iv_crap_4)
    ImageView crap4;
    @BindView(R.id.iv_crap_5)
    ImageView crap5;

    @BindView(R.id.tv_snow)
    TextView snow;
    @BindView(R.id.tv_sun)
    TextView sun;

    PlantAPI plantAPI = APIHelper.getPlantAPI();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plant_detail);
        ButterKnife.bind(this);

        setupControlEvents();
        configActionBar();
        initData();
    }

    private void configActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupControlEvents() {
        mSegmented.setOnSegmentedChangedListener(new SegmentedControl.OnSegmentedChangedListener() {
            @Override
            public void onSegmentedChanged(int index) {
                if (index == 0) {
                    mGeneralContainer.setVisibility(View.VISIBLE);
                    mGrowingContainer.setVisibility(View.GONE);
                } else {
                    mGeneralContainer.setVisibility(View.GONE);
                    mGrowingContainer.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void initData() {
        mSegmented.setTextA(getString(R.string.plant_detail_segmented_general));
        mSegmented.setTextB(getString(R.string.plant_detail_segmented_growing));

        long id = getIntent().getLongExtra("id", 0);

        getPlantData(id);
    }

    private void initData(Plant data) {
        getSupportActionBar().setTitle(correctString(Utils.getValueFromMultiLanguageString(data.plant_name), false));
        List<String> images = new Gson().fromJson(data.plant_photos, new TypeToken<List<String>>() {
        }.getType());
        mAdapter = new PlantImageAdapter(PlantDetailActivity.this, images);
        mPlantImages.setAdapter(mAdapter);

        PagerSnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(mPlantImages);

        commonName.setText(correctString(Utils.getValueFromMultiLanguageString(data.plant_name), false));
        scientistName.setText(correctString(data.plant_scientific_name, false));
        family.setText(correctString(Utils.getValueFromMultiLanguageString(data.plant_genus), false));

//        information.setText(correctString(Html.fromHtml(Utils.getValueFromMultiLanguageString(data.plant_description.value)), true));

        type.setText(ResourceUtils.getPlantTypeName(PlantDetailActivity.this, data.plant_type));
        lifeCycle.setText(ResourceUtils.getPlantLifeCycleName(PlantDetailActivity.this, data.plant_lifetime));
        bloomColor.setText(ResourceUtils.getPlantElementColorName(PlantDetailActivity.this, data.plant_bloom_color));
        leafColor.setText(ResourceUtils.getPlantElementColorName(PlantDetailActivity.this, data.plant_leaf_color));
        bloomSeason.setText(ResourceUtils.getPlantSeasonName(PlantDetailActivity.this, data.plant_bloom_season));
        shape.setText(ResourceUtils.getPlantShapeName(PlantDetailActivity.this, data.plant_shape));

//        seedling.setText(correctString(Html.fromHtml(Utils.getValueFromMultiLanguageString(data.plant_planting.value)), true));
//        soilWater.setText(correctString(Html.fromHtml(Utils.getValueFromMultiLanguageString(data.plant_soil_irrigation.value)), true));
//        pruning.setText(correctString(Html.fromHtml(Utils.getValueFromMultiLanguageString(data.plant_pruning.value)), true));

        setWatering((int) data.plant_watering);
        setSunlight(((int) data.plant_sunlight));
        setCrapPackage(((int) data.plant_fertilizer));
        setTemperatures(data.plant_temperatures);

        mGeneralContainer.setVisibility(View.VISIBLE);
        mGrowingContainer.setVisibility(View.GONE);
    }

    private void getPlantData(long id) {
        plantAPI.getPlantByID(id, new APIHelper.Callback<Plant>() {
            @Override
            public void onSuccess(final Plant data) {
                PlantDetailActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        initData(data);
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                PlantDetailActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }

    private Spanned correctString(Spanned text, boolean isBig) {
        if (text == null || text.length() == 0) {
            if (isBig) {
                return Html.fromHtml("------");
            }

            return Html.fromHtml("---");
        }

        return text;
    }

    private String correctString(String text, boolean isBig) {
        if (text == null || text.trim().isEmpty()) {
            if (isBig) {
                return "------";
            }

            return "---";
        }

        return text;
    }

    private void setWatering(int input) {
        water1.setImageResource(R.mipmap.ic_water_0);
        water2.setImageResource(R.mipmap.ic_water_0);
        water3.setImageResource(R.mipmap.ic_water_0);
        water4.setImageResource(R.mipmap.ic_water_0);
        water5.setImageResource(R.mipmap.ic_water_0);

        if (input > 0) {
            water1.setImageResource(R.mipmap.ic_water_1);
        }

        if (input > 1) {
            water2.setImageResource(R.mipmap.ic_water_1);
        }

        if (input > 2) {
            water3.setImageResource(R.mipmap.ic_water_1);
        }

        if (input > 3) {
            water4.setImageResource(R.mipmap.ic_water_1);
        }

        if (input > 4) {
            water5.setImageResource(R.mipmap.ic_water_1);
        }
    }

    private void setSunlight(int input) {
        sun1.setImageResource(R.mipmap.ic_sun_0);
        sun2.setImageResource(R.mipmap.ic_sun_0);
        sun3.setImageResource(R.mipmap.ic_sun_0);
        sun4.setImageResource(R.mipmap.ic_sun_0);
        sun5.setImageResource(R.mipmap.ic_sun_0);

        if (input > 0) {
            sun1.setImageResource(R.mipmap.ic_sun_1);
        }

        if (input > 1) {
            sun2.setImageResource(R.mipmap.ic_sun_1);
        }

        if (input > 2) {
            sun3.setImageResource(R.mipmap.ic_sun_1);
        }

        if (input > 3) {
            sun4.setImageResource(R.mipmap.ic_sun_1);
        }

        if (input > 4) {
            sun5.setImageResource(R.mipmap.ic_sun_1);
        }
    }

    private void setCrapPackage(int input) {
        crap1.setImageResource(R.mipmap.ic_crap_package_0);
        crap2.setImageResource(R.mipmap.ic_crap_package_0);
        crap3.setImageResource(R.mipmap.ic_crap_package_0);
        crap4.setImageResource(R.mipmap.ic_crap_package_0);
        crap5.setImageResource(R.mipmap.ic_crap_package_0);

        if (input > 0) {
            crap1.setImageResource(R.mipmap.ic_crap_package_1);
        }

        if (input > 1) {
            crap2.setImageResource(R.mipmap.ic_crap_package_1);
        }

        if (input > 2) {
            crap3.setImageResource(R.mipmap.ic_crap_package_1);
        }

        if (input > 3) {
            crap4.setImageResource(R.mipmap.ic_crap_package_1);
        }

        if (input > 4) {
            crap5.setImageResource(R.mipmap.ic_crap_package_1);
        }
    }

    private void setTemperatures(String input) {
        List<Double> inputJSON = new Gson().fromJson(input, new TypeToken<List<Double>>() {
        }.getType());

        if (inputJSON.size() == 0) {
            snow.setText("--");
            sun.setText("--");
            return;
        }

        snow.setText(inputJSON.get(0) + "");
        sun.setText(inputJSON.get(1) + "");
    }
}
