package mobile.agrhub.farmbox.activities.sensor;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.IMarker;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.MPPointF;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.custom_view.segment_control.FourSegmentedControl;
import mobile.agrhub.farmbox.custom_view.segment_control.SegmentedControl;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.UtilAPI;
import mobile.agrhub.farmbox.service.model.Farm;
import mobile.agrhub.farmbox.service.model.Sensor;
import mobile.agrhub.farmbox.service.model.SensorChartPointItem;
import mobile.agrhub.farmbox.service.model.UnitItem;
import mobile.agrhub.farmbox.service.model.response.ChartDataResponse;
import mobile.agrhub.farmbox.utils.BaseActivity;

public class ChartActivity extends BaseActivity implements IAxisValueFormatter {

    private NumberFormat mNumberFormat;
    private long sensorType;
    private long farmType;

    private class ChartMaker extends MarkerView implements IMarker {
        public ChartMaker(Context context, int layoutResource) {
            super(context, layoutResource);
        }

        @Override
        public MPPointF getOffset() {
            return new MPPointF(-(getWidth() / 2), -(getHeight() / 2));
        }
    }

    private enum DiaryType {
        UTM(0),
        Hourly(1),
        Daily(2),
        Weekly(3),
        Monthly(4);

        private final int code;

        DiaryType(int code) {
            this.code = code;
        }

        public int getCode() {
            return this.code;
        }
    }

    @BindView(R.id.lc_chart)
    LineChart lineChartView;
    @BindView(R.id.fsc_segment)
    FourSegmentedControl segmentedControl;

    @BindView(R.id.tv_highest)
    TextView highestTextView;
    @BindView(R.id.tv_value)
    TextView valueTextView;
    @BindView(R.id.tv_lowest)
    TextView lowestTextView;

    @BindView(R.id.ll_highest)
    LinearLayout highestContainer;
    @BindView(R.id.ll_value)
    LinearLayout valueContainer;
    @BindView(R.id.ll_lowest)
    LinearLayout lowestContainer;

    private int currentDairy = 0;
    private List<SensorChartPointItem> lastedSensorData;
    private LineDataSet lineDataSet;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);

        ButterKnife.bind(this);

        mNumberFormat = NumberFormat.getNumberInstance();
    }

    @Override
    protected void onResume() {
        super.onResume();

        configUI();
        configActionBar();
        loadChartData();
    }

    private void configActionBar() {
        farmType = getIntent().getLongExtra("farmType", 0);
        sensorType = getIntent().getLongExtra("type", 0);
        String name = Sensor.displayName(getBaseContext(), (int) sensorType);

        getSupportActionBar().setTitle(name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void configUI() {
        lineChartView.setPinchZoom(false);
        lineChartView.setDoubleTapToZoomEnabled(false);
        lineChartView.setDescription(null);
        lineChartView.setDrawGridBackground(false);

        lineChartView.getAxisRight().setEnabled(false);

        lineChartView.getAxisLeft().setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        lineChartView.getAxisLeft().setDrawLabels(true);
        lineChartView.getAxisLeft().setDrawAxisLine(true);
        lineChartView.getAxisLeft().setDrawGridLines(true);

        lineChartView.getXAxis().setDrawGridLines(true);
        lineChartView.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        lineChartView.getXAxis().setValueFormatter(this);

        lineChartView.getLegend().setEnabled(false);

        lineChartView.setDrawMarkers(true);
        lineChartView.setMarker(new ChartMaker(ChartActivity.this, R.layout.layout_chart_maker));

        lineChartView.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                updateDetailData(e);
            }

            @Override
            public void onNothingSelected() {
                updateDetailData(null);
            }
        });

        segmentedControl.setTextA(getString(R.string.dairy_utm));
        segmentedControl.setTextB(getString(R.string.dairy_hourly));
        segmentedControl.setTextC(getString(R.string.dairy_daily));
        segmentedControl.setTextD(getString(R.string.dairy_monthly));
        segmentedControl.setColorIndex(1);
        segmentedControl.setSelected(0);

        segmentedControl.setOnSegmentedChangedListener(new SegmentedControl.OnSegmentedChangedListener() {
            @Override
            public void onSegmentedChanged(int index) {
                currentDairy = index;
                loadChartData();
            }
        });
    }

    private void updateDetailData(Entry entry) {
        if (entry != null) {
            int index = (int) entry.getX();

            SensorChartPointItem item = lastedSensorData.get(index);

            if (null != item) {
                if (null != item.getSensor_max()) {
                    highestContainer.setVisibility(View.VISIBLE);
                    highestTextView.setText(mNumberFormat.format(this.formatValue(item.getSensor_max())));
                } else {
                    highestContainer.setVisibility(View.GONE);
                }

                if (null != item.getValue()) {
                    valueContainer.setVisibility(View.VISIBLE);
                    valueTextView.setText(mNumberFormat.format(this.formatValue(item.getValue())));
                } else {
                    valueContainer.setVisibility(View.GONE);
                }

                if (null != item.getSensor_min()) {
                    lowestContainer.setVisibility(View.VISIBLE);
                    lowestTextView.setText(mNumberFormat.format(this.formatValue(item.getSensor_min())));
                } else {
                    lowestContainer.setVisibility(View.GONE);
                }
            } else {
                highestContainer.setVisibility(View.GONE);
                valueContainer.setVisibility(View.GONE);
                lowestContainer.setVisibility(View.GONE);
            }
        }
    }

    private void showMaxMin() {
        highestContainer.setVisibility(View.VISIBLE);
        lowestContainer.setVisibility(View.VISIBLE);
    }

    private void hideMaxMin() {
        highestContainer.setVisibility(View.GONE);
        lowestContainer.setVisibility(View.GONE);
    }

    private void loadChartData() {
        showLoading();
        highestContainer.setVisibility(View.GONE);
        valueContainer.setVisibility(View.GONE);
        lowestContainer.setVisibility(View.GONE);

        updateDetailData(null);

        long sensorID = getIntent().getLongExtra("id", 0);
        String id = String.valueOf(sensorID);
        String dairy = String.valueOf(currentDairy == 3 ? 4 : currentDairy);

        UtilAPI utilAPI = APIHelper.getUtilAPI();
        utilAPI.getSensorChartData(id, dairy, new APIHelper.Callback<ChartDataResponse>() {
            @Override
            public void onSuccess(final ChartDataResponse data) {
                ChartActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        updateDataForChart(data);
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                ChartActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }

    void updateDataForChart(ChartDataResponse _data) {
        if (_data == null || _data.data == null || _data.data.size() == 0) {
            return;
        }

        ArrayList<Entry> values = new ArrayList<Entry>();

        lastedSensorData = _data.data;
        float theIndex = 0;
        for (SensorChartPointItem item : _data.data) {
            values.add(new Entry((float) theIndex, this.formatValue(item.getValue())));
            theIndex++;
        }

        int theColor = mobile.agrhub.farmbox.utils.Utils.getRandomColor(0);

        lineDataSet = new LineDataSet(values, null);

        lineDataSet.setAxisDependency(YAxis.AxisDependency.LEFT);

        lineDataSet.setHighlightEnabled(true);
        lineDataSet.setDrawHorizontalHighlightIndicator(false);
        lineDataSet.setDrawVerticalHighlightIndicator(false);

        lineDataSet.setDrawCircleHole(true);
        lineDataSet.setDrawValues(false);
        lineDataSet.setDrawFilled(false);

        lineDataSet.setLineWidth(2);
        lineDataSet.setCircleRadius(5);
        lineDataSet.setFillAlpha(65);
        lineDataSet.setCircleColor(theColor);
        lineDataSet.setFillColor(theColor);
        lineDataSet.setCircleHoleRadius(3);

        lineDataSet.setHighlightLineWidth(1);
        lineDataSet.setColor(theColor);

        lineDataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSet.setCubicIntensity(0.2f);

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(lineDataSet);
        LineData data = new LineData(dataSets);

        lineChartView.setData(data);
        lineChartView.invalidate();
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        int idx = Math.round(value);
        if (idx < 0 && lastedSensorData.size() <= 0) {
            return "";
        }
        long dateValue;
        if (idx < lastedSensorData.size()) {
            dateValue = lastedSensorData.get(idx).diary_date;
        } else {
            dateValue = lastedSensorData.get(lastedSensorData.size() - 1).diary_date;
        }
        if (segmentedControl.getSelected() > 1) {
            return mobile.agrhub.farmbox.utils.Utils.getDate(dateValue, "dd:MMM");
        }
        return mobile.agrhub.farmbox.utils.Utils.getDate(dateValue, "HH:mm");
    }

    private Float formatValue(double doubleVal) {
        Float returnVal;

        FarmBoxAppController app = FarmBoxAppController.getInstance();
        if (this.sensorType == Sensor.AIR_TEMPERATURE_SENSOR ||
                this.sensorType == Sensor.SOIL_TEMPERATURE_SENSOR ||
                this.sensorType == Sensor.WATER_TEMPERATURE_SENSOR) {
            long tmpVal = Math.round(doubleVal);
            tmpVal = app.getTemperatureValue(tmpVal, UnitItem.TEMPERATURE_C_VALUE);
            returnVal = new Float(tmpVal);
        } else if (this.sensorType == Sensor.SOIL_EC_SENSOR) {
            String fromUnit = UnitItem.NUTRITION_PARTS_PER_MILLION_VALUE;
            if (this.farmType == Farm.HYDROPONIC
                    || this.farmType == Farm.AQUAPONIC
                    || this.farmType == Farm.CONTAINER) {
                fromUnit = UnitItem.NUTRITION_MILLISIEMENS_VALUE;
            }
            doubleVal = app.getNutritionValue(doubleVal, fromUnit);
            returnVal = new Float(doubleVal);
        } else if (this.sensorType == Sensor.WATER_EC_SENSOR) {
            doubleVal = app.getNutritionValue(doubleVal, UnitItem.NUTRITION_MILLISIEMENS_VALUE);
            returnVal = new Float(doubleVal);
        } else {
            returnVal = new Float(doubleVal);
        }
        return returnVal;
    }
}
