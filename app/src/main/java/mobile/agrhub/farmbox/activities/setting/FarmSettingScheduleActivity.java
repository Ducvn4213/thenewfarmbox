package mobile.agrhub.farmbox.activities.setting;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.List;

import butterknife.BindView;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.FarmSettingDetailData;
import mobile.agrhub.farmbox.service.network.Param;
import mobile.agrhub.farmbox.utils.Utils;

public class FarmSettingScheduleActivity extends BaseFarmSettingItemActivity {
    @BindView(R.id.tv_schedule_time)
    TextView mScheduleTime;
    @BindView(R.id.tv_schedule_loop_day)
    TextView mScheduleLoopDay;

    FarmSettingDetailData mData;
    int hour, minute, loopDay;

    @Override
    protected int contentView() {
        return R.layout.activity_farm_setting_schedule;
    }

    @Override
    protected void initView() {
        super.initView();

        if (item.getData() != null && item.getData().size() > 0) {
            mData = item.getData().get(0);
        }

        if (mData == null || mData.getValue() == null || mData.getValue().isEmpty()) {
            finish();
            return;
        }
        String val = mData.getValue();
        String[] valPart = val.split(" ");


        if (valPart[0] == "*" || valPart[1] == "*") {
            minute = 0;
            hour = 0;
        } else {
            try {
                minute = Utils.str2Int(valPart[0]);
                hour = Utils.str2Int(valPart[1]);
            } catch (Exception e) {
                minute = 0;
                hour = 0;
            }
        }
        mScheduleTime.setText( Utils.correctTimeUnit(hour) + ":" + Utils.correctTimeUnit(minute));

        String d = valPart[4];
        switch (d) {
            case "*":
                mScheduleLoopDay.setText(getString(R.string.cron_time_every_day_week));
                loopDay = -1;
                break;
            case "1":
                mScheduleLoopDay.setText(getString(R.string.cron_time_monday));
                loopDay = 1;
                break;
            case "2":
                mScheduleLoopDay.setText(getString(R.string.cron_time_tuesday));
                loopDay = 2;
                break;
            case "3":
                mScheduleLoopDay.setText(getString(R.string.cron_time_wednesday));
                loopDay = 3;
                break;
            case "4":
                mScheduleLoopDay.setText(getString(R.string.cron_time_thursday));
                loopDay = 4;
                break;
            case "5":
                mScheduleLoopDay.setText(getString(R.string.cron_time_friday));
                loopDay = 5;
                break;
            case "6":
                mScheduleLoopDay.setText(getString(R.string.cron_time_saturday));
                loopDay = 6;
                break;
            case "7":
                mScheduleLoopDay.setText(getString(R.string.cron_time_sunday));
                loopDay = 7;
                break;
            default:
                break;
        }

        mScheduleTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(FarmSettingScheduleActivity.this, R.style.AppTheme_Dialog, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        hour = selectedHour;
                        minute = selectedMinute;
                        mScheduleTime.setText( Utils.correctTimeUnit(selectedHour) + ":" + Utils.correctTimeUnit(selectedMinute));
                    }
                }, hour, minute, true);
                mTimePicker.show();
            }
        });

        mScheduleLoopDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestChangeDate();
            }
        });
    }

    private void requestChangeDate() {
        final Dialog dialog = new Dialog(FarmSettingScheduleActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_number_picker);

        TextView title = (TextView) dialog.findViewById(R.id.tv_select_title);
        final NumberPicker numberPicker = (NumberPicker) dialog.findViewById(R.id.np_value);
        TextView choose = (TextView) dialog.findViewById(R.id.tv_choose);
        TextView cancel = (TextView) dialog.findViewById(R.id.tv_cancel);

        title.setText(R.string.setting_cront_time_select_date);

        numberPicker.setMinValue(0);
        numberPicker.setMaxValue(7);
        numberPicker.setDisplayedValues(new String[] {
                getString(R.string.cron_time_every_day_week),
                getString(R.string.cron_time_monday),
                getString(R.string.cron_time_tuesday),
                getString(R.string.cron_time_wednesday),
                getString(R.string.cron_time_thursday),
                getString(R.string.cron_time_friday),
                getString(R.string.cron_time_saturday),
                getString(R.string.cron_time_sunday)});
        numberPicker.setWrapSelectorWheel(true);
        changeDividerColor(numberPicker, Color.TRANSPARENT);

        if (loopDay> 0) {
            numberPicker.setValue(loopDay);
        }

        choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mScheduleLoopDay.setText(numberPicker.getDisplayedValues()[numberPicker.getValue()]);
                if (numberPicker.getValue() == 0) {
                    loopDay = -1;
                }
                else {
                    loopDay = numberPicker.getValue();
                }
                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void changeDividerColor(NumberPicker picker, int color) {

        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    @Override
    protected boolean validate() {
        return true;
    }

    @Override
    protected List<Param> buildParam(List<Param> params) {
        String cron = minute + " " + hour + " * * " + loopDay;
        params.add(new Param(mData.getField(), cron));

        return params;
    }
}
