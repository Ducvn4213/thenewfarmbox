package mobile.agrhub.farmbox.activities.setting;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.FarmAPI;
import mobile.agrhub.farmbox.service.network.Param;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Utils;

public class SettingCronTimeActivity extends BaseActivity{
    Button mDone;
    Button mTime, mDay;

    int cronMinute = -1;
    int cronHour = -1;
    int cronDay = -1;
    long farmType;

    FarmAPI farmAPI = APIHelper.getFarmAPI();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_cron_time);

        Intent intent = getIntent();
        if (null != intent && intent.hasExtra("unit")) {
            farmType = intent.getLongExtra("unit", -1);
        } else {
            farmType = -1;
        }

        bindingControls();
        setupControlEvents();
        initData();
        configActionBar();
    }

    private void configActionBar() {
        getSupportActionBar().setTitle(R.string.setting_washing_name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void bindingControls() {
        mTime = (Button) findViewById(R.id.btn_time);
        mDay = (Button) findViewById(R.id.btn_day);

        mDone = (Button) findViewById(R.id.btn_set);
    }

    private void setupControlEvents() {

        mDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestDone();
            }
        });

        mTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                final TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(SettingCronTimeActivity.this, R.style.AppTheme_Dialog, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        cronHour = selectedHour;
                        cronMinute = selectedMinute;
                        mTime.setText( Utils.correctTimeUnit(selectedHour) + ":" + Utils.correctTimeUnit(selectedMinute));
                    }
                }, hour, minute, true);
                mTimePicker.setTitle(R.string.setting_cron_time_time);
                mTimePicker.show();
            }
        });

        mDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestChangeDate();
            }
        });
    }

    private void initData() {
        String theValue = getIntent().getStringExtra("data");
        if (theValue == null) {
            return;
        }

        String[] crons = theValue.split(" ");


        if (crons[0] == "*" || crons[1] == "*") {
            mTime.setText("00:00");

            cronMinute = -1;
            cronHour = -1;
        }
        else {
            try {
                cronMinute = Utils.str2Int(crons[0]);
                cronHour = Utils.str2Int(crons[1]);

                mTime.setText( Utils.correctTimeUnit(cronHour) + ":" + Utils.correctTimeUnit(cronMinute));
            }
            catch (Exception e) {
                e.printStackTrace();
                mTime.setText("00:00");

                cronMinute = -1;
                cronHour = -1;
            }
        }

        String d = crons[4];
        switch (d) {
            case "*":
                mDay.setText(getString(R.string.cron_time_every_day_week));
                cronDay = -1;
                break;
            case "1":
                mDay.setText(getString(R.string.cron_time_monday));
                cronDay = 1;
                break;
            case "2":
                mDay.setText(getString(R.string.cron_time_tuesday));
                cronDay = 2;
                break;
            case "3":
                mDay.setText(getString(R.string.cron_time_wednesday));
                cronDay = 3;
                break;
            case "4":
                mDay.setText(getString(R.string.cron_time_thursday));
                cronDay = 4;
                break;
            case "5":
                mDay.setText(getString(R.string.cron_time_friday));
                cronDay = 5;
                break;
            case "6":
                mDay.setText(getString(R.string.cron_time_saturday));
                cronDay = 6;
                break;
            case "7":
                mDay.setText(getString(R.string.cron_time_sunday));
                cronDay = 7;
                break;
            default:
                break;
        }
    }

    private void requestChangeDate() {
        final Dialog dialog = new Dialog(SettingCronTimeActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_number_picker);

        TextView title = (TextView) dialog.findViewById(R.id.tv_select_title);
        final NumberPicker numberPicker = (NumberPicker) dialog.findViewById(R.id.np_value);
        TextView choose = (TextView) dialog.findViewById(R.id.tv_choose);
        TextView cancel = (TextView) dialog.findViewById(R.id.tv_cancel);

        title.setText(R.string.setting_cront_time_select_date);

        numberPicker.setMinValue(0);
        numberPicker.setMaxValue(7);
        numberPicker.setDisplayedValues(new String[] {
                getString(R.string.cron_time_every_day_week),
                getString(R.string.cron_time_monday),
                getString(R.string.cron_time_tuesday),
                getString(R.string.cron_time_wednesday),
                getString(R.string.cron_time_thursday),
                getString(R.string.cron_time_friday),
                getString(R.string.cron_time_saturday),
                getString(R.string.cron_time_sunday)});
        numberPicker.setWrapSelectorWheel(true);
        changeDividerColor(numberPicker, Color.TRANSPARENT);

        if (cronDay> 0) {
            numberPicker.setValue(cronDay);
        }

        choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDay.setText(numberPicker.getDisplayedValues()[numberPicker.getValue()]);
                if (numberPicker.getValue() == 0) {
                    cronDay = -1;
                }
                else {
                    cronDay = numberPicker.getValue();
                }
                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void changeDividerColor(NumberPicker picker, int color) {

        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    private void requestDone() {
        String m = cronMinute < 0 ? "*" : cronMinute + "";
        String h = cronHour < 0 ? "*" : cronHour + "";

        String d = cronDay + "";
        if (cronDay <= 0) {
            d = "*";
        }
        else if (cronDay == 7) {
            d = "0";
        }

        String cron = m + " " + h + " * * " + d;

        String farmID = getIntent().getStringExtra("farmID");
        String factoryID = getIntent().getStringExtra("factoryID");

        List<Param> params = new ArrayList<>();
        params.add(new Param("factory_washing_mode_timer", cron));

        showLoading();
        farmAPI.saveFarmSetting(factoryID, farmID, params, new APIHelper.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                SettingCronTimeActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        final AlertDialog dialog = new AlertDialog.Builder(SettingCronTimeActivity.this, R.style.AppTheme_Dialog_Alert)
                                .setTitle(R.string.app_name)
                                .setMessage(R.string.setting_save_success)
                                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        SettingCronTimeActivity.this.finish();
                                    }
                                }).create();
                        if (!dialog.isShowing()) {
                            dialog.show();
                        }
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                SettingCronTimeActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }
}
