package mobile.agrhub.farmbox.activities.add;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.model.Farm;
import mobile.agrhub.farmbox.utils.BaseActivity;

public class AddFarmActivity extends BaseActivity {
    @BindView(R.id.ll_add_home)
    LinearLayout mAddHome;
    @BindView(R.id.ll_add_greenhouse)
    LinearLayout mAddGreenHouse;
    @BindView(R.id.ll_add_mushroom)
    LinearLayout mAddMushRoom;
    @BindView(R.id.ll_add_hydroponic)
    LinearLayout mAddHydroponic;
    @BindView(R.id.ll_add_container)
    LinearLayout mAddContainer;
    @BindView(R.id.ll_add_storage)
    LinearLayout mAddStorage;
    @BindView(R.id.ll_add_cattle_house)
    LinearLayout mAddCattleHouse;
    @BindView(R.id.ll_add_general)
    LinearLayout mAddGeneral;
    @BindView(R.id.ll_add_device)
    LinearLayout mAddDevice;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_farm);
        ButterKnife.bind(this);

        configActionBar();
    }

    private void configActionBar() {
        getSupportActionBar().setTitle(R.string.add_farm_title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.ll_add_home)
    void onAddHomeClick() {
        addFarm(Farm.FAMILY);
    }

    @OnClick(R.id.ll_add_greenhouse)
    void onAddGreenHouseClick() {
        addFarm(Farm.GREEN_HOUSE);
    }

    @OnClick(R.id.ll_add_mushroom)
    void onAddMushroomClick() {
        addFarm(Farm.MUSHROOM);
    }

    @OnClick(R.id.ll_add_hydroponic)
    void onAddHydroponicClick() {
        addFarm(Farm.HYDROPONIC);
    }

    @OnClick(R.id.ll_add_container)
    void onAddContainerClick() {
        addFarm(Farm.CONTAINER);
    }

    @OnClick(R.id.ll_add_storage)
    void onAddStorageClick() {
        addFarm(Farm.STORAGE);
    }

    @OnClick(R.id.ll_add_cattle_house)
    void onAddCattleHouseClick() {
        addFarm(Farm.CATTLE_HOUSE);
    }

    @OnClick(R.id.ll_add_general)
    void onAddGeneralClick() {
        addFarm(Farm.GENERAL);
    }

    @OnClick(R.id.ll_add_device)
    void onAddDeviceClick() {
        addFarm(Farm.ADD_DEVICE);
    }

    private void addFarm(int type) {
        if (type == Farm.ADD_DEVICE && getIntent().getIntExtra("disable_add_device", -1) == 1) {
            showErrorDialog(getString(R.string.add_device_farm_missing));
            return;
        }

        if (type == Farm.ADD_DEVICE && FarmBoxAppController.getInstance().getSelectedFarm() == null) {
            showErrorDialog(getString(R.string.add_device_farm_missing));
            return;
        }

        Intent intent = null;
        if (type == Farm.ADD_DEVICE || type == Farm.CONTAINER) {
            intent = new Intent(AddFarmActivity.this, AddByQRCodeActivity.class);
        } else {
            intent = new Intent(AddFarmActivity.this, AddByNormalWayActivity.class);
        }

        intent.putExtra("type", type);
        startActivityForResult(intent, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            finish();
        }
    }
}
