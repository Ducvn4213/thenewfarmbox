package mobile.agrhub.farmbox.activities.setting;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.FarmAPI;
import mobile.agrhub.farmbox.service.model.FarmSettingDetailItem;
import mobile.agrhub.farmbox.service.network.Param;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Constants;

public class SettingFactoryControlModeActivity extends BaseActivity implements CompoundButton.OnCheckedChangeListener {

    FarmSettingDetailItem item;

    @BindView(R.id.rb_auto)
    RadioButton mAuto;
    @BindView(R.id.rb_manual)
    RadioButton mManual;
    @BindView(R.id.rb_threshold)
    RadioButton mThreshold;
    @BindView(R.id.rb_smart)
    RadioButton mSmart;

    long farmId, factoryId;
    FarmAPI farmAPI = APIHelper.getFarmAPI();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_factory_control_mode);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        ButterKnife.bind(this);

        mAuto.setOnCheckedChangeListener(this);
        mManual.setOnCheckedChangeListener(this);
        mThreshold.setOnCheckedChangeListener(this);
        mSmart.setOnCheckedChangeListener(this);

        Intent intent = getIntent();
        farmId = intent.getLongExtra(Constants.FARM_ID_KEY, 0);
        factoryId = intent.getLongExtra(Constants.FACTORY_ID_KEY, 0);
        String settingInfo = intent.getStringExtra(Constants.SETTING_INFO_KEY);
        if (farmId <= 0 || factoryId <= 0 || settingInfo == null || settingInfo.isEmpty()) {
            finish();
            return;
        } else {
            Gson gson = new Gson();
            item = gson.fromJson(settingInfo, FarmSettingDetailItem.class);
        }

        if ("1".equals(item.getValue())) {
            mThreshold.setChecked(true);
        } else if ("2".equals(item.getValue())) {
            mManual.setChecked(true);
        } else if ("3".equals(item.getValue())) {
            mSmart.setChecked(true);
        } else {
            mAuto.setChecked(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.setting_factory_control_mode_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_apply:
                applyChange();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.rb_auto:
                if (isChecked) {
                    if (mManual.isChecked()) {
                        mManual.setChecked(false);
                    }
                    if (mThreshold.isChecked()) {
                        mThreshold.setChecked(false);
                    }
                    if (mSmart.isChecked()) {
                        mSmart.setChecked(false);
                    }
                }
                break;
            case R.id.rb_manual:
                if (isChecked) {
                    if (mAuto.isChecked()) {
                        mAuto.setChecked(false);
                    }
                    if (mThreshold.isChecked()) {
                        mThreshold.setChecked(false);
                    }
                    if (mSmart.isChecked()) {
                        mSmart.setChecked(false);
                    }
                }
                break;
            case R.id.rb_threshold:
                if (isChecked) {
                    if (mAuto.isChecked()) {
                        mAuto.setChecked(false);
                    }
                    if (mManual.isChecked()) {
                        mManual.setChecked(false);
                    }
                    if (mSmart.isChecked()) {
                        mSmart.setChecked(false);
                    }
                }
                break;
            case R.id.rb_smart:
                if (isChecked) {
                    if (mAuto.isChecked()) {
                        mAuto.setChecked(false);
                    }
                    if (mManual.isChecked()) {
                        mManual.setChecked(false);
                    }
                    if (mThreshold.isChecked()) {
                        mThreshold.setChecked(false);
                    }
                }
                break;
        }
    }

    private void applyChange() {
        String value = "0";
        if (mThreshold.isChecked()) {
            value = "1";
        } else if (mManual.isChecked()) {
            value = "2";
        } else if (mSmart.isChecked()) {
            value = "3";
        }

        List<Param> params = new ArrayList<>();
        params.add(new Param(Constants.FACTORY_CONTROL_MODE_KEY, value));
        showLoading();

        farmAPI.saveFarmSetting(factoryId + "", farmId + "", params, new APIHelper.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                SettingFactoryControlModeActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        final AlertDialog dialog = new AlertDialog.Builder(SettingFactoryControlModeActivity.this, R.style.AppTheme_Dialog_Alert)
                                .setTitle(R.string.app_name)
                                .setMessage(R.string.setting_save_success)
                                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        SettingFactoryControlModeActivity.this.finish();
                                    }
                                }).create();
                        if (!dialog.isShowing()) {
                            dialog.show();
                        }
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                SettingFactoryControlModeActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }
}
