package mobile.agrhub.farmbox.activities.account;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.model.User;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Utils;

public class AccountActivity extends BaseActivity {

    private TextView mName;
    private TextView mGender;
    private TextView mBirth;
    private TextView mAddress;
    private TextView mPhone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        bindingControls();
    }

    @Override
    public void onResume() {
        super.onResume();
        initData();
    }

    private void bindingControls() {
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mName = (TextView) findViewById(R.id.tv_name);
        mGender = (TextView) findViewById(R.id.tv_gender);
        mBirth = (TextView) findViewById(R.id.tv_birth);
        mAddress = (TextView) findViewById(R.id.tv_address);
        mPhone = (TextView) findViewById(R.id.tv_phone);

    }

    private void initData() {
        User user = FarmBoxAppController.getInstance().getUser();

        mName.setText(user.user_first_name + " " + user.user_last_name);
        mGender.setText(user.user_gender == 1 ? R.string.gender_male : R.string.gender_female);
        mBirth.setText(Utils.getDate(user.user_birthday, "dd/MM/yyyy"));
        mAddress.setText(user.user_address);
        mPhone.setText(user.user_phone);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.account_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        Intent intent;
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_account_edit:
                intent = new Intent(this, EditAccountActivity.class);
                startActivity(intent);
                return true;
        }
        return false;
    }
}
