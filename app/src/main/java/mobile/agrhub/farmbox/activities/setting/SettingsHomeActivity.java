package mobile.agrhub.farmbox.activities.setting;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.view.View;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.custom_view.setting_item.SettingItemView;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.FarmAPI;
import mobile.agrhub.farmbox.service.model.Farm;
import mobile.agrhub.farmbox.service.model.Setting;

public class SettingsHomeActivity extends FarmSettingV1Activity {
    SettingItemView mLight, mAirTemp, mSoilHumi, mSoilNutri, mWaterPump;

    FarmAPI farmAPI = APIHelper.getFarmAPI();
    long farmType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_home);
        farmType = -1;

        bindingControls();
        configUI();
        setupControlEvents();
        configActionBar();
    }

    private void configActionBar() {
        String farmName = FarmBoxAppController.getInstance().getSelectedFarmName();
        getSupportActionBar().setTitle(farmName);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        loadData();
    }

    @Override
    protected void bindingControls() {
        super.bindingControls();

        mLight = (SettingItemView) findViewById(R.id.siv_light);
        mAirTemp = (SettingItemView) findViewById(R.id.siv_air_temp);
        mSoilHumi = (SettingItemView) findViewById(R.id.siv_soil_humidity);
        mSoilNutri = (SettingItemView) findViewById(R.id.siv_soil_nutrition);
        mWaterPump = (SettingItemView) findViewById(R.id.siv_water_pump);
    }

    @Override
    public void configUI() {
        super.configUI();
        FarmBoxAppController app = FarmBoxAppController.getInstance();

        mLight.setName(getString(R.string.setting_light));
        mAirTemp.setName(getString(R.string.setting_air_temp, app.getTemperatureUnit()));
        mSoilHumi.setName(getString(R.string.setting_soil_humi));
        mSoilNutri.setName(getString(R.string.setting_soil_nutri, app.getNutritionUnit()));
        mWaterPump.setName(getString(R.string.setting_water_pump));
    }

    @Override
    public void setupControlEvents() {
        super.setupControlEvents();

        setupControlEventsForSettingItem(mLight);
        setupControlEventsForSettingItem(mAirTemp);
        setupControlEventsForSettingItem(mSoilHumi);
        setupControlEventsForSettingItem(mSoilNutri);
        setupControlEventsForSettingItem(mWaterPump);
    }

    private void setupControlEventsForSettingItem(final SettingItemView itemView) {
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = itemView.getIntentToChangeSetting(SettingsHomeActivity.this);
                intent.putExtra("unit", SettingsHomeActivity.this.farmType);
                startActivity(intent);
            }
        });
    }

    private void loadData() {
        Farm farm = FarmBoxAppController.getInstance().getSelectedFarm();
        if (null == farm) {
            finish();
            return;
        }

        long farmID = farm.farm_id;
        farmType = farm.farm_type;

//        showLoading();
//        farmAPI.getFarmSettings(String.valueOf(farmID), new APIHelper.Callback<Setting>() {
//            @Override
//            public void onSuccess(final Setting data) {
//                SettingsHomeActivity.this.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        hideLoading();
//                        doLoadData(data);
//                    }
//                });
//            }
//
//            @Override
//            public void onFail(final int errorCode, final String error) {
//                SettingsHomeActivity.this.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        hideLoading();
//                        handleError(errorCode, error);
//                    }
//                });
//            }
//        });
    }

    @Override
    public void doLoadData(Setting data) {
        super.doLoadData(data);

        if (data.farm_id == 0) {
            data.farm_id = FarmBoxAppController.getInstance().getSelectedFarmId();
        }

        mLight.setValue(data.getLightValue());
        mLight.setBasicMetaData(SettingItemView.TYPE.MINMAX, data.farm_id + "", data.factory_id + "");
        mLight.setMetaData("factory_light", getString(R.string.setting_light_name), data.factory_light_min + "", data.factory_light_max + "");

        mAirTemp.setValue(data.getTempValue());
        mAirTemp.setBasicMetaData(SettingItemView.TYPE.MINMAX, data.farm_id + "", data.factory_id + "");
        mAirTemp.setMetaData("factory_temperature_level", getString(R.string.setting_air_temp_name), data.factory_temperature_level_min + "", data.factory_temperature_level_max + "");

        mSoilHumi.setValue(data.getSoilHumiValue());
        mSoilHumi.setBasicMetaData(SettingItemView.TYPE.MINMAX, data.farm_id + "", data.factory_id + "");
        mSoilHumi.setMetaData("factory_soil_humidity", getString(R.string.setting_soil_humi_name), data.factory_soil_humidity_min + "", data.factory_soil_humidity_max + "");

        mSoilNutri.setValue(data.getECValue(this.farmType));
        mSoilNutri.setBasicMetaData(SettingItemView.TYPE.MINMAX, data.farm_id + "", data.factory_id + "");
        mSoilNutri.setMetaData("factory_ec", getString(R.string.setting_soil_nutri_name), data.factory_ec_min + "", data.factory_ec_max + "");

        mWaterPump.setValue(data.getWaterPumpTime());
        mWaterPump.setBasicMetaData(SettingItemView.TYPE.ONOFF, data.farm_id + "", data.factory_id + "");
        mWaterPump.setMetaData("factory_water_pump", getString(R.string.setting_water_pump_name), data.factory_water_pump_started_time + "", data.factory_water_pump_stopped_time + "");
    }
}
