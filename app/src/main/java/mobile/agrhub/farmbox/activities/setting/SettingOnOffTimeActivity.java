package mobile.agrhub.farmbox.activities.setting;

import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.FarmAPI;
import mobile.agrhub.farmbox.service.network.Param;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Utils;

public class SettingOnOffTimeActivity extends BaseActivity {
    Button mStart, mStop;
    Button mDone;

    int mSelectedStartTime, mSelectedStopTime;

    FarmAPI farmAPI = APIHelper.getFarmAPI();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_on_off);

        bindingControls();
        setupControlEvents();
        initData();
        configActionBar();
    }

    private void configActionBar() {
        String name = getIntent().getStringExtra("name");
        getSupportActionBar().setTitle(name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void bindingControls() {
        mDone = (Button) findViewById(R.id.btn_set);
        mStart = (Button) findViewById(R.id.btn_start);
        mStop = (Button) findViewById(R.id.btn_stop);
    }

    private void setupControlEvents() {
        mDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestDone();
            }
        });

        mStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(SettingOnOffTimeActivity.this, R.style.AppTheme_Dialog, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        mSelectedStartTime = selectedHour * 60 + selectedMinute;
                        mStart.setText( Utils.correctTimeUnit(selectedHour) + ":" + Utils.correctTimeUnit(selectedMinute));
                    }
                }, hour, minute, true);
                mTimePicker.setTitle(R.string.setting_start_time);
                mTimePicker.show();
            }
        });

        mStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar mCurrentTime = Calendar.getInstance();
                int hour = mCurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mCurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(SettingOnOffTimeActivity.this, R.style.AppTheme_Dialog, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        mSelectedStopTime = selectedHour * 60 + selectedMinute;
                        mStop.setText( Utils.correctTimeUnit(selectedHour) + ":" + Utils.correctTimeUnit(selectedMinute));
                    }
                }, hour, minute, true);
                mTimePicker.setTitle(R.string.setting_stop_time);
                mTimePicker.show();
            }
        });
    }

    private void initData() {
        Intent intent = getIntent();

        String start = intent.getStringExtra("start");
        String stop = intent.getStringExtra("stop");

        mSelectedStartTime = Integer.parseInt(start);
        mSelectedStopTime = Integer.parseInt(stop);

        if (mSelectedStartTime == -1 || mSelectedStopTime == -1) {
            mStart.setHint("--");
            mStop.setHint("--");
        }
        else {
            mStart.setText(Utils.getTime(mSelectedStartTime));
            mStop.setText(Utils.getTime(mSelectedStopTime));
        }
    }

    private void requestDone() {
        String field = getIntent().getStringExtra("field");
        String farmID = getIntent().getStringExtra("farmID");
        String factoryID = getIntent().getStringExtra("factoryID");

        List<Param> params = new ArrayList<>();
        params.add(new Param(field + "_started_time", mSelectedStartTime + ""));
        params.add(new Param(field + "_stopped_time", mSelectedStopTime + ""));

        showLoading();
        farmAPI.saveFarmSetting(factoryID, farmID, params, new APIHelper.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                SettingOnOffTimeActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        final AlertDialog dialog = new AlertDialog.Builder(SettingOnOffTimeActivity.this, R.style.AppTheme_Dialog_Alert)
                                .setTitle(R.string.app_name)
                                .setMessage(R.string.setting_save_success)
                                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        SettingOnOffTimeActivity.this.finish();
                                    }
                                }).create();
                        if (!dialog.isShowing()) {
                            dialog.show();
                        }
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                SettingOnOffTimeActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }
}
