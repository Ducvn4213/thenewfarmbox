package mobile.agrhub.farmbox.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;

import me.relex.circleindicator.CircleIndicator;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.activities.registration.PhoneRegistrationActivity;
import mobile.agrhub.farmbox.adapters.IntroAdapter;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.UtilAPI;
import mobile.agrhub.farmbox.utils.BaseActivity;

public class IntroActivity extends BaseActivity {
    private static final String TAG = "IntroActivity";
    ViewPager mViewPager;
    IntroAdapter mAdapter;
    CircleIndicator mIndicator;
    TextView mChangeLanguage;
    LinearLayout mNextLayout;
    LinearLayout mNext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        bindingControls();
        setupControlEvents();
        init();
    }

    void bindingControls() {
        mViewPager = (ViewPager) findViewById(R.id.vp_container);
        mIndicator = (CircleIndicator) findViewById(R.id.indicator);
        mChangeLanguage = (TextView) findViewById(R.id.tv_change_language);
        mNextLayout = (LinearLayout) findViewById(R.id.ll_next_layout);
        mNext = (LinearLayout) findViewById(R.id.ll_next);
    }

    void setupControlEvents() {
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 3) {
                    showNextButton();
                } else {
                    hideNetButton();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mChangeLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FarmBoxAppController app = FarmBoxAppController.getInstance();
                if ("vi".equals(app.getLanguage())) {
                    app.setLanguage("en");
                } else {
                    app.setLanguage("vi");
                }
                IntroActivity.this.recreate();
            }
        });

        mNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mViewPager.getCurrentItem() > 0) {
                    Intent intent = new Intent(IntroActivity.this, PhoneRegistrationActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    void init() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        if (refreshedToken != null && !refreshedToken.isEmpty()) {
            UtilAPI utilAPI = APIHelper.getUtilAPI();
            utilAPI.unsubscribeNotification(refreshedToken, new APIHelper.Callback<String>() {
                @Override
                public void onSuccess(String data) {
                    Log.d(TAG, data);
                }

                @Override
                public void onFail(int errorCode, String error) {
                    Log.d(TAG, error);
                }
            });
        }

        mAdapter = new IntroAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mAdapter);
        mIndicator.setViewPager(mViewPager);

        FarmBoxAppController app = FarmBoxAppController.getInstance();
        if ("vi".equals(app.getLanguage())) {
            mChangeLanguage.setText(R.string.language_popup_english);
        } else {
            mChangeLanguage.setText(R.string.language_popup_vietnamese);
        }
    }

    private void hideNetButton() {
        mNextLayout.animate().alpha(0).withEndAction(new Runnable() {
            @Override
            public void run() {
                mNextLayout.setVisibility(View.GONE);
            }
        });
    }

    private void showNextButton() {
        mNextLayout.setAlpha(0);
        mNextLayout.setVisibility(View.VISIBLE);

        mNextLayout.animate().alpha(1);
    }
}
