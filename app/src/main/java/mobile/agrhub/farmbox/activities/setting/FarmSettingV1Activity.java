package mobile.agrhub.farmbox.activities.setting;

import android.content.Intent;
import android.view.View;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.custom_view.setting_item.SettingItemView;
import mobile.agrhub.farmbox.service.model.Setting;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Constants;

public class FarmSettingV1Activity extends BaseActivity {
    protected SettingItemView mFactoryControlMode, mDeviceSetting;

    protected void bindingControls() {
        mFactoryControlMode = (SettingItemView) findViewById(R.id.factory_control_mode);
        mDeviceSetting = (SettingItemView) findViewById(R.id.device_setting);
    }

    protected void configUI() {
        if (null != mFactoryControlMode) {
            mFactoryControlMode.setName(getString(R.string.settings_factory_control_mode));
        }
        if (null != mDeviceSetting) {
            mDeviceSetting.setName(getString(R.string.device_setting));
        }
    }

    protected void setupControlEvents() {
        if (null != mFactoryControlMode) {
            mFactoryControlMode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(FarmSettingV1Activity.this, SettingFactoryControlModeActivity.class);
                    intent.putExtra(Constants.FARM_ID_KEY, mFactoryControlMode.getFarmID());
                    intent.putExtra(Constants.FACTORY_ID_KEY, mFactoryControlMode.getFactoryID());
                    intent.putExtra(Constants.SETTING_VALUE_KEY, mFactoryControlMode.getSettingValue());
                    startActivity(intent);
                }
            });
        }
        if (null != mDeviceSetting) {
            mDeviceSetting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(FarmSettingV1Activity.this, SettingDeviceActivity.class);
                    intent.putExtra(Constants.FARM_ID_KEY, mDeviceSetting.getFarmID());
                    startActivity(intent);
                }
            });
        }
    }

    protected void doLoadData(Setting setting) {
        if (null != setting) {
            if (null != mFactoryControlMode) {
                mFactoryControlMode.setBasicMetaData(SettingItemView.TYPE.SINGLE, setting.farm_id + "", setting.factory_id + "");
                mFactoryControlMode.setMetaData(Constants.FACTORY_CONTROL_MODE_KEY, getString(R.string.settings_factory_control_mode), setting.factoryControlMode + "", "");
                switch (setting.factoryControlMode) {
                    case 1:
                        mFactoryControlMode.setValue(getString(R.string.settings_factory_control_mode_threshold));
                        break;
                    case 2:
                        mFactoryControlMode.setValue(getString(R.string.settings_factory_control_mode_manual));
                        break;
                    case 3:
                        mFactoryControlMode.setValue(getString(R.string.settings_factory_control_mode_smart));
                        break;
                    default:
                        mFactoryControlMode.setValue(getString(R.string.settings_factory_control_mode_auto));
                        break;
                }
            }
            if (null != mDeviceSetting) {
                mDeviceSetting.setBasicMetaData(SettingItemView.TYPE.SINGLE, setting.farm_id + "", setting.factory_id + "");
                mDeviceSetting.setValue("");
            }
        }
    }
}
