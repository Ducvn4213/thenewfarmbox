package mobile.agrhub.farmbox.activities.edit;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.FarmAPI;
import mobile.agrhub.farmbox.service.model.Farm;
import mobile.agrhub.farmbox.service.model.FarmBasic;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.ResourceUtils;

public class EditFarmActivity extends BaseActivity {
    private final int PLACE_PICKER_REQUEST = 999;

    @BindView(R.id.iv_background)
    ImageView backgroundImageView;
    @BindView(R.id.btn_next)
    Button nextButton;
    @BindView(R.id.btn_address)
    Button addressButton;
    @BindView(R.id.et_name)
    EditText nameEditText;
    @BindView(R.id.et_acreages)
    EditText acreagesEditText;

    FarmAPI farmAPI = APIHelper.getFarmAPI();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_farm_by_normal);
        ButterKnife.bind(this);

        initData();
    }

    private void initData() {
        final Farm farm = FarmBoxAppController.getInstance().getSelectedFarm();
        if (farm == null) {
            finish();
        }
        nameEditText.setText(farm.farm_name);
        addressButton.setText(farm.farm_address);
        acreagesEditText.setText(String.valueOf(farm.farm_acreage));

        backgroundImageView.setImageResource(ResourceUtils.getFarmImageResourceByType(farm.farm_type));

        setGetCurrentLocationCallback(new GetCurrentLocationCallback() {
            @Override
            public void afterGotCurrentLocation(double lat, double lon) {
                EditFarmActivity.this.afterGotCurrentLocation(lat, lon);
            }

            @Override
            public void afterGetCurrentLocationFail() {
                EditFarmActivity.this.afterGetCurrentLocationFail();
            }
        });

        configActionBar();
    }

    @OnClick(R.id.btn_address)
    void onAddressButtonClick() {
        try {
            requestChangeAddress();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_next)
    void onNextButtonClick() {
        doAddNewFarm();
    }

    private void configActionBar() {
        getSupportActionBar().setTitle(R.string.edit_farm_title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void requestChangeAddress() throws GooglePlayServicesNotAvailableException, GooglePlayServicesRepairableException {

        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        startActivityForResult(builder.build(EditFarmActivity.this), PLACE_PICKER_REQUEST);
    }

    private void doAddNewFarm() {
        String name = nameEditText.getText().toString();
        String acreages = acreagesEditText.getText().toString();
        String address = addressButton.getText().toString();

        if (name == null || name.trim().isEmpty()) {
            showErrorDialog(getString(R.string.add_normal_farm_missing_name));
            return;
        }

        if (acreages == null || acreages.trim().isEmpty()) {
            showErrorDialog(getString(R.string.add_normal_farm_missing_acreages));
            return;
        }

        if (address == null || address.trim().isEmpty()) {
            showErrorDialog(getString(R.string.add_normal_farm_missing_address));
            return;
        }

        getCurrentLocationAndContinue();
    }

    private void afterGotCurrentLocation(double lat, double lon) {
        final String name = nameEditText.getText().toString();
        final String acreages = acreagesEditText.getText().toString();
        final String address = addressButton.getText().toString();

        showLoading();
        farmAPI.editFarm(name, address, acreages, String.valueOf(lat), String.valueOf(lon), new APIHelper.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                EditFarmActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        String title = getString(R.string.app_name);
                        String mess = getString(R.string.edit_farm_success);

                        FarmBoxAppController app = FarmBoxAppController.getInstance();
                        app.setNeedLoadFarmList(true);

                        Farm farm = app.getSelectedFarm();
                        if (farm != null) {
                            farm.farm_name = name;
                            farm.farm_address = address;
                            farm.farm_acreage = Long.parseLong(acreages);
                            app.setSelectedFarm(farm);
                        }

                        List<FarmBasic> farmList = app.getFarmList();
                        for (FarmBasic farmBasic : farmList) {
                            if (farmBasic.farm_id == farm.farm_id) {
                                farmBasic.farm_name = name;
                            }
                        }

                        final AlertDialog dialog = new AlertDialog.Builder(EditFarmActivity.this, R.style.AppTheme_Dialog_Alert)
                                .setTitle(title)
                                .setMessage(mess)
                                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        EditFarmActivity.this.finish();
                                    }
                                }).create();
                        if (!dialog.isShowing()) {
                            dialog.show();
                        }
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                EditFarmActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }

    private void goToEditSeason(long farmid) {
        Intent intent = new Intent(EditFarmActivity.this, EditSeasonActivity.class);
        intent.putExtra("id", farmid);
        intent.putExtra("enableBack", false);
        startActivity(intent);

        finish();
    }

    private void afterGetCurrentLocationFail() {
        showErrorDialog(getString(R.string.dialog_message_get_location_fail));
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                addressButton.setText(place.getAddress());
            }
        }
    }
}
