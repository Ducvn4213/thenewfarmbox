package mobile.agrhub.farmbox.activities.common;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.adapters.SelectOptionAdapter;
import mobile.agrhub.farmbox.service.model.OptionItem;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Constants;

public class SingleChooseActivity extends BaseActivity implements SelectOptionAdapter.SelectOptionListener {

    RecyclerView mRecyclerView;
    SelectOptionAdapter mAdapter;
    List<OptionItem> mData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_choose);

        bindingControls();
        setupControlEvents();

        initData();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.single_choose_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_done:
                done();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void bindingControls() {
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_content);
    }

    private void initData() {
        Intent intent = getIntent();

        String title = intent.getStringExtra(Constants.SCREEN_TITLE_KEY);
        if (title != null && !title.isEmpty()) {
            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle(title);
            } else {
                setTitle(title);
            }
        }

        String optionsStr = intent.getStringExtra(Constants.OPTION_LIST_KEY);
        if (optionsStr == null || optionsStr.isEmpty()) {
            finish();
        } else {
            Gson gson = new Gson();
            mData = gson.fromJson(optionsStr, new TypeToken<List<OptionItem>>() {
            }.getType());
        }

        mAdapter = new SelectOptionAdapter(this, mData, this);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void setupControlEvents() {
    }

    @Override
    public void onClick(OptionItem item) {
        done();
    }

    private void done() {
        Intent returnIntent = new Intent();
        for (OptionItem optionItem : mData) {
            if (optionItem.isChecked()) {
                returnIntent.putExtra(Constants.SELECTED_OPTION_KEY, optionItem.getKey());
                break;
            }
        }
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
