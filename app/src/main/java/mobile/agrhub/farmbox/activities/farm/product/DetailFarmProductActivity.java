package mobile.agrhub.farmbox.activities.farm.product;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.BindView;
import butterknife.ButterKnife;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.adapters.ProductImageAdapter;
import mobile.agrhub.farmbox.service.model.FarmProduct;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Constants;
import ss.com.bannerslider.Slider;

public class DetailFarmProductActivity extends BaseActivity {
    public static final int REQUEST_UPDATE_FARM_PRODUCT_CODE = 401;

    @BindView(R.id.sld_photo_slider)
    Slider mSliderPhotos;
    private ProductImageAdapter imageAdapter;
    @BindView(R.id.tv_product_origin_price)
    TextView mOriginPrice;
    @BindView(R.id.tv_product_discount)
    TextView mDiscount;
    @BindView(R.id.tv_product_price)
    TextView mPrice;
    @BindView(R.id.tv_product_weight)
    TextView mWeight;
    @BindView(R.id.tv_product_name)
    TextView mName;
    @BindView(R.id.tv_product_description)
    TextView mDescription;
    ImageLoader imageLoader;

    private FarmProduct product;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_farm_product);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        String productStr = intent.getStringExtra(Constants.FARM_PRODUCT_INFO_KEY);
        if (productStr == null || productStr.isEmpty()) {
            finish();
        } else {
            Gson gson = new Gson();
            product = gson.fromJson(productStr, FarmProduct.class);
        }

        if (product == null) {
            finish();
        }

        refreshView();
    }

    private void configActionBar() {
        getSupportActionBar().setTitle(product.productName);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.farm_product_detail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.action_copy) {
            FarmProduct copyProduct = product.copy();

            Gson gson = new Gson();
            String productStr = gson.toJson(copyProduct);

            Intent intent = new Intent(this, AddFarmProductActivity.class);
            intent.putExtra(Constants.FARM_PRODUCT_INFO_KEY, productStr);
            startActivity(intent);

            finish();
        } else if (item.getItemId() == R.id.action_edit) {
            Gson gson = new Gson();
            String productStr = gson.toJson(product);

            Intent intent = new Intent(this, AddFarmProductActivity.class);
            intent.putExtra(Constants.FARM_PRODUCT_INFO_KEY, productStr);
            startActivityForResult(intent, REQUEST_UPDATE_FARM_PRODUCT_CODE);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_UPDATE_FARM_PRODUCT_CODE && resultCode == RESULT_OK) {
            Bundle bundle = data.getExtras();
            if (bundle != null) {
                if (bundle.containsKey("FARM_PRODUCT_INFO_KEY")) {
                    String productStr = bundle.getString("FARM_PRODUCT_INFO_KEY");
                    if (productStr != null && !productStr.isEmpty()) {
                        Gson gson = new Gson();
                        product = gson.fromJson(productStr, FarmProduct.class);

                        refreshView();
                    }
                }
            }
        }
    }

    private void refreshView() {
        configActionBar();

        imageAdapter = new ProductImageAdapter(product.getProductAvatar());
        mSliderPhotos.setAdapter(imageAdapter);

        mOriginPrice.setText(product.displayOriginPrice());
        mDiscount.setText(product.displayDiscount());
        mPrice.setText(product.displayPrice());
        mWeight.setText(product.displayWeight(this));
        mName.setText(product.productName);
        mDescription.setText(product.productDescription);
    }

}
