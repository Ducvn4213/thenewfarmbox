package mobile.agrhub.farmbox.activities.setting;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.TimePicker;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.adapters.FarmSettingAdapter;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.FarmAPI;
import mobile.agrhub.farmbox.service.model.Farm;
import mobile.agrhub.farmbox.service.model.FarmSetting;
import mobile.agrhub.farmbox.service.model.FarmSettingDetailData;
import mobile.agrhub.farmbox.service.model.FarmSettingDetailItem;
import mobile.agrhub.farmbox.service.model.FarmSettingItem;
import mobile.agrhub.farmbox.service.network.Param;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Constants;
import mobile.agrhub.farmbox.utils.Utils;

public class FarmSettingActivity extends BaseActivity implements FarmSettingAdapter.SettingItemClickListener {
    public static final int REQUEST_CHANGE_SETTING = 201;
    FarmAPI farmAPI = APIHelper.getFarmAPI();

    RecyclerView mRecycleView;
    FarmSettingAdapter mAdapter;

    long farmId, factoryId;
    String title;
    int settingPosition;

    FarmSetting farmSetting;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farm_setting);

        Intent intent = getIntent();
        farmId = intent.getLongExtra(Constants.FARM_ID_KEY, 0);
        factoryId = intent.getLongExtra(Constants.FACTORY_ID_KEY, 0);
        title = intent.getStringExtra(Constants.SCREEN_TITLE_KEY);
        settingPosition = intent.getIntExtra(Constants.SETTING_POSITION_KEY, -1);

        bindingControls();
        configUI();
        setupControlEvents();
        configActionBar();
    }

    private void configActionBar() {
        if (this.title == null || this.title.isEmpty()) {
            this.title = FarmBoxAppController.getInstance().getSelectedFarmName();
        }
        getSupportActionBar().setTitle(this.title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        FarmBoxAppController app = FarmBoxAppController.getInstance();
        if (app.isNeedReloadFarmSetting() && this.factoryId == 0) {
            app.setNeedReloadFarmSetting(false);
            loadData();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CHANGE_SETTING && resultCode == Activity.RESULT_OK) {
            loadData();
        }
    }

    protected void bindingControls() {
        mRecycleView = (RecyclerView) findViewById(R.id.rv_content);
    }


    public void configUI() {
        loadData();
    }

    public void setupControlEvents() {
    }

    private void loadData() {
        if (this.farmId == 0) {
            Farm farm = FarmBoxAppController.getInstance().getSelectedFarm();
            if (null == farm) {
                finish();
                return;
            }
            this.farmId = farm.farm_id;
        }

        showLoading();
        if (this.factoryId > 0) {
            farmAPI.getFarmSessionSettings(String.valueOf(this.factoryId), new APIHelper.Callback<FarmSetting>() {
                @Override
                public void onSuccess(final FarmSetting data) {
                    FarmSettingActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();
                            didLoadData(data);
                        }
                    });
                }

                @Override
                public void onFail(final int errorCode, final String error) {
                    FarmSettingActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();
                            handleError(errorCode, error);
                        }
                    });
                }
            });
        } else {
            farmAPI.getFarmSettings(String.valueOf(this.farmId), new APIHelper.Callback<FarmSetting>() {
                @Override
                public void onSuccess(final FarmSetting data) {
                    FarmSettingActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();
                            didLoadData(data);
                        }
                    });
                }

                @Override
                public void onFail(final int errorCode, final String error) {
                    FarmSettingActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();
                            handleError(errorCode, error);
                        }
                    });
                }
            });
        }
    }

    public void didLoadData(FarmSetting data) {
        if (data == null) {
            finish();
            return;
        }

        List<FarmSettingItem> settings = data.getSettings();
        if (settings != null && settings.size() > 0) {
            boolean needSetTitle = true;
            for (FarmSettingItem item : settings) {
                if (item.getSettings() != null && item.getSettings().size() > 0) {
                    for (FarmSettingDetailItem detailItem: item.getSettings()) {
                        if (Constants.FIELD_FACTORY_START_TIME.equals(detailItem.getField())) {
                            ActionBar actionBar = getSupportActionBar();
                            if (actionBar != null) {
                                actionBar.setTitle(detailItem.getValue());
                            } else {
                                setTitle(detailItem.getValue());
                            }

                            needSetTitle = false;
                            break;
                        }
                    }
                }
                if (!needSetTitle) {
                    break;
                }
            }
        }

        this.farmSetting = data;
        if (mAdapter == null) {
            mAdapter = new FarmSettingAdapter(this, this.farmSetting.getSettings(), this);
            mRecycleView.setAdapter(mAdapter);
        } else {
            mAdapter.setData(this.farmSetting.getSettings());
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(FarmSettingDetailItem setting, int settingPosition) {
        Gson gson = new Gson();
        Intent intent = null;
        if ("one-choice".equals(setting.getType())) {
            intent = new Intent(this, FarmSettingOneChooseActivity.class);
        } else if ("device-setting".equals(setting.getType())) {
            intent = new Intent(this, SettingDeviceActivity.class);
        } else if ("min-max".equals(setting.getType())) {
            intent = new Intent(this, SettingMinMaxActivity.class);
        } else if ("min-medium-max".equals(setting.getType())) {
            intent = new Intent(this, FarmSettingMinMediumMaxActivity.class);
        } else if ("run-on".equals(setting.getType())) {
            if (setting.getData() != null && !setting.getData().isEmpty()) {
                intent = new Intent(this, FarmSettingRunOnActivity.class);
            } else {
                changeStartTimeDialog(setting.getField(), setting.getValue());
            }
        } else if ("nutrition-formula".equals(setting.getType())) {
            intent = new Intent(this, FarmSettingNutritionFormulaActivity.class);
        } else if ("simple".equals(setting.getType())) {
            intent = new Intent(this, FarmSettingSimpleActivity.class);
        } else if ("schedule".equals(setting.getType())) {
            intent = new Intent(this, FarmSettingScheduleActivity.class);
        } else if ("session".equals(setting.getType())) {
            intent = new Intent(this, FarmSettingActivity.class);
            long tmpVal = 0;
            try {
                tmpVal = Long.parseLong(setting.getField());
            } catch (Exception e) {}
            intent.putExtra(Constants.FACTORY_ID_KEY, tmpVal);
            intent.putExtra(Constants.SCREEN_TITLE_KEY, setting.getName());
            intent.putExtra(Constants.SETTING_POSITION_KEY, settingPosition);
        }

        if (intent != null) {
            intent.putExtra(Constants.FARM_ID_KEY, this.farmId);
            if (!intent.hasExtra(Constants.FACTORY_ID_KEY)) {
                intent.putExtra(Constants.FACTORY_ID_KEY, this.factoryId);
            }
            intent.putExtra(Constants.SETTING_INFO_KEY, gson.toJson(setting));

            startActivityForResult(intent, REQUEST_CHANGE_SETTING);
        }
    }

    @Override
    public void onChangeProfileSession(FarmSettingDetailItem setting, boolean val) {
        List<Param> params = new ArrayList<>();
        params.add(new Param(Constants.FIELD_IS_ACTIVE, val ? "true" : "false"));
        saveSetting(setting.getField(), params);
    }

    @Override
    public void onCopyProfileSession(final FarmSettingDetailItem setting) {
        final AlertDialog dialog = new AlertDialog.Builder(this, R.style.AppTheme_Dialog_Alert)
                .setTitle(R.string.copy)
                .setMessage(R.string.copy_profile_setting_session)
                .setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        getSettingForClone(setting);
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    private void getSettingForClone(final FarmSettingDetailItem setting) {
        showLoading();
        farmAPI.getFarmSessionSettings(setting.getField(), new APIHelper.Callback<FarmSetting>() {
            @Override
            public void onSuccess(FarmSetting data) {
                List<Param> params = new ArrayList<>();
                if (data != null && data.getSettings() != null && data.getSettings().size() > 0) {
                    for (FarmSettingItem settingItem : data.getSettings()) {
                        if (settingItem.getSettings() != null && settingItem.getSettings().size() > 0) {
                            for (FarmSettingDetailItem item : settingItem.getSettings()) {
                                if ("one-choice".equals(item.getType())) {
                                    if (item.getValue() != null && item.getData() != null && item.getData().size() > 0) {
                                        for (FarmSettingDetailData dataItem : item.getData()) {
                                            if (item.getValue().equals(dataItem.getName())) {
                                                params.add(new Param(item.getField(), dataItem.getValue()));
                                                break;
                                            }
                                        }
                                    }
                                } else {
                                    if (item.getData() != null && item.getData().size() > 0) {
                                        for (FarmSettingDetailData dataItem : item.getData()) {
                                            if ("time".equals(item.getDataType())) {
                                                String[] minPart =  dataItem.getValue().split(":");
                                                int minVal = 0;
                                                try {
                                                    minVal = Utils.str2Int(minPart[0]) * 60 + Utils.str2Int(minPart[1]);
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                                params.add(new Param(dataItem.getField(), minVal + ""));
                                            } else {
                                                params.add(new Param(dataItem.getField(), dataItem.getValue()));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                saveSetting("0", params);
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                FarmSettingActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }

    private void saveSetting(String factoryId, List<Param> params) {
        showLoading();
        farmAPI.saveFarmSetting(factoryId, farmId + "", params, new APIHelper.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                FarmSettingActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadData();
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                FarmSettingActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }

    private void changeStartTimeDialog(final String field, String value) {
        int hour = 0;
        int minute = 0;
        if (value != null && !value.isEmpty()) {
            String tmpVal = value;
            String[] timePart = tmpVal.split(" ");
            if (timePart.length > 0) {
                tmpVal = timePart[0];
            }
            timePart = tmpVal.split(":");
            try {
                hour = Utils.str2Int(timePart[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                minute = Utils.str2Int(timePart[1]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Calendar calendar = Calendar.getInstance();
            hour = calendar.get(Calendar.HOUR_OF_DAY);
            minute = calendar.get(Calendar.MINUTE);
        }
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(this, R.style.AppTheme_Dialog, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                doUpdateStartTime(field, selectedHour, selectedMinute);
            }
        }, hour, minute, true);
        mTimePicker.show();
    }

    private void doUpdateStartTime(String field, int hour, int minute) {
        int startTime = hour * 60 + minute;

        List<Param> params = new ArrayList<>();
        params.add(new Param(field, startTime + ""));
        saveSetting(this.factoryId + "", params);

        FarmBoxAppController.getInstance().setNeedReloadFarmSetting(true);
    }

}
