package mobile.agrhub.farmbox.activities.setting;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.FarmAPI;
import mobile.agrhub.farmbox.service.network.Param;
import mobile.agrhub.farmbox.utils.BaseActivity;

public class SettingSingleActivity extends BaseActivity {

    TextView mValueLabel;
    EditText mValue;
    Button mDone;

    FarmAPI farmAPI = APIHelper.getFarmAPI();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_single);

        bindingControls();
        setupControlEvents();
        initData();
        configActionBar();
    }

    private void configActionBar() {
        String name = getIntent().getStringExtra("name");
        getSupportActionBar().setTitle(name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void bindingControls() {
        mDone = (Button) findViewById(R.id.btn_set);
        mValueLabel = (TextView) findViewById(R.id.tv_value_label);
        mValue = (EditText) findViewById(R.id.et_value);
    }

    private void setupControlEvents() {
        mDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestDone();
            }
        });
    }

    private void initData() {
        Intent intent = getIntent();

        String field = intent.getStringExtra("field");
        String value = intent.getStringExtra("value");

        if (value.equalsIgnoreCase("-1")) {
            mValue.setHint("---");
        }
        else {
            mValue.setText(value);
        }

        mValueLabel.setText(getString(R.string.setting_single_current_value) + getUnitFromField(field));
    }

    private String getUnitFromField(String field) {
        switch (field) {
            case "factory_water_volume":
                return " (l):";
            case "factory_liquid_volume":
                return " (ml):";
            case "factory_tank_height":
                return "cm";
        }

        return ":";
    }

    private void requestDone() {
        String value = mValue.getText().toString();

        String field = getIntent().getStringExtra("field");
        String farmID = getIntent().getStringExtra("farmID");
        String factoryID = getIntent().getStringExtra("factoryID");

        List<Param> params = new ArrayList<>();
        params.add(new Param(field, value));

        showLoading();
        farmAPI.saveFarmSetting(factoryID, farmID, params, new APIHelper.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                SettingSingleActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        final AlertDialog dialog = new AlertDialog.Builder(SettingSingleActivity.this, R.style.AppTheme_Dialog_Alert)
                                .setTitle(R.string.app_name)
                                .setMessage(R.string.setting_save_success)
                                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        SettingSingleActivity.this.finish();
                                    }
                                }).create();
                        if (!dialog.isShowing()) {
                            dialog.show();
                        }
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                SettingSingleActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }
}
