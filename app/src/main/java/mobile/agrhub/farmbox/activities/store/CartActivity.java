package mobile.agrhub.farmbox.activities.store;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.adapters.CartAdapter;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.StoreAPI;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Utils;

public class CartActivity extends BaseActivity implements CartAdapter.OnClickListener {
    private ListView listView;
    private CartAdapter adapter;

    private TextView numberOfItem;
    private TextView discountPrice;
    private ImageButton orderBtn;

    private StoreAPI storeAPI = APIHelper.getStoreAPI();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        bindingControls();
        initData();
        setupControlEvents();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void bindingControls() {
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        listView = (ListView) findViewById(R.id.listView);

        numberOfItem = (TextView) findViewById(R.id.number_of_item);
        discountPrice = (TextView) findViewById(R.id.tv_discount_price);
        orderBtn = (ImageButton) findViewById(R.id.ib_order);
    }

    private void initData() {
        if (adapter == null) {
            adapter = new CartAdapter(this, this);
            listView.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }

        updateCart();
    }

    private void setupControlEvents() {
        orderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postCart();
            }
        });
    }

    private void postCart() {
        showLoading();
        storeAPI.postCart(new APIHelper.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                CartActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        CartActivity.this.hideLoading();
                        FarmBoxAppController app = FarmBoxAppController.getInstance();
                        app.clearAll();

                        final AlertDialog dialog = new AlertDialog.Builder(CartActivity.this, R.style.AppTheme_Dialog_Alert)
                                .setMessage(R.string.order_completed)
                                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                })
                                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {
                                        CartActivity.this.finish();
                                    }
                                })
                                .create();
                        if (!dialog.isShowing()) {
                            dialog.show();
                        }
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                CartActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }

    private void updateCart() {
        FarmBoxAppController app = FarmBoxAppController.getInstance();

        numberOfItem.setText("" + app.getTotalAmount());

        long finalPrice = app.getTotalDiscountPrice();
        discountPrice.setText(Utils.formatPrice(finalPrice));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onMinusClicked(String productId) {
        FarmBoxAppController app = FarmBoxAppController.getInstance();
        app.minusProduct(productId);
        updateCart();

        adapter.notifyDataSetChanged();
    }

    @Override
    public void onPlusClicked(String productId) {
        FarmBoxAppController app = FarmBoxAppController.getInstance();
        app.plusProduct(productId);
        updateCart();

        adapter.notifyDataSetChanged();
    }
}
