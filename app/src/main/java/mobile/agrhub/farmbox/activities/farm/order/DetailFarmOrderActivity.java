package mobile.agrhub.farmbox.activities.farm.order;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.adapters.order.DetailOrderCartAdapter;
import mobile.agrhub.farmbox.custom_view.layout.ExpandableHeightListView;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.model.FarmOrder;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Constants;

public class DetailFarmOrderActivity extends BaseActivity implements View.OnClickListener {
    public static final int REQUEST_UPDATE_FARM_PRODUCT_CODE = 401;

    @BindView(R.id.tv_order_id)
    TextView mOrderId;
    @BindView(R.id.tv_order_date)
    TextView mOrderDate;
    @BindView(R.id.tv_buyer)
    TextView mBuyer;
    @BindView(R.id.tv_status)
    TextView mStatus;
    @BindView(R.id.tv_total_item)
    TextView mTotalItem;
    @BindView(R.id.tv_sub_total)
    TextView mSubTotal;
    @BindView(R.id.tv_last_update)
    TextView mLastUpdate;
    @BindView(R.id.lv_cart)
    ExpandableHeightListView mCartTableView;
    DetailOrderCartAdapter mAdapter;
    @BindView(R.id.tv_total_item_2)
    TextView mTotalItem2;
    @BindView(R.id.tv_sub_total_2)
    TextView mSubTotal2;
    @BindView(R.id.ll_control_view)
    LinearLayout mControlView;
    @BindView(R.id.ll_cancel_order_view)
    LinearLayout mCancelOrderView;
    @BindView(R.id.btn_cancel_order)
    Button mCancelOrderBtn;
    @BindView(R.id.ll_return_order_view)
    LinearLayout mReturnOrderView;
    @BindView(R.id.btn_return_order)
    Button mReturnOrderBtn;
    @BindView(R.id.ll_accept_order_view)
    LinearLayout mAcceptOrderView;
    @BindView(R.id.btn_accept_order)
    Button mAcceptOrderBtn;
    @BindView(R.id.ll_assign_traceability_code_view)
    LinearLayout mAssignCodeView;
    @BindView(R.id.btn_assign_traceability_code)
    Button mAssignCodeBtn;
    @BindView(R.id.ll_delivery_order_view)
    LinearLayout mDeliveryOrderView;
    @BindView(R.id.btn_delivery_order)
    Button mDeliveryOrderBtn;
    @BindView(R.id.ll_finish_order_view)
    LinearLayout mFinishOrderView;
    @BindView(R.id.btn_finish_order)
    Button mFinishOrderBtn;

    private long orderId;
    private FarmOrder order;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_farm_order);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        orderId = intent.getLongExtra(Constants.ORDER_ID_KEY, 0);
        if (orderId <= 0) {
            finish();
        }

        mCancelOrderBtn.setOnClickListener(this);
        mReturnOrderBtn.setOnClickListener(this);
        mAcceptOrderBtn.setOnClickListener(this);
        mAssignCodeBtn.setOnClickListener(this);
        mDeliveryOrderBtn.setOnClickListener(this);
        mFinishOrderBtn.setOnClickListener(this);

        configActionBar();
    }

    private void configActionBar() {
        if (order == null) {
            getSupportActionBar().setTitle(getString(R.string.order_detail));
        } else {
            getSupportActionBar().setTitle(order.displayOrderNumber());
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();

        loadData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    private void refreshView() {
        configActionBar();

        if (order != null) {
            mOrderId.setText(order.displayOrderNumber());
            mOrderDate.setText(order.displayOrderDate());
            mBuyer.setText("");
            mStatus.setText(order.displayStatus(this));
            mTotalItem.setText(order.displayTotalItem());
            mSubTotal.setText(order.displaySubTotalPrice());
            mLastUpdate.setText(order.displayUpdateDate());

            if (mAdapter == null) {
                mAdapter = new DetailOrderCartAdapter(this, order.carts);
                mCartTableView.setAdapter(mAdapter);
                mCartTableView.setExpanded(true);
            } else {
                mAdapter.update(order.carts);
            }
            mTotalItem2.setText(order.displayTotalItem());
            mSubTotal2.setText(order.displaySubTotalPrice());

            if (order.canControl()) {
                mControlView.setVisibility(View.VISIBLE);
                switch (order.orderState) {
                    case FarmOrder.STATE_NEW:
                        mCancelOrderView.setVisibility(View.VISIBLE);
                        mReturnOrderView.setVisibility(View.GONE);
                        mAcceptOrderView.setVisibility(View.VISIBLE);
                        mAssignCodeView.setVisibility(View.VISIBLE);
                        mDeliveryOrderView.setVisibility(View.GONE);
                        mFinishOrderView.setVisibility(View.GONE);
                        break;
                    case FarmOrder.STATE_PROCESS:
                        mCancelOrderView.setVisibility(View.VISIBLE);
                        mReturnOrderView.setVisibility(View.GONE);
                        mAcceptOrderView.setVisibility(View.GONE);
                        mAssignCodeView.setVisibility(View.VISIBLE);
                        mDeliveryOrderView.setVisibility(View.VISIBLE);
                        mFinishOrderView.setVisibility(View.GONE);
                        break;
                    case FarmOrder.STATE_DELIVERY:
                        mCancelOrderView.setVisibility(View.GONE);
                        mReturnOrderView.setVisibility(View.VISIBLE);
                        mAcceptOrderView.setVisibility(View.GONE);
                        mAssignCodeView.setVisibility(View.GONE);
                        mDeliveryOrderView.setVisibility(View.GONE);
                        mFinishOrderView.setVisibility(View.VISIBLE);
                        break;
                    case FarmOrder.STATE_RETURN:
                        mCancelOrderView.setVisibility(View.VISIBLE);
                        mReturnOrderView.setVisibility(View.GONE);
                        mAcceptOrderView.setVisibility(View.GONE);
                        mAssignCodeView.setVisibility(View.GONE);
                        mDeliveryOrderView.setVisibility(View.VISIBLE);
                        mFinishOrderView.setVisibility(View.VISIBLE);
                        break;
                }
            } else {
                mControlView.setVisibility(View.GONE);
            }
        }
    }

    private void loadData() {
        showLoading();
        APIHelper.getSaleAPI().getOrder(orderId, new APIHelper.Callback<FarmOrder>() {
            @Override
            public void onSuccess(FarmOrder data) {
                order = data;
                DetailFarmOrderActivity activity = DetailFarmOrderActivity.this;
                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            DetailFarmOrderActivity activity = DetailFarmOrderActivity.this;
                            if (activity != null) {
                                activity.hideLoading();
                                activity.refreshView();
                            }
                        }
                    });
                }
            }

            @Override
            public void onFail(final int errorCode, final String error) {

                DetailFarmOrderActivity activity = DetailFarmOrderActivity.this;
                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            DetailFarmOrderActivity activity = DetailFarmOrderActivity.this;
                            if (activity != null) {
                                activity.hideLoading();
                                activity.handleError(errorCode, error);
                            }
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel_order:
                changeOrderStatus(orderId, FarmOrder.STATE_CANCEL);
                break;
            case R.id.btn_return_order:
                changeOrderStatus(orderId, FarmOrder.STATE_RETURN);
                break;
            case R.id.btn_accept_order:
                changeOrderStatus(orderId, FarmOrder.STATE_PROCESS);
                break;
            case R.id.btn_assign_traceability_code:
                assignTraceabilityCode();
                break;
            case R.id.btn_delivery_order:
                changeOrderStatus(orderId, FarmOrder.STATE_DELIVERY);
                break;
            case R.id.btn_finish_order:
                changeOrderStatus(orderId, FarmOrder.STATE_FINISH);
                break;
            default:
                break;
        }
    }

    private void assignTraceabilityCode() {
        Gson gson = new Gson();

        Intent intent = new Intent(this, AssignTraceabilityCodeActivity.class);
        intent.putExtra(Constants.ORDER_INFO_KEY, gson.toJson(order));
        startActivity(intent);
    }

    private void changeOrderStatus(long orderId, int status) {
        showLoading();
        APIHelper.getSaleAPI().updateOrderField(orderId, Constants.FIELD_ORDER_STATE, String.valueOf(status), new APIHelper.Callback<FarmOrder>() {
            @Override
            public void onSuccess(final FarmOrder data) {
                DetailFarmOrderActivity activity = DetailFarmOrderActivity.this;
                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            DetailFarmOrderActivity activity = DetailFarmOrderActivity.this;
                            if (activity != null) {
                                activity.order = data;
                                activity.refreshView();
                            }
                        }
                    });
                }
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                DetailFarmOrderActivity activity = DetailFarmOrderActivity.this;
                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            DetailFarmOrderActivity activity = DetailFarmOrderActivity.this;
                            if (activity != null) {
                                activity.hideLoading();
                                activity.handleError(errorCode, error);
                            }
                        }
                    });
                }
            }
        });
    }
}
