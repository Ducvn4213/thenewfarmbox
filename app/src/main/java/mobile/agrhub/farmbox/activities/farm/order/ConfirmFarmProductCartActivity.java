package mobile.agrhub.farmbox.activities.farm.order;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.adapters.order.CartAdapter;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.model.FarmCart;
import mobile.agrhub.farmbox.service.model.FarmOrder;
import mobile.agrhub.farmbox.service.model.User;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Constants;
import mobile.agrhub.farmbox.utils.Utils;

public class ConfirmFarmProductCartActivity extends BaseActivity implements CartAdapter.OnClickListener {
    @BindView(R.id.gv_farm_product)
    GridView gvProducts;
    @BindView(R.id.tv_total)
    TextView mTotalPrice;

    private CartAdapter listAdapter;
    private List<FarmCart> carts;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_farm_product_cart);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        String data = intent.getStringExtra(Constants.ORDER_CART_LIST_KEY);
        if (data != null && !data.isEmpty()) {
            Gson gson = new Gson();
            carts = gson.fromJson(data, new TypeToken<List<FarmCart>>() {
            }.getType());
        }
        if (carts == null || carts.size() <= 0) {
            finish();
        }

        configActionBar();

        listAdapter = new CartAdapter(this, carts, this);
        gvProducts.setAdapter(listAdapter);
        mTotalPrice.setText(Utils.formatPriceVND(listAdapter.totalPrice()));
    }

    private void configActionBar() {
        getSupportActionBar().setTitle(R.string.confirm_order_title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.save_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Gson gson = new Gson();
            String data = gson.toJson(carts);

            // Prepare data intent
            Intent intent = new Intent();
            intent.putExtra(Constants.ORDER_CART_LIST_KEY, data);

            // Activity finished ok, return the data
            setResult(Activity.RESULT_OK, intent);
            finish();
        } else if (item.getItemId() == R.id.action_save) {
            saveOrder();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMinusClicked(String productId) {
        listAdapter.notifyDataSetChanged();
        mTotalPrice.setText(Utils.formatPriceVND(listAdapter.totalPrice()));
    }

    @Override
    public void onProductAmountClicked(final long productId, int amount) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppTheme_Dialog_Alert);
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setText(String.valueOf(amount));
        input.setSingleLine(true);

        FrameLayout container = new FrameLayout(this);
        FrameLayout.LayoutParams params = new  FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.leftMargin = getResources().getDimensionPixelSize(R.dimen.margin_16);
        params.rightMargin = getResources().getDimensionPixelSize(R.dimen.margin_16);
        input.setLayoutParams(params);
        container.addView(input);

        builder
                .setTitle(R.string.amount)
                .setView(container)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        String value = input.getText().toString();

                        int amount = 0;
                        try {
                            amount = Utils.str2Int(value);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        ConfirmFarmProductCartActivity.this.addToCart(productId, amount);

                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                    }

                });

        builder.show();
        input.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    @Override
    public void onPlusClicked(String productId) {
        listAdapter.notifyDataSetChanged();
        mTotalPrice.setText(Utils.formatPriceVND(listAdapter.totalPrice()));
    }

    @Override
    public void onAddToCartClicked(long productId) {
        listAdapter.notifyDataSetChanged();
        mTotalPrice.setText(Utils.formatPriceVND(listAdapter.totalPrice()));
    }

    @Override
    public void onAssignQRCodeToCartClicked(FarmCart cart) {

    }

    private void addToCart(long productId, int amount) {
        this.listAdapter.addToCart(productId, amount, 0);
        this.listAdapter.notifyDataSetChanged();
        mTotalPrice.setText(Utils.formatPriceVND(listAdapter.totalPrice()));
    }

    private void refreshListView() {
        if (null != gvProducts) {
            if (null == listAdapter) {
                listAdapter = new CartAdapter(this, carts, this);
                gvProducts.setAdapter(listAdapter);
            } else {
                listAdapter.update(carts);
                listAdapter.notifyDataSetChanged();
            }
            gvProducts.smoothScrollToPosition(0);
        }
    }

    private void saveOrder() {
        if (listAdapter.totalItem() <= 0) {
            showErrorDialog(R.string.cart_is_empty);
            return;
        }

        User user = FarmBoxAppController.getInstance().getUser();
        if (user == null) {
            return;
        }
        showLoading();
        List<FarmCart> realCart = new ArrayList<>();
        for (FarmCart item : carts) {
            if (item.cartItemQuantity > 0) {
                realCart.add(item);
            }
        }
        APIHelper.getSaleAPI().createOrder(user.userId, realCart, new APIHelper.Callback<FarmOrder>() {
            @Override
            public void onSuccess(FarmOrder data) {
                Activity activity = ConfirmFarmProductCartActivity.this;
                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (ConfirmFarmProductCartActivity.this != null) {
                                ConfirmFarmProductCartActivity.this.hideLoading();
                                ConfirmFarmProductCartActivity.this.saveCompleted();
                            }
                        }
                    });
                }
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                Activity activity = ConfirmFarmProductCartActivity.this;
                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (ConfirmFarmProductCartActivity.this != null) {
                                ConfirmFarmProductCartActivity.this.hideLoading();
                                ConfirmFarmProductCartActivity.this.handleError(errorCode, error);
                            }
                        }
                    });
                }
            }
        });
    }

    private void saveCompleted() {
        // Prepare data intent
        Intent intent = new Intent();

        // Activity finished ok, return the data
        setResult(Activity.RESULT_CANCELED, intent);

        finish();
    }
}
