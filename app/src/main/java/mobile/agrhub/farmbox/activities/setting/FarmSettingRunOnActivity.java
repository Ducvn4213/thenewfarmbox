package mobile.agrhub.farmbox.activities.setting;

import android.app.TimePickerDialog;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.FarmSettingDetailData;
import mobile.agrhub.farmbox.service.network.Param;
import mobile.agrhub.farmbox.utils.Utils;

public class FarmSettingRunOnActivity extends BaseFarmSettingItemActivity {
    @BindView(R.id.tv_title_from)
    TextView mTitleFrom;
    @BindView(R.id.tv_run_from)
    TextView mRunFrom;
    @BindView(R.id.tv_title_to)
    TextView mTitleTo;
    @BindView(R.id.tv_run_to)
    TextView mRunTo;

    FarmSettingDetailData fromData;
    FarmSettingDetailData toData;

    @Override
    protected int contentView() {
        return R.layout.activity_farm_setting_run_on;
    }

    @Override
    protected void initView() {
        super.initView();
        if (item.getData() != null && item.getData().size() >= 2) {
            fromData = item.getData().get(0);
            toData = item.getData().get(1);
        }

        if (fromData == null || toData == null) {
            finish();
            return;
        }

        mTitleFrom.setText(fromData.getName());
        mRunFrom.setText(fromData.getValue());

        mTitleTo.setText(toData.getName());
        mRunTo.setText(toData.getValue());

        mRunFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int hour, minute;
                if (fromData.getValue() != null && !fromData.getValue().isEmpty() && fromData.getValue().contains(":")) {
                    String[] timePart = fromData.getValue().split(":");
                    hour = Integer.parseInt(timePart[0]);
                    minute = Integer.parseInt(timePart[1]);
                } else {
                    Calendar calendar = Calendar.getInstance();
                    hour = calendar.get(Calendar.HOUR_OF_DAY);
                    minute = calendar.get(Calendar.MINUTE);
                }
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(FarmSettingRunOnActivity.this, R.style.AppTheme_Dialog, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        mRunFrom.setText( Utils.correctTimeUnit(selectedHour) + ":" + Utils.correctTimeUnit(selectedMinute));
                    }
                }, hour, minute, true);
                mTimePicker.show();
            }
        });

        mRunTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int hour, minute;
                if (toData.getValue() != null && !toData.getValue().isEmpty() && toData.getValue().contains(":")) {
                    String[] timePart = toData.getValue().split(":");
                    hour = Integer.parseInt(timePart[0]);
                    minute = Integer.parseInt(timePart[1]);
                } else {
                    Calendar calendar = Calendar.getInstance();
                    hour = calendar.get(Calendar.HOUR_OF_DAY);
                    minute = calendar.get(Calendar.MINUTE);
                }

                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(FarmSettingRunOnActivity.this, R.style.AppTheme_Dialog, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        mRunTo.setText( Utils.correctTimeUnit(selectedHour) + ":" + Utils.correctTimeUnit(selectedMinute));
                    }
                }, hour, minute, true);
                mTimePicker.show();
            }
        });
    }

    @Override
    protected boolean validate() {
        String minVal = mRunFrom.getText().toString().trim();
        String maxVal = mRunTo.getText().toString().trim();
        int runFromVal = 0;
        int runToVal = 0;

        // check empty min
        if (minVal.isEmpty()) {
            showErrorDialog(getString(R.string.field_is_required, fromData.getName()));
            return false;
        }
        // check empty max
        if (maxVal.isEmpty()) {
            showErrorDialog(getString(R.string.field_is_required, toData.getName()));
            return false;
        }

        runFromVal = Integer.parseInt(minVal.replace(":", ""));
        runToVal = Integer.parseInt(maxVal.replace(":", ""));

        // check min in range
        if (fromData.getExpectData() != null && fromData.getExpectData().size() >= 2) {
            String fromStr = fromData.getExpectData().get(0).toString();
            int fromVal = Integer.parseInt(fromStr.replace(":", ""));

            String toStr = fromData.getExpectData().get(1).toString();
            int toVal = Integer.parseInt(toStr.replace(":", ""));

            if (runFromVal < fromVal || runFromVal > toVal) {
                showErrorDialog(getString(R.string.field_between, fromData.getName(), fromStr, toStr));
                return false;
            }
        }

        // check max in range
        if (toData.getExpectData() != null && toData.getExpectData().size() >= 2) {
            String fromStr = toData.getExpectData().get(0).toString();
            int fromVal = Integer.parseInt(fromStr.replace(":", ""));

            String toStr = toData.getExpectData().get(1).toString();
            int toVal = Integer.parseInt(toStr.replace(":", ""));

            if (runToVal < fromVal || runToVal > toVal) {
                showErrorDialog(getString(R.string.field_between, toData.getName(), fromStr, toStr));
                return false;
            }
        }

        // check min <= max
//        if (runToVal < runFromVal) {
//            showErrorDialog(getString(R.string.field_lower_field, fromData.getName(), toData.getName()));
//            return false;
//        }

        return true;
    }

    @Override
    protected List<Param> buildParam(List<Param> params) {
        String minStr = mRunFrom.getText().toString().trim();
        String maxStr = mRunTo.getText().toString().trim();

        String[] minPart = minStr.split(":");
        int minVal = Integer.parseInt(minPart[0]) * 60 + Integer.parseInt(minPart[1]);

        String[] maxPart = maxStr.split(":");
        int maxVal = Integer.parseInt(maxPart[0]) * 60 + Integer.parseInt(maxPart[1]);

        params.add(new Param(fromData.getField(), minVal + ""));
        params.add(new Param(toData.getField(), maxVal + ""));
        return params;
    }
}
