package mobile.agrhub.farmbox.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TypefaceSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.activities.account.EditAccountActivity;
import mobile.agrhub.farmbox.activities.add.AddFarmActivity;
import mobile.agrhub.farmbox.activities.dairy.AddDiaryActivity;
import mobile.agrhub.farmbox.activities.dairy.FilterDiaryActivity;
import mobile.agrhub.farmbox.activities.registration.PhoneRegistrationActivity;
import mobile.agrhub.farmbox.activities.setting.SettingsContainerActivity;
import mobile.agrhub.farmbox.activities.setting.SettingsGreenhouseActivity;
import mobile.agrhub.farmbox.activities.setting.SettingsHomeActivity;
import mobile.agrhub.farmbox.activities.setting.SettingsMushroomActivity;
import mobile.agrhub.farmbox.activities.store.CartActivity;
import mobile.agrhub.farmbox.adapters.MainFragmentAdapter;
import mobile.agrhub.farmbox.fragments.DiaryFragment;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.model.Farm;
import mobile.agrhub.farmbox.service.model.User;
import mobile.agrhub.farmbox.service.network.Config;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Utils;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {
    public static final int REQUEST_DIARY_FILTER_CODE = 104;
    DrawerLayout mDrawer;
    NavigationView mNavigationView;
    ImageButton mMenu;

    MainFragmentAdapter mMainFragmentAdapter;
    ViewPager mViewPager;

    ImageButton mSettings;
    ImageButton mAdd;
    ImageButton mEdit;
    ImageButton mFilter;
    FrameLayout mStore;

    FrameLayout totalCartItemBadge;
    TextView totalCartItem;

    ImageView mHeader;
    TextView mFirstName;
    TextView mLastName;
    TextView mTitle;

    private Intent intentService;
    private int currentSelectedScreenIndex;

    private static MainActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.animator.activity_open_translate, R.animator.activity_close_scale);
        setContentView(R.layout.activity_main);

        bindingControls();
        setupControlEvents();
        configUI();
        initData();
    }

    @Override
    protected void onResume() {
        super.onResume();

        instance = MainActivity.this;

        if (currentSelectedScreenIndex == MainFragmentAdapter.DIARY_FRAGMENT_INDEX) {
//            DiaryFragment diary = mMainFragmentAdapter.getDiaryFragment();
//            this.mFilter.setSelected(diary.hasFilter());
        } else if (currentSelectedScreenIndex == MainFragmentAdapter.STORE_FRAGMENT_INDEX) {
            this.updateAlertIcon();
        }
    }

    private void bindingControls() {
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mMenu = (ImageButton) findViewById(R.id.ib_menu);

        mViewPager = (ViewPager) findViewById(R.id.vp_container);

        mSettings = (ImageButton) findViewById(R.id.ib_settings);
        mFilter = (ImageButton) findViewById(R.id.ib_calendar);
        mAdd = (ImageButton) findViewById(R.id.ib_add);
        mEdit = (ImageButton) findViewById(R.id.ib_edit);
        mStore = (FrameLayout) findViewById(R.id.fl_store);

        totalCartItemBadge = (FrameLayout) findViewById(R.id.view_alert_red_circle);
        totalCartItem = (TextView) findViewById(R.id.view_alert_count_text_view);

        mHeader = (ImageView) mNavigationView.getHeaderView(0).findViewById(R.id.iv_header);
        mFirstName = (TextView) mNavigationView.getHeaderView(0).findViewById(R.id.tv_firstname);
        mLastName = (TextView) mNavigationView.getHeaderView(0).findViewById(R.id.tv_lastname);

        mTitle = (TextView) findViewById(R.id.tv_title);
    }

    public static void changeAppTitle(String title) {
        try {
            if (instance.currentSelectedScreenIndex == MainFragmentAdapter.HOME_FRAGMENT_INDEX) {
                instance.privateChangeAppTitle(title);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void privateChangeAppTitle(String title) {
        mTitle.setText(title);
    }

    public static void hideSettingIcon() {
        instance.mSettings.setVisibility(View.GONE);
    }

    private void initData() {
        mMainFragmentAdapter = new MainFragmentAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mMainFragmentAdapter);
        mViewPager.setOffscreenPageLimit(3);

        mTitle.setText(R.string.nav_menu_home);
        mSettings.setVisibility(View.VISIBLE);
        mFilter.setVisibility(View.GONE);
        mAdd.setVisibility(View.VISIBLE);
        mEdit.setVisibility(View.GONE);
        mStore.setVisibility(View.GONE);


        User user = FarmBoxAppController.getInstance().getUser();
        mFirstName.setText(user.user_first_name);
        mLastName.setText(user.user_last_name);
    }

    private void setupControlEvents() {
        mNavigationView.setNavigationItemSelectedListener(this);

        mMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawer.openDrawer(GravityCompat.START);
            }
        });

        mAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentSelectedScreenIndex == MainFragmentAdapter.DIARY_FRAGMENT_INDEX) { // click add button at diary screen
                    Intent intent = new Intent(MainActivity.this, AddDiaryActivity.class);
                    MainActivity.this.startActivity(intent);
                } else {
                    Intent intent = new Intent(MainActivity.this, AddFarmActivity.class);
                    MainActivity.this.startActivity(intent);
                }
            }
        });

        mFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentSelectedScreenIndex == MainFragmentAdapter.DIARY_FRAGMENT_INDEX) { // click add button at diary screen
                    DiaryFragment diary = mMainFragmentAdapter.getDiaryFragment();

                    Intent intent = new Intent(MainActivity.this, FilterDiaryActivity.class);

                    Bundle bundle = new Bundle();
//                    bundle.putString("DIARY_FILTER_SELECTED_FARM_ID", diary.getmSelectedFarmId());
//                    bundle.putInt("DIARY_FILTER_SELECTED_FROM_YEAR", diary.getmSelectedFromYear());
//                    bundle.putInt("DIARY_FILTER_SELECTED_FROM_MONTH", diary.getmSelectedFromMonth());
//                    bundle.putInt("DIARY_FILTER_SELECTED_FROM_DATE", diary.getmSelectedFromDate());
//                    bundle.putInt("DIARY_FILTER_SELECTED_TO_YEAR", diary.getmSelectedToYear());
//                    bundle.putInt("DIARY_FILTER_SELECTED_TO_MONTH", diary.getmSelectedToMonth());
//                    bundle.putInt("DIARY_FILTER_SELECTED_TO_DATE", diary.getmSelectedToDate());

                    intent.putExtras(bundle); //Put your id to your next Intent

                    MainActivity.this.startActivityForResult(intent, REQUEST_DIARY_FILTER_CODE);
                }
            }
        });

        mSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Farm farm = FarmBoxAppController.getInstance().getSelectedFarm();
                if (null == farm) {
                    return;
                }

                Intent intent = new Intent(MainActivity.this, SettingsHomeActivity.class);
                int type = (int) farm.farm_type;
                switch (type) {
                    case Farm.FAMILY:
                        intent = new Intent(MainActivity.this, SettingsHomeActivity.class);
                        break;
                    case Farm.MUSHROOM:
                        intent = new Intent(MainActivity.this, SettingsMushroomActivity.class);
                        break;
                    case Farm.GREEN_HOUSE:
                        intent = new Intent(MainActivity.this, SettingsGreenhouseActivity.class);
                        break;
                    case Farm.HYDROPONIC:
                        intent = new Intent(MainActivity.this, SettingsContainerActivity.class);
                        break;
                    case Farm.AQUAPONIC:
                        intent = new Intent(MainActivity.this, SettingsContainerActivity.class);
                        break;
                    case Farm.FIELD:
                        break;
                    case Farm.CONTAINER:
                        intent = new Intent(MainActivity.this, SettingsContainerActivity.class);
                        break;
                }
                startActivity(intent);
            }
        });

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentSelectedScreenIndex = position;
                switch (position) {
                    case MainFragmentAdapter.HOME_FRAGMENT_INDEX:
                        mTitle.setText(R.string.nav_menu_home);
                        mSettings.setVisibility(View.VISIBLE);
                        mFilter.setVisibility(View.GONE);
                        mAdd.setVisibility(View.VISIBLE);
                        mEdit.setVisibility(View.GONE);
                        mStore.setVisibility(View.GONE);
                        break;
                    case MainFragmentAdapter.DIARY_FRAGMENT_INDEX:
                        mTitle.setText(R.string.nav_menu_diary);
                        mSettings.setVisibility(View.GONE);
                        mFilter.setVisibility(View.VISIBLE);
                        mAdd.setVisibility(View.VISIBLE);
                        mEdit.setVisibility(View.GONE);
                        mStore.setVisibility(View.GONE);

                        DiaryFragment diary = mMainFragmentAdapter.getDiaryFragment();
//                        diary.setmSelectedFarmId("");
//                        diary.setmSelectedFromYear(0);
//                        diary.setmSelectedFromMonth(0);
//                        diary.setmSelectedFromDate(0);
//                        diary.setmSelectedToYear(0);
//                        diary.setmSelectedToMonth(0);
//                        diary.setmSelectedToDate(0);
                        break;
                    case MainFragmentAdapter.STORE_FRAGMENT_INDEX:
                        mTitle.setText(R.string.nav_menu_store);
                        mSettings.setVisibility(View.GONE);
                        mFilter.setVisibility(View.GONE);
                        mAdd.setVisibility(View.GONE);
                        mEdit.setVisibility(View.GONE);
                        mStore.setVisibility(View.VISIBLE);
                        break;
                    case MainFragmentAdapter.ACCOUNT_FRAGMENT_INDEX:
                        mTitle.setText(R.string.nav_menu_account);
                        mSettings.setVisibility(View.GONE);
                        mFilter.setVisibility(View.GONE);
                        mAdd.setVisibility(View.GONE);
                        mEdit.setVisibility(View.VISIBLE);
                        mStore.setVisibility(View.GONE);
                        break;
                    case MainFragmentAdapter.SETTING_FRAGMENT_INDEX:
                        mTitle.setText(R.string.nav_menu_settings);
                        mSettings.setVisibility(View.GONE);
                        mFilter.setVisibility(View.GONE);
                        mAdd.setVisibility(View.GONE);
                        mEdit.setVisibility(View.GONE);
                        mStore.setVisibility(View.GONE);
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, EditAccountActivity.class);
                startActivity(intent);
            }
        });

        mStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FarmBoxAppController app = FarmBoxAppController.getInstance();
                if (app.getTotalAmount() > 0) {
                    Intent intent = new Intent(MainActivity.this, CartActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(MainActivity.this, R.string.please_add_product_to_cart, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void configUI() {
        Menu menu = mNavigationView.getMenu();
        for (int i = 0; i < menu.size(); i++) {
            MenuItem menuItem = menu.getItem(i);
            if (menuItem != null) {
                SpannableString spannableString = new SpannableString(menuItem.getTitle());
                spannableString.setSpan(new TypefaceSpan("sans-serif-light"), 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                menuItem.setTitle(spannableString);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        } else {
            onHBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            mViewPager.setCurrentItem(MainFragmentAdapter.HOME_FRAGMENT_INDEX, false);
        } else if (id == R.id.nav_diary) {
            mViewPager.setCurrentItem(MainFragmentAdapter.DIARY_FRAGMENT_INDEX, false);
        } else if (id == R.id.nav_store) {
            mViewPager.setCurrentItem(MainFragmentAdapter.STORE_FRAGMENT_INDEX, false);
        } else if (id == R.id.nav_account) {
            mViewPager.setCurrentItem(MainFragmentAdapter.ACCOUNT_FRAGMENT_INDEX, false);
        } else if (id == R.id.nav_settings) {
            mViewPager.setCurrentItem(MainFragmentAdapter.SETTING_FRAGMENT_INDEX, false);
        } else if (id == R.id.nav_logout) {
            requestLogout();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void requestLogout() {
        final AlertDialog dialog = new AlertDialog.Builder(MainActivity.this, R.style.AppTheme_Dialog_Alert)
                .setTitle(R.string.app_name)
                .setMessage(R.string.logout_message)
                .setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        doLogout();
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    private void doLogout() {
        Utils.savePreference(MainActivity.this, Config.USER_TOKEN_KEY, "");
        Intent intent = new Intent(MainActivity.this, PhoneRegistrationActivity.class);
        startActivity(intent);
        MainActivity.this.finish();
    }

    boolean doubleBackToExitPressedOnce = false;

    public void onHBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, getString(R.string.double_back_to_exit_message), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    public void updateAlertIcon() {
        FarmBoxAppController app = FarmBoxAppController.getInstance();
        int numberOfCartItem = app.getTotalAmount();
        if (0 < numberOfCartItem) {
            totalCartItem.setText(String.valueOf(numberOfCartItem));
        } else {
            totalCartItem.setText("");
        }

        totalCartItemBadge.setVisibility((numberOfCartItem > 0) ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_DIARY_FILTER_CODE) {
                Bundle bundle = data.getExtras();
                if (bundle != null) {
//                    DiaryFragment diary = mMainFragmentAdapter.getDiaryFragment();
//                    if (bundle.containsKey("DIARY_FILTER_SELECTED_FARM_ID")) {
//                        diary.setmSelectedFarmId(bundle.getString("DIARY_FILTER_SELECTED_FARM_ID"));
//                    }
//                    if (bundle.containsKey("DIARY_FILTER_SELECTED_FROM_YEAR")) {
//                        diary.setmSelectedFromYear(bundle.getInt("DIARY_FILTER_SELECTED_FROM_YEAR", 0));
//                    }
//                    if (bundle.containsKey("DIARY_FILTER_SELECTED_FROM_MONTH")) {
//                        diary.setmSelectedFromMonth(bundle.getInt("DIARY_FILTER_SELECTED_FROM_MONTH", 0));
//                    }
//                    if (bundle.containsKey("DIARY_FILTER_SELECTED_FROM_DATE")) {
//                        diary.setmSelectedFromDate(bundle.getInt("DIARY_FILTER_SELECTED_FROM_DATE", 0));
//                    }
//                    if (bundle.containsKey("DIARY_FILTER_SELECTED_TO_YEAR")) {
//                        diary.setmSelectedToYear(bundle.getInt("DIARY_FILTER_SELECTED_TO_YEAR", 0));
//                    }
//                    if (bundle.containsKey("DIARY_FILTER_SELECTED_TO_MONTH")) {
//                        diary.setmSelectedToMonth(bundle.getInt("DIARY_FILTER_SELECTED_TO_MONTH", 0));
//                    }
//                    if (bundle.containsKey("DIARY_FILTER_SELECTED_TO_DATE")) {
//                        diary.setmSelectedToDate(bundle.getInt("DIARY_FILTER_SELECTED_TO_DATE", 0));
//                    }
                }
            }
        }
    }
}
