package mobile.agrhub.farmbox.activities.farm.order;

import android.Manifest;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PointF;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.activities.common.SingleChooseActivity;
import mobile.agrhub.farmbox.adapters.order.AssignQRCodeAdapter;
import mobile.agrhub.farmbox.custom_view.cell.InputChooseOptionCellView;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.model.FarmBasic;
import mobile.agrhub.farmbox.service.model.FarmCart;
import mobile.agrhub.farmbox.service.model.OptionItem;
import mobile.agrhub.farmbox.service.model.SerialPackage;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Constants;
import mobile.agrhub.farmbox.utils.Utils;

import static mobile.agrhub.farmbox.utils.Utils.PERMISSIONS_CAMERA_REQUEST;

public class AssignTraceabilityCodeDetailActivity extends BaseActivity implements AssignQRCodeAdapter.OnClickListener, QRCodeReaderView.OnQRCodeReadListener {
    private static final int REQUEST_SELECT_FARM = 204;

    @BindView(R.id.ci_product_avatar)
    CircleImageView mProductImage;
    @BindView(R.id.tv_product_name)
    TextView mProductName;
    @BindView(R.id.tv_product_weight)
    TextView mProductWeight;
    @BindView(R.id.tv_product_quantity)
    TextView mProductQuantity;
    @BindView(R.id.tv_product_remain)
    TextView mProductRemain;
    @BindView(R.id.iv_farm)
    InputChooseOptionCellView mFarm;
    @BindView(R.id.fl_scan_view)
    FrameLayout mScanView;
    @BindView(R.id.ib_close)
    ImageButton mCloseScanViewBtn;
    @BindView(R.id.ib_enter_code)
    ImageButton mEnterCodeBtn;
    @BindView(R.id.rd_qr_code)
    QRCodeReaderView mQRCodeReader;
    @BindView(R.id.lv_qr_code)
    ListView mListView;
    @BindView(android.R.id.empty)
    LinearLayout mEmptyView;
    @BindView(R.id.btn_add_with_serial)
    Button mAddWithSerial;
    @BindView(R.id.btn_add_with_qr_code)
    Button mAddWithQRCode;

    ImageLoader imageLoader = ImageLoader.getInstance();
    private AssignQRCodeAdapter mAdapter;
    private FarmCart cart;
    private boolean isShow = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign_traceability_code_detail);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        String dataStr = intent.getStringExtra(Constants.CART_INFO_KEY);
        if (dataStr != null && !dataStr.isEmpty()) {
            Gson gson = new Gson();
            cart = gson.fromJson(dataStr, FarmCart.class);
        }

        if (cart == null) {
            finish();
        }

        configActionBar();

        mProductImage.setImageResource(R.drawable.ic_photo_gray_24dp);
        if (cart.cartProduct != null) {
            String avatar = cart.cartProduct.getDisplayAvatar();
            if (avatar == null || avatar.isEmpty()) {
                mProductImage.setImageResource(R.drawable.ic_photo_gray_24dp);
            } else {
                if (imageLoader == null) {
                    imageLoader = ImageLoader.getInstance();
                }
                imageLoader.displayImage(avatar, mProductImage);
            }

            mProductName.setText(cart.cartProduct.productName);
            mProductWeight.setText(cart.cartProduct.displayWeight(this));
        }

        mProductQuantity.setText(cart.displayQuantity());
        mProductRemain.setText(Utils.i2s(remainQRCode()));

        mFarm.setTitle(R.string.farm);
        FarmBoxAppController app = FarmBoxAppController.getInstance();
        List<FarmBasic> farms = app.getFarmList();

        String name = "";
        long val = InputChooseOptionCellView.NONE;
        if (farms != null && farms.size() > 0) {
            for (FarmBasic item : farms) {
                if (item.farm_id == this.cart.farmId) {
                    name = item.farm_name;
                    val = item.farm_id;
                    break;
                }
            }
        }

        mFarm.setSeparatorVisibility(View.GONE);
        mFarm.setValue(name, val);
        mFarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseFarm();
            }
        });

        if (cart.serialList == null) {
            cart.serialList = new ArrayList<>();
        }
        mAdapter = new AssignQRCodeAdapter(this, cart.serialList, this);
        mListView.setAdapter(mAdapter);
        mListView.setEmptyView(mEmptyView);

        mAddWithSerial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogAddSerial();
            }
        });

        mAddWithQRCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isShow) {
                    mQRCodeReader.stopCamera();

                    ViewGroup.LayoutParams layoutParams = mScanView.getLayoutParams();
                    ValueAnimator slideAnimator = ValueAnimator
                            .ofInt(layoutParams.height, 0)
                            .setDuration(200);


                    slideAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                        @Override
                        public void onAnimationUpdate(ValueAnimator animation) {
                            // get the value the interpolator is at
                            Integer value = (Integer) animation.getAnimatedValue();
                            // I'm going to set the layout's height 1:1 to the tick
                            ViewGroup.LayoutParams layoutParams = mScanView.getLayoutParams();
                            layoutParams.height = value.intValue();
                            mScanView.setLayoutParams(layoutParams);
                            // force all layouts to see which ones are affected by
                            // this layouts height change
                            mScanView.requestLayout();
                        }
                    });

                    // create a new animationset
                    AnimatorSet set = new AnimatorSet();
                    set.play(slideAnimator);
                    set.setInterpolator(new AccelerateDecelerateInterpolator());
                    set.start();

                    isShow = !isShow;
                } else {
                    if (ActivityCompat.checkSelfPermission(AssignTraceabilityCodeDetailActivity.this, Manifest.permission.CAMERA)
                            == PackageManager.PERMISSION_GRANTED) {
                        initQRCodeReaderView();
                    } else {
                        requestCameraPermission();
                    }
                }
            }
        });

        mCloseScanViewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isShow) {
                    mQRCodeReader.stopCamera();

                    ViewGroup.LayoutParams layoutParams = mScanView.getLayoutParams();
                    ValueAnimator slideAnimator = ValueAnimator
                            .ofInt(layoutParams.height, 0)
                            .setDuration(100);
                    slideAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                        @Override
                        public void onAnimationUpdate(ValueAnimator animation) {
                            // get the value the interpolator is at
                            Integer value = (Integer) animation.getAnimatedValue();
                            // I'm going to set the layout's height 1:1 to the tick
                            ViewGroup.LayoutParams layoutParams = mScanView.getLayoutParams();
                            layoutParams.height = value.intValue();
                            mScanView.setLayoutParams(layoutParams);
                            // force all layouts to see which ones are affected by
                            // this layouts height change
                            mScanView.requestLayout();
                        }
                    });

                    // create a new animationset
                    AnimatorSet set = new AnimatorSet();
                    set.play(slideAnimator);
                    set.setInterpolator(new AccelerateDecelerateInterpolator());
                    set.start();

                    isShow = !isShow;
                }
            }
        });

        mEnterCodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogAddQRCode();
            }
        });
    }

    private void configActionBar() {
        getSupportActionBar().setTitle(R.string.assign_traceability_code_title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isShow) {
            mQRCodeReader.startCamera();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isShow) {
            mQRCodeReader.stopCamera();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.done_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.action_done) {
            int totalQRCode = totalQRCode();
            if (cart.farmId <= 0) {
                showErrorDialog(R.string.please_choose_farm);
            } else if (cart.cartItemQuantity < totalQRCode) {
                showErrorDialog(R.string.qr_code_greater_than_cart_item);
            } else if (cart.cartItemQuantity > totalQRCode) {
                showErrorDialog(R.string.qr_code_less_than_cart_item);
            } else {
                Gson gson = new Gson();

                // Prepare data intent
                Intent data = new Intent();
                data.putExtra(Constants.CART_INFO_KEY, gson.toJson(this.cart));
                // Activity finished ok, return the data
                setResult(Activity.RESULT_OK, data);
                finish();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_SELECT_FARM) {
                String selectedVal = data.getStringExtra(Constants.SELECTED_OPTION_KEY);
                if (selectedVal != null && !selectedVal.isEmpty()) {
                    long farmId = Long.parseLong(selectedVal);

                    FarmBoxAppController app = FarmBoxAppController.getInstance();
                    List<FarmBasic> farmList = app.getFarmList();
                    if (farmList != null && farmList.size() > 0) {
                        for (FarmBasic farm : farmList) {
                            if (farmId == farm.farm_id) {
                                mFarm.setValue(farm.farm_name, farm.farm_id);
                                break;
                            }
                        }
                    }

                    this.cart.farmId = farmId;
                }
            }
        }
    }

    private void refreshListView() {
        if (null != mListView) {
            if (null == mAdapter) {
                mAdapter = new AssignQRCodeAdapter(this, cart.serialList, this);
                mListView.setAdapter(mAdapter);
            } else {
                mAdapter.setData(cart.serialList);
                mAdapter.notifyDataSetChanged();
            }
        }

        mProductRemain.setText(Utils.i2s(remainQRCode()));
    }

    private void showDialogAddSerial() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppTheme_Dialog_Alert);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_input_serial_dialog, null);

        final EditText fromSerialNumber = dialogView.findViewById(R.id.et_from);
        final EditText toSerialNumber = dialogView.findViewById(R.id.et_to);
        final TextView numberOfTemp = dialogView.findViewById(R.id.tv_amount);

        fromSerialNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String from = fromSerialNumber.getText().toString();
                    String to = toSerialNumber.getText().toString();
                    int remain = remainQRCode();
                    if (!from.isEmpty() && remain > 0) {
                        if (to.isEmpty()) {
                            long fromVal = Utils.serialNumber(from);
                            if (fromVal >= 0) {
                                toSerialNumber.setText(Utils.toSerialNumber(from, remain));
                                numberOfTemp.setText(Utils.i2s(remain));
                            } else {
                                numberOfTemp.setText("0");
                            }
                        } else {
                            numberOfTemp.setText(Utils.sizeOfSerialNumber(from, to) + "");
                        }
                    }
                }
            }
        });
        toSerialNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String from = fromSerialNumber.getText().toString();
                    String to = toSerialNumber.getText().toString();
                    int remain = remainQRCode();
                    if (!from.isEmpty() && remain > 0) {
                        if (to.isEmpty()) {
                            long fromVal = Utils.serialNumber(from);
                            if (fromVal >= 0) {
                                toSerialNumber.setText(Utils.toSerialNumber(from, remain));
                                numberOfTemp.setText(Utils.i2s(remain));
                            } else {
                                numberOfTemp.setText("0");
                            }
                        } else {
                            numberOfTemp.setText(Utils.sizeOfSerialNumber(from, to) + "");
                        }
                    }
                }
            }
        });

        builder
                .setTitle(R.string.add_with_serial)
                .setView(dialogView)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        String from = fromSerialNumber.getText().toString().trim();
                        String to = toSerialNumber.getText().toString();
                        if (from.isEmpty()) {
                            fromSerialNumber.requestFocus();
                            return;
                        }
                        if (to.isEmpty()) {
                            toSerialNumber.requestFocus();
                            return;
                        }
                        int quantity = Utils.sizeOfSerialNumber(from, to);
                        if (quantity <= 0) {
                            fromSerialNumber.requestFocus();
                            return;
                        }
                        SerialPackage serialPackage = new SerialPackage(from, to, quantity);
                        cart.serialList.add(serialPackage);
                        refreshListView();

                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(fromSerialNumber.getWindowToken(), 0);
                        imm.hideSoftInputFromWindow(toSerialNumber.getWindowToken(), 0);
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(fromSerialNumber.getWindowToken(), 0);
                        imm.hideSoftInputFromWindow(toSerialNumber.getWindowToken(), 0);
                    }

                });

        builder.show();
        fromSerialNumber.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    private void showDialogAddQRCode() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppTheme_Dialog_Alert);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_input_qr_code_dialog, null);

        final EditText QRCode = dialogView.findViewById(R.id.et_qr_code);

        builder
                .setTitle(R.string.add_with_qr_code)
                .setView(dialogView)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        String code = QRCode.getText().toString().trim();
                        if (code.isEmpty()) {
                            QRCode.requestFocus();
                            return;
                        }
                        addQRCode(code);

                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(QRCode.getWindowToken(), 0);
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(QRCode.getWindowToken(), 0);
                    }

                });

        builder.show();
        QRCode.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    private int totalQRCode() {
        int total = 0;
        if (cart.serialList != null && cart.serialList.size() > 0) {
            for (SerialPackage item : cart.serialList) {
                total += item.quantity;
            }
        }
        return total;
    }

    private int remainQRCode() {
        int remain = cart.cartItemQuantity - totalQRCode();
        if (remain < 0) {
            remain = 0;
        }
        return remain;
    }

    private void chooseFarm() {
        List<OptionItem> options = new ArrayList<>();
        OptionItem optionItem;

        FarmBoxAppController app = FarmBoxAppController.getInstance();
        if (app.getFarmList() != null && app.getFarmList().size() > 0) {
            for (FarmBasic farm : app.getFarmList()) {
                optionItem = new OptionItem();
                optionItem.setKey(farm.farm_id + "");
                optionItem.setValue(farm.farm_name);
                optionItem.setChecked(farm.farm_id == mFarm.getVal());

                options.add(optionItem);
            }
        }

        Gson gson = new Gson();
        Intent intent = new Intent(this, SingleChooseActivity.class);
        intent.putExtra(Constants.SCREEN_TITLE_KEY, getString(R.string.choose_farm));
        intent.putExtra(Constants.OPTION_LIST_KEY, gson.toJson(options));

        startActivityForResult(intent, REQUEST_SELECT_FARM);
    }

    private void addQRCode(String code) {
        boolean existed = false;
        for (SerialPackage item : cart.serialList) {
            if (item.type == SerialPackage.TYPE_CODE && code.equals(item.code)) {
                existed = true;
                break;
            }
        }
        if (!existed) {
            SerialPackage serial = new SerialPackage(code);
            cart.serialList.add(serial);
            refreshListView();
        }
    }

    @Override
    public void onRemoveSerialPackage(final SerialPackage serial) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this, R.style.AppTheme_Dialog_Alert);
        alertDialog.setMessage(getString(R.string.confirm_remove_serial));

        alertDialog.setPositiveButton(getString(R.string.button_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                cart.serialList.remove(serial);
                refreshListView();
            }
        });

        alertDialog.setNegativeButton(getString(R.string.button_no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    @Override
    public void onQRCodeRead(String text, PointF[] points) {
        Utils.doVibrator(this);

        Uri uri = Uri.parse(text);
        if (uri != null) {
            String QRCode = uri.getQueryParameter("q");

            if (QRCode != null && !QRCode.isEmpty()) {
                addQRCode(QRCode);
            }
        }
    }



    private void requestCameraPermission() {
        List<String> permissions = new ArrayList<>();
        permissions.add(Manifest.permission.CAMERA);
        String[] perArr = new String[permissions.size()];
        perArr = permissions.toArray(perArr);
        ActivityCompat.requestPermissions(this,
                perArr,
                PERMISSIONS_CAMERA_REQUEST);
    }

    private void initQRCodeReaderView() {
        ViewGroup.LayoutParams layoutParams = mScanView.getLayoutParams();
        layoutParams.height = 0;
        mScanView.setLayoutParams(layoutParams);

        mQRCodeReader.setOnQRCodeReadListener(this);
        mQRCodeReader.setQRDecodingEnabled(true);
        mQRCodeReader.setAutofocusInterval(2000L);
        mQRCodeReader.setTorchEnabled(true);
        mQRCodeReader.setBackCamera();
        mQRCodeReader.startCamera();

        int height = (int)Utils.getPixelFromDP(200, AssignTraceabilityCodeDetailActivity.this);
        ValueAnimator slideAnimator = ValueAnimator
                .ofInt(0, height)
                .setDuration(200);


        slideAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // get the value the interpolator is at
                Integer value = (Integer) animation.getAnimatedValue();
                // I'm going to set the layout's height 1:1 to the tick
                ViewGroup.LayoutParams layoutParams = mScanView.getLayoutParams();
                layoutParams.height = value.intValue();
                mScanView.setLayoutParams(layoutParams);
                // force all layouts to see which ones are affected by
                // this layouts height change
                mScanView.requestLayout();
            }
        });

        // create a new animationset
        AnimatorSet set = new AnimatorSet();
        set.play(slideAnimator);
        set.setInterpolator(new AccelerateDecelerateInterpolator());
        set.start();

        isShow = !isShow;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSIONS_CAMERA_REQUEST) {
            if (grantResults.length > 0) {
                boolean isGrant = false;
                for (int gr : grantResults) {
                    if (gr == PackageManager.PERMISSION_GRANTED) {
                        isGrant = true;
                        break;
                    }
                }

                if (isGrant) {
                    initQRCodeReaderView();
                } else {
                    showErrorDialog(getString(R.string.dialog_message_missing_camera_permission));
                }
            } else {
                showErrorDialog(getString(R.string.dialog_message_missing_camera_permission));
            }
        }
    }
}
