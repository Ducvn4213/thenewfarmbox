package mobile.agrhub.farmbox.activities.setting;

import android.app.TimePickerDialog;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.network.Param;
import mobile.agrhub.farmbox.utils.Utils;

public class FarmSettingStartTimeActivity extends BaseFarmSettingItemActivity {
    @BindView(R.id.tv_title_from)
    TextView mTitleFrom;
    @BindView(R.id.tv_run_from)
    TextView mRunFrom;

    @Override
    protected int contentView() {
        return R.layout.activity_farm_setting_start_time;
    }

    @Override
    protected void initView() {
        super.initView();

        mTitleFrom.setText(item.getName());
        mRunFrom.setText(item.getValue());

        mRunFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int hour, minute;
                if (item.getValue() != null && !item.getValue().isEmpty()) {
                    String tmpVal = item.getValue();
                    String[] timePart = tmpVal.split(" ");
                    if (timePart.length > 0) {
                        tmpVal = timePart[0];
                    }
                    timePart = tmpVal.split(":");
                    hour = Integer.parseInt(timePart[0]);
                    minute = Integer.parseInt(timePart[1]);
                } else {
                    Calendar calendar = Calendar.getInstance();
                    hour = calendar.get(Calendar.HOUR_OF_DAY);
                    minute = calendar.get(Calendar.MINUTE);
                }
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(FarmSettingStartTimeActivity.this, R.style.AppTheme_Dialog, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        mRunFrom.setText( Utils.correctTimeUnit(selectedHour) + ":" + Utils.correctTimeUnit(selectedMinute));
                    }
                }, hour, minute, true);
                mTimePicker.show();
            }
        });
    }

    @Override
    protected boolean validate() {
        String minVal = mRunFrom.getText().toString().trim();

        // check empty min
        if (minVal.isEmpty()) {
            showErrorDialog(getString(R.string.field_is_required, item.getName()));
            return false;
        }

        return true;
    }

    @Override
    protected List<Param> buildParam(List<Param> params) {
        String minStr = mRunFrom.getText().toString().trim();

        if (minStr.contains(" ")) {
            minStr = minStr.split(" ")[0];
        }

        String[] minPart = minStr.split(":");
        int minVal = Integer.parseInt(minPart[0]) * 60 + Integer.parseInt(minPart[1]);

        params.add(new Param(item.getField(), minVal + ""));
        return params;
    }

    @Override
    public void closeAndReloadSetting() {
        super.closeAndReloadSetting();
        FarmBoxAppController.getInstance().setNeedReloadFarmSetting(true);
    }
}
