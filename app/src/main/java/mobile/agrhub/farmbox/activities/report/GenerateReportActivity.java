package mobile.agrhub.farmbox.activities.report;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.activities.common.SingleChooseActivity;
import mobile.agrhub.farmbox.adapters.DataAdapter;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.ReportAPI;
import mobile.agrhub.farmbox.service.model.DataItem;
import mobile.agrhub.farmbox.service.model.FarmBasic;
import mobile.agrhub.farmbox.service.model.FarmReport;
import mobile.agrhub.farmbox.service.model.OptionItem;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Constants;
import mobile.agrhub.farmbox.utils.Utils;

public class GenerateReportActivity extends BaseActivity implements DataAdapter.DataItemListener {
    private static final int REQUEST_SELECT_FARM = 204;
    private static final int REQUEST_SELECT_GRANULARITY = 205;
    private static final int REQUEST_SELECT_REPORT_FORMAT = 206;
    public static final String DATE_FORMAT = "dd/MM/yyyy HH:mm";


    RecyclerView mRecyclerView;
    DataAdapter mAdapter;
    List<DataItem> mData;
    long farmId = -1;
    int granularity = FarmReport.GRANULARITY_DAILY;
    Calendar startTime = Calendar.getInstance();
    Calendar endTime = Calendar.getInstance();
    int reportFormat = FarmReport.FORMAT_PDF;

    ReportAPI reportAPI = APIHelper.getReportAPI();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate_report);

        bindingControls();
        setupControlEvents();

        initData();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.report_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_export:
                doGenerateReport();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_SELECT_FARM) {
                String selectedVal = data.getStringExtra(Constants.SELECTED_OPTION_KEY);
                if (selectedVal != null && !selectedVal.isEmpty()) {
                    this.farmId = Long.parseLong(selectedVal);

                    for (DataItem item : mData) {
                        if ("choose-farm".equals(item.getKey())) {
                            FarmBoxAppController app = FarmBoxAppController.getInstance();
                            List<FarmBasic> farmList = app.getFarmList();
                            if (farmList != null && farmList.size() > 0) {
                                for (FarmBasic farm : farmList) {
                                    if (farmId == farm.farm_id) {
                                        item.setDisplayValue(farm.farm_name);
                                        item.setValue(farm);
                                        break;
                                    }
                                }
                            }

                            mAdapter.notifyDataSetChanged();
                            break;
                        }
                    }
                }
            } else if (requestCode == REQUEST_SELECT_GRANULARITY) {
                String selectedVal = data.getStringExtra(Constants.SELECTED_OPTION_KEY);
                if (selectedVal != null && !selectedVal.isEmpty()) {
                    this.granularity = Integer.parseInt(selectedVal);

                    for (DataItem item : mData) {
                        if ("choose-granularity".equals(item.getKey())) {
                            item.setDisplayValue(FarmReport.granularityName(this.granularity));
                            item.setValue(this.granularity);

                            mAdapter.notifyDataSetChanged();
                            break;
                        }
                    }
                }
            } else if (requestCode == REQUEST_SELECT_REPORT_FORMAT) {
                String selectedVal = data.getStringExtra(Constants.SELECTED_OPTION_KEY);
                if (selectedVal != null && !selectedVal.isEmpty()) {
                    this.reportFormat = Integer.parseInt(selectedVal);

                    for (DataItem item : mData) {
                        if ("choose-export-format".equals(item.getKey())) {
                            item.setDisplayValue(FarmReport.formatName(this.reportFormat));
                            item.setValue(this.reportFormat);

                            mAdapter.notifyDataSetChanged();
                            break;
                        }
                    }
                }
            }
        }

    }

    private void bindingControls() {
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_content);
    }

    private void initData() {
        Intent intent = getIntent();
        this.farmId = intent.getLongExtra(Constants.FARM_ID_KEY, -1);
        if (this.farmId <= 0) {
            finish();
            return;
        }

        mData = new ArrayList<>();

        DataItem item;

        item = new DataItem();
        item.setTitle(getString(R.string.export_farm));
        FarmBoxAppController app = FarmBoxAppController.getInstance();
        if (app.getFarmList() != null && app.getFarmList().size() > 0) {
            for (FarmBasic farm : app.getFarmList()) {
                if (farm.farm_id == this.farmId) {
                    item.setDisplayValue(farm.farm_name);
                    item.setValue(farm);
                    break;
                }
            }
        }
        item.setPlaceholder(getString(R.string.export_farm_placeholder));
        item.setKey("choose-farm");
        mData.add(item);

        item = new DataItem();
        item.setTitle(getString(R.string.export_granularity));
        item.setDisplayValue(FarmReport.granularityName(this.granularity));
        item.setPlaceholder(getString(R.string.export_granularity_placeholder));
        item.setKey("choose-granularity");
        mData.add(item);

        item = new DataItem();
        item.setTitle(getString(R.string.export_start_time));
        item.setDisplayValue(Utils.getDate(startTime.getTimeInMillis(), DATE_FORMAT));
        item.setPlaceholder(getString(R.string.export_start_time_placeholder));
        item.setKey("start-time");
        mData.add(item);

        item = new DataItem();
        item.setTitle(getString(R.string.export_end_time));
        item.setDisplayValue(Utils.getDate(endTime.getTimeInMillis(), DATE_FORMAT));
        item.setPlaceholder(getString(R.string.export_end_time_placeholder));
        item.setKey("end-time");
        mData.add(item);

        item = new DataItem();
        item.setTitle(getString(R.string.export_format));
        item.setDisplayValue(FarmReport.formatName(this.reportFormat));
        item.setPlaceholder(getString(R.string.export_format_placeholder));
        item.setKey("choose-export-format");
        item.setValue(reportFormat);
        mData.add(item);

        mAdapter = new DataAdapter(this, mData, this);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void setupControlEvents() {
    }


    @Override
    public void onClick(DataItem item) {
        if ("choose-farm".equals(item.getKey())) {
            List<OptionItem> options = new ArrayList<>();
            OptionItem optionItem;

            FarmBoxAppController app = FarmBoxAppController.getInstance();
            if (app.getFarmList() != null && app.getFarmList().size() > 0) {
                for (FarmBasic farm : app.getFarmList()) {
                    optionItem = new OptionItem();
                    optionItem.setKey(farm.farm_id + "");
                    optionItem.setValue(farm.farm_name);
                    optionItem.setChecked(farm.farm_id == farmId);

                    options.add(optionItem);
                }
            }

            Gson gson = new Gson();
            Intent intent = new Intent(this, SingleChooseActivity.class);
            intent.putExtra(Constants.SCREEN_TITLE_KEY, getString(R.string.choose_farm));
            intent.putExtra(Constants.OPTION_LIST_KEY, gson.toJson(options));

            startActivityForResult(intent, REQUEST_SELECT_FARM);
        } else if ("choose-granularity".equals(item.getKey())) {
            List<OptionItem> options = new ArrayList<>();
            OptionItem optionItem;

            optionItem = new OptionItem();
            optionItem.setKey(FarmReport.GRANULARITY_UTM + "");
            optionItem.setValue(FarmReport.granularityName(FarmReport.GRANULARITY_UTM));
            optionItem.setChecked(FarmReport.GRANULARITY_UTM == granularity);
            options.add(optionItem);

            optionItem = new OptionItem();
            optionItem.setKey(FarmReport.GRANULARITY_HOURLY + "");
            optionItem.setValue(FarmReport.granularityName(FarmReport.GRANULARITY_HOURLY));
            optionItem.setChecked(FarmReport.GRANULARITY_HOURLY == granularity);
            options.add(optionItem);

            optionItem = new OptionItem();
            optionItem.setKey(FarmReport.GRANULARITY_DAILY + "");
            optionItem.setValue(FarmReport.granularityName(FarmReport.GRANULARITY_DAILY));
            optionItem.setChecked(FarmReport.GRANULARITY_DAILY == granularity);
            options.add(optionItem);

//            optionItem = new OptionItem();
//            optionItem.setKey(FarmReport.GRANULARITY_WEEKLY + "");
//            optionItem.setValue(FarmReport.granularityName(FarmReport.GRANULARITY_WEEKLY));
//            optionItem.setChecked(FarmReport.GRANULARITY_WEEKLY == granularity);
//            options.add(optionItem);
//
//            optionItem = new OptionItem();
//            optionItem.setKey(FarmReport.GRANULARITY_MONTHLY + "");
//            optionItem.setValue(FarmReport.granularityName(FarmReport.GRANULARITY_MONTHLY));
//            optionItem.setChecked(FarmReport.GRANULARITY_MONTHLY == granularity);
//            options.add(optionItem);

            Gson gson = new Gson();
            Intent intent = new Intent(this, SingleChooseActivity.class);
            intent.putExtra(Constants.SCREEN_TITLE_KEY, getString(R.string.choose_granularity));
            intent.putExtra(Constants.OPTION_LIST_KEY, gson.toJson(options));

            startActivityForResult(intent, REQUEST_SELECT_GRANULARITY);
        } else if ("start-time".equals(item.getKey())) {
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.AppTheme_Dialog, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    startTime.set(year, monthOfYear, dayOfMonth);
                    new TimePickerDialog(GenerateReportActivity.this, R.style.AppTheme_Dialog, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                            startTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            startTime.set(Calendar.MINUTE, minute);

                            for (DataItem item : mData) {
                                if ("start-time".equals(item.getKey())) {
                                    item.setDisplayValue(Utils.getDate(startTime.getTimeInMillis(), DATE_FORMAT));
                                    item.setValue(startTime);

                                    mAdapter.notifyDataSetChanged();
                                    break;
                                }
                            }
                        }
                    }, startTime.get(Calendar.HOUR_OF_DAY), startTime.get(Calendar.MINUTE), true).show();
                }
            }, startTime.get(Calendar.YEAR), startTime.get(Calendar.MONTH), startTime.get(Calendar.DATE));
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        } else if ("end-time".equals(item.getKey())) {
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.AppTheme_Dialog, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    endTime.set(year, monthOfYear, dayOfMonth);
                    new TimePickerDialog(GenerateReportActivity.this, R.style.AppTheme_Dialog, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                            endTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            endTime.set(Calendar.MINUTE, minute);

                            for (DataItem item : mData) {
                                if ("end-time".equals(item.getKey())) {
                                    item.setDisplayValue(Utils.getDate(endTime.getTimeInMillis(), DATE_FORMAT));
                                    item.setValue(endTime);

                                    mAdapter.notifyDataSetChanged();
                                    break;
                                }
                            }
                        }
                    }, endTime.get(Calendar.HOUR_OF_DAY), endTime.get(Calendar.MINUTE), true).show();
                }
            }, endTime.get(Calendar.YEAR), endTime.get(Calendar.MONTH), endTime.get(Calendar.DATE));
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        } else if ("choose-export-format".equals(item.getKey())) {
            List<OptionItem> options = new ArrayList<>();
            OptionItem optionItem;

            optionItem = new OptionItem();
            optionItem.setKey(FarmReport.FORMAT_PDF + "");
            optionItem.setValue(FarmReport.formatName(FarmReport.FORMAT_PDF));
            optionItem.setChecked(FarmReport.FORMAT_PDF == this.reportFormat);
            options.add(optionItem);

            optionItem = new OptionItem();
            optionItem.setKey(FarmReport.FORMAT_EXCEL + "");
            optionItem.setValue(FarmReport.formatName(FarmReport.FORMAT_EXCEL));
            optionItem.setChecked(FarmReport.FORMAT_EXCEL == this.reportFormat);
            options.add(optionItem);

            Gson gson = new Gson();
            Intent intent = new Intent(this, SingleChooseActivity.class);
            intent.putExtra(Constants.SCREEN_TITLE_KEY, getString(R.string.choose_report_format));
            intent.putExtra(Constants.OPTION_LIST_KEY, gson.toJson(options));

            startActivityForResult(intent, REQUEST_SELECT_REPORT_FORMAT);
        }
    }

    private void doGenerateReport() {
        if (this.farmId <= 0) {
            showErrorDialog(getString(R.string.please_choose_farm));
            return;
        }

        if (this.startTime.compareTo(this.endTime) >= 0) {
            showErrorDialog(getString(R.string.start_date_less_than_end_date));
            return;
        }

        long minutes = Utils.minutesBetween(startTime, endTime);
        if (this.granularity == FarmReport.GRANULARITY_UTM && minutes > 24 * 60) {
            showErrorDialog(getString(R.string.report_limit_time_utm));
            return;
        }

        showLoading();
        FarmBoxAppController app = FarmBoxAppController.getInstance();

        reportAPI.generateFarmReport(this.farmId, this.granularity, this.startTime.getTimeInMillis()
                , this.endTime.getTimeInMillis(), this.reportFormat, app.getLanguage(), new APIHelper.Callback<FarmReport>() {
                    @Override
                    public void onSuccess(final FarmReport data) {
                        GenerateReportActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideLoading();
                                generateReportCompleted(data);
                            }
                        });
                    }

                    @Override
                    public void onFail(final int errorCode, final String error) {
                        GenerateReportActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideLoading();
                                handleError(errorCode, error);
                            }
                        });
                    }
                });
    }

    private void generateReportCompleted(FarmReport report) {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
