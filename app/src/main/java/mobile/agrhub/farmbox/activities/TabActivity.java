package mobile.agrhub.farmbox.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.activities.add.AddFarmActivity;
import mobile.agrhub.farmbox.activities.dairy.AddDiaryActivity;
import mobile.agrhub.farmbox.activities.dairy.FilterDiaryActivity;
import mobile.agrhub.farmbox.activities.registration.PhoneRegistrationActivity;
import mobile.agrhub.farmbox.activities.report.ReportListActivity;
import mobile.agrhub.farmbox.activities.setting.FarmSettingActivity;
import mobile.agrhub.farmbox.activities.store.CartActivity;
import mobile.agrhub.farmbox.fragments.DiaryFragment;
import mobile.agrhub.farmbox.fragments.MainFragment;
import mobile.agrhub.farmbox.fragments.SaleManagerFragment;
import mobile.agrhub.farmbox.fragments.SettingsFragment;
import mobile.agrhub.farmbox.fragments.StorageFragment;
import mobile.agrhub.farmbox.fragments.StoreFragment;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.UtilAPI;
import mobile.agrhub.farmbox.service.model.Farm;
import mobile.agrhub.farmbox.service.model.User;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.BottomNavigationViewHelper;

public class TabActivity extends BaseActivity {
    private static final String TAG = "TabActivity";
    public static final int REQUEST_DIARY_FILTER_CODE = 104;
    public static final int REQUEST_EXPORT_REPORT_CODE = 105;
    private static final String SELECTED_TAB_ID = "SELECTED_TAB_ID";

    private int mSelectedTabId;
    private Fragment mSelectedFragment;
    private BottomNavigationView mNavigation;

    private FrameLayout mStoreCartActionBadge;
    private TextView mStoreCartActionBadgeValue;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            if (mSelectedTabId == item.getItemId()) {
                return false;
            }

            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mSelectedTabId = item.getItemId();
                    mSelectedFragment = new MainFragment();
                    break;
                case R.id.navigation_sale_manager:
                    mSelectedTabId = item.getItemId();
                    mSelectedFragment = new SaleManagerFragment();
                    break;
                case R.id.navigation_diary:
                    mSelectedTabId = item.getItemId();
                    mSelectedFragment = new DiaryFragment();
                    break;
//                case R.id.navigation_storage:
//                    mSelectedTabId = item.getItemId();
//                    mSelectedFragment = new StorageFragment();
//                    break;
                case R.id.navigation_store:
                    mSelectedTabId = item.getItemId();
                    mSelectedFragment = new StoreFragment();
                    break;
                case R.id.navigation_more:
                    mSelectedTabId = item.getItemId();
                    mSelectedFragment = new SettingsFragment();
                    break;
            }
            if (null != mSelectedFragment) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, mSelectedFragment);
                transaction.commit();

                // apply menu
                TabActivity.this.didTabChanged();

                return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);

        FarmBoxAppController app = FarmBoxAppController.getInstance();
        User user = app.getUser();
        if (user == null) {
            Intent intent = new Intent(this, PhoneRegistrationActivity.class);
            startActivity(intent);
            finish();

            app.logout();
            return;
        }
        app.setLanguage(app.getLanguage());

        mNavigation = (BottomNavigationView) findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(mNavigation);
        mNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        if (savedInstanceState != null) {
            mSelectedTabId = savedInstanceState.getInt(SELECTED_TAB_ID, R.id.navigation_home);
        } else {
            mSelectedTabId = R.id.navigation_home;
        }
        mNavigation.setSelectedItemId(mSelectedTabId);

        switch (mSelectedTabId) {
            case R.id.navigation_home:
                mSelectedFragment = new MainFragment();
                break;
            case R.id.navigation_sale_manager:
                mSelectedFragment = new SaleManagerFragment();
                break;
            case R.id.navigation_diary:
                mSelectedFragment = new DiaryFragment();
                break;
//            case R.id.navigation_storage:
//                mSelectedFragment = new StorageFragment();
//                break;
            case R.id.navigation_store:
                mSelectedFragment = new StoreFragment();
                break;
            case R.id.navigation_more:
                mSelectedFragment = new SettingsFragment();
                break;
        }
        //Manually displaying the first fragment - one time only
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, mSelectedFragment);
        transaction.commit();

        if (!app.isDemoMode()) {
            subscribeNotification();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(SELECTED_TAB_ID, mSelectedTabId);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onResume() {
        super.onResume();

        switch (mSelectedTabId) {
            case R.id.navigation_diary:
            case R.id.navigation_store:
                invalidateOptionsMenu();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_DIARY_FILTER_CODE) {
                Bundle bundle = data.getExtras();
                if (bundle != null && mSelectedFragment instanceof DiaryFragment) {
                    DiaryFragment diary = (DiaryFragment) mSelectedFragment;
                    diary.setFilter(bundle);
                }
            } else if (requestCode == REQUEST_EXPORT_REPORT_CODE) {
                StorageFragment fragment = (StorageFragment) mSelectedFragment;
                fragment.reloadData();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (null != menu) menu.clear();

        MenuInflater inflater = getMenuInflater();
        switch (mSelectedTabId) {
            case R.id.navigation_home:
                inflater.inflate(R.menu.home_menu, menu);
                return true;
//            case R.id.navigation_sale_manager:
//                inflater.inflate(R.menu.home_menu, menu);
//                return true;
            case R.id.navigation_diary:
                if (mSelectedFragment instanceof DiaryFragment) {
                    DiaryFragment diary = (DiaryFragment) mSelectedFragment;
                    if (diary.diaryTabLayout != null && diary.diaryTabLayout.getSelectedTabPosition() == DiaryFragment.PRODUCTIVITY_POSITION) {
                        inflater.inflate(R.menu.diary_menu, menu);
                        MenuItem filterItem = menu.findItem(R.id.action_diary_filter);
                        if (filterItem != null) {
                            if (diary.hasFilter()) {
                                filterItem.setIcon(R.drawable.ic_filter_white_24dp);
                            } else {
                                filterItem.setIcon(R.drawable.ic_filter_outline_white_24dp);
                            }
                        }
                        return true;
                    }
                }
                return super.onCreateOptionsMenu(menu);
//            case R.id.navigation_storage:
//                inflater.inflate(R.menu.storage_menu, menu);
//                return true;
            case R.id.navigation_store:
                inflater.inflate(R.menu.store_menu, menu);
                return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        mStoreCartActionBadge = null;
        mStoreCartActionBadgeValue = null;

        if (R.id.navigation_store == mSelectedTabId) {
            final MenuItem cartMenuItem = menu.findItem(R.id.action_store_cart);
            FrameLayout rootView = (FrameLayout) cartMenuItem.getActionView();

            mStoreCartActionBadge = (FrameLayout) rootView.findViewById(R.id.view_alert_red_circle);
            mStoreCartActionBadgeValue = (TextView) rootView.findViewById(R.id.view_alert_count_text_view);

            updateAlertIcon();

            rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TabActivity.this.storeCartActionClicked();
                }
            });
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home_export_report:
                gotoExportReportActivity();
                return true;
            case R.id.action_home_setting:
                homeSettingActionClicked();
                return true;
            case R.id.action_home_add:
                homeAddActionClicked();
                return true;
            case R.id.action_storage_add:
                gotoExportReportActivity();
                return true;
            case R.id.action_diary_filter:
                diaryFilterActionClicked();
                return true;
            case R.id.action_diary_add:
                diaryAddActionClicked();
                return true;
            case R.id.action_store_cart:
                storeCartActionClicked();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void didTabChanged() {
        switch (mSelectedTabId) {
            case R.id.navigation_home:
                setTitle(R.string.app_name);
                break;
            case R.id.navigation_sale_manager:
                if (mSelectedFragment instanceof SaleManagerFragment) {
                    SaleManagerFragment fragment = (SaleManagerFragment)mSelectedFragment;
                    setTitle(fragment.getTitle(this));
                } else {
                    setTitle(R.string.sale_manager_title);
                }
                break;
            case R.id.navigation_diary:
                if (mSelectedFragment instanceof DiaryFragment) {
                    DiaryFragment fragment = (DiaryFragment)mSelectedFragment;
                    setTitle(fragment.getTitle(this));
                } else {
                    setTitle(R.string.diary_title);
                }
                break;
//            case R.id.navigation_storage:
//                setTitle(R.string.storage_title);
//                break;
            case R.id.navigation_store:
                setTitle(R.string.navigation_store);
                break;
            case R.id.navigation_more:
                setTitle(R.string.navigation_more);
                break;
        }

        // apply menu
        invalidateOptionsMenu();
    }

    private void homeSettingActionClicked() {
        Farm selectedFarm = FarmBoxAppController.getInstance().getSelectedFarm();
        if (null == selectedFarm) {
            Toast.makeText(this, R.string.setting_farm_but_missing_farm, Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(this, FarmSettingActivity.class);
            startActivity(intent);


//            Intent intent = null;
//            int type = (int) selectedFarm.farm_type;
//            switch (type) {
//                case Farm.MUSHROOM:
//                    intent = new Intent(this, SettingsMushroomActivity.class);
//                    break;
//                case Farm.GREEN_HOUSE:
//                    intent = new Intent(this, SettingsGreenhouseActivity.class);
//                    break;
//                case Farm.HYDROPONIC:
//                    intent = new Intent(this, SettingsContainerActivity.class);
//                    break;
//                case Farm.AQUAPONIC:
//                    intent = new Intent(this, SettingsContainerActivity.class);
//                    break;
//                case Farm.CONTAINER:
//                    intent = new Intent(this, SettingsContainerActivity.class);
//                    break;
//                default:
//                    intent = new Intent(this, SettingsHomeActivity.class);
//                    break;
//            }
//            startActivity(intent);
        }
    }

    private void homeAddActionClicked() {
        Intent intent = new Intent(this, AddFarmActivity.class);
        this.startActivity(intent);
    }

    private void diaryFilterActionClicked() {
        if (mSelectedFragment instanceof DiaryFragment) {
            DiaryFragment diary = (DiaryFragment) mSelectedFragment;

            Bundle bundle = diary.getFilterBundle();
            if (bundle != null) {
                Intent intent = new Intent(this, FilterDiaryActivity.class);
                intent.putExtras(bundle); //Put your id to your next Intent
                this.startActivityForResult(intent, REQUEST_DIARY_FILTER_CODE);
            }
        }
    }

    private void diaryAddActionClicked() {
        Intent intent = new Intent(this, AddDiaryActivity.class);
        this.startActivity(intent);
    }

    private void storeCartActionClicked() {
        FarmBoxAppController app = FarmBoxAppController.getInstance();
        if (app.getTotalAmount() > 0) {
            Intent intent = new Intent(this, CartActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(this, R.string.please_add_product_to_cart, Toast.LENGTH_SHORT).show();
        }
    }

    private void gotoExportReportActivity() {
        Intent intent = new Intent(this, ReportListActivity.class);
        this.startActivityForResult(intent, REQUEST_EXPORT_REPORT_CODE);
    }

    public void updateAlertIcon() {
        if (R.id.navigation_store == mSelectedTabId
                && null != mStoreCartActionBadge
                && null != mStoreCartActionBadgeValue) {
            FarmBoxAppController app = FarmBoxAppController.getInstance();
            int numberOfCartItem = app.getTotalAmount();
            if (0 < numberOfCartItem) {
                mStoreCartActionBadgeValue.setText(String.valueOf(numberOfCartItem));
            } else {
                mStoreCartActionBadgeValue.setText("");
            }

            mStoreCartActionBadge.setVisibility((numberOfCartItem > 0) ? View.VISIBLE : View.GONE);
        }
    }

    public int getmSelectedTabId() {
        return mSelectedTabId;
    }

    public void setmSelectedTabId(int mSelectedTabId) {
        this.mSelectedTabId = mSelectedTabId;
    }

    public void refreshNavigationView() {
        recreate();
    }

    private void subscribeNotification() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        if (refreshedToken != null && !refreshedToken.isEmpty()) {
            UtilAPI utilAPI = APIHelper.getUtilAPI();
            utilAPI.subscribeNotification(refreshedToken, new APIHelper.Callback<String>() {
                @Override
                public void onSuccess(String data) {
                    Log.d(TAG, data);
                }

                @Override
                public void onFail(int errorCode, String error) {
                    Log.d(TAG, error);
                }
            });
        }
    }
}
