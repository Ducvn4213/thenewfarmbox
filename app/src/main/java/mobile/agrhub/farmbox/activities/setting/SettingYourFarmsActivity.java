package mobile.agrhub.farmbox.activities.setting;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.adapters.SettingFarmAdapter;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.FarmAPI;
import mobile.agrhub.farmbox.service.model.response.GetFarmBasicResponse;
import mobile.agrhub.farmbox.utils.BaseActivity;

public class SettingYourFarmsActivity extends BaseActivity {

    @BindView(R.id.rv_content) RecyclerView mRecycleView;
    @BindView(R.id.tv_message) TextView tvMessage;

    FarmAPI farmAPI = APIHelper.getFarmAPI();
    SettingFarmAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_your_farm);
        ButterKnife.bind(this);

        configActionBar();
        initData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void configActionBar() {
        getSupportActionBar().setTitle(R.string.edit_your_farm);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initData() {
        tvMessage.setText(R.string.edit_your_farm_hint);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(SettingYourFarmsActivity.this, DividerItemDecoration.VERTICAL);
        mRecycleView.addItemDecoration(dividerItemDecoration);

        loadData();
    }

    private void loadData() {
        showLoading();
        farmAPI.getAllFarmBasic(true, new APIHelper.Callback<GetFarmBasicResponse>() {
            @Override
            public void onSuccess(final GetFarmBasicResponse data) {
                Activity activity = SettingYourFarmsActivity.this;
                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();

                            try {
                                mAdapter = new SettingFarmAdapter(data.farms);
                                mRecycleView.setAdapter(mAdapter);
                            }
                            catch (Exception ex) {
                                ex.printStackTrace();
                                onNoFarm();
                            }
                        }
                    });
                }
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                Activity activity = SettingYourFarmsActivity.this;
                if (activity != null) {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();
                            onNoFarm();
                        }
                    });
                }
            }
        });
    }

    private void onNoFarm() {
        tvMessage.setText(R.string.edit_your_farm_hint_no_farm);
    }
}

