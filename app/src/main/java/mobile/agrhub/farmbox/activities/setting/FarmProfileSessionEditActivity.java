package mobile.agrhub.farmbox.activities.setting;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.adapters.FarmSettingAdapter;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.FarmAPI;
import mobile.agrhub.farmbox.service.model.Farm;
import mobile.agrhub.farmbox.service.model.FarmSetting;
import mobile.agrhub.farmbox.service.model.FarmSettingDetailItem;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Constants;

public class FarmProfileSessionEditActivity extends BaseActivity implements FarmSettingAdapter.SettingItemClickListener {
    public static final int REQUEST_CHANGE_SETTING = 203;
    FarmAPI farmAPI = APIHelper.getFarmAPI();

    RecyclerView mRecycleView;
    FarmSettingAdapter mAdapter;

    long farmId;
    String title;
    List<FarmSettingDetailItem> profileList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farm_profile_session_edit);

        Intent intent = getIntent();
        farmId = intent.getLongExtra(Constants.FARM_ID_KEY, 0);
        title = intent.getStringExtra(Constants.SCREEN_TITLE_KEY);
        String settingInfo = intent.getStringExtra(Constants.SETTING_INFO_KEY);
        if (farmId <= 0 || settingInfo == null || settingInfo.isEmpty()) {
            finish();
            return;
        } else {
            Gson gson = new Gson();
            profileList = gson.fromJson(settingInfo, new TypeToken<List<FarmSettingDetailItem>>() {}.getType());
        }

        bindingControls();
        configUI();
        setupControlEvents();
        configActionBar();
    }

    private void configActionBar() {
        if (this.title == null || this.title.isEmpty()) {
            this.title = FarmBoxAppController.getInstance().getSelectedFarmName();
        }
        getSupportActionBar().setTitle(this.title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CHANGE_SETTING && resultCode == Activity.RESULT_OK) {
            loadData();
        }
    }

    protected void bindingControls() {
        mRecycleView = (RecyclerView) findViewById(R.id.rv_content);
    }


    public void configUI() {

    }

    public void setupControlEvents() {
    }

    private void loadData() {
        if (this.farmId == 0) {
            Farm farm = FarmBoxAppController.getInstance().getSelectedFarm();
            if (null == farm) {
                finish();
                return;
            }
            this.farmId = farm.farm_id;
        }
//
//        showLoading();
//        if (this.factoryId > 0) {
//            farmAPI.getFarmSessionSettings(String.valueOf(this.factoryId), new APIHelper.Callback<FarmSetting>() {
//                @Override
//                public void onSuccess(final FarmSetting data) {
//                    FarmProfileSessionEditActivity.this.runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            hideLoading();
//                            didLoadData(data);
//                        }
//                    });
//                }
//
//                @Override
//                public void onFail(final int errorCode, final String error) {
//                    FarmProfileSessionEditActivity.this.runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            hideLoading();
//                            handleError(errorCode, error);
//                        }
//                    });
//                }
//            });
//        } else {
//            farmAPI.getFarmSettings(String.valueOf(this.farmId), new APIHelper.Callback<FarmSetting>() {
//                @Override
//                public void onSuccess(final FarmSetting data) {
//                    FarmProfileSessionEditActivity.this.runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            hideLoading();
//                            didLoadData(data);
//                        }
//                    });
//                }
//
//                @Override
//                public void onFail(final int errorCode, final String error) {
//                    FarmProfileSessionEditActivity.this.runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            hideLoading();
//                            handleError(errorCode, error);
//                        }
//                    });
//                }
//            });
//        }
    }

    public void didLoadData(FarmSetting data) {
        if (data == null) {
            finish();
            return;
        }
//        this.farmSetting = data;
//        if (mAdapter == null) {
//            mAdapter = new FarmSettingAdapter(this, this.farmSetting.getSettings(), this);
//            mRecycleView.setAdapter(mAdapter);
//        } else {
//            mAdapter.setData(this.farmSetting.getSettings());
//            mAdapter.notifyDataSetChanged();
//        }
    }

    @Override
    public void onClick(FarmSettingDetailItem setting, int position) {
        Gson gson = new Gson();
        Intent intent = null;
        if ("one-choice".equals(setting.getType())) {
            intent = new Intent(this, FarmSettingOneChooseActivity.class);
        } else if ("device-setting".equals(setting.getType())) {
            intent = new Intent(this, SettingDeviceActivity.class);
        } else if ("min-max".equals(setting.getType())) {
            intent = new Intent(this, SettingMinMaxActivity.class);
        } else if ("min-medium-max".equals(setting.getType())) {
            intent = new Intent(this, FarmSettingMinMediumMaxActivity.class);
        } else if ("run-on".equals(setting.getType())) {
            intent = new Intent(this, FarmSettingRunOnActivity.class);
        } else if ("nutrition-formula".equals(setting.getType())) {
            intent = new Intent(this, FarmSettingNutritionFormulaActivity.class);
        } else if ("simple".equals(setting.getType())) {
            intent = new Intent(this, FarmSettingSimpleActivity.class);
        } else if ("schedule".equals(setting.getType())) {
            intent = new Intent(this, FarmSettingScheduleActivity.class);
        } else if ("session".equals(setting.getType())) {
            intent = new Intent(this, FarmProfileSessionEditActivity.class);
            long tmpVal = 0;
            try {
                tmpVal = Long.parseLong(setting.getField());
            } catch (Exception e) {}
            intent.putExtra(Constants.FACTORY_ID_KEY, tmpVal);
            intent.putExtra(Constants.SCREEN_TITLE_KEY, setting.getName());
        }

//        if (intent != null) {
//            intent.putExtra(Constants.FARM_ID_KEY, this.farmId);
//            if (!intent.hasExtra(Constants.FACTORY_ID_KEY)) {
//                intent.putExtra(Constants.FACTORY_ID_KEY, this.factoryId);
//            }
//            intent.putExtra(Constants.SETTING_INFO_KEY, gson.toJson(setting));
//
//            startActivityForResult(intent, REQUEST_CHANGE_SETTING);
//        }
    }

    @Override
    public void onChangeProfileSession(FarmSettingDetailItem setting, boolean val) {

    }

    @Override
    public void onCopyProfileSession(FarmSettingDetailItem setting) {

    }
}
