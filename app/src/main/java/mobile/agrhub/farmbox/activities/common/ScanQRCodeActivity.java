package mobile.agrhub.farmbox.activities.common;

import android.Manifest;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.PointF;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Utils;

import static mobile.agrhub.farmbox.utils.Utils.PERMISSIONS_CAMERA_REQUEST;

public class ScanQRCodeActivity extends BaseActivity implements QRCodeReaderView.OnQRCodeReadListener {

    @BindView(R.id.fl_scan_view)
    FrameLayout mScanView;
    @BindView(R.id.ib_close)
    ImageButton mCloseScanViewBtn;
    @BindView(R.id.ib_enter_code)
    ImageButton mEnterCodeBtn;
    @BindView(R.id.rd_qr_code)
    QRCodeReaderView mQRCodeReader;
    @BindView(R.id.et_address)
    EditText mAddress;
    @BindView(R.id.web_view)
    WebView mWebView;

    boolean isShowed = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_qr_code);
        ButterKnife.bind(this);

        bindingControls();
        setupControlEvents();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.scanner_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_scanner:
                showScanner();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isShowed && mQRCodeReader != null) {
            mQRCodeReader.startCamera();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isShowed && mQRCodeReader != null) {
            mQRCodeReader.stopCamera();
        }
    }

    private void bindingControls() {
        if (null != getSupportActionBar()) {
            getSupportActionBar().setTitle(getString(R.string.scan_qr_code));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mWebView.clearCache(true);
        mWebView.clearHistory();
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            initQRCodeReaderView();
        } else {
            requestCameraPermission();
        }

        mAddress.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    mWebView.loadUrl(v.getText().toString().trim());

                    InputMethodManager imm = (InputMethodManager)v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });
    }

    private void setupControlEvents() {
        mCloseScanViewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideScanner();
            }
        });

        mEnterCodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogAddQRCode();
            }
        });
    }

    private void hideScanner() {
        if (isShowed) {
            isShowed = false;

            mQRCodeReader.stopCamera();

            ViewGroup.LayoutParams layoutParams = mScanView.getLayoutParams();
            ValueAnimator slideAnimator = ValueAnimator
                    .ofInt(layoutParams.height, 0)
                    .setDuration(100);
            slideAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    // get the value the interpolator is at
                    Integer value = (Integer) animation.getAnimatedValue();
                    // I'm going to set the layout's height 1:1 to the tick
                    ViewGroup.LayoutParams layoutParams = mScanView.getLayoutParams();
                    layoutParams.height = value.intValue();
                    mScanView.setLayoutParams(layoutParams);
                    // force all layouts to see which ones are affected by
                    // this layouts height change
                    mScanView.requestLayout();
                }
            });

            // create a new animationset
            AnimatorSet set = new AnimatorSet();
            set.play(slideAnimator);
            set.setInterpolator(new AccelerateDecelerateInterpolator());
            set.start();
        }
    }

    private void showScanner() {
        if (!isShowed) {
            isShowed = true;

            mQRCodeReader.startCamera();

            int height = (int)Utils.getPixelFromDP(200, this);
            ViewGroup.LayoutParams layoutParams = mScanView.getLayoutParams();
            ValueAnimator slideAnimator = ValueAnimator
                    .ofInt(0, height)
                    .setDuration(200);
            slideAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    // get the value the interpolator is at
                    Integer value = (Integer) animation.getAnimatedValue();
                    // I'm going to set the layout's height 1:1 to the tick
                    ViewGroup.LayoutParams layoutParams = mScanView.getLayoutParams();
                    layoutParams.height = value.intValue();
                    mScanView.setLayoutParams(layoutParams);
                    // force all layouts to see which ones are affected by
                    // this layouts height change
                    mScanView.requestLayout();
                }
            });

            // create a new animationset
            AnimatorSet set = new AnimatorSet();
            set.play(slideAnimator);
            set.setInterpolator(new AccelerateDecelerateInterpolator());
            set.start();
        }
    }

    private void showDialogAddQRCode() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppTheme_Dialog_Alert);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_input_qr_code_dialog, null);

        final EditText QRCode = dialogView.findViewById(R.id.et_qr_code);

        builder
                .setTitle(R.string.add_with_qr_code)
                .setView(dialogView)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        String code = QRCode.getText().toString().trim();
                        if (code.isEmpty()) {
                            QRCode.requestFocus();
                            return;
                        }
                        hideScanner();
                        String address = "https://agrhub.com/f-trace/?q=" + code;
                        mAddress.setText(address);
                        mWebView.loadUrl(address);


                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(QRCode.getWindowToken(), 0);
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(QRCode.getWindowToken(), 0);
                    }

                });

        builder.show();
        QRCode.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    @Override
    public void onQRCodeRead(String text, PointF[] points) {
        Utils.doVibrator(this);

        Uri uri = Uri.parse(text);
        if (uri != null) {
            mAddress.setText(text);
            mWebView.loadUrl(text);

            hideScanner();
        }
    }

    private void requestCameraPermission() {
        List<String> permissions = new ArrayList<>();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            showScanner();
        } else {
            permissions.add(Manifest.permission.CAMERA);
            String[] perArr = new String[permissions.size()];
            perArr = permissions.toArray(perArr);
            ActivityCompat.requestPermissions(this,
                    perArr,
                    PERMISSIONS_CAMERA_REQUEST);
        }
    }

    private void initQRCodeReaderView() {
        mScanView.setVisibility(View.VISIBLE);

        ViewGroup.LayoutParams layoutParams = mScanView.getLayoutParams();
        layoutParams.height = (int)Utils.getPixelFromDP(200, this);
        mScanView.setLayoutParams(layoutParams);

        mQRCodeReader.setOnQRCodeReadListener(this);
        mQRCodeReader.setAutofocusInterval(1000L);
        mQRCodeReader.setTorchEnabled(true);
        mQRCodeReader.setBackCamera();
        mQRCodeReader.startCamera();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSIONS_CAMERA_REQUEST) {
            if (grantResults.length > 0) {
                boolean isGrant = false;
                for (int gr : grantResults) {
                    if (gr == PackageManager.PERMISSION_GRANTED) {
                        isGrant = true;
                        break;
                    }
                }

                if (isGrant) {
                    initQRCodeReaderView();
                } else {
                    showErrorDialog(getString(R.string.dialog_message_missing_camera_permission));
                    hideScanner();
                }
            } else {
                showErrorDialog(getString(R.string.dialog_message_missing_camera_permission));
                hideScanner();
            }
        }
    }
}
