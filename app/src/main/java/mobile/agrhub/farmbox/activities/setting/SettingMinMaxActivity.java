package mobile.agrhub.farmbox.activities.setting;

import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.model.FarmSettingDetailData;
import mobile.agrhub.farmbox.service.model.UnitItem;
import mobile.agrhub.farmbox.service.network.Param;
import mobile.agrhub.farmbox.utils.Utils;

public class SettingMinMaxActivity extends BaseFarmSettingItemActivity {
    @BindView(R.id.tv_min_title)
    TextView mTitleMin;
    @BindView(R.id.tv_max_title)
    TextView mTitleMax;
    @BindView(R.id.tv_unit_min)
    TextView mUnitMin;
    @BindView(R.id.tv_unit_max)
    TextView mUnitMax;
    @BindView(R.id.et_min)
    TextView mMin;
    @BindView(R.id.et_max)
    TextView mMax;

    FarmSettingDetailData minData;
    FarmSettingDetailData maxData;

    @Override
    protected int contentView() {
        return R.layout.activity_setting_min_max;
    }

    @Override
    protected void initView() {
        super.initView();
        if (item.getData() != null && item.getData().size() >= 2) {
            minData = item.getData().get(0);
            maxData = item.getData().get(1);
        }

        if (minData == null || maxData == null) {
            finish();
            return;
        }

        if (UnitItem.TEMPERATURE_DOT_C_VALUE.equals(item.getUnit())) {
            FarmBoxAppController app = FarmBoxAppController.getInstance();
            if (app.isTemperatureFUnit()) {
                long min = app.getTemperatureValue(item.getMinVal(), UnitItem.TEMPERATURE_C_VALUE);
                long max = app.getTemperatureValue(item.getMaxVal(), UnitItem.TEMPERATURE_C_VALUE);

                mTitleMin.setText(minData.getName());
                mMin.setText(min + "");
                mUnitMin.setText(app.getTemperatureUnit());

                mTitleMax.setText(maxData.getName());
                mMax.setText(max + "");
                mUnitMax.setText(app.getTemperatureUnit());
            } else {
                mTitleMin.setText(minData.getName());
                mMin.setText(item.getMinVal() + "");
                mUnitMin.setText(item.getUnit());

                mTitleMax.setText(maxData.getName());
                mMax.setText(item.getMaxVal() + "");
                mUnitMax.setText(item.getUnit());
            }
        } else if (UnitItem.NUTRITION_MILLISIEMENS_VALUE.equals(item.getUnit())) {
            FarmBoxAppController app = FarmBoxAppController.getInstance();
            if (app.isNutritionPPMUnit()) {
                double min = app.getNutritionValue(item.getMinDoubleVal(), UnitItem.NUTRITION_MILLISIEMENS_VALUE);
                double max = app.getNutritionValue(item.getMaxDoubleVal(), UnitItem.NUTRITION_MILLISIEMENS_VALUE);

                mTitleMin.setText(minData.getName());
                mMin.setText(Utils.f2s(min, true));
                mUnitMin.setText(app.getNutritionUnit());

                mTitleMax.setText(maxData.getName());
                mMax.setText(Utils.f2s(max, true));
                mUnitMax.setText(app.getNutritionUnit());
            } else {
                mTitleMin.setText(minData.getName());
                mMin.setText(Utils.f2s(item.getMinDoubleVal()));
                mUnitMin.setText(item.getUnit());

                mTitleMax.setText(maxData.getName());
                mMax.setText(Utils.f2s(item.getMaxDoubleVal()));
                mUnitMax.setText(item.getUnit());
            }
        } else {
            if ("float".equals(item.getDataType())) {
                mTitleMin.setText(minData.getName());
                mMin.setText(Utils.f2s(item.getMinDoubleVal()));
                mUnitMin.setText(item.getUnit());

                mTitleMax.setText(maxData.getName());
                mMax.setText(Utils.f2s(item.getMaxDoubleVal()));
                mUnitMax.setText(item.getUnit());
            } else {
                mTitleMin.setText(minData.getName());
                mMin.setText(item.getMinVal() + "");
                mUnitMin.setText(item.getUnit());

                mTitleMax.setText(maxData.getName());
                mMax.setText(item.getMaxVal() + "");
                mUnitMax.setText(item.getUnit());
            }
        }
    }

    @Override
    protected boolean validate() {
        String minVal = mMin.getText().toString().trim();
        String maxVal = mMax.getText().toString().trim();
        int intMinVal = 0;
        int intMaxVal = 0;
        double doubleMinVal = 0f;
        double doubleMaxVal = 0f;

        // check empty min
        if (minVal.isEmpty()) {
            showErrorDialog(getString(R.string.field_is_required, minData.getName()));
            return false;
        }
        // check empty max
        if (maxVal.isEmpty()) {
            showErrorDialog(getString(R.string.field_is_required, maxData.getName()));
            return false;
        }

        // check min is number
        try {
            if ("int".equals(item.getDataType())) {
                intMinVal = Utils.str2Int(minVal);
            } else if ("float".equals(item.getDataType())) {
                doubleMinVal = Utils.str2Double(minVal);
            }
        } catch (Exception e) {
            showErrorDialog(getString(R.string.field_is_not_number, minData.getName()));
            return false;
        }

        // check max is number
        try {
            if ("int".equals(item.getDataType())) {
                intMaxVal = Utils.str2Int(maxVal);
            } else if ("float".equals(item.getDataType())) {
                doubleMaxVal = Utils.str2Double(maxVal);
            }
        } catch (Exception e) {
            showErrorDialog(getString(R.string.field_is_not_number, maxData.getName()));
            return false;
        }

        // check min in range
        if (minData.getExpectData() != null && minData.getExpectData().size() >= 2) {
            try {
                if ("int".equals(item.getDataType())) {
                    int fromVal = (int)Math.round(Double.parseDouble(minData.getExpectData().get(0).toString()));
                    int toVal = (int)Math.round(Double.parseDouble(minData.getExpectData().get(1).toString()));
                    if (UnitItem.TEMPERATURE_DOT_C_VALUE.equals(item.getUnit())) {
                        FarmBoxAppController app = FarmBoxAppController.getInstance();
                        if (app.isTemperatureFUnit()) {
                            fromVal = (int)app.getTemperatureValue((long)fromVal, UnitItem.TEMPERATURE_C_VALUE);
                            toVal = (int)app.getTemperatureValue((long)toVal, UnitItem.TEMPERATURE_C_VALUE);
                        }
                    }

                    if (intMinVal < fromVal || intMinVal > toVal) {
                        showErrorDialog(getString(R.string.field_between, minData.getName(), fromVal + "", toVal + ""));
                        return false;
                    }
                } else if ("float".equals(item.getDataType())) {
                    double fromVal = Double.parseDouble(minData.getExpectData().get(0).toString());
                    double toVal = Double.parseDouble(minData.getExpectData().get(1).toString());
                    if (UnitItem.NUTRITION_MILLISIEMENS_VALUE.equals(item.getUnit())) {
                        FarmBoxAppController app = FarmBoxAppController.getInstance();
                        if (app.isNutritionPPMUnit()) {
                            fromVal = app.getNutritionValue(fromVal, UnitItem.NUTRITION_MILLISIEMENS_VALUE);
                            toVal = app.getNutritionValue(toVal, UnitItem.NUTRITION_MILLISIEMENS_VALUE);
                        }
                    }

                    if (doubleMinVal < fromVal || doubleMinVal > toVal) {
                        showErrorDialog(getString(R.string.field_between, minData.getName(), Utils.f2s(fromVal), Utils.f2s(toVal)));
                        return false;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // check max in range
        if (maxData.getExpectData() != null && maxData.getExpectData().size() >= 2) {
            try {
                if ("int".equals(item.getDataType())) {
                    int fromVal = (int)Math.round(Utils.str2Double(maxData.getExpectData().get(0).toString()));
                    int toVal = (int)Math.round(Utils.str2Double(maxData.getExpectData().get(1).toString()));
                    if (UnitItem.TEMPERATURE_DOT_C_VALUE.equals(item.getUnit())) {
                        FarmBoxAppController app = FarmBoxAppController.getInstance();
                        if (app.isTemperatureFUnit()) {
                            fromVal = (int)app.getTemperatureValue((long)fromVal, UnitItem.TEMPERATURE_C_VALUE);
                            toVal = (int)app.getTemperatureValue((long)toVal, UnitItem.TEMPERATURE_C_VALUE);
                        }
                    }

                    if (intMaxVal < fromVal || intMaxVal > toVal) {
                        showErrorDialog(getString(R.string.field_between, maxData.getName(), fromVal + "", toVal + ""));
                        return false;
                    }
                } else if ("float".equals(item.getDataType())) {
                    double fromVal = Utils.str2Double(maxData.getExpectData().get(0).toString());
                    double toVal = Utils.str2Double(maxData.getExpectData().get(1).toString());
                    if (UnitItem.NUTRITION_MILLISIEMENS_VALUE.equals(item.getUnit())) {
                        FarmBoxAppController app = FarmBoxAppController.getInstance();
                        if (app.isNutritionPPMUnit()) {
                            fromVal = app.getNutritionValue(fromVal, UnitItem.NUTRITION_MILLISIEMENS_VALUE);
                            toVal = app.getNutritionValue(toVal, UnitItem.NUTRITION_MILLISIEMENS_VALUE);
                        }
                    }

                    if (doubleMaxVal < fromVal || doubleMaxVal > toVal) {
                        showErrorDialog(getString(R.string.field_between, maxData.getName(), Utils.f2s(fromVal), Utils.f2s(toVal)));
                        return false;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // check min <= max
        if ("int".equals(item.getDataType())) {
            if (intMaxVal < intMinVal) {
                showErrorDialog(getString(R.string.field_lower_field, minData.getName(), maxData.getName()));
                return false;
            }
        } else if ("float".equals(item.getDataType())) {
            if (doubleMaxVal < doubleMinVal) {
                showErrorDialog(getString(R.string.field_lower_field, minData.getName(), maxData.getName()));
                return false;
            }
        }

        return true;
    }

    @Override
    protected List<Param> buildParam(List<Param> params) {
        String minStr = mMin.getText().toString().trim();
        String maxStr = mMax.getText().toString().trim();

        if (UnitItem.TEMPERATURE_DOT_C_VALUE.equals(item.getUnit())) {
            FarmBoxAppController app = FarmBoxAppController.getInstance();
            if (app.isTemperatureFUnit()) {
                long minVal = 0;
                try {
                    minVal = Utils.str2Long(minStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                long maxVal = 0;
                try {
                    maxVal = Utils.str2Long(maxStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                minVal = app.parseTemperatureValue(minVal, UnitItem.TEMPERATURE_C_VALUE);
                maxVal = app.parseTemperatureValue(maxVal, UnitItem.TEMPERATURE_C_VALUE);

                params.add(new Param(minData.getField(), minVal + ""));
                params.add(new Param(maxData.getField(), maxVal + ""));
            } else {
                if ("int".equals(item.getDataType())) {
                    int minVal = 0;
                    try {
                        minVal = Utils.str2Int(minStr);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    int maxVal = 0;
                    try {
                        maxVal = Utils.str2Int(maxStr);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    params.add(new Param(minData.getField(), minVal + ""));
                    params.add(new Param(maxData.getField(), maxVal + ""));
                } else if ("float".equals(item.getDataType())) {
                    double minVal = 0;
                    try {
                        minVal = Utils.str2Double(minStr);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    double maxVal = 0;
                    try {
                        maxVal = Utils.str2Double(maxStr);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    params.add(new Param(minData.getField(), Utils.f2s(minVal, true)));
                    params.add(new Param(maxData.getField(), Utils.f2s(maxVal, true)));
                }
            }
        } else if (UnitItem.NUTRITION_MILLISIEMENS_VALUE.equals(item.getUnit())) {
            FarmBoxAppController app = FarmBoxAppController.getInstance();
            if (app.isNutritionPPMUnit()) {
                double minVal = 0;
                try {
                    minVal = Utils.str2Double(minStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                double maxVal = 0;
                try {
                    maxVal = Utils.str2Double(maxStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                minVal = app.parseNutritionValue(minVal, UnitItem.NUTRITION_MILLISIEMENS_VALUE);
                maxVal = app.parseNutritionValue(maxVal, UnitItem.NUTRITION_MILLISIEMENS_VALUE);

                params.add(new Param(minData.getField(), Utils.f2s(minVal, true)));
                params.add(new Param(maxData.getField(), Utils.f2s(maxVal, true)));
            } else {
                if ("int".equals(item.getDataType())) {
                    int minVal = 0;
                    try {
                        minVal = Utils.str2Int(minStr);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    int maxVal = 0;
                    try {
                        maxVal = Utils.str2Int(maxStr);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    params.add(new Param(minData.getField(), minVal + ""));
                    params.add(new Param(maxData.getField(), maxVal + ""));
                } else if ("float".equals(item.getDataType())) {
                    double minVal = 0;
                    try {
                        minVal = Utils.str2Double(minStr);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    double maxVal = 0;
                    try {
                        maxVal = Utils.str2Double(maxStr);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    params.add(new Param(minData.getField(), Utils.f2s(minVal, true)));
                    params.add(new Param(maxData.getField(), Utils.f2s(maxVal, true)));
                }
            }
        } else {
            if ("int".equals(item.getDataType())) {
                int minVal = 0;
                try {
                    minVal = Utils.str2Int(minStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                int maxVal = 0;
                try {
                    maxVal = Utils.str2Int(maxStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                params.add(new Param(minData.getField(), minVal + ""));
                params.add(new Param(maxData.getField(), maxVal + ""));
            } else if ("float".equals(item.getDataType())) {
                double minVal = 0;
                try {
                    minVal = Utils.str2Double(minStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                double maxVal = 0;
                try {
                    maxVal = Utils.str2Double(maxStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                params.add(new Param(minData.getField(), Utils.f2s(minVal, true)));
                params.add(new Param(maxData.getField(), Utils.f2s(maxVal, true)));
            }
        }
        return params;
    }
}
