package mobile.agrhub.farmbox.activities.camera;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import org.videolan.libvlc.IVLCVout;
import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaPlayer;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.Camera;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Constants;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class PlayCameraActivity extends BaseActivity implements IVLCVout.Callback {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    private SurfaceView mContentView;
    private SurfaceHolder mSurfaceHolder;
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
        }
    };
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };

    private MediaPlayer.EventListener mPlayerListener = new MyPlayerListener(this);
    private ProgressBar mProgressBar;
    private Camera mCamera;
    private LibVLC mLibVLC;
    private MediaPlayer mMediaPlayer;
    private int mVideoWidth;
    private int mVideoHeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_play_camera);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        mVisible = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
        mProgressBar = findViewById(R.id.progress_bar);
        mContentView = findViewById(R.id.sv_surface);
        mSurfaceHolder = mContentView.getHolder();

        // Set up the user interaction to manually show or hide the system UI.
        mContentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle();
            }
        });

        if (null != getIntent() && null != getIntent().getExtras()) {
            Bundle extras = getIntent().getExtras();
            if (extras.containsKey(Constants.CAMERA_INFO_KEY)) {
                mCamera = (Camera) extras.getSerializable(Constants.CAMERA_INFO_KEY);
            }
        }
        if (mCamera == null) {
            finish();
        } else {
            setTitle(mCamera.getCameraName());
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            if (!mVisible) {
                show();
            }
        } else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            if (mVisible) {
                hide();
            }
        }

        setSize(mVideoWidth, mVideoHeight);
    }

    @Override
    protected void onResume() {
        super.onResume();
        play();
    }

    @Override
    protected void onPause() {
        super.onPause();
        releasePlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releasePlayer();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            delayedHide(100);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button.
//            NavUtils.navigateUpFromSameTask(this);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Schedules a call to hide() in delay milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    private void play() {
        releasePlayer();
        if (mCamera != null
                && mCamera.getCameraStreamUri() != null
                && !mCamera.getCameraStreamUri().isEmpty()) {
            try {
                mProgressBar.setVisibility(View.VISIBLE);
                // Create LibVLC
                // TODO: make this more robust, and sync with audio demo
                ArrayList<String> options = new ArrayList<String>();
                //options.add("--subsdec-encoding <encoding>");
                options.add("--aout=opensles");
                options.add("--audio-time-stretch"); // time stretching
                options.add("-vvv"); // verbosity
                options.add("--http-reconnect");
                options.add("--network-caching=" + 6 * 1000);
                mLibVLC = new LibVLC(this, options);

                //libvlc.setOnHardwareAccelerationError(this);
                if (mSurfaceHolder == null) {
                    mSurfaceHolder = mContentView.getHolder();
                }
                mSurfaceHolder.setKeepScreenOn(true);

                // Creating media player
                mMediaPlayer = new MediaPlayer(mLibVLC);
                mMediaPlayer.setEventListener(mPlayerListener);

                // Set up video output
                final IVLCVout vout = mMediaPlayer.getVLCVout();
                vout.setVideoView(mContentView);
                vout.addCallback(this);
                vout.attachViews();

                Media media = new Media(mLibVLC, Uri.parse(mCamera.getCameraStreamUri()));
                mMediaPlayer.setMedia(media);
                mMediaPlayer.play();
            } catch (Exception e) {
                e.printStackTrace();
                mProgressBar.setVisibility(View.GONE);
                showErrorMessage();
            }
        } else {
            showErrorMessage();
        }
    }

    private void releasePlayer() {
        if (mLibVLC == null)
            return;
        mProgressBar.setVisibility(View.GONE);

        mMediaPlayer.stop();
        final IVLCVout vout = mMediaPlayer.getVLCVout();
        vout.removeCallback(this);
        vout.detachViews();

        mSurfaceHolder = null;
        mLibVLC.release();
        mLibVLC = null;

        mVideoWidth = 0;
        mVideoHeight = 0;
    }

    private void setSize(int width, int height) {
        mVideoWidth = width;
        mVideoHeight = height;
        if (mVideoWidth * mVideoHeight <= 1)
            return;

        if (mSurfaceHolder == null || mContentView == null)
            return;

        // get screen size
        int w = getWindow().getDecorView().getWidth();
        int h = getWindow().getDecorView().getHeight();

        // getWindow().getDecorView() doesn't always take orientation into
        // account, we have to correct the values
        boolean isPortrait = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
        if (w > h && isPortrait || w < h && !isPortrait) {
            int i = w;
            w = h;
            h = i;
        }

        float videoAR = (float) mVideoWidth / (float) mVideoHeight;
        float screenAR = (float) w / (float) h;

        if (screenAR < videoAR)
            h = (int) (w / videoAR);
        else
            w = (int) (h * videoAR);

        // force surface buffer size
        mSurfaceHolder.setFixedSize(mVideoWidth, mVideoHeight);

        // set display size
        ViewGroup.LayoutParams lp = mContentView.getLayoutParams();
        lp.width = w;
        lp.height = h;
        mContentView.setLayoutParams(lp);
        mContentView.invalidate();
    }

    private void showErrorMessage() {
        AlertDialog errorDialog = new AlertDialog.Builder(this, R.style.AppTheme_Dialog_Alert)
                .setTitle(R.string.button_error)
                .setMessage(R.string.play_camera_error)
                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        PlayCameraActivity.this.finish();
                    }
                }).create();
        errorDialog.show();
    }

    @Override
    public void onNewLayout(IVLCVout vlcVout, int width, int height, int visibleWidth, int visibleHeight, int sarNum, int sarDen) {
        if (width * height == 0)
            return;

        // store video size
        mVideoWidth = width;
        mVideoHeight = height;
        setSize(mVideoWidth, mVideoHeight);
    }

    @Override
    public void onSurfacesCreated(IVLCVout vlcVout) {

    }

    @Override
    public void onSurfacesDestroyed(IVLCVout vlcVout) {

    }

    @Override
    public void onHardwareAccelerationError(IVLCVout vlcVout) {
        releasePlayer();
    }

    private static class MyPlayerListener implements MediaPlayer.EventListener {
        private WeakReference<PlayCameraActivity> mOwner;

        public MyPlayerListener(PlayCameraActivity owner) {
            mOwner = new WeakReference<PlayCameraActivity>(owner);
        }

        @Override
        public void onEvent(MediaPlayer.Event event) {
            PlayCameraActivity player = mOwner.get();
            switch (event.type) {
                case MediaPlayer.Event.EndReached:
                    player.releasePlayer();
                    player.showErrorMessage();
                    break;
                case MediaPlayer.Event.EncounteredError:
                    player.releasePlayer();
                    player.showErrorMessage();
                    break;
                case MediaPlayer.Event.Playing:
                    player.mProgressBar.setVisibility(View.GONE);
                    break;
                case MediaPlayer.Event.Paused:
                case MediaPlayer.Event.Stopped:
                default:
                    break;
            }
        }
    }
}
