package mobile.agrhub.farmbox.activities.report;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.adapters.ReportAdapter;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.ReportAPI;
import mobile.agrhub.farmbox.service.model.Farm;
import mobile.agrhub.farmbox.service.model.FarmReport;
import mobile.agrhub.farmbox.service.model.FarmReportList;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Constants;

public class ReportListActivity extends BaseActivity implements ReportAdapter.ReportItemListener {
    public static final int REQUEST_EXPORT_REPORT_CODE = 105;

    LinearLayout mContainer;
    LinearLayout mEmptyView;
    RecyclerView mRecyclerView;
    ReportAdapter mAdapter;
    List<FarmReport> mReports;
    FarmReport selectedReport;
    long farmId = -1;

    ReportAPI reportAPI = APIHelper.getReportAPI();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_list);

        bindingControls();
        setupControlEvents();

        initData();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.report_list_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_report_add:
                gotoGenerateReport();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            doLoadData();
        }

    }

    private void bindingControls() {
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mContainer = (LinearLayout) findViewById(R.id.cv_container);
        mRecyclerView = (RecyclerView) findViewById(R.id.rv_content);
        mEmptyView = (LinearLayout) findViewById(R.id.ll_empty_view);
    }

    private void initData() {
        FarmBoxAppController app = FarmBoxAppController.getInstance();
        Farm farm = app.getSelectedFarm();
        if (farm == null) {
            finish();
            return;
        }

        String title = getString(R.string.report_list_title, farm.farm_name);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        } else {
            setTitle(title);
        }

        this.farmId = farm.farm_id;

        doLoadData();
    }

    private void setupControlEvents() {
    }

    private void doLoadData() {
        showLoading();
        reportAPI.getByFarm(farmId, new APIHelper.Callback<FarmReportList>() {
            @Override
            public void onSuccess(final FarmReportList data) {
                ReportListActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        mReports = data.getFarmReportList();
                        if (mAdapter == null) {
                            mAdapter = new ReportAdapter(ReportListActivity.this, mReports, ReportListActivity.this);
                            mRecyclerView.setAdapter(mAdapter);
                        } else {
                            mAdapter.setData(mReports);
                        }
                        if (mReports.size() > 0) {
                            mContainer.setVisibility(View.VISIBLE);
                            mEmptyView.setVisibility(View.GONE);
                        } else {
                            mContainer.setVisibility(View.GONE);
                            mEmptyView.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                ReportListActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }

    private void gotoGenerateReport() {
        Intent intent = new Intent(this, GenerateReportActivity.class);
        intent.putExtra(Constants.FARM_ID_KEY, farmId);
        this.startActivityForResult(intent, REQUEST_EXPORT_REPORT_CODE);
    }

    @Override
    public void onClick(FarmReport item) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(item.getReportUrl())));
    }

    @Override
    public void onOpenInClick(FarmReport item) {

    }
}
