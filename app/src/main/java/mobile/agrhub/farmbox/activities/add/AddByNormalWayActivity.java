package mobile.agrhub.farmbox.activities.add;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.activities.edit.EditSeasonActivity;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.FarmAPI;
import mobile.agrhub.farmbox.service.model.Farm;
import mobile.agrhub.farmbox.utils.BaseActivity;

public class AddByNormalWayActivity extends BaseActivity {

    private final int PLACE_PICKER_REQUEST = 999;

    @BindView(R.id.iv_background)
    ImageView mBackground;
    @BindView(R.id.btn_next)
    Button mNext;
    @BindView(R.id.et_name)
    EditText mName;
    @BindView(R.id.et_acreages)
    EditText mAcreages;
    @BindView(R.id.btn_address)
    Button mAddress;
    int type = -1;

    FarmAPI farmAPI = APIHelper.getFarmAPI();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_farm_by_normal);
        ButterKnife.bind(this);

        initGetLocationCallback();
        configActionBar();
        configUI();
    }

    @OnClick(R.id.btn_address)
    void onAddressButtonClick() {
        try {
            requestChangeAddress();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_next)
    void onNextButtonClick() {
        doAddNewFarm();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void configActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initGetLocationCallback() {
        setGetCurrentLocationCallback(new GetCurrentLocationCallback() {
            @Override
            public void afterGotCurrentLocation(double lat, double lon) {
                AddByNormalWayActivity.this.afterGotCurrentLocation(lat, lon);
            }

            @Override
            public void afterGetCurrentLocationFail() {
                AddByNormalWayActivity.this.afterGetCurrentLocationFail();
            }
        });
    }

    private void configUI() {
        this.type = getIntent().getIntExtra("type", -1);

        int title = 0;
        int background = 0;
        switch (this.type) {
            case Farm.GENERAL:
                title = R.string.add_general;
                background = R.drawable.add_farm_home_background;
                break;
            case Farm.FAMILY:
                title = R.string.add_farm_home;
                background = R.drawable.add_farm_home_background;
                break;
            case Farm.GREEN_HOUSE:
                title = R.string.add_farm_greenhouse;
                background = R.drawable.add_farm_greenhouse_background;
                break;
            case Farm.MUSHROOM:
                title = R.string.add_farm_mushroom;
                background = R.drawable.add_farm_mushroom_background;
                break;
            case Farm.HYDROPONIC:
                title = R.string.add_farm_hydroponic;
                background = R.drawable.add_farm_hydroponic_background;
                break;
            case Farm.STORAGE:
                mName.setHint(R.string.add_normal_storage_name);
                title = R.string.add_storage;
                background = R.drawable.add_farm_hydroponic_background;
                break;
            case Farm.CATTLE_HOUSE:
                mName.setHint(R.string.add_normal_cattle_house_name);
                title = R.string.add_cattle_house;
                background = R.drawable.add_farm_hydroponic_background;
                break;
            default:
                title = R.string.add_general;
                background = R.drawable.add_farm_home_background;
                break;

        }

        getSupportActionBar().setTitle(title);
        mBackground.setImageResource(background);
    }

    private void requestChangeAddress() throws GooglePlayServicesNotAvailableException, GooglePlayServicesRepairableException {

        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        startActivityForResult(builder.build(AddByNormalWayActivity.this), PLACE_PICKER_REQUEST);
    }

    private void doAddNewFarm() {
        String name = mName.getText().toString();
        String acreages = mAcreages.getText().toString();
        String address = mAddress.getText().toString();

        if (name == null || name.trim().isEmpty()) {
            if (Farm.STORAGE == this.type) {
                showErrorDialog(getString(R.string.add_normal_storage_missing_name));
            } else if (Farm.CATTLE_HOUSE == this.type) {
                showErrorDialog(getString(R.string.add_normal_cattle_house_missing_name));
            } else {
                showErrorDialog(getString(R.string.add_normal_farm_missing_name));
            }
            return;
        }

        if (acreages == null || acreages.trim().isEmpty()) {
            showErrorDialog(getString(R.string.add_normal_farm_missing_acreages));
            return;
        }

        if (address == null || address.trim().isEmpty()) {
            showErrorDialog(getString(R.string.add_normal_farm_missing_address));
            return;
        }

        getCurrentLocationAndContinue();
    }

    private void afterGotCurrentLocation(double lat, double lon) {
        String name = mName.getText().toString();
        String acreages = mAcreages.getText().toString();
        String address = mAddress.getText().toString();

        showLoading();

        farmAPI.addNewFarm(name, address, acreages, String.valueOf(this.type), String.valueOf(lat), String.valueOf(lon), new APIHelper.Callback<Farm>() {
            @Override
            public void onSuccess(final Farm data) {
                AddByNormalWayActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        FarmBoxAppController.getInstance().setNeedLoadFarmList(true);
                        goToEditSeason(data.farm_id);
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                AddByNormalWayActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }

    private void goToEditSeason(long farmID) {
        if (Farm.GENERAL == this.type
                || Farm.STORAGE == this.type
                || Farm.CATTLE_HOUSE == this.type) {
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
        } else {
            Intent intent = new Intent(AddByNormalWayActivity.this, EditSeasonActivity.class);
            intent.putExtra("id", farmID);
            intent.putExtra("enableBack", false);
            startActivityForResult(intent, 0);
        }

        finish();
    }

    private void afterGetCurrentLocationFail() {
        showErrorDialog(getString(R.string.dialog_message_get_location_fail));
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                mAddress.setText(place.getAddress());
            }
        }
    }
}
