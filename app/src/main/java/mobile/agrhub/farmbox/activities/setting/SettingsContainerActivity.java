package mobile.agrhub.farmbox.activities.setting;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.view.View;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.custom_view.setting_item.SettingItemView;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.FarmAPI;
import mobile.agrhub.farmbox.service.model.Farm;
import mobile.agrhub.farmbox.service.model.Setting;
import mobile.agrhub.farmbox.utils.Utils;

public class SettingsContainerActivity extends FarmSettingV1Activity {

    SettingItemView mLight, mAirTemp, mAirHumi, mAirCO2, mWaterPH, mWaterNutri, mNutriFormula, mWaterVol, mLiquidVol, mWaterLevel, mTankHeight, mWashing, mLamp, mWaterPump, mFan, mMistingPump, mAC, mSensorHeight;
    SettingItemView mWaterPumpFreqSetting;
    SettingItemView mOxygenPumpFreqSetting;

    FarmAPI farmAPI = APIHelper.getFarmAPI();
    long farmType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_container);

        farmType = -1;
        bindingControls();
        configUI();
        setupControlEvents();
        configActionBar();
    }

    private void configActionBar() {
        String farmName = FarmBoxAppController.getInstance().getSelectedFarmName();
        getSupportActionBar().setTitle(farmName);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        loadData();
    }

    @Override
    protected void bindingControls() {
        super.bindingControls();

        mLight = (SettingItemView) findViewById(R.id.siv_light);
        mAirTemp = (SettingItemView) findViewById(R.id.siv_air_temp);
        mAirHumi = (SettingItemView) findViewById(R.id.siv_air_humi);
        mAirCO2 = (SettingItemView) findViewById(R.id.siv_air_co2);
        mWaterPH = (SettingItemView) findViewById(R.id.siv_water_ph);
        mWaterNutri = (SettingItemView) findViewById(R.id.siv_water_nutri);
        mNutriFormula = (SettingItemView) findViewById(R.id.siv_formula);
        mWaterVol = (SettingItemView) findViewById(R.id.siv_water_vol);
        mLiquidVol = (SettingItemView) findViewById(R.id.siv_liquid_vol);
        mWaterLevel = (SettingItemView) findViewById(R.id.siv_water_level);
        mTankHeight = (SettingItemView) findViewById(R.id.siv_tank_height);
        mSensorHeight = (SettingItemView) findViewById(R.id.siv_sensor_height);
        mWashing = (SettingItemView) findViewById(R.id.siv_washing);
        mLamp = (SettingItemView) findViewById(R.id.siv_lamp);
        mWaterPump = (SettingItemView) findViewById(R.id.siv_water_pump);
        mFan = (SettingItemView) findViewById(R.id.siv_fan);
        mMistingPump = (SettingItemView) findViewById(R.id.siv_misting_pump);
        mAC = (SettingItemView) findViewById(R.id.siv_ac);
        mWaterPumpFreqSetting = (SettingItemView) findViewById(R.id.siv_water_pump_freq);
        mOxygenPumpFreqSetting = (SettingItemView) findViewById(R.id.siv_oxygen_pump_freq);
    }

    @Override
    protected void configUI() {
        super.configUI();
        FarmBoxAppController app = FarmBoxAppController.getInstance();

        mLight.setName(getString(R.string.setting_light));
        mAirTemp.setName(getString(R.string.setting_air_temp, app.getTemperatureUnit()));
        mAirHumi.setName(getString(R.string.setting_air_humi));
        mAirCO2.setName(getString(R.string.setting_air_co2));
        mWaterPH.setName(getString(R.string.setting_water_ph));
        mWaterNutri.setName(getString(R.string.setting_water_nutri, app.getNutritionUnit()));
        mNutriFormula.setName(getString(R.string.setting_nutri_formula));
        mWaterVol.setName(getString(R.string.setting_water_volume));
        mLiquidVol.setName(getString(R.string.setting_liquid_volume));
        mWaterLevel.setName(getString(R.string.setting_water_level));
        mTankHeight.setName(getString(R.string.setting_water_tank_height));
        mSensorHeight.setName(getString(R.string.setting_water_sensor_height));
        mWashing.setName(getString(R.string.setting_washing));
        mLamp.setName(getString(R.string.setting_lamp));
        mWaterPump.setName(getString(R.string.setting_water_pump));
        mFan.setName(getString(R.string.setting_fan));
        mMistingPump.setName(getString(R.string.setting_misting_pump));
        mAC.setName(getString(R.string.setting_ac));

        mWaterPumpFreqSetting.setName(getString(R.string.setting_water_pump_freq));
        mOxygenPumpFreqSetting.setName(getString(R.string.setting_oxygen_pump_freq));
    }

    @Override
    protected void setupControlEvents() {
        super.setupControlEvents();

        setupControlEventsForSettingItem(mLight);
        setupControlEventsForSettingItem(mAirTemp);
        setupControlEventsForSettingItem(mAirHumi);
        setupControlEventsForSettingItem(mAirCO2);
        setupControlEventsForSettingItem(mWaterPH);
        setupControlEventsForSettingItem(mWaterNutri);
        setupControlEventsForSettingItem(mNutriFormula);
        setupControlEventsForSettingItem(mWaterVol);
        setupControlEventsForSettingItem(mLiquidVol);
        setupControlEventsForSettingItem(mWaterLevel);
        setupControlEventsForSettingItem(mTankHeight);
        setupControlEventsForSettingItem(mSensorHeight);
        setupControlEventsForSettingItem(mWashing);
        setupControlEventsForSettingItem(mLamp);
        setupControlEventsForSettingItem(mWaterPump);
        setupControlEventsForSettingItem(mFan);
        setupControlEventsForSettingItem(mMistingPump);
        setupControlEventsForSettingItem(mAC);
        setupControlEventsForSettingItem(mWaterPumpFreqSetting);
        setupControlEventsForSettingItem(mOxygenPumpFreqSetting);
    }

    private void setupControlEventsForSettingItem(final SettingItemView itemView) {
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = itemView.getIntentToChangeSetting(SettingsContainerActivity.this);
                intent.putExtra("unit", SettingsContainerActivity.this.farmType);
                startActivity(intent);
            }
        });
    }

    private void loadData() {
        Farm farm = FarmBoxAppController.getInstance().getSelectedFarm();
        if (null == farm) {
            finish();
            return;
        }

        long farmID = farm.farm_id;
        farmType = farm.farm_type;

//        showLoading();
//        farmAPI.getFarmSettings(String.valueOf(farmID), new APIHelper.Callback<Setting>() {
//            @Override
//            public void onSuccess(final Setting data) {
//                SettingsContainerActivity.this.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        hideLoading();
//                        doLoadData(data);
//                    }
//                });
//            }
//
//            @Override
//            public void onFail(final int errorCode, final String error) {
//                SettingsContainerActivity.this.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        hideLoading();
//                        handleError(errorCode, error);
//                    }
//                });
//            }
//        });
    }

    @Override
    protected void doLoadData(Setting data) {
        super.doLoadData(data);

        if (data.farm_id == 0) {
            data.farm_id = FarmBoxAppController.getInstance().getSelectedFarmId();
        }
        //min max settings
        mLight.setValue(data.getLightValue());
        mLight.setBasicMetaData(SettingItemView.TYPE.MINMAX, data.farm_id + "", data.factory_id + "");
        mLight.setMetaData("factory_light", getString(R.string.setting_light_name), data.factory_light_min + "", data.factory_light_max + "");

        mAirTemp.setValue(data.getTempValue());
        mAirTemp.setBasicMetaData(SettingItemView.TYPE.MINMAX, data.farm_id + "", data.factory_id + "");
        mAirTemp.setMetaData("factory_temperature_level", getString(R.string.setting_air_temp_name), data.factory_temperature_level_min + "", data.factory_temperature_level_max + "");

        mAirHumi.setValue(data.getAirHumiValue());
        mAirHumi.setBasicMetaData(SettingItemView.TYPE.MINMAX, data.farm_id + "", data.factory_id + "");
        mAirHumi.setMetaData("factory_air_humidity", getString(R.string.setting_air_humi_name), data.factory_air_humidity_min + "", data.factory_air_humidity_max + "");

        mAirCO2.setValue(data.getCO2Value());
        mAirCO2.setBasicMetaData(SettingItemView.TYPE.MINMAX, data.farm_id + "", data.factory_id + "");
        mAirCO2.setMetaData("factory_co2", getString(R.string.setting_air_co2_name), data.factory_co2_min + "", data.factory_co2_max + "");

        mWaterPH.setValue(data.getPHValue());
        mWaterPH.setBasicMetaData(SettingItemView.TYPE.MINMAX, data.farm_id + "", data.factory_id + "");
        mWaterPH.setMetaData("factory_ph", getString(R.string.setting_water_ph_name), data.factory_ph_min + "", data.factory_ph_max + "");

        mWaterNutri.setValue(data.getECValue(this.farmType));
        mWaterNutri.setBasicMetaData(SettingItemView.TYPE.MINMAX, data.farm_id + "", data.factory_id + "");
        mWaterNutri.setMetaData("factory_ec", getString(R.string.setting_water_nutri_name), data.factory_ec_min + "", data.factory_ec_max + "");

        mWaterLevel.setValue(data.getWaterLevelValue());
        mWaterLevel.setBasicMetaData(SettingItemView.TYPE.MINMAX, data.farm_id + "", data.factory_id + "");
        mWaterLevel.setMetaData("factory_water_level", getString(R.string.setting_water_level_name), data.factory_water_level_min + "", data.factory_water_level_max + "");

        //formula settings
        mNutriFormula.setValue(data.getECFormula());
        mNutriFormula.setBasicMetaData(SettingItemView.TYPE.FORMULA, data.farm_id + "", data.factory_id + "");
        mNutriFormula.setMetaData(data.factory_ec_formula_a + "", data.factory_ec_formula_b + "", data.factory_ec_formula_c + "" + "", "");

        //single settings
        mWaterVol.setValue(data.getWaterVolumeValue());
        mWaterVol.setBasicMetaData(SettingItemView.TYPE.SINGLE, data.farm_id + "", data.factory_id + "");
        mWaterVol.setMetaData("factory_water_volume", getString(R.string.setting_water_volume_name), data.factory_water_volume + "", "");

        mLiquidVol.setValue(data.getLiquidVolumeValue());
        mLiquidVol.setBasicMetaData(SettingItemView.TYPE.SINGLE, data.farm_id + "", data.factory_id + "");
        mLiquidVol.setMetaData("factory_liquid_volume", getString(R.string.setting_liquid_volume_name), data.factory_liquid_volume + "", "");

        mTankHeight.setValue(data.getTankHeightValue());
        mTankHeight.setBasicMetaData(SettingItemView.TYPE.SINGLE, data.farm_id + "", data.factory_id + "");
        mTankHeight.setMetaData("factory_tank_height", getString(R.string.setting_water_tank_height_name), data.factory_tank_height + "", "");

        mSensorHeight.setValue(data.getSensorHeightValue());
        mSensorHeight.setBasicMetaData(SettingItemView.TYPE.SINGLE, data.farm_id + "", data.factory_id + "");
        mSensorHeight.setMetaData("factory_sensor_height", getString(R.string.setting_water_sensor_height_name), data.factory_sensor_height + "", "");

        //cron time settings
        mWashing.setValue(Utils.getStringFromCron(SettingsContainerActivity.this, data.factory_washing_mode_timer));
        mWashing.setBasicMetaData(SettingItemView.TYPE.CRON, data.farm_id + "", data.factory_id + "");
        mWashing.setMetaData(data.factory_washing_mode_timer, "", "", "");

        //on off settings
        mLamp.setValue(data.getLampTime());
        mLamp.setBasicMetaData(SettingItemView.TYPE.ONOFF, data.farm_id + "", data.factory_id  + "");
        mLamp.setMetaData("factory_lamp", getString(R.string.setting_lamp_name), data.factory_lamp_started_time + "", data.factory_lamp_stopped_time + "");

        mWaterPump.setValue(data.getWaterPumpTime());
        mWaterPump.setBasicMetaData(SettingItemView.TYPE.ONOFF, data.farm_id + "", data.factory_id  + "");
        mWaterPump.setMetaData("factory_water_pump", getString(R.string.setting_water_pump_name), data.factory_water_pump_started_time + "", data.factory_water_pump_stopped_time + "");

        mFan.setValue(data.getFanTime());
        mFan.setBasicMetaData(SettingItemView.TYPE.ONOFF, data.farm_id + "", data.factory_id  + "");
        mFan.setMetaData("factory_fan", getString(R.string.setting_fan_name), data.factory_fan_started_time + "", data.factory_fan_stopped_time + "");

        mMistingPump.setValue(data.getMistingPumpTime());
        mMistingPump.setBasicMetaData(SettingItemView.TYPE.ONOFF, data.farm_id + "", data.factory_id  + "");
        mMistingPump.setMetaData("factory_misting_pump", getString(R.string.setting_misting_pump_name), data.factory_misting_pump_started_time + "", data.factory_misting_pump_stopped_time + "");

        mAC.setValue(data.getACTime());
        mAC.setBasicMetaData(SettingItemView.TYPE.ONOFF, data.farm_id + "", data.factory_id  + "");
        mAC.setMetaData("factory_ac", getString(R.string.setting_ac_name), data.factory_ac_started_time + "", data.factory_ac_stopped_time + "");

        mWaterPumpFreqSetting.setValue(data.getWaterPumpFreqValue(this));
        mWaterPumpFreqSetting.setBasicMetaData(SettingItemView.TYPE.TIME_LOOP, data.farm_id + "", data.factory_id  + "");
        mWaterPumpFreqSetting.setMetaData("factory_water_pump_freq", getString(R.string.setting_water_pump_freq), data.factoryWaterPumpFreq + "", data.factoryWaterPumpOffFreq + "");

        mOxygenPumpFreqSetting.setValue(data.getOxygenPumpFreqValue(this));
        mOxygenPumpFreqSetting.setBasicMetaData(SettingItemView.TYPE.TIME_LOOP, data.farm_id + "", data.factory_id  + "");
        mOxygenPumpFreqSetting.setMetaData("factory_oxygen_pump_freq", getString(R.string.setting_oxygen_pump_freq), data.factoryOxygenPumpFreq + "", data.factoryOxygenPumpOffFreq + "");
    }
}
