package mobile.agrhub.farmbox.activities.setting;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.view.View;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.custom_view.setting_item.SettingItemView;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.FarmAPI;
import mobile.agrhub.farmbox.service.model.Farm;
import mobile.agrhub.farmbox.service.model.Setting;

public class SettingsMushroomActivity extends FarmSettingV1Activity {
    SettingItemView mLight, mAirTemp, mAirHumi, mLamp, mMistingPump, mAC;

    FarmAPI farmAPI = APIHelper.getFarmAPI();
    long farmType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_mushroom);
        farmType = -1;

        bindingControls();
        configUI();
        setupControlEvents();
        configActionBar();
    }

    private void configActionBar() {
        String farmName = FarmBoxAppController.getInstance().getSelectedFarmName();
        getSupportActionBar().setTitle(farmName);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        loadData();
    }

    @Override
    protected void bindingControls() {
        super.bindingControls();

        mLight = (SettingItemView) findViewById(R.id.siv_light);
        mAirTemp = (SettingItemView) findViewById(R.id.siv_air_temp);
        mAirHumi = (SettingItemView) findViewById(R.id.siv_air_humi);
        mLamp = (SettingItemView) findViewById(R.id.siv_lamp);
        mMistingPump = (SettingItemView) findViewById(R.id.siv_misting_pump);
        mAC = (SettingItemView) findViewById(R.id.siv_ac);
    }

    @Override
    protected void configUI() {
        super.configUI();
        FarmBoxAppController app = FarmBoxAppController.getInstance();

        mLight.setName(getString(R.string.setting_light));
        mAirTemp.setName(getString(R.string.setting_air_temp, app.getTemperatureUnit()));
        mAirHumi.setName(getString(R.string.setting_air_humi));
        mLamp.setName(getString(R.string.setting_lamp));
        mMistingPump.setName(getString(R.string.setting_misting_pump));
        mAC.setName(getString(R.string.setting_ac));
    }

    @Override
    protected void setupControlEvents() {
        super.setupControlEvents();

        setupControlEventsForSettingItem(mLight);
        setupControlEventsForSettingItem(mAirTemp);
        setupControlEventsForSettingItem(mAirHumi);
        setupControlEventsForSettingItem(mLamp);
        setupControlEventsForSettingItem(mMistingPump);
        setupControlEventsForSettingItem(mAC);
    }

    private void setupControlEventsForSettingItem(final SettingItemView itemView) {
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = itemView.getIntentToChangeSetting(SettingsMushroomActivity.this);
                intent.putExtra("unit", SettingsMushroomActivity.this.farmType);
                startActivity(intent);
            }
        });
    }

    private void loadData() {
        Farm farm = FarmBoxAppController.getInstance().getSelectedFarm();
        if (null == farm) {
            finish();
            return;
        }
        long farmID = farm.farm_id;
        farmType = farm.farm_type;

//        showLoading();
//        farmAPI.getFarmSettings(String.valueOf(farmID), new APIHelper.Callback<Setting>() {
//            @Override
//            public void onSuccess(final Setting data) {
//                SettingsMushroomActivity.this.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        hideLoading();
//                        doLoadData(data);
//                    }
//                });
//            }
//
//            @Override
//            public void onFail(final int errorCode, final String error) {
//                SettingsMushroomActivity.this.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        hideLoading();
//                        handleError(errorCode, error);
//                    }
//                });
//            }
//        });
    }

    @Override
    protected void doLoadData(Setting data) {
        super.doLoadData(data);

        if (data.farm_id == 0) {
            data.farm_id = FarmBoxAppController.getInstance().getSelectedFarmId();
        }

        mLight.setValue(data.getLightValue());
        mLight.setBasicMetaData(SettingItemView.TYPE.MINMAX, data.farm_id + "", data.factory_id + "");
        mLight.setMetaData("factory_light", getString(R.string.setting_light_name), data.factory_light_min + "", data.factory_light_max + "");

        mAirTemp.setValue(data.getTempValue());
        mAirTemp.setBasicMetaData(SettingItemView.TYPE.MINMAX, data.farm_id + "", data.factory_id + "");
        mAirTemp.setMetaData("factory_temperature_level", getString(R.string.setting_air_temp_name), data.factory_temperature_level_min + "", data.factory_temperature_level_max + "");

        mAirHumi.setValue(data.getAirHumiValue());
        mAirHumi.setBasicMetaData(SettingItemView.TYPE.MINMAX, data.farm_id + "", data.factory_id + "");
        mAirHumi.setMetaData("factory_air_humidity", getString(R.string.setting_air_humi_name), data.factory_air_humidity_min + "", data.factory_air_humidity_max + "");

        mLamp.setValue(data.getLampTime());
        mLamp.setBasicMetaData(SettingItemView.TYPE.ONOFF, data.farm_id + "", data.factory_id  + "");
        mLamp.setMetaData("factory_lamp", getString(R.string.setting_lamp_name), data.factory_lamp_started_time + "", data.factory_lamp_stopped_time + "");

        mMistingPump.setValue(data.getMistingPumpTime());
        mMistingPump.setBasicMetaData(SettingItemView.TYPE.ONOFF, data.farm_id + "", data.factory_id  + "");
        mMistingPump.setMetaData("factory_water_pump", getString(R.string.setting_misting_pump_name), data.factory_water_pump_started_time + "", data.factory_water_pump_stopped_time + "");

        mAC.setValue(data.getACTime());
        mAC.setBasicMetaData(SettingItemView.TYPE.ONOFF, data.farm_id + "", data.factory_id  + "");
        mAC.setMetaData("factory_ac", getString(R.string.setting_ac_name), data.factory_ac_started_time + "", data.factory_ac_stopped_time + "");
    }
}
