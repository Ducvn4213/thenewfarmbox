package mobile.agrhub.farmbox.activities.farm.order;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andremion.counterfab.CounterFab;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.activities.farm.product.AddFarmProductActivity;
import mobile.agrhub.farmbox.adapters.product.FarmProductAdapter;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.model.FarmCart;
import mobile.agrhub.farmbox.service.model.FarmProduct;
import mobile.agrhub.farmbox.service.model.User;
import mobile.agrhub.farmbox.service.model.response.GetFarmProductResponse;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Constants;
import mobile.agrhub.farmbox.utils.Utils;

public class AddProductToCartActivity extends BaseActivity implements FarmProductAdapter.OnClickListener {
    public static final int REQUEST_CONFIRM_CART_FARM_PRODUCT_CODE = 402;

    @BindView(R.id.gv_farm_product)
    GridView gvProducts;
    @BindView(android.R.id.empty)
    LinearLayout mEmptyView;
    @BindView(R.id.tv_empty_message)
    TextView mEmptyMessage;
    @BindView(R.id.btn_add_product)
    Button mAddProduct;
    @BindView(R.id.fb_add)
    CounterFab mAddBtn;

    private FarmProductAdapter listAdapter;
    private List<FarmProduct> products;
    private List<FarmCart> carts;

    int page = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product_to_cart);
        ButterKnife.bind(this);

//        Intent intent = getIntent();
//        String productStr = intent.getStringExtra(Constants.FARM_PRODUCT_INFO_KEY);
//        if (productStr != null && !productStr.isEmpty()) {
//            Gson gson = new Gson();
//            productInfo = gson.fromJson(productStr, FarmProduct.class);
//        }

        configActionBar();

        mAddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddProductToCartActivity.this, AddFarmProductActivity.class);
                AddProductToCartActivity.this.startActivity(intent);
            }
        });

        mAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showConfirmCart();
            }
        });

        products = new ArrayList<FarmProduct>();
        carts = new ArrayList<>();
        listAdapter = new FarmProductAdapter(this, products, carts, this);
        gvProducts.setAdapter(listAdapter);
        gvProducts.setEmptyView(mEmptyView);
    }

    private void configActionBar() {
        getSupportActionBar().setTitle(R.string.add_product_to_cart_title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();

        User user = FarmBoxAppController.getInstance().getUser();
        if (user == null) {
            return;
        }

        APIHelper.getSaleAPI().getProducts(page, user.userId, true, new APIHelper.Callback<GetFarmProductResponse>() {
            @Override
            public void onSuccess(GetFarmProductResponse data) {
                if (data == null) {
                    products.clear();
                } else {
                    products = data.farmProducts;
                    FragmentActivity activity = AddProductToCartActivity.this;
                    if (activity != null) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (AddProductToCartActivity.this != null) {
                                    AddProductToCartActivity.this.hideLoading();
                                    AddProductToCartActivity.this.refreshListView();
                                }
                            }
                        });
                    }
                }
            }

            @Override
            public void onFail(int errorCode, String error) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.next_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.action_next) {
            showConfirmCart();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onProductClicked(FarmProduct product) {
        refreshCartItem();
    }

    @Override
    public void onMinusClicked(String productId) {
        listAdapter.notifyDataSetChanged();
        refreshCartItem();
    }

    @Override
    public void onProductAmountClicked(final long productId, int amount) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppTheme_Dialog_Alert);
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setText(String.valueOf(amount));
        input.setSingleLine(true);

        FrameLayout container = new FrameLayout(this);
        FrameLayout.LayoutParams params = new  FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.leftMargin = getResources().getDimensionPixelSize(R.dimen.margin_16);
        params.rightMargin = getResources().getDimensionPixelSize(R.dimen.margin_16);
        input.setLayoutParams(params);
        container.addView(input);

        builder
                .setTitle(R.string.amount)
                .setView(container)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        String value = input.getText().toString();

                        int amount = 0;
                        try {
                            amount = Utils.str2Int(value);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        AddProductToCartActivity.this.listAdapter.addToCart(productId, amount, 0);
                        AddProductToCartActivity.this.listAdapter.notifyDataSetChanged();
                        AddProductToCartActivity.this.refreshCartItem();

                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                    }

                });

        builder.show();
        input.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    @Override
    public void onPlusClicked(String productId) {
        listAdapter.notifyDataSetChanged();
        refreshCartItem();
    }

    @Override
    public void onAddToCartClicked(long productId) {
        listAdapter.notifyDataSetChanged();
        refreshCartItem();
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CONFIRM_CART_FARM_PRODUCT_CODE) {
            if (resultCode == RESULT_OK) {
                Bundle bundle = data.getExtras();
                if (bundle != null) {
                    if (bundle.containsKey(Constants.ORDER_CART_LIST_KEY)) {
                        String cartListStr = bundle.getString(Constants.ORDER_CART_LIST_KEY);
                        if (cartListStr != null && !cartListStr.isEmpty()) {
                            Gson gson = new Gson();
                            carts = gson.fromJson(cartListStr, new TypeToken<List<FarmCart>>() {
                            }.getType());
                        }
                        refreshListView();
                    }
                }
            } else {
                finish();
            }
        }
    }

    private void refreshListView() {
        if (null != gvProducts) {
            if (null == listAdapter) {
                listAdapter = new FarmProductAdapter(this, products, this);
                gvProducts.setAdapter(listAdapter);
            } else {
                listAdapter.update(products, carts);
                listAdapter.notifyDataSetChanged();
            }
            mEmptyMessage.setText(getString(R.string.product_empty_message));
            mAddProduct.setVisibility(View.VISIBLE);
            gvProducts.smoothScrollToPosition(0);

            refreshCartItem();
        }
    }

    private void refreshCartItem() {
        int totalCartItem = listAdapter.totalItem();
        this.mAddBtn.setCount(totalCartItem);
    }

    private void showConfirmCart() {
        if (listAdapter.getCount() > 0) {
            List<FarmCart> realCart = new ArrayList<>();
            if (carts != null && carts.size() > 0) {
                for (FarmCart item : carts) {
                    if (item.cartItemQuantity > 0) {
                        realCart.add(item);
                    }
                }
            }

            Gson gson = new Gson();
            String data = gson.toJson(realCart);

            Intent intent = new Intent(this, ConfirmFarmProductCartActivity.class);
            intent.putExtra(Constants.ORDER_CART_LIST_KEY, data);

            startActivityForResult(intent, REQUEST_CONFIRM_CART_FARM_PRODUCT_CODE);
        } else {
            showErrorDialog(R.string.choose_product_first);
        }
    }
}
