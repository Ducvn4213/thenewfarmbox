package mobile.agrhub.farmbox.activities.dairy;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.Calendar;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.model.FarmBasic;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Utils;

public class FilterDiaryActivity extends BaseActivity {
    public static final String DIARY_DATE_FORMAT = "dd/MM/yyyy";

    RadioGroup mFilterFarmList;
    LinearLayout mFilterFromDate;
    TextView mFilterFromDateValue;
    ImageView mClearFilterFromDate;
    LinearLayout mFilterToDate;
    TextView mFilterToDateValue;
    ImageView mClearFilterToDate;
    LinearLayout mClearFilter;

    String mSelectedFarmId = "";
    int mSelectedFromYear;
    int mSelectedFromMonth;
    int mSelectedFromDate;
    int mSelectedToYear;
    int mSelectedToMonth;
    int mSelectedToDate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_diary);

        bindingControls();
        initData();
        setupControlEvents();
    }

    private void bindingControls() {
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mFilterFarmList = (RadioGroup) findViewById(R.id.rg_filter_farm_list);
        mFilterFromDate = (LinearLayout) findViewById(R.id.ll_from_date);
        mFilterFromDateValue = (TextView) findViewById(R.id.tv_from_date);
        mClearFilterFromDate = (ImageView) findViewById(R.id.iv_clear_from_date);
        mFilterToDate = (LinearLayout) findViewById(R.id.ll_to_date);
        mFilterToDateValue = (TextView) findViewById(R.id.tv_to_date);
        mClearFilterToDate = (ImageView) findViewById(R.id.iv_clear_to_date);
        mClearFilter = (LinearLayout) findViewById(R.id.ll_clear_filter);
    }

    private void initData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle.containsKey("DIARY_FILTER_SELECTED_FARM_ID")) {
            mSelectedFarmId = bundle.getString("DIARY_FILTER_SELECTED_FARM_ID");
        }
        if (bundle.containsKey("DIARY_FILTER_SELECTED_FROM_YEAR")) {
            mSelectedFromYear = bundle.getInt("DIARY_FILTER_SELECTED_FROM_YEAR", 0);
        }
        if (bundle.containsKey("DIARY_FILTER_SELECTED_FROM_MONTH")) {
            mSelectedFromMonth = bundle.getInt("DIARY_FILTER_SELECTED_FROM_MONTH", 0);
        }
        if (bundle.containsKey("DIARY_FILTER_SELECTED_FROM_DATE")) {
            mSelectedFromDate = bundle.getInt("DIARY_FILTER_SELECTED_FROM_DATE", 0);
        }
        if (bundle.containsKey("DIARY_FILTER_SELECTED_TO_YEAR")) {
            mSelectedToYear = bundle.getInt("DIARY_FILTER_SELECTED_TO_YEAR", 0);
        }
        if (bundle.containsKey("DIARY_FILTER_SELECTED_TO_MONTH")) {
            mSelectedToMonth = bundle.getInt("DIARY_FILTER_SELECTED_TO_MONTH", 0);
        }
        if (bundle.containsKey("DIARY_FILTER_SELECTED_TO_DATE")) {
            mSelectedToDate = bundle.getInt("DIARY_FILTER_SELECTED_TO_DATE", 0);
        }

        // clear old data
        mFilterFarmList.removeAllViews();

        // add new view
        FarmBoxAppController app = FarmBoxAppController.getInstance();
        List<FarmBasic> farms = app.getFarmList();
        if (farms != null && farms.size() > 0) {
            RadioButton radio;

            LinearLayout.LayoutParams radioItemParam = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);

            // Convert to pixels
            float paddingDp = 10f;
            int paddingPx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, paddingDp, getResources().getDisplayMetrics());

            // list farm
            if (farms.size() == 1) {
                radio = new RadioButton(this);
                radio.setTag(String.valueOf(farms.get(0).farm_id));
                radio.setLayoutParams(radioItemParam);
                radio.setPadding(paddingPx, paddingPx, paddingPx, paddingPx);
                radio.setTextColor(getResources().getColor(android.R.color.black));
                radio.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
                radio.setText(farms.get(0).farm_name);

                mFilterFarmList.addView(radio);
            } else {
                radio = new RadioButton(this);
                radio.setTag("ALL_FARM");
                radio.setLayoutParams(radioItemParam);
                radio.setPadding(paddingPx, paddingPx, paddingPx, paddingPx);
                radio.setTextColor(getResources().getColor(android.R.color.black));
                radio.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
                radio.setText(getString(R.string.filter_diary_all_farm));

                mFilterFarmList.addView(radio);

                for (FarmBasic farm : farms) {
                    radio = new RadioButton(this);
                    radio.setTag(String.valueOf(farm.farm_id));
                    radio.setLayoutParams(radioItemParam);
                    radio.setPadding(paddingPx, paddingPx, paddingPx, paddingPx);
                    radio.setTextColor(getResources().getColor(android.R.color.black));
                    radio.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
                    radio.setText(farm.farm_name);

                    mFilterFarmList.addView(radio);
                }
            }
        }

        if (null == mSelectedFarmId || mSelectedFarmId.isEmpty()) {
            RadioButton radio = (RadioButton) mFilterFarmList.findViewWithTag("ALL_FARM");
            if (null != radio) {
                mFilterFarmList.check(radio.getId());
            }
        } else {
            RadioButton radio = (RadioButton) mFilterFarmList.findViewWithTag(mSelectedFarmId);
            if (null != radio) {
                mFilterFarmList.check(radio.getId());
            }
        }

        // from date
        if (mSelectedFromYear > 0 && mSelectedFromMonth > 0 && mSelectedFromDate > 0) {
            mFilterFromDateValue.setText(Utils.getDate(mSelectedFromYear, mSelectedFromMonth, mSelectedFromDate, DIARY_DATE_FORMAT));
        } else {
            mFilterFromDateValue.setText("");
        }

        // to date
        if (mSelectedToYear > 0 && mSelectedToMonth > 0 && mSelectedToDate > 0) {
            mFilterToDateValue.setText(Utils.getDate(mSelectedToYear, mSelectedToMonth, mSelectedToDate, DIARY_DATE_FORMAT));
        } else {
            mFilterToDateValue.setText("");
        }
    }

    private void setupControlEvents() {
        mFilterFarmList.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radio = group.findViewById(checkedId);
                if (null != radio && null != radio.getTag()
                        && radio.getTag() instanceof String) {
                    mSelectedFarmId = (String) radio.getTag();
                    if ("ALL_FARM".equals(mSelectedFarmId)) {
                        mSelectedFarmId = "";
                    }
                } else {
                    mSelectedFarmId = "";
                }
            }
        });

        mFilterFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int initYear;
                int initMonth;
                int initDate;
                if (mSelectedFromYear == 0 || mSelectedFromMonth == 0 || mSelectedFromDate == 0) {
                    Calendar calendar = Calendar.getInstance();
                    initYear = calendar.get(Calendar.YEAR);
                    initMonth = calendar.get(Calendar.MONTH);
                    initDate = calendar.get(Calendar.DATE);
                } else {
                    initYear = mSelectedFromYear;
                    initMonth = mSelectedFromMonth;
                    initDate = mSelectedFromDate;
                }

                DatePickerDialog datePickerDialog = new DatePickerDialog(FilterDiaryActivity.this, R.style.AppTheme_Dialog, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        mSelectedFromYear = year;
                        mSelectedFromMonth = monthOfYear;
                        mSelectedFromDate = dayOfMonth;

                        mFilterFromDateValue.setText(Utils.getDate(year, monthOfYear, dayOfMonth, DIARY_DATE_FORMAT));
                    }
                }, initYear, initMonth, initDate);
                // set max date
                if (mSelectedToYear > 0 && mSelectedToMonth > 0 && mSelectedToDate > 0) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(mSelectedToYear, mSelectedToMonth, mSelectedToDate);

                    datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
                }
                datePickerDialog.show();
            }
        });

        mClearFilterFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectedFromYear = 0;
                mSelectedFromMonth = 0;
                mSelectedFromDate = 0;
                mFilterFromDateValue.setText("");
            }
        });

        mFilterToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int initYear;
                int initMonth;
                int initDate;
                if (mSelectedToYear == 0 || mSelectedToMonth == 0 || mSelectedToDate == 0) {
                    Calendar calendar = Calendar.getInstance();
                    initYear = calendar.get(Calendar.YEAR);
                    initMonth = calendar.get(Calendar.MONTH);
                    initDate = calendar.get(Calendar.DATE);
                } else {
                    initYear = mSelectedToYear;
                    initMonth = mSelectedToMonth;
                    initDate = mSelectedToDate;
                }

                DatePickerDialog datePickerDialog = new DatePickerDialog(FilterDiaryActivity.this, R.style.AppTheme_Dialog, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        mSelectedToYear = year;
                        mSelectedToMonth = monthOfYear;
                        mSelectedToDate = dayOfMonth;

                        mFilterToDateValue.setText(Utils.getDate(year, monthOfYear, dayOfMonth, DIARY_DATE_FORMAT));
                    }
                }, initYear, initMonth, initDate);
                // set max date
                if (mSelectedFromYear > 0 && mSelectedFromMonth > 0 && mSelectedFromDate > 0) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(mSelectedFromYear, mSelectedFromMonth, mSelectedFromDate);

                    datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
                }
                datePickerDialog.show();
            }
        });

        mClearFilterToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectedToYear = 0;
                mSelectedToMonth = 0;
                mSelectedToDate = 0;
                mFilterToDateValue.setText("");
            }
        });

        mClearFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectedFarmId = "";
                mSelectedFromYear = 0;
                mSelectedFromMonth = 0;
                mSelectedFromDate = 0;
                mSelectedToYear = 0;
                mSelectedToMonth = 0;
                mSelectedToDate = 0;

                dismissView();
            }
        });
    }

    private void dismissView() {
        // Prepare data intent
        Intent data = new Intent();
        data.putExtra("DIARY_FILTER_SELECTED_FARM_ID", (mSelectedFarmId == null ? "" : mSelectedFarmId));
        data.putExtra("DIARY_FILTER_SELECTED_FROM_YEAR", mSelectedFromYear);
        data.putExtra("DIARY_FILTER_SELECTED_FROM_MONTH", mSelectedFromMonth);
        data.putExtra("DIARY_FILTER_SELECTED_FROM_DATE", mSelectedFromDate);
        data.putExtra("DIARY_FILTER_SELECTED_TO_YEAR", mSelectedToYear);
        data.putExtra("DIARY_FILTER_SELECTED_TO_MONTH", mSelectedToMonth);
        data.putExtra("DIARY_FILTER_SELECTED_TO_DATE", mSelectedToDate);

        // Activity finished ok, return the data
        this.setResult(Activity.RESULT_OK, data);
        this.finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.filter_diary_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_apply_filter:
                dismissView();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
