package mobile.agrhub.farmbox.activities.setting;

import android.support.v7.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.adapters.FarmSettingInputDataAdapter;
import mobile.agrhub.farmbox.service.model.FarmSettingDetailData;
import mobile.agrhub.farmbox.service.network.Param;
import mobile.agrhub.farmbox.utils.Utils;

public class FarmSettingNutritionFormulaActivity extends BaseFarmSettingItemActivity {
    @BindView(R.id.rv_content)
    RecyclerView mRecyclerView;

    FarmSettingInputDataAdapter mAdapter;

    @Override
    protected int contentView() {
        return R.layout.activity_farm_setting_nutrition_formula;
    }

    @Override
    protected void initView() {
        super.initView();

        mAdapter = new FarmSettingInputDataAdapter(this, item);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    protected boolean validate() {
        FarmSettingInputDataAdapter.ViewHolder viewHolder;
        FarmSettingDetailData settingData;

        String val;
        int intVal;
        int totalVal = 0;
        for (int i = 0; i < item.getData().size(); i++) {
            settingData = item.getData().get(i);

            viewHolder = (FarmSettingInputDataAdapter.ViewHolder)mRecyclerView.findViewHolderForAdapterPosition(i);

            val = viewHolder.getVal();

            // check empty
            if (val.isEmpty()) {
                showErrorDialog(getString(R.string.field_is_required, settingData.getName()));
                return false;
            }

            // check is number
            try {
                intVal = Utils.str2Int(val);
                totalVal += intVal;
            } catch (Exception e) {
                showErrorDialog(getString(R.string.field_is_not_number, settingData.getName()));
                return false;
            }

            // check num in range
            if (settingData.getExpectData() != null && settingData.getExpectData().size() >= 2) {
                try {
                    int fromVal = (int)Math.round(Utils.str2Double(settingData.getExpectData().get(0).toString()));
                    int toVal = (int)Math.round(Utils.str2Double(settingData.getExpectData().get(1).toString()));
                    if (intVal < fromVal || intVal > toVal) {
                        showErrorDialog(getString(R.string.field_between, settingData.getName(), fromVal + "", toVal + ""));
                        return false;
                    }
                } catch (Exception e) {

                }
            }
        }

        if (totalVal != 100) {
            showErrorDialog(getString(R.string.message_sum_all_item, "100"));
            return false;
        }

        return true;
    }

    @Override
    protected List<Param> buildParam(List<Param> params) {
        FarmSettingInputDataAdapter.ViewHolder viewHolder;
        FarmSettingDetailData settingData;

        String val;
        int intVal;
        for (int i = 0; i < item.getData().size(); i++) {
            settingData = item.getData().get(i);
            viewHolder = (FarmSettingInputDataAdapter.ViewHolder) mRecyclerView.findViewHolderForAdapterPosition(i);
            val = viewHolder.getVal();

            try {
                intVal = Utils.str2Int(val);
            } catch (Exception e) {
                e.printStackTrace();
                intVal = 0;
            }

            params.add(new Param(settingData.getField(), intVal + ""));
        }
        return params;
    }
}
