package mobile.agrhub.farmbox.activities.setting;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.adapters.SettingUnitAdapter;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.model.UnitItem;
import mobile.agrhub.farmbox.service.model.UnitOptionItem;
import mobile.agrhub.farmbox.service.model.User;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Constants;

public class SettingUnitActivity extends BaseActivity {

    @BindView(R.id.rv_content) RecyclerView mRecycleView;

    SettingUnitAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_unit);
        ButterKnife.bind(this);

        configActionBar();
        initData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void configActionBar() {
        getSupportActionBar().setTitle(R.string.setting_unit_title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initData() {
        User userInfo = FarmBoxAppController.getInstance().getUser();

        List<UnitItem> data = new ArrayList<>();

        UnitItem item;
        UnitOptionItem optionItem;

        // temperature
        item = new UnitItem();
        item.setKey(Constants.SETTING_TEMPERATURE_UNIT_KEY + userInfo.user_phone);
        item.setTitle(getString(R.string.setting_temperature_unit));
        item.setValue(userInfo.getTemperatureUnit());

        optionItem = new UnitOptionItem();
        optionItem.setKey(Constants.SETTING_TEMPERATURE_UNIT_KEY + userInfo.user_phone);
        optionItem.setValue(UnitItem.TEMPERATURE_C_VALUE);
        optionItem.setTitle(getString(R.string.temperature_unit_c));
        item.add(optionItem);

        optionItem = new UnitOptionItem();
        optionItem.setKey(Constants.SETTING_TEMPERATURE_UNIT_KEY + userInfo.user_phone);
        optionItem.setValue(UnitItem.TEMPERATURE_F_VALUE);
        optionItem.setTitle(getString(R.string.temperature_unit_f));
        item.add(optionItem);

        data.add(item);


        // nutrition
        item = new UnitItem();
        item.setKey(Constants.SETTING_NUTRITION_UNIT_KEY + userInfo.user_phone);
        item.setTitle(getString(R.string.setting_nutrition_unit));
        item.setValue(userInfo.getNutritionUnit());

        optionItem = new UnitOptionItem();
        optionItem.setKey(Constants.SETTING_NUTRITION_UNIT_KEY + userInfo.user_phone);
        optionItem.setValue(UnitItem.NUTRITION_MILLISIEMENS_VALUE);
        optionItem.setTitle(getString(R.string.nutrition_unit_ms_cm));
        item.add(optionItem);

        optionItem = new UnitOptionItem();
        optionItem.setKey(Constants.SETTING_NUTRITION_UNIT_KEY + userInfo.user_phone);
        optionItem.setValue(UnitItem.NUTRITION_PARTS_PER_MILLION_VALUE);
        optionItem.setTitle(getString(R.string.nutrition_unit_ppm));
        item.add(optionItem);

        data.add(item);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(SettingUnitActivity.this, DividerItemDecoration.VERTICAL);
        mRecycleView.addItemDecoration(dividerItemDecoration);

        mAdapter = new SettingUnitAdapter(this, data);
        mRecycleView.setAdapter(mAdapter);
    }
}

