package mobile.agrhub.farmbox.activities.registration;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.myhexaville.smartimagepicker.ImagePicker;
import com.myhexaville.smartimagepicker.OnImagePickedListener;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.UserAPI;
import mobile.agrhub.farmbox.service.model.UploadImageItem;
import mobile.agrhub.farmbox.service.model.User;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Utils;


public class PhoneRegistrationAvatarActivity extends BaseActivity {

    private final int PICK_IMAGE_REQUEST_CODE = 9999;
    private final int TAKE_IMAGE_REQUEST_CODE = 9998;

    private String userPhone;
    private ImagePicker mImagePicker;
    private Uri avatarUri;
    private String avatarUrl;

    CircleImageView avatarButton;
    Button continueButton;

    UserAPI userAPI = APIHelper.getUserAPI();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.phone_registration_avatar_activity);

        Intent intent = getIntent();
        userPhone = intent.getStringExtra("user_phone");
        if (userPhone == null || userPhone.isEmpty()) {
            finish();
        }

        bindingControl();
        setupControlEvents();
        initData();
    }

    public void bindingControl() {
        avatarButton = (CircleImageView) findViewById(R.id.ib_avatar);
        continueButton = (Button) findViewById(R.id.btn_continue);
    }

    public void setupControlEvents() {
        mImagePicker = new ImagePicker(this, null, new OnImagePickedListener() {
            @Override
            public void onImagePicked(Uri imageUri) {
                avatarUri = imageUri;
                avatarButton.setImageURI(imageUri);
            }
        })
                .setWithImageCrop(1, 1);

        avatarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mImagePicker.choosePicture(true);
            }
        });

        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateAvatar();
            }
        });
    }

    public void initData() {
        Bitmap defaultAvatar = BitmapFactory.decodeResource(PhoneRegistrationAvatarActivity.this.getResources(), R.drawable.avatar);
        avatarButton.setImageBitmap(Utils.getCircleBitmap(defaultAvatar));
    }

    public void requestChangeAvatar() {
        final Dialog actionDialog = new Dialog(PhoneRegistrationAvatarActivity.this);
        actionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        actionDialog.setCancelable(true);
        actionDialog.setContentView(R.layout.layout_change_avatar_action);

        TextView camera = (TextView) actionDialog.findViewById(R.id.tv_take_from_camera);
        TextView gallery = (TextView) actionDialog.findViewById(R.id.tv_get_from_gallery);

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionDialog.dismiss();
                checkCameraPermission();
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionDialog.dismiss();
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                ActivityCompat.startActivityForResult(PhoneRegistrationAvatarActivity.this, Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST_CODE, null);
            }
        });

        actionDialog.show();
    }

    void checkCameraPermission() {
        List<String> permissions = new ArrayList<>();
        if (ContextCompat.checkSelfPermission(PhoneRegistrationAvatarActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            gotoTakeImageFromCamera();
        } else {
            permissions.add(Manifest.permission.CAMERA);
            String[] perArr = new String[permissions.size()];
            perArr = permissions.toArray(perArr);
            ActivityCompat.requestPermissions(PhoneRegistrationAvatarActivity.this,
                    perArr,
                    Utils.PERMISSIONS_CAMERA_REQUEST);
        }
    }

    void gotoTakeImageFromCamera() {
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePicture.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePicture, TAKE_IMAGE_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mImagePicker.handlePermission(requestCode, grantResults);
    }

    public void updateAvatar() {
        if (avatarUri == null) {
            gotoCompleted();
            return;
        }

        try {
            showLoading();
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), avatarUri);
            APIHelper.getUtilAPI().uploadBitmap(bitmap, new APIHelper.Callback<UploadImageItem>() {
                @Override
                public void onSuccess(final UploadImageItem data) {
                    PhoneRegistrationAvatarActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            PhoneRegistrationAvatarActivity.this.saveAvatar(data.file_url);
                        }
                    });
                }

                @Override
                public void onFail(final int errorCode, final String error) {
                    PhoneRegistrationAvatarActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();
                            handleError(errorCode, error);
                        }
                    });
                }
            });
        } catch (Exception ex) {
            hideLoading();
            gotoCompleted();
        }
    }

    private void saveAvatar(String url) {
        APIHelper.getUserAPI().updateField(userPhone, "user_avatar", url, new APIHelper.Callback<User>() {
            @Override
            public void onSuccess(final User data) {
                PhoneRegistrationAvatarActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        avatarUrl = data.user_avatar;
                        PhoneRegistrationAvatarActivity.this.gotoCompleted();
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                PhoneRegistrationAvatarActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }

    public void gotoCompleted() {
        Intent intent = new Intent(PhoneRegistrationAvatarActivity.this, PhoneRegistrationCompletedActivity.class);
        intent.putExtra("user_phone", userPhone);
        intent.putExtra("user_avatar", avatarUrl == null ? "" : avatarUrl);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mImagePicker.handleActivityResult(resultCode, requestCode, data);
    }
}
