package mobile.agrhub.farmbox.activities.account;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.myhexaville.smartimagepicker.ImagePicker;
import com.myhexaville.smartimagepicker.OnImagePickedListener;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.UserAPI;
import mobile.agrhub.farmbox.service.interfaces.UtilAPI;
import mobile.agrhub.farmbox.service.model.UploadImageItem;
import mobile.agrhub.farmbox.service.model.User;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Utils;

public class EditAccountActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener {
    public static final int REQUEST_AVATAR_PHOTO_CODE = 105;
    private final int PLACE_PICKER_REQUEST = 999;

    private ImagePicker mImagePicker;
    private UtilAPI utilAPI = APIHelper.getUtilAPI();
    private UserAPI userAPI = APIHelper.getUserAPI();
    ImageLoader mImageLoader = ImageLoader.getInstance();

    @BindView(R.id.iv_avatar)
    ImageView mAvatar;
    @BindView(R.id.et_fname)
    EditText firstNameEditText;
    @BindView(R.id.et_lname)
    EditText lastNameEditText;
    @BindView(R.id.et_phone)
    EditText phoneEditText;
    @BindView(R.id.btn_gender)
    Button genderButton;
    @BindView(R.id.btn_address)
    Button addressButton;
    @BindView(R.id.btn_birth)
    Button birthButton;

    @BindView(R.id.btn_set)
    Button setButton;

    Calendar myCalendar = Calendar.getInstance();
    private long mBirthByTM = 0;
    private long mSelectionGender = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_account);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        ButterKnife.bind(this);

        mImagePicker = new ImagePicker(this, null, new OnImagePickedListener() {
            @Override
            public void onImagePicked(Uri imageUri) {
                uploadProfileImage(imageUri);
            }
        })
                .setWithImageCrop(1, 1);

        loadData();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @OnClick(R.id.btn_birth)
    void onBirthButtonClick() {
        new DatePickerDialog(EditAccountActivity.this, R.style.AppTheme_Dialog, EditAccountActivity.this, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    @OnClick(R.id.btn_address)
    void onAddressButtonClick() {
        try {
            requestChangeAddress();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_gender)
    void onGenderButtonClick() {
        requestChangeGender();
    }

    @OnClick(R.id.btn_set)
    void onSetButtonClick() {
        requestSaveData();
    }

    @OnClick(R.id.iv_avatar)
    void onEditAvatarClick() {
//        Intent intent = new Intent(this, PickerActivity.class);
//        intent.putExtra(PickerConfig.SELECT_MODE, PickerConfig.PICKER_IMAGE);//default image and video (Optional)
//        intent.putExtra(PickerConfig.MAX_SELECT_COUNT,1);  //default 40 (Optional)
//        startActivityForResult(intent, REQUEST_AVATAR_PHOTO_CODE);
        mImagePicker.choosePicture(true);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mImagePicker.handleActivityResult(resultCode, requestCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == PLACE_PICKER_REQUEST) {
                Place place = PlacePicker.getPlace(this, data);
                addressButton.setText(place.getAddress());
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mImagePicker.handlePermission(requestCode, grantResults);
    }

    private void requestChangeGender() {
        final Dialog dialog = new Dialog(EditAccountActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_option_gender);

        TextView male = (TextView) dialog.findViewById(R.id.tv_male);
        TextView female = (TextView) dialog.findViewById(R.id.tv_female);
        TextView cancel = (TextView) dialog.findViewById(R.id.tv_cancel);

        male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectionGender = 1;
                genderButton.setText(mSelectionGender == 1 ? R.string.gender_male : R.string.gender_female);
                dialog.dismiss();
            }
        });

        female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectionGender = 0;
                genderButton.setText(mSelectionGender == 1 ? R.string.gender_male : R.string.gender_female);
                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void requestChangeAddress() throws GooglePlayServicesNotAvailableException, GooglePlayServicesRepairableException {

        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        startActivityForResult(builder.build(EditAccountActivity.this), PLACE_PICKER_REQUEST);
    }

    private void loadData() {
        User user = FarmBoxAppController.getInstance().getUser();

        mBirthByTM = user.user_birthday;
        mSelectionGender = user.user_gender;


        firstNameEditText.setText(user.user_first_name);
        lastNameEditText.setText(user.user_last_name);
        phoneEditText.setText(user.user_phone);
        genderButton.setText(user.user_gender == 1 ? R.string.gender_male : R.string.gender_female);
        birthButton.setText(Utils.getDate(user.user_birthday, "dd/MM/yyyy"));
        addressButton.setText(user.user_address);

        if (null != user.user_avatar && !user.user_avatar.isEmpty()) {
            mImageLoader.displayImage(user.user_avatar, mAvatar);
        } else {
            mAvatar.setImageResource(R.drawable.ic_no_avatar_gray_360dp);
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, month);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        this.mBirthByTM = myCalendar.getTimeInMillis();
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);

        birthButton.setText(sdf.format(myCalendar.getTime()));
    }

    private void requestSaveData() {
        String fname = firstNameEditText.getText().toString();
        String lname = lastNameEditText.getText().toString();
        String address = addressButton.getText().toString();

        if (fname == null || fname.trim().isEmpty()) {
            showErrorDialog(getString(R.string.edit_account_missing_fname));
            return;
        }

        if (lname == null || lname.trim().isEmpty()) {
            showErrorDialog(getString(R.string.edit_account_missing_lname));
            return;
        }

        if (address == null || address.trim().isEmpty()) {
            showErrorDialog(getString(R.string.edit_account_missing_address));
            return;
        }

        User user = FarmBoxAppController.getInstance().getUser();
        if (user == null) {
            return;
        }

        showLoading();
        userAPI.updateProfile(user.user_phone, user.user_email, fname, lname, String.valueOf(mBirthByTM), String.valueOf(mSelectionGender), address, new APIHelper.Callback<User>() {
            @Override
            public void onSuccess(final User data) {
                FarmBoxAppController app = FarmBoxAppController.getInstance();
                app.setUser(data, app.getAccessToken());

                EditAccountActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        final AlertDialog dialog = new AlertDialog.Builder(EditAccountActivity.this, R.style.AppTheme_Dialog_Alert)
                                .setTitle(R.string.app_name)
                                .setMessage(R.string.edit_account_success)
                                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        EditAccountActivity.this.finish();
                                    }
                                }).create();
                        if (!dialog.isShowing()) {
                            dialog.show();
                        }
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                EditAccountActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_account_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_change_avatar:
                mImagePicker.choosePicture(true);
                return true;
        }
        return false;
    }

    private void uploadProfileImage(Uri uri) {
        try {
            showLoading();
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
            utilAPI.uploadBitmap(bitmap, new APIHelper.Callback<UploadImageItem>() {
                @Override
                public void onSuccess(final UploadImageItem data) {
                    EditAccountActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            EditAccountActivity.this.onUploadProfileImageSuccess(data.file_url);
                        }
                    });
                }

                @Override
                public void onFail(final int errorCode, final String error) {
                    EditAccountActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();
                            handleError(errorCode, error);
                        }
                    });
                }
            });
        } catch (Exception ex) {
            hideLoading();
            Utils.showFarmBoxNotInitMessage(this, null);
        }
    }

    private void onUploadProfileImageSuccess(String url) {
        if (null == url || url.isEmpty()) {
            hideLoading();
            showErrorDialog(getString(R.string.edit_account_avatar_fail));
        } else {
            try {
                User user = FarmBoxAppController.getInstance().getUser();
                if (user == null) {
                    return;
                }
                userAPI.updateField(user.user_phone, "user_avatar", url, new APIHelper.Callback<User>() {
                    @Override
                    public void onSuccess(final User data) {
                        FarmBoxAppController app = FarmBoxAppController.getInstance();
                        app.setUser(data, app.getAccessToken());

                        EditAccountActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                EditAccountActivity.this.onUpdateProfileImageSuccess(data);
                            }
                        });
                    }

                    @Override
                    public void onFail(final int errorCode, final String error) {
                        EditAccountActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideLoading();
                                handleError(errorCode, error);
                            }
                        });
                    }
                });
            } catch (Exception ex) {
                hideLoading();
                Utils.showFarmBoxNotInitMessage(this, null);
            }
        }
    }

    private void onUpdateProfileImageSuccess(User user) {
        hideLoading();
        if (mAvatar != null) {
            if (null != user.user_avatar && !user.user_avatar.isEmpty()) {
                mImageLoader.displayImage(user.user_avatar, mAvatar);
            } else {
                mAvatar.setImageResource(R.drawable.ic_no_avatar_gray_360dp);
            }
        }
    }
}
