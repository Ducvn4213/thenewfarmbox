package mobile.agrhub.farmbox.activities.certificate;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.adapters.ProductImageAdapter;
import mobile.agrhub.farmbox.service.model.GapCertificate;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Constants;
import mobile.agrhub.farmbox.utils.Utils;
import ss.com.bannerslider.Slider;

public class DetailCertificateActivity extends BaseActivity {
    public static final int REQUEST_UPDATE_FARM_PRODUCT_CODE = 401;

    @BindView(R.id.sld_photo_slider)
    Slider mSliderPhotos;
    private ProductImageAdapter imageAdapter;
    @BindView(R.id.tv_certificate_no)
    TextView mCertificateNo;
    @BindView(R.id.tv_certificate_issuer)
    TextView mCertificateIssuer;
    @BindView(R.id.tv_certificate_owner)
    TextView mCertificateOwner;
    @BindView(R.id.tv_certificate_address)
    TextView mCertificateAddress;
    @BindView(R.id.tv_certificate_product)
    TextView mCertificateProduct;
    @BindView(R.id.tv_certificate_valid_date)
    TextView mCertificateValidDate;
    @BindView(R.id.tv_certificate_expired_date)
    TextView mCertificateExpiredDate;
    @BindView(R.id.tv_certificate_update_date)
    TextView mCertificateUpdateDate;

    private GapCertificate certificate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_certificate);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        String productStr = intent.getStringExtra(Constants.GAP_CERTIFICATE_INFO_KEY);
        if (productStr == null || productStr.isEmpty()) {
            finish();
        } else {
            Gson gson = new Gson();
            certificate = gson.fromJson(productStr, GapCertificate.class);
        }

        if (certificate == null) {
            finish();
        }

        refreshView();
    }

    private void configActionBar() {
        getSupportActionBar().setTitle(certificate.gapCertificateName);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void refreshView() {
        configActionBar();

        imageAdapter = new ProductImageAdapter(certificate.gapCertificateMedia);
        mSliderPhotos.setAdapter(imageAdapter);

        mCertificateNo.setText(certificate.gapCertificateNo);
        mCertificateIssuer.setText(certificate.gapCertificateIssuer);
        mCertificateOwner.setText(certificate.gapCertificateOwner);
        mCertificateAddress.setText(certificate.gapCertificateAddress);
        mCertificateProduct.setText(certificate.gapCertificateProduct);
        mCertificateValidDate.setText(Utils.getDate(certificate.gapCertificateValidDate, "yyyy/MM/dd"));
        mCertificateExpiredDate.setText(Utils.getDate(certificate.gapCertificateExpiredDate, "yyyy/MM/dd"));
        mCertificateUpdateDate.setText(Utils.getDate(certificate.gapCertificateUpdatedDate, "yyyy/MM/dd"));
    }

}
