package mobile.agrhub.farmbox.activities.farm.product;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.dmcbig.mediapicker.PickerConfig;
import com.dmcbig.mediapicker.entity.Media;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.custom_view.cell.InputAreaCellView;
import mobile.agrhub.farmbox.custom_view.cell.InputCellView;
import mobile.agrhub.farmbox.custom_view.cell.InputPhotosCellView;
import mobile.agrhub.farmbox.custom_view.cell.InputWithUnitCellView;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.model.FarmProduct;
import mobile.agrhub.farmbox.service.model.KeyValue;
import mobile.agrhub.farmbox.service.model.Photo;
import mobile.agrhub.farmbox.service.model.UploadImageItem;
import mobile.agrhub.farmbox.service.network.Network;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Constants;
import mobile.agrhub.farmbox.utils.Utils;

public class AddFarmProductActivity extends BaseActivity implements InputPhotosCellView.InputPhotosCellListener {
    @BindView(R.id.ic_product_name)
    InputCellView mName;
    @BindView(R.id.ic_product_weight)
    InputWithUnitCellView mWeight;
    @BindView(R.id.ic_product_price)
    InputCellView mPrice;
    @BindView(R.id.ic_product_discount)
    InputCellView mDiscount;
    @BindView(R.id.ic_product_description)
    InputAreaCellView mDescription;
    @BindView(R.id.ic_product_photos)
    InputPhotosCellView mPhotos;

    private FarmProduct product;
    private List<String> mediaUrlList;
    private int photoIdx;

    private List<KeyValue> weightUnitData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_farm_product);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        String productStr = intent.getStringExtra(Constants.FARM_PRODUCT_INFO_KEY);
        if (productStr != null && !productStr.isEmpty()) {
            Gson gson = new Gson();
            product = gson.fromJson(productStr, FarmProduct.class);
        }

        configActionBar();

        weightUnitData = new ArrayList<>();
        KeyValue item = new KeyValue();
        item.id = FarmProduct.UNIT_KG;
        item.value = getString(R.string.unit_kg);
        weightUnitData.add(item);

        item = new KeyValue();
        item.id = FarmProduct.UNIT_HEAD;
        item.value = getString(R.string.unit_head);
        weightUnitData.add(item);

        item = new KeyValue();
        item.id = FarmProduct.UNIT_FRUIT;
        item.value = getString(R.string.unit_fruit);
        weightUnitData.add(item);

        item = new KeyValue();
        item.id = FarmProduct.UNIT_PIECE;
        item.value = getString(R.string.unit_piece);
        weightUnitData.add(item);


        mName.setTitle(R.string.farm_product_title);
        if (product == null) {
            mName.setValue("", getString(R.string.farm_product_title));
        } else {
            mName.setValue(product.productName, getString(R.string.farm_product_title));
        }

        mWeight.setTitle(R.string.farm_product_weight);
        if (product == null) {
            mWeight.setValue("", getString(R.string.farm_product_weight));
            mWeight.setUnitData(weightUnitData, FarmProduct.UNIT_KG);

            mWeight.setEnabled(true);
        } else {
            mWeight.setValue(product.productWeight, getString(R.string.farm_product_weight));
            mWeight.setUnitData(weightUnitData, product.productUnit);

            if (product.productId <= 0) {
                mWeight.setEnabled(true);
            } else {
                mWeight.setEnabled(false);
            }
        }

        mPrice.setTitle(R.string.farm_product_price);
        mPrice.setInputType(InputType.TYPE_CLASS_NUMBER);
        if (product == null) {
            mPrice.setValue("", getString(R.string.farm_product_price));
        } else {
            int price = 0;
            try {
                price = Utils.str2Int(product.productPrice);
            } catch (Exception e) {
                e.printStackTrace();
            }
            mPrice.setValue(price + "", getString(R.string.farm_product_price));
        }

        mDiscount.setTitle(R.string.farm_product_discount);
        mDiscount.setInputType(InputType.TYPE_CLASS_NUMBER);
        if (product == null) {
            mDiscount.setValue("", getString(R.string.farm_product_discount));
        } else {
            int discount = 0;
            try {
                discount = Utils.str2Int(product.productDiscount);
            } catch (Exception e) {
                e.printStackTrace();
            }
            mDiscount.setValue(discount + "", getString(R.string.farm_product_discount));
        }

        mDescription.setTitle(R.string.farm_product_description);
        if (product == null) {
            mDescription.setValue("", getString(R.string.farm_product_description));
        } else {
            mDescription.setValue(product.productDescription, getString(R.string.farm_product_description));
        }

        mPhotos.setTitle(R.string.farm_product_photos);
        if (product != null) {
            String[] avatarArr = product.getProductAvatar();
            if (avatarArr != null && avatarArr.length > 0) {
                Photo photo;
                for (String photoUrl : avatarArr) {
                    photo = new Photo(photoUrl);
                    mPhotos.addPhoto(photo, false);
                }
            }
        }
        mPhotos.setListener(this);
    }

    private void configActionBar() {
        if (product == null) {
            getSupportActionBar().setTitle(R.string.product_add_product);
        } else if (product.productId <= 0) {
            getSupportActionBar().setTitle(R.string.product_copy_product);
        } else {
            getSupportActionBar().setTitle(R.string.product_edit_product);
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.save_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.action_save) {
            if (validate()) {
                save();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == InputPhotosCellView.REQUEST_CHOOSE_PHOTO_CODE && resultCode == PickerConfig.RESULT_CODE) {
            List<Media> mediaList = data.getParcelableArrayListExtra(PickerConfig.EXTRA_RESULT);
            mPhotos.addMediaList(mediaList);
        }
    }

    private boolean validate() {
        if (mName.isEmpty()) {
            showErrorDialog(getString(R.string.field_is_required, mName.getTitle()));
            return false;
        }

        if (mWeight.isEmpty()) {
            showErrorDialog(getString(R.string.field_is_required, mWeight.getTitle()));
            return false;
        }

        if (mWeight.getFloatVal() <= 0f) {
            showErrorDialog(getString(R.string.field_large_field, mWeight.getTitle(), "0"));
            return false;
        }

        if (mWeight.isUnitEmpty()) {
            showErrorDialog(getString(R.string.unit_of_field_is_required, mWeight.getTitle()));
            return false;
        }

        if (mPrice.isEmpty()) {
            showErrorDialog(getString(R.string.field_is_required, mPrice.getTitle()));
            return false;
        }

        if (mPrice.getLongVal() <= 0) {
            showErrorDialog(getString(R.string.field_large_field, mPrice.getTitle(), "0"));
            return false;
        }

        if (!mDiscount.isEmpty() && (mDiscount.getIntVal() < 0 || mDiscount.getIntVal() > 100)) {
            showErrorDialog(getString(R.string.field_between, mDiscount.getTitle(), "0", "100"));
            return false;
        }

        return true;
    }

    private void save() {
        showLoading();

        photoIdx = -1;
        if (mediaUrlList == null) {
            mediaUrlList = new ArrayList<>();
        } else {
            mediaUrlList.clear();
        }

        uploadMedia();
    }

    private void uploadMedia() {
        photoIdx += 1;
        if (photoIdx < mPhotos.size()) {
            Photo photo = mPhotos.get(photoIdx);
            if (photo != null) {
                if (photo.isOnServer) {
                    mediaUrlList.add(photo.uri);

                    uploadMedia();
                } else {
                    APIHelper.getUtilAPI().uploadFile(photo.media, new APIHelper.Callback<UploadImageItem>() {
                        @Override
                        public void onSuccess(UploadImageItem data) {
                            mediaUrlList.add(data.file_url);

                            uploadMedia();
                        }

                        @Override
                        public void onFail(int errorCode, String error) {
                            if (errorCode == Network.DEMO_MODE_ERROR) {
                                AddFarmProductActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Utils.showDialog(AddFarmProductActivity.this, getString(R.string.demo_mode), getString(R.string.demo_mode_message));
                                    }
                                });
                            } else {
                                uploadMedia();
                            }
                        }
                    });
                }
            }
        } else {
            saveProduct();
        }
    }

    private void saveProduct() {
        if (product != null && product.productId > 0) {
            APIHelper.getSaleAPI().updateProduct(product.productId, mName.getVal(), mediaUrlList, mPrice.getLongVal(), mDiscount.getIntVal(), mDescription.getVal(), "", new APIHelper.Callback<FarmProduct>() {
                @Override
                public void onSuccess(FarmProduct data) {
                    AddFarmProductActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();
                            AddFarmProductActivity.this.finish();
                        }
                    });
                }

                @Override
                public void onFail(final int errorCode, final String error) {
                    AddFarmProductActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();
                            handleError(errorCode, error);
                        }
                    });
                }
            });
        } else {
            APIHelper.getSaleAPI().createProduct(mName.getVal(), mediaUrlList, mWeight.getFloatVal(), mWeight.getUnitVal(), mPrice.getLongVal(), mDiscount.getIntVal(), mDescription.getVal(), "", new APIHelper.Callback<FarmProduct>() {
                @Override
                public void onSuccess(final FarmProduct data) {
                    AddFarmProductActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();

                            Gson gson = new Gson();
                            String productStr = gson.toJson(data);

                            // Prepare data intent
                            Intent intent = new Intent();
                            intent.putExtra(Constants.FARM_PRODUCT_INFO_KEY, productStr);

                            // Activity finished ok, return the data
                            AddFarmProductActivity.this.setResult(Activity.RESULT_OK, intent);
                            AddFarmProductActivity.this.finish();
                        }
                    });
                }

                @Override
                public void onFail(final int errorCode, final String error) {
                    AddFarmProductActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();
                            handleError(errorCode, error);
                        }
                    });
                }
            });
        }
    }



}
