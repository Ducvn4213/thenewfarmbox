package mobile.agrhub.farmbox.activities.dairy;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.dmcbig.mediapicker.PickerActivity;
import com.dmcbig.mediapicker.PickerConfig;
import com.dmcbig.mediapicker.entity.Media;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.volokh.danylo.hashtaghelper.HashTagHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.adapters.DiaryPhotoGridAdapter;
import mobile.agrhub.farmbox.custom_view.layout.SquareGridView;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.GapAPI;
import mobile.agrhub.farmbox.service.interfaces.UtilAPI;
import mobile.agrhub.farmbox.service.model.FarmBasic;
import mobile.agrhub.farmbox.service.model.KeyValue;
import mobile.agrhub.farmbox.service.model.UploadImageItem;
import mobile.agrhub.farmbox.service.model.User;
import mobile.agrhub.farmbox.service.network.Network;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Utils;

public class AddDiaryActivity extends BaseActivity {
    public static final int REQUEST_ACTIVITY_TYPE_CODE = 101;
    public static final int REQUEST_FARM_CODE = 102;
    public static final int REQUEST_DIARY_PHOTO_CODE = 103;
    public static final String DIARY_DATE_FORMAT = "HH:mm a dd/MM/yyyy";

    ImageView mAvatar;
    TextView mAccountName;
    HashTagHelper mEditTextHashTagHelper;
    EditText mContent;
    TextView mActivityType;
    TextView mActivityDate;
    TextView mActivityLocation;
    TextView mAddDiaryPhotos;
    SquareGridView mDiaryPhotos;
    DiaryPhotoGridAdapter mDiaryPhotoAdapter;

    List<KeyValue> mSelectedActivity;
    Calendar mDiaryCalendar;
    FarmBasic mSelectedFarm;
    List<Media> mSelectedImageList;

    int mCurrentUpdatePhotoIndex;
    List<String> mSelectedMedias;
    Map<Integer, Integer> countRetry;
    ImageLoader mImageLoader = ImageLoader.getInstance();

    UtilAPI utilAPI = APIHelper.getUtilAPI();
    GapAPI gapAPI = APIHelper.getGapAPI();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_diary);

        bindingControls();
        setupControlEvents();

        initData();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.add_diary_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_send:
                send();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void bindingControls() {
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mAvatar = (ImageView) findViewById(R.id.iv_avatar);
        mAccountName = (TextView) findViewById(R.id.tv_name);
        mContent = (EditText) findViewById(R.id.et_content);
        mActivityType = (TextView) findViewById(R.id.tv_activity_type);
        mActivityDate = (TextView) findViewById(R.id.tv_activity_date);
        mActivityLocation = (TextView) findViewById(R.id.tv_activity_location);
        mAddDiaryPhotos = (TextView) findViewById(R.id.tv_add_photo);
        mDiaryPhotos = (SquareGridView) findViewById(R.id.gv_diary_photos);

        // Here we don't specify additionalSymbols. It means that in EditText only letters and digits will be valid symbols
        mEditTextHashTagHelper = HashTagHelper.Creator.create(getResources().getColor(R.color.colorPrimary), null);
        mEditTextHashTagHelper.handle(mContent);
    }

    private void initData() {
        FarmBoxAppController app = FarmBoxAppController.getInstance();
        User user = app.getUser();
        mDiaryCalendar = Calendar.getInstance();

        // account name via post
        String accountNameHTML = getString(R.string.post_diary_via_name, (user.user_first_name + " " + user.user_last_name));
        mAccountName.setText(Html.fromHtml(accountNameHTML));

        // avatar
        if (null != user.user_avatar && !user.user_avatar.isEmpty()) {
            mImageLoader.displayImage(user.user_avatar, mAvatar);
        } else {
            mAvatar.setImageResource(R.drawable.ic_no_avatar_gray_360dp);
        }

        // activity type
        if (mSelectedActivity == null) {
            mSelectedActivity = new ArrayList<KeyValue>();
        } else {
            mSelectedActivity.clear();
        }
        mActivityType.setText(R.string.add_activity_type);

        // activity date
        mActivityDate.setText(Utils.getDate(mDiaryCalendar.getTimeInMillis(), DIARY_DATE_FORMAT));

        // activity farm
        if (app.getFarmList().size() == 1) {
            mSelectedFarm = app.getFarmList().get(0);
        }
        if (mSelectedFarm != null) {
            String atFarmNameHtml = getString(R.string.post_diary_at_farm, mSelectedFarm.farm_name);
            mActivityLocation.setText(Html.fromHtml(atFarmNameHtml));
        } else {
            mActivityLocation.setText(R.string.add_diary_location);
        }

        // diary photos
        mSelectedImageList = new ArrayList<Media>();
        mDiaryPhotoAdapter = new DiaryPhotoGridAdapter(this, mSelectedImageList);
        mDiaryPhotos.setAdapter(mDiaryPhotoAdapter);
    }

    private void setupControlEvents() {
        mActivityType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StringBuilder data = new StringBuilder();
                if (mSelectedActivity != null && !mSelectedActivity.isEmpty()) {
                    for (KeyValue item : mSelectedActivity) {
                        if (data.length() > 0) {
                            data.append(";");
                        }
                        data.append(item.id);
                    }
                }

                Intent intent = new Intent(AddDiaryActivity.this, ChooseDiaryActivityTypeActivity.class);

                Bundle bundle = new Bundle();
                bundle.putString("SELECTED_ACTIVITY_TYPE_LIST", data.toString());

                intent.putExtras(bundle); //Put your id to your next Intent
                startActivityForResult(intent, REQUEST_ACTIVITY_TYPE_CODE);
            }
        });

        mActivityDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(AddDiaryActivity.this, R.style.AppTheme_Dialog, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        mDiaryCalendar.set(year, monthOfYear, dayOfMonth);
                        new TimePickerDialog(AddDiaryActivity.this, R.style.AppTheme_Dialog, new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                mDiaryCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                mDiaryCalendar.set(Calendar.MINUTE, minute);

                                mActivityDate.setText(Utils.getDate(mDiaryCalendar.getTimeInMillis(), DIARY_DATE_FORMAT));
                            }
                        }, mDiaryCalendar.get(Calendar.HOUR_OF_DAY), mDiaryCalendar.get(Calendar.MINUTE), false).show();
                    }
                }, mDiaryCalendar.get(Calendar.YEAR), mDiaryCalendar.get(Calendar.MONTH), mDiaryCalendar.get(Calendar.DATE));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });

        mActivityLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AddDiaryActivity.this, ChooseFarmActivity.class);
                AddDiaryActivity.this.startActivityForResult(intent, REQUEST_FARM_CODE);
            }
        });

        mAddDiaryPhotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(Intent.ACTION_PICK);
//                intent.setType("image/* video/*");
////                Intent intent = new Intent();
////                intent.setType("image/*");
//                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
//                intent.setAction(Intent.ACTION_GET_CONTENT);
//                startActivityForResult(Intent.createChooser(intent, getString(R.string.add_diary_choose_photos)), REQUEST_DIARY_PHOTO_CODE);
                Intent intent = new Intent(AddDiaryActivity.this, PickerActivity.class);
//                intent.putExtra(PickerConfig.SELECT_MODE, PickerConfig.PICKER_IMAGE_VIDEO);//default image and video (Optional)
                intent.putExtra(PickerConfig.SELECT_MODE, PickerConfig.PICKER_IMAGE);//default image and video (Optional)
                long maxSize = 188743680L;//long long long
                intent.putExtra(PickerConfig.MAX_SELECT_SIZE, maxSize); //default 180MB (Optional)
                intent.putExtra(PickerConfig.MAX_SELECT_COUNT, 10);  //default 40 (Optional)
//                intent.putExtra(PickerConfig.DEFAULT_SELECTED_LIST, mSelectedImageList); // (Optional)
                AddDiaryActivity.this.startActivityForResult(intent, REQUEST_DIARY_PHOTO_CODE);
            }
        });
    }

    private void send() {
        String content = mContent.getText().toString().trim();
        if (content.isEmpty()) {
            Toast.makeText(AddDiaryActivity.this, R.string.add_diary_enter_content_message, Toast.LENGTH_SHORT).show();
            return;
        }
        if (mSelectedActivity.isEmpty()) {
            Toast.makeText(AddDiaryActivity.this, R.string.add_diary_choose_activity_message, Toast.LENGTH_SHORT).show();
            return;
        }
        if (mSelectedFarm == null) {
            Toast.makeText(AddDiaryActivity.this, R.string.add_diary_choose_farm_message, Toast.LENGTH_SHORT).show();
            return;
        }

        AddDiaryActivity.this.showLoading();
        mCurrentUpdatePhotoIndex = 0;
        if (mSelectedMedias == null) {
            mSelectedMedias = new ArrayList<>();
        } else {
            mSelectedMedias.clear();
        }

        doUploadMedia();
    }

    private void addImageUrl(Media url) {
        for (Media imageUrl : mSelectedImageList) {
            if (url.id == imageUrl.id) {
                return;
            }
        }

        mSelectedImageList.add(url);
    }

    private void doUploadMedia() {
        if (mCurrentUpdatePhotoIndex < mSelectedImageList.size()) {
            Media media = mSelectedImageList.get(mCurrentUpdatePhotoIndex);
            utilAPI.uploadFile(media, new APIHelper.Callback<UploadImageItem>() {
                @Override
                public void onSuccess(UploadImageItem data) {
                    mSelectedMedias.add(data.file_url);

                    mCurrentUpdatePhotoIndex++;
                    AddDiaryActivity.this.doUploadMedia();
                }

                @Override
                public void onFail(int errorCode, String error) {
                    if (errorCode == Network.DEMO_MODE_ERROR) {
                        AddDiaryActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Utils.showDialog(AddDiaryActivity.this, getString(R.string.demo_mode), getString(R.string.demo_mode_message));
                            }
                        });
                    } else {
                        if (countRetry == null) countRetry = new HashMap<>();

                        boolean needTry = true;
                        Integer key = Integer.valueOf(mCurrentUpdatePhotoIndex);
                        if (countRetry.containsKey(key)) {
                            Integer val = countRetry.get(key);
                            val += 1;
                            countRetry.put(key, val);

                            if (val >= 3) {
                                needTry = false;

                                AddDiaryActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        AddDiaryActivity.this.hideLoading();
                                        showErrorDialog(AddDiaryActivity.this.getString(R.string.cannot_post_diary_on_this_thime));
                                    }
                                });
                            }
                        } else {
                            countRetry.put(key, 1);
                        }

                        if (needTry) {
                            AddDiaryActivity.this.doUploadMedia();
                        }
                    }
                }
            });
        } else {
            doPostDiary();
        }
    }

    private void doPostDiary() {
        String content = mContent.getText().toString().trim();
        String[] mSelectedActivities = new String[mSelectedActivity.size()];
        for (int i = 0; i < mSelectedActivity.size(); i++) {
            mSelectedActivities[i] = String.valueOf(mSelectedActivity.get(i).id);
        }
        String[] mSelectedMediaArray = mSelectedMedias.toArray(new String[0]);

        List<String> tagList = mEditTextHashTagHelper.getAllHashTags();
        String[] tags = tagList.toArray(new String[0]);

        String diaryDateStr = String.valueOf(mDiaryCalendar.getTimeInMillis());

        gapAPI.postDiary(mSelectedFarm.farm_id, content, tags, mSelectedActivities, mSelectedMediaArray, diaryDateStr, new APIHelper.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                AddDiaryActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AddDiaryActivity.this.hideLoading();
                        AddDiaryActivity.this.finish();
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                AddDiaryActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_DIARY_PHOTO_CODE && resultCode == PickerConfig.RESULT_CODE) {
            List<Media> mediaList = data.getParcelableArrayListExtra(PickerConfig.EXTRA_RESULT);
            for (Media media : mediaList) {
                addImageUrl(media);
            }
            mDiaryPhotoAdapter.updateData(mSelectedImageList);
        } else if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_ACTIVITY_TYPE_CODE) {
                if (data.hasExtra("SELECTED_VALUE")) {
                    String selectedData = data.getExtras().getString("SELECTED_VALUE");

                    Gson gson = new Gson();
                    mSelectedActivity = gson.fromJson(selectedData, new TypeToken<List<KeyValue>>() {
                    }.getType());

                    String displayActivities = "";
                    for (KeyValue obj : mSelectedActivity) {
                        if (!displayActivities.isEmpty()) {
                            displayActivities += ", ";
                        }
                        displayActivities += obj.value;
                    }
                    if (displayActivities.isEmpty()) {
                        mActivityType.setText(R.string.add_activity_type);
                    } else {
                        mActivityType.setText(displayActivities);
                    }
                }
            } else if (requestCode == REQUEST_FARM_CODE) {
                if (data.hasExtra("SELECTED_VALUE")) {
                    long farmId = data.getExtras().getLong("SELECTED_VALUE", -1);

                    FarmBoxAppController app = FarmBoxAppController.getInstance();
                    if (app.getFarmList().size() > 0) {
                        for (FarmBasic farmItem : app.getFarmList()) {
                            if (farmItem.farm_id == farmId) {
                                mSelectedFarm = farmItem;
                                break;
                            }
                        }
                    }
                    if (mSelectedFarm != null) {
                        String atFarmNameHtml = getString(R.string.post_diary_at_farm, mSelectedFarm.farm_name);
                        mActivityLocation.setText(Html.fromHtml(atFarmNameHtml));
                    } else {
                        mActivityLocation.setText(R.string.add_diary_location);
                    }
                }
            }
        }
    }
}
