package mobile.agrhub.farmbox.activities.store;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.adapters.ProductImageAdapter;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.model.Product;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Utils;
import ss.com.bannerslider.Slider;

public class ProductInfoActivity extends BaseActivity {
    private Product productInfo;
    private Slider productImageSlider;
    private ProductImageAdapter imageAdapter;

    private TextView productName;
    private LinearLayout originPriceView;
    private TextView originPrice;
    private TextView discountPercent;
    private TextView discountPrice;
    private ImageButton amountMinusBtn;
    private TextView amount;
    private ImageButton amountPlusBtn;
    private ImageButton addToCartBtn;
    private TextView productDescription;

    private FrameLayout totalCartItemBadge;
    private TextView totalCartItem;

    private int currentAmount;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_info);

        bindingControls();
        initData();
        setupControlEvents();
    }

    @Override
    protected void onResume() {
        super.onResume();

        invalidateOptionsMenu();
    }

    private void bindingControls() {
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        productImageSlider = (Slider) findViewById(R.id.product_image_slider);

        productName = (TextView) findViewById(R.id.tv_product_name);
        originPriceView = (LinearLayout) findViewById(R.id.ll_origin_price);
        originPrice = (TextView) findViewById(R.id.tv_origin_price);
        discountPercent = (TextView) findViewById(R.id.tv_discount_percent);
        discountPrice = (TextView) findViewById(R.id.tv_discount_price);
        amountMinusBtn = (ImageButton) findViewById(R.id.ib_amount_minus);
        amount = (TextView) findViewById(R.id.tv_amount);
        amountPlusBtn = (ImageButton) findViewById(R.id.ib_amount_plus);
        addToCartBtn = (ImageButton) findViewById(R.id.ib_add_to_cart);
        productDescription = (TextView) findViewById(R.id.tv_product_description);
    }

    private void initData() {
        this.currentAmount = 1;

        Bundle bundle = getIntent().getExtras();
        if (bundle.containsKey("SELECTED_PRODUCT_INFO")) {
            Gson gson = new Gson();

            String productJsonInfo = bundle.getString("SELECTED_PRODUCT_INFO");
            productInfo = gson.fromJson(productJsonInfo, Product.class);

            setTitle(Utils.getValueFromMultiLanguageString(productInfo.category.categoryName));

            imageAdapter = new ProductImageAdapter(productInfo.productAvatar);
            productImageSlider.setAdapter(imageAdapter);

            productName.setText(Utils.getValueFromMultiLanguageString(productInfo.productName));

            long originPriceVal = Utils.getLongFromMultiLanguageString(productInfo.productPrice);
            long discountVal = Utils.getLongFromMultiLanguageString(productInfo.productDiscount);
            if (discountVal > 0) {
                originPriceView.setVisibility(View.VISIBLE);
                originPrice.setText(Utils.formatPrice(originPriceVal));
                originPrice.setPaintFlags(originPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                discountPercent.setText("↓" + discountVal + "%");

                long currentPrice = originPriceVal - Math.round((originPriceVal * discountVal) / 100.0);
                discountPrice.setText(Utils.formatPrice(currentPrice));
            } else {
                originPriceView.setVisibility(View.GONE);
                discountPercent.setVisibility(View.GONE);

                discountPrice.setText(Utils.formatPrice(originPriceVal));
            }

            amount.setText("" + currentAmount);

            String description = Utils.getValueFromMultiLanguageString(productInfo.productDescription);
            if (Build.VERSION.SDK_INT >= 24) {
                description = Html.fromHtml(description, Html.FROM_HTML_MODE_LEGACY).toString();
                productDescription.setText(Html.fromHtml(description, Html.FROM_HTML_MODE_LEGACY));
            } else {
                description = Html.fromHtml(description).toString();
                productDescription.setText(Html.fromHtml(description));
            }
        }
    }

    private void setupControlEvents() {
        amountMinusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentAmount > 1) {
                    currentAmount--;
                }
                amount.setText("" + currentAmount);
            }
        });

        amountPlusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentAmount++;
                amount.setText("" + currentAmount);
            }
        });

        addToCartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FarmBoxAppController app = FarmBoxAppController.getInstance();
                app.addProduct(productInfo, currentAmount);

                updateAlertIcon();

                Toast.makeText(ProductInfoActivity.this, R.string.added_product_to_cart, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateAlertIcon() {
        FarmBoxAppController app = FarmBoxAppController.getInstance();
        int numberOfCartItem = app.getTotalAmount();
        if (0 < numberOfCartItem) {
            totalCartItem.setText(String.valueOf(numberOfCartItem));
        } else {
            totalCartItem.setText("");
        }

        totalCartItemBadge.setVisibility((numberOfCartItem > 0) ? View.VISIBLE : View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.product_info_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        final MenuItem cartMenuItem = menu.findItem(R.id.action_cart);
        FrameLayout rootView = (FrameLayout) cartMenuItem.getActionView();

        totalCartItemBadge = (FrameLayout) rootView.findViewById(R.id.view_alert_red_circle);
        totalCartItem = (TextView) rootView.findViewById(R.id.view_alert_count_text_view);

        updateAlertIcon();

        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FarmBoxAppController app = FarmBoxAppController.getInstance();
                if (app.getTotalAmount() > 0) {
                    Intent intent = new Intent(ProductInfoActivity.this, CartActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(ProductInfoActivity.this, R.string.please_add_product_to_cart, Toast.LENGTH_SHORT).show();
                }
            }
        });

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_cart:
                FarmBoxAppController app = FarmBoxAppController.getInstance();
                if (app.getTotalAmount() > 0) {
                    Intent intent = new Intent(ProductInfoActivity.this, CartActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(ProductInfoActivity.this, R.string.please_add_product_to_cart, Toast.LENGTH_SHORT).show();
                }

                return true;
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
