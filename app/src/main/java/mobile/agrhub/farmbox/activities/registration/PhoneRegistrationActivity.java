package mobile.agrhub.farmbox.activities.registration;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.iid.FirebaseInstanceId;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.activities.DemoFarmActivity;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.UtilAPI;
import mobile.agrhub.farmbox.service.model.Nation;
import mobile.agrhub.farmbox.service.model.response.NationResponse;
import mobile.agrhub.farmbox.utils.BaseActivity;

public class PhoneRegistrationActivity extends BaseActivity {
    Button mContinue;
    EditText mPhone;
    SearchableSpinner mZone;
    Button mDemoFarm;

    UtilAPI utilAPI = APIHelper.getUtilAPI();

    List<Nation> nationList;
    List<String> nationListValue;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.phone_registration_activity);

        bindingControl();
        setupControlEvents();
        loadData();
    }

    public void bindingControl() {
        mContinue = (Button) findViewById(R.id.btn_continue);
        mPhone = (EditText) findViewById(R.id.et_phone);
        mZone = (SearchableSpinner) findViewById(R.id.spn_zone);
        mZone.setTitle(getString(R.string.choose_country));
        mZone.setPositiveButton("OK");

        mDemoFarm = (Button) findViewById(R.id.btn_demo_farm);
    }

    public void setupControlEvents() {
        mContinue.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                PhoneRegistrationActivity.this.sendCode();
            }
        });

        mDemoFarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhoneRegistrationActivity.this.gotoDemoFarmScreen();
            }
        });
    }

    public void loadData() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        if (refreshedToken != null && !refreshedToken.isEmpty()) {
            UtilAPI utilAPI = APIHelper.getUtilAPI();
            utilAPI.unsubscribeNotification(refreshedToken, new APIHelper.Callback<String>() {
                @Override
                public void onSuccess(String data) {
                }

                @Override
                public void onFail(int errorCode, String error) {
                }
            });
        }

        showLoading();
        utilAPI.getNations(new APIHelper.Callback<NationResponse>() {
            @Override
            public void onSuccess(final NationResponse data) {
                PhoneRegistrationActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        setupZoneData(data.data);
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                PhoneRegistrationActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                        mContinue.setEnabled(false);
                    }
                });
            }
        });
    }

    void setupZoneData(List<Nation> data) {
        nationList = data;

        int initSelectItem = 0;
        nationListValue = new ArrayList<>();
        if (null != data) {
            Nation nation = null;
            for (int i = 0; i < data.size(); i++) {
                nation = data.get(i);
                if (null != nation.nation_code && "vn".equals(nation.nation_code.toLowerCase())) {
                    initSelectItem = i;
                }
                nationListValue.add("(" + nation.phone_code + ") " + nation.nation_name);
            }
        }

        ArrayAdapter<String> pm_adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.layout_spinner_item,
                R.id.tv_item_text, nationListValue);
//        pm_adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item);
        mZone.setAdapter(pm_adapter);
        mZone.setSelection(initSelectItem);
    }

    public void sendCode() {
        String phone = mPhone.getText().toString();
        if (phone.isEmpty()) {
            String errorString = getString(R.string.phone_registration_error_empty_phone);
            showErrorDialog(errorString);
            return;
        }

        phone = correctPhoneNumber();

        showLoading();
        utilAPI.getPasscode(phone, new APIHelper.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                PhoneRegistrationActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        gotoConfirmScreen();
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                PhoneRegistrationActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }

    public void gotoConfirmScreen() {
        Intent intent = new Intent(PhoneRegistrationActivity.this, PhoneRegistrationConfirmActivity.class);
        intent.putExtra("phone", correctPhoneNumber());
        startActivity(intent);
        finish();
    }

    public String correctPhoneNumber() {
        String phone = mPhone.getText().toString();
        phone = phone.trim();

        if (phone.startsWith("0")) {
            phone = phone.substring(1);
        }

        int zoneIndex = mZone.getSelectedItemPosition();

        phone = nationList.get(zoneIndex).phone_code + phone;

        return phone;
    }

    public void gotoDemoFarmScreen() {
        Intent intent = new Intent(this, DemoFarmActivity.class);
        startActivity(intent);
        finish();
    }
}
