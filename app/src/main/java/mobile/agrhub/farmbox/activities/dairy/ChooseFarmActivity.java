package mobile.agrhub.farmbox.activities.dairy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.model.FarmBasic;
import mobile.agrhub.farmbox.utils.BaseActivity;

public class ChooseFarmActivity extends BaseActivity {

    ListView mListView;
    List<String> farms;
    FarmBoxAppController app;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_farm);

        bindingControls();
        initData();
        setupControlEvents();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void bindingControls() {
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mListView = (ListView) findViewById(R.id.listView);
    }

    private void initData() {
        app = FarmBoxAppController.getInstance();

        farms = new ArrayList<String>();
        if (app.getFarmList().size() > 0) {
            for (FarmBasic farmItem : app.getFarmList()) {
                farms.add(farmItem.farm_name);
            }
        }
        mListView.setAdapter(new ArrayAdapter(this, android.R.layout.simple_list_item_1, farms.toArray()));
    }

    private void setupControlEvents() {
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i >= app.getFarmList().size()) {
                    ChooseFarmActivity.this.setResult(Activity.RESULT_CANCELED);
                } else {
                    FarmBasic selectedFarm = app.getFarmList().get(i);
                    // Prepare data intent
                    Intent data = new Intent();
                    data.putExtra("SELECTED_VALUE", selectedFarm.farm_id);
                    data.putExtra("SELECTED_NAME", selectedFarm.farm_name);
                    // Activity finished ok, return the data
                    ChooseFarmActivity.this.setResult(Activity.RESULT_OK, data);
                }
                ChooseFarmActivity.this.finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return false;
    }
}
