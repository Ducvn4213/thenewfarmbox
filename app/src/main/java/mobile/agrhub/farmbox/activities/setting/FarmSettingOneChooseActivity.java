package mobile.agrhub.farmbox.activities.setting;

import android.support.v7.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.adapters.FarmSettingOneChooseItemAdapter;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.model.FarmSettingDetailData;
import mobile.agrhub.farmbox.service.network.Param;

public class FarmSettingOneChooseActivity extends BaseFarmSettingItemActivity {

    @BindView(R.id.rv_options)
    RecyclerView mOptions;
    FarmSettingOneChooseItemAdapter mAdapter;

    @Override
    protected int contentView() {
        return R.layout.activity_farm_setting_one_choose;
    }

    @Override
    protected void initView() {
        super.initView();

        String selectedVal = "";
        if (item.getData() != null && item.getData().size() > 0) {
            for (FarmSettingDetailData option : item.getData()) {
                if (item.getValue() != null && item.getValue().equals(option.getName())) {
                    selectedVal = option.getValue();
                    break;
                }
            }
        }

        mAdapter = new FarmSettingOneChooseItemAdapter(this, item.getData(), selectedVal);
        mOptions.setAdapter(mAdapter);
    }

    @Override
    protected List<Param> buildParam(List<Param> params) {
        params.add(new Param(item.getField(), mAdapter.getSelectedVal()));
        return params;
    }

    @Override
    protected void applyChange() {
        if ("farm_type".equals(item.getField())) {
            farmAPI.editFarmField(String.valueOf(this.farmId), "farm_type", mAdapter.getSelectedVal(), new APIHelper.Callback<String>() {
                @Override
                public void onSuccess(String data) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();
                            FarmSettingOneChooseActivity.this.closeAndReloadSetting();
                        }
                    });
                }

                @Override
                public void onFail(final int errorCode, final String error) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();
                            handleError(errorCode, error);
                        }
                    });
                }
            });
        } else {
            super.applyChange();
        }
    }
}
