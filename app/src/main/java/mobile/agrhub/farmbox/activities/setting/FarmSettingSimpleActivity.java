package mobile.agrhub.farmbox.activities.setting;

import android.support.v7.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.adapters.FarmSettingInputDataAdapter;
import mobile.agrhub.farmbox.service.model.FarmSettingDetailData;
import mobile.agrhub.farmbox.service.network.Param;
import mobile.agrhub.farmbox.utils.Utils;

public class FarmSettingSimpleActivity extends BaseFarmSettingItemActivity {
    @BindView(R.id.rv_content)
    RecyclerView mRecyclerView;

    FarmSettingInputDataAdapter mAdapter;

    @Override
    protected int contentView() {
        return R.layout.activity_farm_setting_nutrition_formula;
    }

    @Override
    protected void initView() {
        super.initView();

        mAdapter = new FarmSettingInputDataAdapter(this, item);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    protected boolean validate() {
        FarmSettingInputDataAdapter.ViewHolder viewHolder;
        FarmSettingDetailData settingData;

        String val;
        int intVal;
        double doubleVal;
        for (int i = 0; i < item.getData().size(); i++) {
            settingData = item.getData().get(i);
            intVal = 0;
            doubleVal = 0f;

            viewHolder = (FarmSettingInputDataAdapter.ViewHolder)mRecyclerView.findViewHolderForAdapterPosition(i);

            val = viewHolder.getVal();

            // check empty
            if (val.isEmpty()) {
                showErrorDialog(getString(R.string.field_is_required, settingData.getName()));
                return false;
            }

            // check is number
            try {
                if ("int".equals(item.getDataType())) {
                    intVal = Utils.str2Int(val);
                } else if ("float".equals(item.getDataType())) {
                    doubleVal = Utils.str2Double(val);
                }
            } catch (Exception e) {
                showErrorDialog(getString(R.string.field_is_not_number, settingData.getName()));
                return false;
            }

            // check num in range
            if (settingData.getExpectData() != null && settingData.getExpectData().size() >= 2) {
                try {
                    if ("int".equals(item.getDataType())) {
                        int fromVal = (int)Math.round(Utils.str2Double(settingData.getExpectData().get(0).toString()));
                        int toVal = (int)Math.round(Utils.str2Double(settingData.getExpectData().get(1).toString()));
                        if (intVal < fromVal || intVal > toVal) {
                            showErrorDialog(getString(R.string.field_between, settingData.getName(), fromVal + "", toVal + ""));
                            return false;
                        }
                    } else if ("float".equals(item.getDataType())) {
                        double fromVal = Utils.str2Double(settingData.getExpectData().get(0).toString());
                        double toVal = Utils.str2Double(settingData.getExpectData().get(1).toString());
                        if (doubleVal < fromVal || doubleVal > toVal) {
                            showErrorDialog(getString(R.string.field_between, settingData.getName(), fromVal + "", toVal + ""));
                            return false;
                        }
                    }
                } catch (Exception e) {

                }
            }
        }

        return true;
    }

    @Override
    protected List<Param> buildParam(List<Param> params) {
        FarmSettingInputDataAdapter.ViewHolder viewHolder;
        FarmSettingDetailData settingData;

        String val;
        int intVal;
        double doubleVal;
        for (int i = 0; i < item.getData().size(); i++) {
            settingData = item.getData().get(i);
            viewHolder = (FarmSettingInputDataAdapter.ViewHolder) mRecyclerView.findViewHolderForAdapterPosition(i);
            val = viewHolder.getVal();

            if ("int".equals(item.getDataType())) {
                try {
                    intVal = Utils.str2Int(val);
                } catch (Exception e) {
                    e.printStackTrace();
                    intVal = 0;
                }
                params.add(new Param(settingData.getField(), intVal + ""));
            } else if ("float".equals(item.getDataType())) {
                try {
                    doubleVal = Utils.str2Double(val);
                } catch (Exception e) {
                    e.printStackTrace();
                    doubleVal = 0.0;
                }
                params.add(new Param(settingData.getField(), Utils.f2s(doubleVal, true)));
            }
        }
        return params;
    }
}
