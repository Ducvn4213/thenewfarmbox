package mobile.agrhub.farmbox.activities.setting;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.FarmAPI;
import mobile.agrhub.farmbox.service.network.Param;
import mobile.agrhub.farmbox.utils.BaseActivity;

public class SettingTimeLoopActivity extends BaseActivity {
    TextView mMinLabel, mMaxLabel;
    EditText mMin, mMax;
    Button mDone;

    FarmAPI farmAPI = APIHelper.getFarmAPI();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_time_loop);

        bindingControls();
        setupControlEvents();
        initData();
        configActionBar();
    }

    private void configActionBar() {
        String name = getIntent().getStringExtra("name");
        getSupportActionBar().setTitle(name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void bindingControls() {
        mDone = (Button) findViewById(R.id.btn_set);
        mMinLabel = (TextView) findViewById(R.id.tv_min_label);
        mMaxLabel = (TextView) findViewById(R.id.tv_max_label);
        mMin = (EditText) findViewById(R.id.et_min);
        mMax = (EditText) findViewById(R.id.et_max);
    }

    private void setupControlEvents() {
        mDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestDone();
            }
        });
    }

    private void initData() {
        Intent intent = getIntent();

        String field = intent.getStringExtra("field");
        String runTime = intent.getStringExtra("run_time");
        String stopTime = intent.getStringExtra("stop_time");

        if (runTime.equalsIgnoreCase("-1") || stopTime.equalsIgnoreCase("-1")) {
            mMin.setHint("--");
            mMax.setHint("--");
        } else {
            mMin.setText(runTime);
            mMax.setText(stopTime);
        }
    }

    private void requestDone() {
        String minValue = mMin.getText().toString();
        String maxValue = mMax.getText().toString();

        String field = getIntent().getStringExtra("field");
        String farmID = getIntent().getStringExtra("farmID");
        String factoryID = getIntent().getStringExtra("factoryID");

        List<Param> params = new ArrayList<>();
        if ("factory_water_pump_freq".equals(field)) {
            params.add(new Param("factory_water_pump_on_freq", minValue));
            params.add(new Param("factory_water_pump_off_freq", maxValue));
        } else if ("factory_oxygen_pump_freq".equals(field)) {
            params.add(new Param("factory_oxygen_pump_on_freq", minValue));
            params.add(new Param("factory_oxygen_pump_off_freq", maxValue));
        } else {
            finish();
        }

        showLoading();
        farmAPI.saveFarmSetting(factoryID, farmID, params, new APIHelper.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                SettingTimeLoopActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        final AlertDialog dialog = new AlertDialog.Builder(SettingTimeLoopActivity.this, R.style.AppTheme_Dialog_Alert)
                                .setTitle(R.string.app_name)
                                .setMessage(R.string.setting_save_success)
                                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        SettingTimeLoopActivity.this.finish();
                                    }
                                }).create();
                        if (!dialog.isShowing()) {
                            dialog.show();
                        }
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                SettingTimeLoopActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }
}
