package mobile.agrhub.farmbox.service.model;

import com.google.gson.annotations.SerializedName;

import mobile.agrhub.farmbox.service.FarmBoxAppController;

public class PlantItemDetail {
    @SerializedName("vi")
    String vi;
    @SerializedName("en")
    String en;

    public String getValue() {
        if ("vi".equals(FarmBoxAppController.getInstance().getLanguage())) {
            return vi;
        }

        return en;
    }

    public PlantItemDetail() {
    }
}
