package mobile.agrhub.farmbox.service.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.ParseException;
import java.util.List;

import mobile.agrhub.farmbox.utils.Utils;

public class FarmSettingDetailItem implements Serializable {
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("field")
    private String field;
    @SerializedName("value")
    private String value;
    @SerializedName("type")
    private String type;
    @SerializedName("dataType")
    private String dataType;
    @SerializedName("unit")
    private String unit;
    @SerializedName("not_null")
    private boolean isNotNull;
    @SerializedName("datas")
    private List<FarmSettingDetailData> data;

    public FarmSettingDetailItem() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public boolean isNotNull() {
        return isNotNull;
    }

    public void setNotNull(boolean notNull) {
        isNotNull = notNull;
    }

    public List<FarmSettingDetailData> getData() {
        return data;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setData(List<FarmSettingDetailData> data) {
        this.data = data;
    }

    public int getMinVal() {
        int returnVal = 0;
        if (data != null && data.size() >= 1) {
            String val = data.get(0).getValue();
            if (val != null && !val.isEmpty()) {
                try {
                    returnVal = Utils.str2Int(val);
                } catch (ParseException e) {
                    returnVal = 0;
                }
            }
        }
        return returnVal;
    }

    public int getMediumVal() {
        int returnVal = 0;
        if (data != null && data.size() >= 3) {
            String val = data.get(1).getValue();
            if (val != null && !val.isEmpty()) {
                returnVal = (int)Math.round(Double.parseDouble(val));
            }
        }
        return returnVal;
    }

    public int getMaxVal() {
        int returnVal = 0;
        if (data != null && data.size() >= 2) {
            String val = data.get(data.size() - 1).getValue();
            if (val != null && !val.isEmpty()) {
                try {
                    returnVal = Utils.str2Int(val);
                } catch (ParseException e) {
                    returnVal = 0;
                }
            }
        }
        return returnVal;
    }

    public double getMinDoubleVal() {
        double returnVal = 0f;
        if (data != null && data.size() >= 1) {
            String val = data.get(0).getValue();
            if (val != null && !val.isEmpty()) {
                returnVal = Double.parseDouble(val);
            }
        }
        return returnVal;
    }

    public double getMediumDoubleVal() {
        double returnVal = 0f;
        if (data != null && data.size() >= 3) {
            String val = data.get(1).getValue();
            if (val != null && !val.isEmpty()) {
                try {
                    returnVal = Utils.str2Double(val);
                } catch (ParseException e) {
                    returnVal = 0.0;
                }
            }
        }
        return returnVal;
    }

    public double getMaxDoubleVal() {
        double returnVal = 0;
        if (data != null && data.size() >= 2) {
            String val = data.get(data.size() - 1).getValue();
            if (val != null && !val.isEmpty()) {
                returnVal = Double.parseDouble(val);
            }
        }
        return returnVal;
    }
}
