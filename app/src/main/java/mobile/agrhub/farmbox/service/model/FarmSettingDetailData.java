package mobile.agrhub.farmbox.service.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class FarmSettingDetailData implements Serializable {
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("field")
    private String field;
    @SerializedName("value")
    private String value;
    @SerializedName("expect_data")
    private List<Object> expectData;

    public FarmSettingDetailData() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<Object> getExpectData() {
        return expectData;
    }

    public void setExpectData(List<Object> expectData) {
        this.expectData = expectData;
    }
}
