package mobile.agrhub.farmbox.service.implementations;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.interfaces.DeviceAPI;
import mobile.agrhub.farmbox.service.network.Config;
import mobile.agrhub.farmbox.service.network.Network;
import mobile.agrhub.farmbox.service.network.Param;
import mobile.agrhub.farmbox.utils.Utils;

public class DeviceAPIImplementation implements DeviceAPI {
    Network network = Network.getInstance();

    @Override
    public void activeDevice(String deviceId, boolean isActive, final APIHelper.Callback<String> callback) {
        List<Param> params = new ArrayList<>();
        params.add(new Param("device_name", "is_active"));
        params.add(new Param("device_value", isActive ? "true" : "false"));

        this.edit(deviceId, params, callback);
    }

    @Override
    public void edit(String deviceId, List<Param> params, final APIHelper.Callback<String> callback) {
        params.add(new Param("device_id", deviceId));

        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        String link = Config.EDIT_DEVICE + Utils.decode(accessToken);
        network.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                callback.onSuccess(response);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }
}