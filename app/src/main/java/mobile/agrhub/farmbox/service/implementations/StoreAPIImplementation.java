package mobile.agrhub.farmbox.service.implementations;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.interfaces.StoreAPI;
import mobile.agrhub.farmbox.service.model.Product;
import mobile.agrhub.farmbox.service.model.ProductCategory;
import mobile.agrhub.farmbox.service.model.response.GetProductCategoryResponse;
import mobile.agrhub.farmbox.service.model.response.GetProductResponse;
import mobile.agrhub.farmbox.service.network.Config;
import mobile.agrhub.farmbox.service.network.Network;
import mobile.agrhub.farmbox.service.network.Param;
import mobile.agrhub.farmbox.utils.Utils;

public class StoreAPIImplementation implements StoreAPI {
    Network network = Network.getInstance();

    @Override
    public void getProductCategoryList(final APIHelper.Callback<GetProductCategoryResponse> callback) {
        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        String link = Config.PRODUCT_CATEGORY_API_GETS + Utils.decode(accessToken);

        network.executeGet(link, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                if (response.equalsIgnoreCase("[]")) {
                    callback.onSuccess(null);
                    return;
                }

                GetProductCategoryResponse productCategoryResponse = new GetProductCategoryResponse();

                Gson gson = new Gson();
                List<ProductCategory> productCategoryList = gson.fromJson(response, new TypeToken<List<ProductCategory>>() {
                }.getType());
                productCategoryResponse.productCategories = productCategoryList;

                callback.onSuccess(productCategoryResponse);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void getProductList(final APIHelper.Callback<GetProductResponse> callback) {
        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        String link = Config.PRODUCT_API_GETS + Utils.decode(accessToken);

        network.executeGet(link, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                if (response.equalsIgnoreCase("[]")) {
                    callback.onSuccess(null);
                    return;
                }

                GetProductResponse productResponse = new GetProductResponse();

                Gson gson = new Gson();
                List<Product> productList = gson.fromJson(response, new TypeToken<List<Product>>() {
                }.getType());
                productResponse.products = productList;

                callback.onSuccess(productResponse);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void postCart(final APIHelper.Callback<String> callback) {
        FarmBoxAppController app = FarmBoxAppController.getInstance();

        List<Param> params = new ArrayList<>();
        params.add(new Param("cart", app.getCartJson()));
        String currency = "USD";
        if ("vi".equals(FarmBoxAppController.getInstance().getLanguage())) {
            currency = "VND";
        }
        params.add(new Param("currency", currency));

        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        String link = Config.ORDER_API_POST + Utils.decode(accessToken);
        network.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                callback.onSuccess(response);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }
}
