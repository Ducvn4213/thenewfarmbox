package mobile.agrhub.farmbox.service.model.response;

import java.util.List;

import mobile.agrhub.farmbox.service.model.Nation;


public class NationResponse {
    public List<Nation> data;

    public NationResponse() {
    }
}
