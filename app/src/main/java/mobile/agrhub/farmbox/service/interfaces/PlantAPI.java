package mobile.agrhub.farmbox.service.interfaces;

import java.util.List;

import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.model.Plant;
import mobile.agrhub.farmbox.service.model.response.GetPlantBasicResponse;
import mobile.agrhub.farmbox.service.model.response.GetPlantResponse;

public interface PlantAPI {
    void getPlants(APIHelper.Callback<GetPlantResponse> callback);

    void getPlantByID(long id, APIHelper.Callback<Plant> callback);

    String getPlanImageByID(long id);

    void getPlantListByOffset(int offset, APIHelper.Callback<GetPlantBasicResponse> callback);
    void searchPlant(int page, String lang, String keyword, APIHelper.Callback<GetPlantBasicResponse> callback);

    List<Plant> getPlantList();
}
