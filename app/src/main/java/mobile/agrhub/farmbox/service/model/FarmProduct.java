package mobile.agrhub.farmbox.service.model;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.utils.Utils;

public class FarmProduct {
    public static final int UNIT_UNKNOWN = 0;
    public static final int UNIT_KG = 1;
    public static final int UNIT_HEAD = 2;
    public static final int UNIT_FRUIT = 3;
    public static final int UNIT_PIECE = 4;

    @SerializedName("product_id")
    public long productId;
    @SerializedName("category")
    public ProductCategory category;
    @SerializedName("product_code")
    public String productCode;
    @SerializedName("product_name")
    public String productName;
    @SerializedName("product_avatar")
    public String productAvatarStr;
    private String[] productAvatar;
    @SerializedName("product_price")
    public String productPrice;
    @SerializedName("product_discount")
    public String productDiscount;
    @SerializedName("product_description")
    public String productDescription;
    @SerializedName("product_full_description")
    public String productFullDescription;
    @SerializedName("product_tags")
    public String productTags;
    @SerializedName("is_active")
    public boolean isActive;
    @SerializedName("product_unit")
    public int productUnit;
    @SerializedName("product_weight")
    public float productWeight;


    public FarmProduct() {
    }

    public String displayWeight(Context context, int quantity) {
        switch (productUnit) {
            case FarmProduct.UNIT_KG:
                return Utils.f2s(productWeight * quantity) + " " + context.getString(R.string.unit_kg);
            case FarmProduct.UNIT_HEAD:
                return Utils.f2s(productWeight * quantity) + " " + context.getString(R.string.unit_head);
            case FarmProduct.UNIT_FRUIT:
                return Utils.f2s(productWeight * quantity) + " " + context.getString(R.string.unit_fruit);
            case FarmProduct.UNIT_PIECE:
                return Utils.f2s(productWeight * quantity) + " " + context.getString(R.string.unit_piece);
            default:
                return Utils.f2s(productWeight * quantity);
        }
    }

    public String displayWeight(Context context) {
        return displayWeight(context, 1);
    }

    public long originPrice() {
        long price = 0;
        if (productPrice != null && !productPrice.isEmpty()) {
            try {
                price = Utils.str2Long(productPrice);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return price;
    }

    public String displayOriginPrice() {
        return Utils.formatPriceVND(originPrice());
    }

    public int discount() {
        int discount = 0;
        if (productDiscount != null && !productDiscount.isEmpty()) {
            try {
                discount = Utils.str2Int(productDiscount);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return discount;
    }

    public long price() {
        long price = originPrice();
        long discount = discount();

        return Math.round(price - (price * discount / 100));
    }

    public String displayPrice() {
        return Utils.formatPriceVND(price());
    }

    public String displayDiscount() {
        return discount() + "%";
    }

    public FarmProduct copy() {
        FarmProduct product = new FarmProduct();
        product.productId = -1;
        product.category = category;
        product.productCode = productCode;
        product.productName = productName;
        product.productAvatar = productAvatar;
        product.productPrice = productPrice;
        product.productDiscount = productDiscount;
        product.productDescription = productDescription;
        product.productFullDescription = productFullDescription;
        product.productTags = productTags;
        product.productUnit = productUnit;
        product.productWeight = productWeight;
        return  product;
    }

    public String getDisplayAvatar() {
        String[] avatarArr = getProductAvatar();
        if (avatarArr != null && avatarArr.length > 0) {
            return avatarArr[0];
        }
        return null;
    }

    public String[] getProductAvatar() {
        if (productAvatar == null) {
            if (productAvatarStr == null || productAvatarStr.isEmpty()) {
                productAvatar = new String[]{};
            } else {
                try {
                    Gson gson = new Gson();
                    productAvatar = gson.fromJson(productAvatarStr, String[].class);
                } catch (Exception e) {
                    e.printStackTrace();
                    productAvatar = new String[]{productAvatarStr};
                }
            }
        }
        return productAvatar;
    }
}

