package mobile.agrhub.farmbox.service.interfaces;

import java.util.List;

import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.model.Farm;
import mobile.agrhub.farmbox.service.model.FarmSetting;
import mobile.agrhub.farmbox.service.model.response.GetFarmBasicResponse;
import mobile.agrhub.farmbox.service.model.response.GetFarmResponse;
import mobile.agrhub.farmbox.service.network.Param;

public interface FarmAPI {
    void getAllFarmBasic(boolean isFull, APIHelper.Callback<GetFarmBasicResponse> callback);

    void getFarmByID(long id, APIHelper.Callback<Farm> callback);

    void getFarm(APIHelper.Callback<GetFarmResponse> callback);

    void addNewFarm(String name, String address, String acreage, String type, String lat, String lon, APIHelper.Callback<Farm> callback);

    void editFarmField(String farmId, String field, String val, APIHelper.Callback<String> callback);

    void editFarm(String name, String address, String acreage, String lat, String lon, APIHelper.Callback<String> callback);

    void addSeason(String farmID, String plantID, String date, String age, APIHelper.Callback<String> callback);

    void addContainer(String id, String lat, String lon, APIHelper.Callback<String> callback);

    void getFarmSettings(String farmID, APIHelper.Callback<FarmSetting> callback);

    void getFarmSessionSettings(String factoryId, APIHelper.Callback<FarmSetting> callback);

    void saveFarmSetting(String factoryID, String farmID, List<Param> params, APIHelper.Callback<String> callback);
}
