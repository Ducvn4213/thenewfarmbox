package mobile.agrhub.farmbox.service.model;

import java.io.Serializable;

public class UnitOptionItem implements Serializable {
    private String key;
    private String value;
    private String title;

    public UnitOptionItem() {

    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
