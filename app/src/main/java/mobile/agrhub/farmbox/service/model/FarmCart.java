package mobile.agrhub.farmbox.service.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import mobile.agrhub.farmbox.utils.Utils;

public class FarmCart {
    @SerializedName("cart_item_note")
    public String cartItemNote;

    @SerializedName("cart_item_quantity")
    public int cartItemQuantity;

    @SerializedName("cart_item_registered_date")
    public long cartItemRegisteredDate;

    @SerializedName("cart_item_state")
    public int cartItemState;

    @SerializedName("cart_item_updated_date")
    public long cartItemUpdatedDate;

    @SerializedName("farm_id")
    public long farmId;

    @SerializedName("cart_id")
    public long cartId;

    @SerializedName("order_id")
    public long orderId;

    @SerializedName("cart_item_product")
    public FarmProduct cartProduct;

    public List<SerialPackage> serialList;

    public FarmCart() {
    }

    public FarmCart(FarmProduct product, int quantity) {
        this.cartProduct = product;
        this.cartItemQuantity = quantity;
    }

    public String displayQuantity() {
        return Utils.i2s(cartItemQuantity);
    }

    public int totalQRCode() {
        int total = 0;
        if (serialList != null && serialList.size() > 0) {
            for (SerialPackage item : serialList) {
                total += item.quantity;
            }
        }
        return total;
    }

    public int getRemain() {
        int remain = cartItemQuantity - totalQRCode();
        if (remain < 0) {
            remain = 0;
        }
        return remain;
    }

    public String displayRemain() {
        return Utils.i2s(getRemain());
    }

    public boolean isCompleteAssignQRCode() {
        return (this.farmId > 0 && getRemain() == 0);
    }
}
