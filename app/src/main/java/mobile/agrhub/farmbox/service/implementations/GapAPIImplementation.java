package mobile.agrhub.farmbox.service.implementations;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.interfaces.GapAPI;
import mobile.agrhub.farmbox.service.model.Diary;
import mobile.agrhub.farmbox.service.model.GapCertificate;
import mobile.agrhub.farmbox.service.model.GapTask;
import mobile.agrhub.farmbox.service.model.response.GetCertificateResponse;
import mobile.agrhub.farmbox.service.model.response.GetDiariesResponse;
import mobile.agrhub.farmbox.service.model.response.GetGapTaskResponse;
import mobile.agrhub.farmbox.service.network.Config;
import mobile.agrhub.farmbox.service.network.Network;
import mobile.agrhub.farmbox.service.network.Param;
import mobile.agrhub.farmbox.utils.Utils;

public class GapAPIImplementation implements GapAPI {
    Network network = Network.getInstance();

    private Map<Long, GapTask> cacheGapTasks;

    @Override
    public String getGapTasksName(String jsonListId) {
        StringBuilder returnVal = new StringBuilder();

        Gson gson = new Gson();
        List<Long> gapTaskIdList = gson.fromJson(jsonListId, new TypeToken<List<Long>>() {
        }.getType());
        if (gapTaskIdList != null && gapTaskIdList.size() > 0) {
            for (Long gapTaskId : gapTaskIdList) {
                if (cacheGapTasks != null && gapTaskId != null && cacheGapTasks.containsKey(gapTaskId)) {
                    if (returnVal.length() > 0) {
                        returnVal.append(", ");
                    }
                    returnVal.append(Utils.getValueFromMultiLanguageString(cacheGapTasks.get(gapTaskId).gapTaskTitle));
                }
            }
        }

        return returnVal.toString();
    }


    @Override
    public void getGAPTaskList(final APIHelper.Callback<GetGapTaskResponse> callback) {
        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        String link = Config.GAP_TASK_API_GETS + Utils.decode(accessToken);

        network.executeGet(link, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                if (response.equalsIgnoreCase("[]")) {
                    callback.onSuccess(null);
                    return;
                }

                GetGapTaskResponse getResponse = new GetGapTaskResponse();

                Gson gson = new Gson();
                List<GapTask> gapTaskList = gson.fromJson(response, new TypeToken<List<GapTask>>() {
                }.getType());
                getResponse.gapTasks = gapTaskList;

                if (gapTaskList != null && gapTaskList.size() > 0) {
                    if (cacheGapTasks == null) {
                        cacheGapTasks = new HashMap<Long, GapTask>();
                    } else {
                        cacheGapTasks.clear();
                    }
                    for (GapTask gapTask : gapTaskList) {
                        cacheGapTasks.put(Long.valueOf(gapTask.gapTaskId), gapTask);
                    }
                }

                callback.onSuccess(getResponse);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void postDiary(Long farmId
            , String content
            , String[] tags
            , String[] activityTypes
            , String[] medias
            , String diaryDate
            , final APIHelper.Callback<String> callback) {

        Gson gson = new Gson();

        List<Param> params = new ArrayList<>();
        params.add(new Param("farm_id", String.valueOf(farmId)));
        params.add(new Param("gap_diary_description", content));
        params.add(new Param("gap_diary_tags", gson.toJson(tags)));
        params.add(new Param("gap_diary_tasks", gson.toJson(activityTypes)));
        params.add(new Param("gap_diary_medias", gson.toJson(medias)));
        params.add(new Param("gap_diary_date", diaryDate));

        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        String link = Config.DIARY_API_PATH_SET + Utils.decode(accessToken);
        network.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                callback.onSuccess(response);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void getDiary(String farmId
            , String fromTime
            , String toTime
            , final APIHelper.Callback<GetDiariesResponse> callback) {
        String accessToken = FarmBoxAppController.getInstance().getAccessToken();

        StringBuilder builder = new StringBuilder();
        builder.append(Config.DIARY_API_PATH_FILTER);
        builder.append("?access_token=").append(Utils.decode(accessToken));
        builder.append("&user_token=").append(accessToken);
        if (farmId != null && !farmId.isEmpty()) {
            builder.append("&farm_id=").append(Utils.encode(farmId));
        }
        if (fromTime != null && !fromTime.isEmpty()) {
            builder.append("&start_time=").append(Utils.encode(fromTime));
        }
        if (toTime != null && !toTime.isEmpty()) {
            builder.append("&end_time=").append(Utils.encode(toTime));
        }
        builder.append("&order=").append(Utils.encode("DESC"));

        network.executeGet(builder.toString(), new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                if (response.equalsIgnoreCase("[]")) {
                    callback.onSuccess(null);
                    return;
                }

                GetDiariesResponse getResponse = new GetDiariesResponse();

                Gson gson = new Gson();
                List<Diary> diaries = gson.fromJson(response, new TypeToken<List<Diary>>() {
                }.getType());
                getResponse.diaries = diaries;
                callback.onSuccess(getResponse);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void createCertificate(long farmId, List<String> medias, final APIHelper.Callback<GapCertificate> callback) {
        Gson gson = new Gson();

        String[] mediaUrl;
        if (medias == null || medias.size() <= 0) {
            mediaUrl = new String[]{};
        } else {
            mediaUrl = medias.toArray(new String[0]);;
        }

        List<Param> params = new ArrayList<>();
        params.add(new Param("farm_id", String.valueOf(farmId)));
        params.add(new Param("gap_certificate_medias", gson.toJson(mediaUrl)));

        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        String link = Config.CREATE_CERTIFICATE + Utils.decode(accessToken);
        network.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                Gson gson = new Gson();
                GapCertificate certificate = gson.fromJson(response, GapCertificate.class);
                callback.onSuccess(certificate);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void updateCertificate(long id, List<String> medias, boolean isActive, final APIHelper.Callback<GapCertificate> callback) {
        Gson gson = new Gson();

        String[] mediaUrl;
        if (medias == null || medias.size() <= 0) {
            mediaUrl = new String[]{};
        } else {
            mediaUrl = medias.toArray(new String[0]);;
        }

        List<Param> params = new ArrayList<>();
        params.add(new Param("gap_certificate_id", String.valueOf(id)));
        params.add(new Param("gap_certificate_medias", gson.toJson(mediaUrl)));
        params.add(new Param("is_active", isActive ? "true" : "false"));

        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        String link = Config.UPDATE_CERTIFICATE + Utils.decode(accessToken);
        network.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                Gson gson = new Gson();
                GapCertificate certificate = gson.fromJson(response, GapCertificate.class);
                callback.onSuccess(certificate);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void getCertificates(final APIHelper.Callback<GetCertificateResponse> callback) {
        String accessToken = FarmBoxAppController.getInstance().getAccessToken();

        StringBuilder builder = new StringBuilder();
        builder.append(Config.GET_CERTIFICATES);
        builder.append(Utils.decode(accessToken));
        builder.append("&id=").append(accessToken);

        network.executeGet(builder.toString(), new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                if (response.equalsIgnoreCase("[]")) {
                    callback.onSuccess(null);
                    return;
                }

                GetCertificateResponse getResponse = new GetCertificateResponse();

                Gson gson = new Gson();
                List<GapCertificate> certificates = gson.fromJson(response, new TypeToken<List<GapCertificate>>() {
                }.getType());
                getResponse.certificates = certificates;
                callback.onSuccess(getResponse);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }
}