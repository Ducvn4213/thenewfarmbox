package mobile.agrhub.farmbox.service.model.response;

import java.util.List;

import mobile.agrhub.farmbox.service.model.FarmProduct;

public class GetFarmProductResponse {
    public List<FarmProduct> farmProducts;

    public GetFarmProductResponse() {
    }
}
