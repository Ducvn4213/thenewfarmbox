package mobile.agrhub.farmbox.service.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Product {
    @SerializedName("product_id")
    public long productId;
    @SerializedName("category")
    public ProductCategory category;
    @SerializedName("product_code")
    public String productCode;
    @SerializedName("product_name")
    public String productName;
    @SerializedName("product_avatar")
    public String productAvatar;
    @SerializedName("product_price")
    public String productPrice;
    @SerializedName("product_discount")
    public String productDiscount;
    @SerializedName("product_description")
    public String productDescription;
    @SerializedName("product_tags")
    public String productTags;
    @SerializedName("is_active")
    public boolean isActive;

    public List<String> productImageList;

    public Product() {
    }
}

