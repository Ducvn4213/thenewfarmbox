package mobile.agrhub.farmbox.service.interfaces;

import android.graphics.Bitmap;

import com.dmcbig.mediapicker.entity.Media;

import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.model.Controller;
import mobile.agrhub.farmbox.service.model.Device;
import mobile.agrhub.farmbox.service.model.UploadImageItem;
import mobile.agrhub.farmbox.service.model.Weather;
import mobile.agrhub.farmbox.service.model.response.ChartDataResponse;
import mobile.agrhub.farmbox.service.model.response.NationResponse;

public interface UtilAPI {
    void getNations(APIHelper.Callback<NationResponse> callback);

    void getPasscode(String phoneNumber, APIHelper.Callback<String> callback);

    void toggleController(String id, boolean isOn, APIHelper.Callback<Controller> callback);

    void addDevice(String farmID, String device, APIHelper.Callback<Device> callback);

    void editDeviceName(String deviceID, String name, APIHelper.Callback<String> callback);

    void addController(String deviceID, String type, APIHelper.Callback<String> callback);

    void editController(String controllerId, String field, String value, APIHelper.Callback<String> callback);

    void getWeatherInfo(String lat, String lon, APIHelper.Callback<Weather> callback);

    void getSensorChartData(String sensorID, String dairy, APIHelper.Callback<ChartDataResponse> callback);

    void uploadBitmap(Bitmap bitmap, final APIHelper.Callback<UploadImageItem> callback);

    void uploadFile(Media media, APIHelper.Callback<UploadImageItem> callback);

    void subscribeNotification(String deviceToken, APIHelper.Callback<String> callback);

    void unsubscribeNotification(String deviceToken, APIHelper.Callback<String> callback);
}
