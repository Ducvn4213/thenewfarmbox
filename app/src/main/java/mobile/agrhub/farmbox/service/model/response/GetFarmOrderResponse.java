package mobile.agrhub.farmbox.service.model.response;

import java.util.List;

import mobile.agrhub.farmbox.service.model.FarmOrder;

public class GetFarmOrderResponse {
    public List<FarmOrder> orders;

    public GetFarmOrderResponse() {
    }
}
