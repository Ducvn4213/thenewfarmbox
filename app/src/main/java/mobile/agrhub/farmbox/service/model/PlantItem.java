package mobile.agrhub.farmbox.service.model;

import com.google.gson.annotations.SerializedName;

public class PlantItem {
    @SerializedName("value")
    public String value;

    public PlantItem() {
    }
}
