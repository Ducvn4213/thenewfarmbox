package mobile.agrhub.farmbox.service.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Notify implements Serializable {
    @SerializedName("notify_id")
    private long id;
    @SerializedName("is_active")
    private boolean isActive;
    @SerializedName("is_finish")
    private boolean isFinish;
    @SerializedName("notify_description")
    private String notifyDescription;
    @SerializedName("notify_registered_date")
    private long notifyRegisteredDate;
    @SerializedName("notify_severity")
    private int notifySeverity;
    @SerializedName("notify_updated_date")
    private long notifyUpdatedDate;
    @SerializedName("user_id")
    private long userId;
    @SerializedName("firebase_message_content")
    private String firebaseMessageContent;
    @SerializedName("firebase_message_id")
    private long firebaseMessageId;

    public Notify() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isFinish() {
        return isFinish;
    }

    public void setFinish(boolean finish) {
        isFinish = finish;
    }

    public String getNotifyDescription() {
        return notifyDescription;
    }

    public void setNotifyDescription(String notifyDescription) {
        this.notifyDescription = notifyDescription;
    }

    public long getNotifyRegisteredDate() {
        return notifyRegisteredDate;
    }

    public void setNotifyRegisteredDate(long notifyRegisteredDate) {
        this.notifyRegisteredDate = notifyRegisteredDate;
    }

    public int getNotifySeverity() {
        return notifySeverity;
    }

    public void setNotifySeverity(int notifySeverity) {
        this.notifySeverity = notifySeverity;
    }

    public long getNotifyUpdatedDate() {
        return notifyUpdatedDate;
    }

    public void setNotifyUpdatedDate(long notifyUpdatedDate) {
        this.notifyUpdatedDate = notifyUpdatedDate;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getFirebaseMessageContent() {
        return firebaseMessageContent;
    }

    public void setFirebaseMessageContent(String firebaseMessageContent) {
        this.firebaseMessageContent = firebaseMessageContent;
    }

    public long getFirebaseMessageId() {
        return firebaseMessageId;
    }

    public void setFirebaseMessageId(long firebaseMessageId) {
        this.firebaseMessageId = firebaseMessageId;
    }
}
