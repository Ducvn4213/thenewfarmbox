package mobile.agrhub.farmbox.service.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class FarmSettingItem implements Serializable {
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("settings")
    private List<FarmSettingDetailItem> settings;

    public FarmSettingItem() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<FarmSettingDetailItem> getSettings() {
        return settings;
    }

    public void setSettings(List<FarmSettingDetailItem> settings) {
        this.settings = settings;
    }
}
