package mobile.agrhub.farmbox.service.model;

import android.content.Context;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.FarmBoxAppController;

public class FarmReport implements Serializable {

    public static final int FORMAT_PDF = 0;
    public static final int FORMAT_EXCEL = 1;
    public static final int FORMAT_CSV = 2;

    public static final int GRANULARITY_UTM = 0;
    public static final int GRANULARITY_HOURLY = 1;
    public static final int GRANULARITY_DAILY = 2;
    public static final int GRANULARITY_WEEKLY = 3;
    public static final int GRANULARITY_MONTHLY = 4;

    @SerializedName("farm_id")
    private long farmId;

    @SerializedName("report_id")
    private long reportId;

    @SerializedName("report_end_time")
    private long reportEndTime;

    @SerializedName("report_format")
    private int reportFormat;

    @SerializedName("report_granularity")
    private int reportGranularity;

    @SerializedName("report_interval")
    private int reportInterval;

    @SerializedName("report_language")
    private String reportLanguage;

    @SerializedName("report_registered_date")
    private long reportRegisteredDate;

    @SerializedName("report_start_time")
    private long reportStartTime;

    @SerializedName("report_updated_date")
    private long reportUpdatedDate;

    @SerializedName("report_url")
    private String reportUrl;

    @SerializedName("sensor_id")
    private long sensorId;

    public long getFarmId() {
        return farmId;
    }

    public void setFarmId(long farmId) {
        this.farmId = farmId;
    }

    public long getReportId() {
        return reportId;
    }

    public void setReportId(long reportId) {
        this.reportId = reportId;
    }

    public long getReportEndTime() {
        return reportEndTime;
    }

    public void setReportEndTime(long reportEndTime) {
        this.reportEndTime = reportEndTime;
    }

    public int getReportFormat() {
        return reportFormat;
    }

    public void setReportFormat(int reportFormat) {
        this.reportFormat = reportFormat;
    }

    public int getReportGranularity() {
        return reportGranularity;
    }

    public void setReportGranularity(int reportGranularity) {
        this.reportGranularity = reportGranularity;
    }

    public int getReportInterval() {
        return reportInterval;
    }

    public void setReportInterval(int reportInterval) {
        this.reportInterval = reportInterval;
    }

    public String getReportLanguage() {
        return reportLanguage;
    }

    public void setReportLanguage(String reportLanguage) {
        this.reportLanguage = reportLanguage;
    }

    public long getReportRegisteredDate() {
        return reportRegisteredDate;
    }

    public void setReportRegisteredDate(long reportRegisteredDate) {
        this.reportRegisteredDate = reportRegisteredDate;
    }

    public long getReportStartTime() {
        return reportStartTime;
    }

    public void setReportStartTime(long reportStartTime) {
        this.reportStartTime = reportStartTime;
    }

    public long getReportUpdatedDate() {
        return reportUpdatedDate;
    }

    public void setReportUpdatedDate(long reportUpdatedDate) {
        this.reportUpdatedDate = reportUpdatedDate;
    }

    public String getReportUrl() {
        return reportUrl;
    }

    public void setReportUrl(String reportUrl) {
        this.reportUrl = reportUrl;
    }

    public long getSensorId() {
        return sensorId;
    }

    public void setSensorId(long sensorId) {
        this.sensorId = sensorId;
    }

    public static String granularityName(int granularity) {
        Context context = FarmBoxAppController.getInstance().getApplicationContext();
        switch (granularity) {
            case GRANULARITY_HOURLY:
                return context.getString(R.string.export_granularity_hourly);
            case GRANULARITY_DAILY:
                return context.getString(R.string.export_granularity_daily);
            case GRANULARITY_WEEKLY:
                return context.getString(R.string.export_granularity_weekly);
            case GRANULARITY_MONTHLY:
                return context.getString(R.string.export_granularity_monthly);
            default:
                return context.getString(R.string.export_granularity_utm);
        }
    }

    public static String formatName(int format) {
        Context context = FarmBoxAppController.getInstance().getApplicationContext();
        switch (format) {
            case FORMAT_PDF:
                return context.getString(R.string.export_format_pdf);
            case FORMAT_EXCEL:
                return context.getString(R.string.export_format_xlsx);
            default:
                return context.getString(R.string.export_format_csv);
        }
    }
}
