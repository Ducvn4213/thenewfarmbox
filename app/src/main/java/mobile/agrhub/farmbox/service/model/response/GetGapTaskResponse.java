package mobile.agrhub.farmbox.service.model.response;

import java.util.List;

import mobile.agrhub.farmbox.service.model.GapTask;

public class GetGapTaskResponse {
    public List<GapTask> gapTasks;

    public GetGapTaskResponse() {
    }
}
