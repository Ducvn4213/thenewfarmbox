package mobile.agrhub.farmbox.service.model;

import com.google.gson.annotations.SerializedName;

public class Season {
    @SerializedName("plant")
    public Plant plant;
    @SerializedName("farm_id")
    public long farm_id;
    @SerializedName("season_age_of_plant")
    public long season_age_of_plant;
    @SerializedName("season_start_date")
    public long season_start_date;
    @SerializedName("season_quantity")
    public long season_quantity;

    public Season() {
    }
}
