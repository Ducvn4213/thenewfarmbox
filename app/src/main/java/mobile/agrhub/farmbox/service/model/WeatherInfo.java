package mobile.agrhub.farmbox.service.model;

import com.google.gson.annotations.SerializedName;

public class WeatherInfo {
    @SerializedName("time")
    public long time;
    @SerializedName("temperature")
    public double temp;
    @SerializedName("humidity")
    public double humi;
    @SerializedName("windSpeed")
    public double wind;

    public WeatherInfo() {
    }
}
