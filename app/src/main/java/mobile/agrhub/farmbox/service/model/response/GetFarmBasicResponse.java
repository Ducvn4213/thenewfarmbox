package mobile.agrhub.farmbox.service.model.response;

import java.util.List;

import mobile.agrhub.farmbox.service.model.FarmBasic;

public class GetFarmBasicResponse {
    public List<FarmBasic> farms;

    public GetFarmBasicResponse() {
    }
}
