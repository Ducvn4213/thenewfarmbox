package mobile.agrhub.farmbox.service.model;

import com.dmcbig.mediapicker.entity.Media;
import com.google.gson.annotations.SerializedName;

/**
 * Created by lecong on 3/27/18.
 */

public class Photo {
    @SerializedName("is_on_server")
    public boolean isOnServer;
    @SerializedName("id")
    public long id;
    @SerializedName("uri")
    public String uri;
    public Media media;

    public Photo() {

    }

    public Photo(Media media) {
        this.isOnServer = false;
        this.id = media.id;
        this.uri = "file://" + media.path;
        this.media = media;
    }

    public Photo(long id, String uri) {
        this.id = id;
        this.uri = uri;
    }

    public Photo(String onlineUrl) {
        this.isOnServer = true;
        this.id = -1;
        this.uri = onlineUrl;
    }

    public boolean isSame(Media media) {
        return this.media != null && media != null && this.media.id == media.id;
    }
}
