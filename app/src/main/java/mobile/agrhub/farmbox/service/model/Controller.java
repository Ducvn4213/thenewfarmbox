package mobile.agrhub.farmbox.service.model;

import android.content.Context;

import com.google.gson.annotations.SerializedName;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.FarmBoxAppController;

public class Controller {
    public static final int TYPE_SWITCH = 0;
    public static final int TYPE_LIGHT = 1;
    public static final int TYPE_WATER_PUMP = 2;
    public static final int TYPE_MISTING_PUMP = 3;
    public static final int TYPE_FAN = 4;
    public static final int TYPE_AIR_CONDITIONER = 5;
    public static final int TYPE_NUTRIENT_DOSING_PUMP = 7;
    public static final int TYPE_VAN = 9;
    public static final int TYPE_PH_DOWN_DOSING_PUMP = 17;
    public static final int TYPE_PH_UP_DOSING_PUMP = 18;

    @SerializedName("controller_id")
    public long controller_id;
    @SerializedName("controller_type")
    public int controller_type;
    @SerializedName("controller_value")
    public long controller_value;
    @SerializedName("controller_is_on")
    public Boolean controller_is_on;
    @SerializedName("controller_on_time")
    public long controller_on_time;
    @SerializedName("is_error")
    public Boolean is_error;
    @SerializedName("controller_registered_date")
    public long controller_registered_date;
    @SerializedName("controller_updated_date")
    public long controller_updated_date;
    @SerializedName("controller_last_manual_control_date")
    public long controller_last_manual_control_date;
    @SerializedName("is_active")
    public Boolean isActive;

    public String customerName;
    public long deviceID;
    public boolean editable;

    public Controller() {
    }

    public String getTypeName() {
        return Controller.typeName(controller_type);
    }

    public static String typeName(int typeId) {
        Context context = FarmBoxAppController.getInstance().getApplicationContext();
        String typeName = context.getString(R.string.unknown);
        if (TYPE_SWITCH == typeId) {
            typeName = context.getString(R.string.controller_switch);
        } else if (TYPE_LIGHT == typeId) {
            typeName = context.getString(R.string.controller_light);
        } else if (TYPE_WATER_PUMP == typeId) {
            typeName = context.getString(R.string.controller_water_pump);
        } else if (TYPE_MISTING_PUMP == typeId) {
            typeName = context.getString(R.string.controller_misting_pump);
        } else if (TYPE_FAN == typeId) {
            typeName = context.getString(R.string.controller_fan);
        } else if (TYPE_AIR_CONDITIONER == typeId) {
            typeName = context.getString(R.string.controller_air_conditioner);
        } else if (TYPE_VAN == typeId) {
            typeName = context.getString(R.string.controller_van);
        } else if (TYPE_NUTRIENT_DOSING_PUMP == typeId) {
            typeName = context.getString(R.string.controller_dosing_pump);
        } else if (TYPE_PH_DOWN_DOSING_PUMP == typeId) {
            typeName = context.getString(R.string.controller_ph_down_dosing_pump);
        } else if (TYPE_PH_UP_DOSING_PUMP == typeId) {
            typeName = context.getString(R.string.controller_ph_up_dosing_pump);
        }

        return typeName;
    }
}

