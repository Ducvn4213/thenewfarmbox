package mobile.agrhub.farmbox.service.implementations;

import android.graphics.Bitmap;

import com.dmcbig.mediapicker.entity.Media;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.interfaces.UtilAPI;
import mobile.agrhub.farmbox.service.model.Controller;
import mobile.agrhub.farmbox.service.model.Device;
import mobile.agrhub.farmbox.service.model.Nation;
import mobile.agrhub.farmbox.service.model.SensorChartPointItem;
import mobile.agrhub.farmbox.service.model.UploadImageItem;
import mobile.agrhub.farmbox.service.model.Weather;
import mobile.agrhub.farmbox.service.model.response.ChartDataResponse;
import mobile.agrhub.farmbox.service.model.response.NationResponse;
import mobile.agrhub.farmbox.service.network.Config;
import mobile.agrhub.farmbox.service.network.Network;
import mobile.agrhub.farmbox.service.network.Param;
import mobile.agrhub.farmbox.utils.Utils;

public class UtilAPIImplementation implements UtilAPI {
    Network network = Network.getInstance();

    @Override
    public void getNations(final APIHelper.Callback<NationResponse> callback) {
        String link = Config.GET_NATION;
        network.executeGet(link, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                if (response.equalsIgnoreCase("[]")) {
                    callback.onSuccess(null);
                    return;
                }

                Gson gson = new Gson();
                NationResponse getNationResponse = new NationResponse();
                getNationResponse.data = new ArrayList<>();
                List<Nation> nations = gson.fromJson(response, new TypeToken<List<Nation>>() {
                }.getType());

                for (Nation n : nations) {
                    if (n.phone_code != null && !n.phone_code.isEmpty()) {
                        getNationResponse.data.add(n);
                    }
                }

                callback.onSuccess(getNationResponse);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void getPasscode(String phoneNumber, final APIHelper.Callback<String> callback) {
        List<Param> params = new ArrayList<>();
        params.add(new Param("phone", phoneNumber));

        network.execute(Config.PASS_CODE_GET, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                callback.onSuccess("200");
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void toggleController(String id, boolean isOn, final APIHelper.Callback<Controller> callback) {
        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        List<Param> params = new ArrayList<>();
        params.add(new Param("id", id));
        params.add(new Param("controller_is_on", isOn ? "true" : "false"));

        String link = Config.TOGGLE_CONTROLLER + Utils.decode(accessToken);
        network.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                try {
                    Gson gson = new Gson();
                    Controller controller = gson.fromJson(response, Controller.class);
                    callback.onSuccess(controller);
                } catch (Exception ex) {
                    callback.onFail(Network.FATAL_ERROR, "response is invalid");
                }
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void addDevice(String farmID, String device, final APIHelper.Callback<Device> callback) {
        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        List<Param> params = new ArrayList<>();
        params.add(new Param("device_mac_address", device));
        params.add(new Param("farm_id", farmID));

        String link = Config.ADD_DEVICE + Utils.decode(accessToken);
        network.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                Gson gson = new Gson();
                Device deviceInfo = gson.fromJson(response, Device.class);
                callback.onSuccess(deviceInfo);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void editDeviceName(String deviceID, String name, final APIHelper.Callback<String> callback) {
        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        List<Param> params = new ArrayList<>();
        params.add(new Param("device_id", deviceID));
        params.add(new Param("device_name", "device_custom_name"));
        params.add(new Param("device_value", name));

        String link = Config.EDIT_DEVICE_NAME + Utils.decode(accessToken);
        network.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                callback.onSuccess(response);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void addController(String deviceID, String type, final APIHelper.Callback<String> callback) {
        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        List<Param> params = new ArrayList<>();
        params.add(new Param("id", deviceID));
        params.add(new Param("controller_name", "controller_type"));
        params.add(new Param("controller_value", type));

        String link = Config.ADD_CONTROLLER + Utils.decode(accessToken);
        network.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                callback.onSuccess(response);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void editController(String controllerId, String field, String value, final APIHelper.Callback<String> callback) {
        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        List<Param> params = new ArrayList<>();
        params.add(new Param("id", controllerId));
        params.add(new Param("controller_name", field));
        params.add(new Param("controller_value", value));

        String link = Config.EDIT_CONTROLLER + Utils.decode(accessToken);
        network.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                callback.onSuccess(response);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void getWeatherInfo(String lat, String lon, final APIHelper.Callback<Weather> callback) {
        String link = "https://api.darksky.net/forecast/60da03f5d6f05ac000a819ed2d70b7ab/" + lat + "," + lon;

        network.executeGet(link, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                Gson gson = new Gson();
                Weather weather = gson.fromJson(response, Weather.class);
                callback.onSuccess(weather);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void getSensorChartData(String sensorID, String dairy, final APIHelper.Callback<ChartDataResponse> callback) {
        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        String sensorIDURL = "&id=" + Utils.encode(sensorID);
        String dairyURL = "&diary_type=" + Utils.encode(dairy);
        String dairyPageURL = "&diary_page=MA==";
        String link = Config.GET_SENSOR + Utils.decode(accessToken) + sensorIDURL + dairyURL + dairyPageURL;

        network.executeGet(link, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                if (response.equalsIgnoreCase("[]")) {
                    callback.onSuccess(null);
                    return;
                }

                Gson gson = new Gson();
                List<SensorChartPointItem> pointItems = gson.fromJson(response, new TypeToken<List<SensorChartPointItem>>() {
                }.getType());
                ChartDataResponse chartDateResponse = new ChartDataResponse();
                chartDateResponse.data = pointItems;
                callback.onSuccess(chartDateResponse);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void uploadBitmap(Bitmap bitmap, final APIHelper.Callback<UploadImageItem> callback) {
        final String accessToken = FarmBoxAppController.getInstance().getAccessToken();

        network.upload(bitmap, accessToken, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                Gson gson = new Gson();
                UploadImageItem uploadImageItem = gson.fromJson(response, UploadImageItem.class);
                callback.onSuccess(uploadImageItem);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void uploadFile(Media media, final APIHelper.Callback<UploadImageItem> callback) {
        final String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        String link = Config.FILE_API_UPLOAD_IMAGE + Utils.decode(accessToken);
        if (media.mediaType == 3) {
            link = Config.FILE_API_UPLOAD_VIDEO + Utils.decode(accessToken);
        }

        network.uploadMedia(link, media, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                Gson gson = new Gson();
                UploadImageItem uploadImageItem = gson.fromJson(response, UploadImageItem.class);
                callback.onSuccess(uploadImageItem);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void subscribeNotification(String deviceToken, final APIHelper.Callback<String> callback) {
        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        List<Param> params = new ArrayList<>();
        params.add(new Param("id", deviceToken));
        params.add(new Param("device_type", "1"));

        String link = Config.SUBSCRIBE_NOTIFICATION + Utils.decode(accessToken);
        network.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                callback.onSuccess(response);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void unsubscribeNotification(String deviceToken, final APIHelper.Callback<String> callback) {
        List<Param> params = new ArrayList<>();
        params.add(new Param("id", deviceToken));

        String link = Config.UNSUBSCRIBE_NOTIFICATION;
        network.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                callback.onSuccess(response);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }
}
