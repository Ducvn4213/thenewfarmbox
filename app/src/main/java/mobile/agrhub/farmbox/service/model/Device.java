package mobile.agrhub.farmbox.service.model;

import android.content.Context;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.FarmBoxAppController;

public class Device {
    public static final int UNKNOWN = 0;
    public static final int SENSOR = 1;
    public static final int CONTROLLER = 2;
    public static final int CONTAINER = 3;
    public static final int GATEWAY = 4;

    public static final int TYPE_SP3_SMART_PLUG = 1;
    public static final int TYPE_ON_OFF_SMART_PLUG = 2;
    public static final int TYPE_XIAOMI_SMART_PLUG = 3;
    public static final int TYPE_SOIL_SENSOR = 4;
    public static final int TYPE_AIR_SENSOR = 5;
    public static final int TYPE_TI_TAG_SENSOR = 6;
    public static final int TYPE_FIOT_SMART_TANK = 7;
    public static final int TYPE_RM3_SMART_REMOTE = 8;
    public static final int TYPE_GATEWAY = 9;
    public static final int TYPE_VIETTEL_CAMERA = 10;
    public static final int TYPE_SENSE_HUB_PRO = 11;
    public static final int TYPE_NRF51822_SENSOR = 12;
    public static final int TYPE_VGV_CAMERA = 13;
    public static final int TYPE_MI_AIR_SENSOR = 14;
    public static final int TYPE_MI_POT = 15;
    public static final int TYPE_KPL_AIR_SENSOR = 16;
    public static final int TYPE_KPL_FLOOD_SENSOR = 17;
    public static final int TYPE_KAMOER_DRIPPING_PRO = 18;
    public static final int TYPE_RTMP_CAMERA = 19;
    public static final int TYPE_THUNDERBOARD_SENSE = 20;
    public static final int TYPE_HYDROPONIC_CONTROLLER = 21;
    public static final int TYPE_FROZEN_WAREHOUSE_CONTROLLER = 22;
    public static final int TYPE_SENSE_PLUG = 23;
    public static final int TYPE_JENCO_PH_METER = 24;
    public static final int TYPE_SENSE_WATER_LEVEL = 25;
    public static final int TYPE_METER_EC_PH = 26;

    @SerializedName("device_id")
    public long device_id;
    @SerializedName("device_name")
    public long device_name;
    @SerializedName("device_mac_address")
    public String device_mac_address;
    @SerializedName("device_custom_name")
    public String device_custom_name;
    @SerializedName("user")
    public User user;
    @SerializedName("device_latitude")
    public float device_latitude;
    @SerializedName("device_longitude")
    public float device_longitude;
    @SerializedName("device_state")
    public long device_state;
    @SerializedName("is_active")
    public Boolean is_active;
    @SerializedName("is_block")
    public Boolean is_block;
    @SerializedName("farm_id")
    public long farm_id;
    @SerializedName("device_type")
    public int device_type;
    @SerializedName("device_time_life")
    public long device_time_life;
    @SerializedName("device_registered_date")
    public long device_registered_date;
    @SerializedName("device_updated_date")
    public long device_updated_date;
    @SerializedName("sensors")
    public List<Sensor> sensors;
    @SerializedName("controllers")
    public List<Controller> controllers;

    public int subDeviceId;


    public Device() {
    }

    public String getDisplayName() {
        if (null == device_custom_name || device_custom_name.isEmpty()) {
            return getTypeName();
        } else {
            return device_custom_name;
        }
    }

    public String getTypeName() {
        Context context = FarmBoxAppController.getInstance().getApplicationContext();
        String displayName;
        switch ((int)device_name) {
            case Device.TYPE_SP3_SMART_PLUG:
                displayName = context.getString(R.string.device_sp3_smart_plug);
                break;
            case Device.TYPE_ON_OFF_SMART_PLUG:
                displayName = context.getString(R.string.device_on_off_smart_plug);
                break;
            case Device.TYPE_XIAOMI_SMART_PLUG:
                displayName = context.getString(R.string.device_xiaomi_smart_plug);
                break;
            case Device.TYPE_SOIL_SENSOR:
                displayName = context.getString(R.string.device_sensor_soil);
                break;
            case Device.TYPE_AIR_SENSOR:
                displayName = context.getString(R.string.device_sensor_air);
                break;
            case Device.TYPE_TI_TAG_SENSOR:
                displayName = context.getString(R.string.device_ti_tag_sensor);
                break;
            case Device.TYPE_FIOT_SMART_TANK:
                displayName = context.getString(R.string.device_fiot_smart_tank);
                break;
            case Device.TYPE_RM3_SMART_REMOTE:
                displayName = context.getString(R.string.device_rm3_smart_remote);
                break;
            case Device.TYPE_GATEWAY:
                displayName = context.getString(R.string.device_gateway);
                break;
            case Device.TYPE_VIETTEL_CAMERA:
                displayName = context.getString(R.string.device_viettel_camera);
                break;
            case Device.TYPE_SENSE_HUB_PRO:
                displayName = context.getString(R.string.device_sense_hub_pro);
                break;
            case Device.TYPE_NRF51822_SENSOR:
                displayName = context.getString(R.string.device_nrf51822_sensor);
                break;
            case Device.TYPE_VGV_CAMERA:
                displayName = context.getString(R.string.device_vgv_camera);
                break;
            case Device.TYPE_MI_AIR_SENSOR:
                displayName = context.getString(R.string.device_mi_air_sensor);
                break;
            case Device.TYPE_MI_POT:
                displayName = context.getString(R.string.device_mi_pot);
                break;
            case Device.TYPE_KPL_AIR_SENSOR:
                displayName = context.getString(R.string.device_kpl_air_sensor);
                break;
            case Device.TYPE_KPL_FLOOD_SENSOR:
                displayName = context.getString(R.string.device_kpl_flood_sensor);
                break;
            case Device.TYPE_KAMOER_DRIPPING_PRO:
                displayName = context.getString(R.string.device_kamoer_dripping_pro);
                break;
            case Device.TYPE_RTMP_CAMERA:
                displayName = context.getString(R.string.device_rtmp_camera);
                break;
            case Device.TYPE_THUNDERBOARD_SENSE:
                displayName = context.getString(R.string.device_thunderboard_sense);
                break;
            case Device.TYPE_HYDROPONIC_CONTROLLER:
                displayName = context.getString(R.string.ariatec_hydroponic_controller);
                break;
            case Device.TYPE_FROZEN_WAREHOUSE_CONTROLLER:
                displayName = context.getString(R.string.frozen_warehouse_controller);
                break;
            case Device.TYPE_SENSE_PLUG:
                displayName = context.getString(R.string.sense_plug);
                break;
            case Device.TYPE_JENCO_PH_METER:
                displayName = context.getString(R.string.jenco_ph_meter);
                break;
            case Device.TYPE_SENSE_WATER_LEVEL:
                displayName = context.getString(R.string.sense_water_level);
                break;
            case Device.TYPE_METER_EC_PH:
                displayName = context.getString(R.string.meter_ec_ph);
                break;
            default:
                displayName = context.getString(R.string.device_type_number, device_name);
                break;
        }
        return displayName;
    }

    public String getControlTypeName() {
        if (controllers != null && controllers.size() > 0) {
            return controllers.get(0).getTypeName();
        }
        Context context = FarmBoxAppController.getInstance().getApplicationContext();
        return context.getString(R.string.unknown);
    }

    public int getControlType() {
        if (controllers != null && controllers.size() > 0) {
            return controllers.get(0).controller_type;
        }
        return -1;
    }
}
