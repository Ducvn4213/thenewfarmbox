package mobile.agrhub.farmbox.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.activities.TabActivity;
import mobile.agrhub.farmbox.service.model.Notify;
import mobile.agrhub.farmbox.utils.Utils;

public class FarmBoxFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";
    private static final String NOTIFICATION_NUMBER = "NOTIFICATION_NUMBER";
    private static final int REQUEST_CODE = 1;
    private static final int NOTIFICATION_ID = 6578;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        FarmBoxAppController app = FarmBoxAppController.getInstance();
        if (app != null && app.isDemoMode()) {
            return;
        }

        if (remoteMessage.getData().size() > 0) {
            String title = remoteMessage.getData().get("title");
            String message = remoteMessage.getData().get("body");
            if (null == message || message.isEmpty()) {
                message = remoteMessage.getData().get("content");
            }
            sendNotification(title, message);
        } else {
            String title = "";
            if (remoteMessage.getNotification().getTitle() != null){
                title = remoteMessage.getNotification().getTitle();
            }

            String message = "";
            if (remoteMessage.getNotification().getBody() != null){
                message = remoteMessage.getNotification().getBody();
            }

            Log.e("notification","recieved");


            sendNotification(title, message);
        }
    }

    private void sendNotification(String title, String msg) {
        Context context = getApplicationContext();

        if (null == title || title.isEmpty()) {
            title = context.getString(R.string.app_name);
        }

        // check message is json
        try {
            Gson gson = new Gson();
            Notify notify = gson.fromJson(msg, Notify.class);
            if (null != notify) {
                if (null != notify.getNotifyDescription() && !notify.getNotifyDescription().isEmpty()) {
                    msg = notify.getNotifyDescription();
                } else if (null != notify.getFirebaseMessageContent() && !notify.getFirebaseMessageContent().isEmpty()) {
                    msg = notify.getFirebaseMessageContent();
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }

        if (null == msg || msg.isEmpty()) {
            return;
        }

        int notificationNumber = Utils.getIntPreference(context, NOTIFICATION_NUMBER);

        Intent intent = new Intent(getApplicationContext(), TabActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, "farm_box_channel_id")
                        .setAutoCancel(true)
                        .setSmallIcon(R.drawable.ic_notification)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_notification_large))
                        .setContentTitle(title)
                        .setContentText(msg)
                        .setSound(uri)
                        .setContentIntent(pendingIntent)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setDefaults(NotificationCompat.DEFAULT_ALL);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(notificationNumber, notificationBuilder.build());

        notificationNumber++;
        Utils.savePreference(context, NOTIFICATION_NUMBER, notificationNumber);
    }
}
