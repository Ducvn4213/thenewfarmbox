package mobile.agrhub.farmbox.service.interfaces;

import java.util.List;

import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.model.FarmCart;
import mobile.agrhub.farmbox.service.model.FarmOrder;
import mobile.agrhub.farmbox.service.model.FarmProduct;
import mobile.agrhub.farmbox.service.model.response.GetFarmOrderResponse;
import mobile.agrhub.farmbox.service.model.response.GetFarmProductResponse;

public interface SaleAPI {
    void createProduct(String name, List<String> avatarList, float weight, int unit, long price, int discount, String description, String fullDescription, APIHelper.Callback<FarmProduct> callback);

    void updateProduct(long id, String name, List<String> avatarList, long price, int discount, String description, String fullDescription, APIHelper.Callback<FarmProduct> callback);

    void getProducts(int page, long userId, boolean verbose, APIHelper.Callback<GetFarmProductResponse> callback);

    void getProduct(long id, APIHelper.Callback<FarmProduct> callback);

    void updateProductField(long id, String field, String val, APIHelper.Callback<String> callback);

    void createOrder(long customerId, List<FarmCart> carts, APIHelper.Callback<FarmOrder> callback);

    void getOrders(int page, Long userId, Long customerId, Integer state, APIHelper.Callback<GetFarmOrderResponse> callback);

    void getOrder(long id, APIHelper.Callback<FarmOrder> callback);

    void updateOrderField(long id, String field, String val, APIHelper.Callback<FarmOrder> callback);

    void updateCartField(long id, String field, String val, APIHelper.Callback<FarmCart> callback);

    void createTrace(long cartId, String qrCode, APIHelper.Callback<String> callback);

    void createTrace(long cartId, String fromSerial, String toSerial, APIHelper.Callback<String> callback);

}
