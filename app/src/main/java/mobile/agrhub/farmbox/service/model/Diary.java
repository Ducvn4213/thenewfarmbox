package mobile.agrhub.farmbox.service.model;

import com.google.gson.annotations.SerializedName;

public class Diary {
    @SerializedName("farm_id")
    public long farmId;

    @SerializedName("gap_diary_date")
    public long gapDiaryDate;

    @SerializedName("gap_diary_description")
    public String gapDiaryDescription;

    @SerializedName("gap_diary_medias")
    public String gapDiaryMedias;

    @SerializedName("gap_diary_registered_date")
    public long gapDiaryRegisteredDate;

    @SerializedName("gap_diary_tags")
    public String gapDiaryTags;

    @SerializedName("gap_diary_tasks")
    public String gapDiaryTasks;

    @SerializedName("gap_diary_updated_date")
    public long gapDiaryUpdatedDate;

    @SerializedName("gap_diary_id")
    public long gapDiaryId;

    @SerializedName("is_active")
    public boolean is_active;

    @SerializedName("user_id")
    public long userId;

    public Diary() {
    }
}
