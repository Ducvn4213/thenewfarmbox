package mobile.agrhub.farmbox.service.model.response;

import com.google.gson.annotations.SerializedName;

public class VerifyCodeResponse {
    @SerializedName("is_new_user")
    public Boolean is_new_user;
    @SerializedName("user_token")
    public String user_token;
    @SerializedName("user")
    public String userString;

    public VerifyCodeResponse() {
    }
}
