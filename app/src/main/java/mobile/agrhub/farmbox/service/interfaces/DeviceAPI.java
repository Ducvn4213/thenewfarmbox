package mobile.agrhub.farmbox.service.interfaces;

import java.util.List;

import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.network.Param;

public interface DeviceAPI {
    void activeDevice(String deviceId, boolean isActive, APIHelper.Callback<String> callback);
    void edit(String deviceId, List<Param> params, APIHelper.Callback<String> callback);
}
