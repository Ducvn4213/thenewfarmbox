package mobile.agrhub.farmbox.service.network;

import android.graphics.Bitmap;

import com.dmcbig.mediapicker.entity.Media;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import mobile.agrhub.farmbox.utils.Utils;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okio.BufferedSink;
import okio.Okio;

public class Network {
    public static final int FATAL_ERROR = 999;
    public static final int DEMO_MODE_ERROR = 998;

    private MediaType CONTENT_TYPE = MediaType.parse("application/x-www-form-urlencoded;charset=utf-8");
    private List<Integer> acceptCode = Arrays.asList(200, 201, 204);

    OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .build();

    public interface Callback {
        public void onCallBack(String response);

        public void onFail(int errorCode, String error);
    }

    public interface DownloadFileCallback {
        public void onCallBack(InputStream responseStream);

        public void onFail(int errorCode, String error);
    }

    private static Network instance;

    private Network() {
    }

    public static Network getInstance() {
        if (instance == null) {
            instance = new Network();
        }
        return instance;
    }

    public void execute(String link, List<Param> params, final Callback callback) {
        if (Utils.isDemoMode()) {
            callback.onFail(DEMO_MODE_ERROR, "");
            return;
        }

        FormBody.Builder builder = new FormBody.Builder();
        for (Param p : params) {
            String value = Utils.encode(p.value);
            if (p.key.equalsIgnoreCase("user_avatar")) {
                builder.add(p.key, p.value);
            } else {
                builder.add(p.key, value);
            }


        }
        RequestBody requestBody = builder.build();

        Request request = new Request.Builder()
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .url(link)
                .method("POST", RequestBody.create(null, new byte[0]))
                .post(requestBody)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callback.onFail(FATAL_ERROR, e.getMessage());
            }

            @Override
            public void onResponse(Call call, okhttp3.Response response) throws IOException {
                if (!acceptCode.contains(response.code())) {
                    callback.onFail(FATAL_ERROR, "network fail");
                    return;
                }

                String body = response.body().string();
                callback.onCallBack(body);
            }
        });
    }

    public void executeGet(String link, final Callback callback) {
        Request request = new Request.Builder()
                .url(link)
                .method("GET", null)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callback.onFail(FATAL_ERROR, e.getMessage());
            }

            @Override
            public void onResponse(Call call, okhttp3.Response response) throws IOException {
                if (!acceptCode.contains(response.code())) {
                    callback.onFail(FATAL_ERROR, "network fail");
                    return;
                }

                String body = response.body().string();
                callback.onCallBack(body);
            }
        });
    }

    public void upload(Bitmap bitmap, String currentAccessToken, final Callback callback) {
        if (Utils.isDemoMode()) {
            callback.onFail(DEMO_MODE_ERROR, "");
            return;
        }

        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);

        final MediaType MEDIA_TYPE = MediaType.parse("image/png");

        //Convert bitmap to byte array
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
        byte[] bitmapData = bos.toByteArray();
        builder.addFormDataPart("file", "test", RequestBody.create(MEDIA_TYPE, bitmapData));

        RequestBody requestBody = builder.build();

        Request request = new Request.Builder()
                .url(Config.FILE_API_UPLOAD_IMAGE + Utils.decode(currentAccessToken))
                .post(requestBody)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                callback.onFail(FATAL_ERROR, e.getMessage());
            }

            @Override
            public void onResponse(Call call, okhttp3.Response response) throws IOException {
                if (!acceptCode.contains(response.code())) {
                    callback.onFail(FATAL_ERROR, "network fail");
                    return;
                }

                String body = response.body().string();
                callback.onCallBack(body);
            }
        });
    }

    public void uploadMedia(String link, final Media media, final Callback callback) {
        if (Utils.isDemoMode()) {
            callback.onFail(DEMO_MODE_ERROR, "");
            return;
        }

        final File file = new File(media.path);
        if (file.exists()) {

            MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
            if (media.mediaType == 3) {
                RequestBody videoFile = new RequestBody() {
                    @Override
                    public long contentLength() {
                        return media.size;
                    }

                    @Override
                    public MediaType contentType() {
                        return MediaType.parse(media.extension);
                    }

                    @Override
                    public void writeTo(BufferedSink sink) throws IOException {
                        try (InputStream is = new FileInputStream(file)) {
                            sink.writeAll(Okio.buffer(Okio.source(is)));
                        }
                    }
                };
                builder.addFormDataPart("file", "video_file_" + System.nanoTime(), videoFile);
            } else {
                final MediaType MEDIA_TYPE = MediaType.parse("image/png");
                builder.addFormDataPart("file", "test", RequestBody.create(MEDIA_TYPE, file));
            }

            RequestBody requestBody = builder.build();
            Request request = new Request.Builder()
                    .url(link)
                    .post(requestBody)
                    .build();

            client.newCall(request).enqueue(new okhttp3.Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                    callback.onFail(FATAL_ERROR, e.getMessage());
                }

                @Override
                public void onResponse(Call call, okhttp3.Response response) throws IOException {
                    if (!acceptCode.contains(response.code())) {
                        callback.onFail(FATAL_ERROR, "network fail");
                        return;
                    }

                    String body = response.body().string();
                    callback.onCallBack(body);
                }
            });
        }
    }

    public void download(String link, final DownloadFileCallback callback) {
        Request request = new Request.Builder()
                .url(link)
                .method("GET", null)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callback.onFail(FATAL_ERROR, e.getMessage());
            }

            @Override
            public void onResponse(Call call, okhttp3.Response response) throws IOException {
                if (!acceptCode.contains(response.code())) {
                    callback.onFail(FATAL_ERROR, "network fail");
                    return;
                }

                callback.onCallBack(response.body().byteStream());
            }
        });
    }
}
