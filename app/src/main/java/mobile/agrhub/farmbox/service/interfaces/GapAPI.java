package mobile.agrhub.farmbox.service.interfaces;

import java.util.List;

import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.model.GapCertificate;
import mobile.agrhub.farmbox.service.model.response.GetCertificateResponse;
import mobile.agrhub.farmbox.service.model.response.GetDiariesResponse;
import mobile.agrhub.farmbox.service.model.response.GetGapTaskResponse;

public interface GapAPI {

    void getGAPTaskList(APIHelper.Callback<GetGapTaskResponse> callback);

    String getGapTasksName(String jsonListId);

    void postDiary(Long farmId
            , String content
            , String[] tags
            , String[] activityTypes
            , String[] medias
            , String diaryDate
            , final APIHelper.Callback<String> callback);

    void getDiary(String farmId
            , String fromTime
            , String toTime
            , final APIHelper.Callback<GetDiariesResponse> callback);

    void createCertificate(long farmId, List<String> medias, APIHelper.Callback<GapCertificate> callback);

    void updateCertificate(long id, List<String> medias, boolean isActive, APIHelper.Callback<GapCertificate> callback);

    void getCertificates(APIHelper.Callback<GetCertificateResponse> callback);
}
