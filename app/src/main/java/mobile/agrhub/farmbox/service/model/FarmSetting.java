package mobile.agrhub.farmbox.service.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class FarmSetting implements Serializable {
    @SerializedName("factory_id")
    private Long factoryId;
    @SerializedName("farm_id")
    private Long farmId;
    @SerializedName("factory_settings")
    private List<FarmSettingItem> settings;

    public FarmSetting() {

    }

    public Long getFactoryId() {
        return factoryId;
    }

    public void setFactoryId(Long factoryId) {
        this.factoryId = factoryId;
    }

    public Long getFarmId() {
        return farmId;
    }

    public void setFarmId(Long farmId) {
        this.farmId = farmId;
    }

    public List<FarmSettingItem> getSettings() {
        return settings;
    }

    public void setSettings(List<FarmSettingItem> settings) {
        this.settings = settings;
    }
}
