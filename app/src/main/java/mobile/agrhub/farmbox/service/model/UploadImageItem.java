package mobile.agrhub.farmbox.service.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class UploadImageItem {
    @SerializedName("file_url")
    public String file_url;
    @SerializedName("file_name")
    public String file_name;
    @SerializedName("file_thumbnail")
    public String file_thumbnail;
    @SerializedName("file_size")
    private Long file_size;
    @SerializedName("file_updated_date")
    private Date file_updated_date;

    public UploadImageItem() {
    }
}
