package mobile.agrhub.farmbox.service.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductCategory {
    @SerializedName("category_id")
    public long categoryId;
    @SerializedName("category_name")
    public String categoryName;
    @SerializedName("category_code")
    public String categoryCode;
    @SerializedName("is_active")
    public boolean isActive;
    @SerializedName("category_registered_date")
    public long categoryRegisteredDate;
    @SerializedName("category_updated_date")
    public long categoryUpdatedDate;

    public List<Product> productList;

    public ProductCategory() {
    }
}

