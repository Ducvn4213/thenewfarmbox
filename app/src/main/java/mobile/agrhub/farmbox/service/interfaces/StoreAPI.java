package mobile.agrhub.farmbox.service.interfaces;

import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.model.response.GetProductCategoryResponse;
import mobile.agrhub.farmbox.service.model.response.GetProductResponse;

public interface StoreAPI {
    void getProductCategoryList(APIHelper.Callback<GetProductCategoryResponse> callback);

    void getProductList(APIHelper.Callback<GetProductResponse> callback);

    void postCart(APIHelper.Callback<String> callback);

}
