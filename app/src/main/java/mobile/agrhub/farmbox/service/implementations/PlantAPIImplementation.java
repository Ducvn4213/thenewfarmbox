package mobile.agrhub.farmbox.service.implementations;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.interfaces.PlantAPI;
import mobile.agrhub.farmbox.service.model.Plant;
import mobile.agrhub.farmbox.service.model.PlantBasic;
import mobile.agrhub.farmbox.service.model.response.GetPlantBasicResponse;
import mobile.agrhub.farmbox.service.model.response.GetPlantResponse;
import mobile.agrhub.farmbox.service.network.Config;
import mobile.agrhub.farmbox.service.network.Network;
import mobile.agrhub.farmbox.utils.Utils;

public class PlantAPIImplementation implements PlantAPI {
    Network network = Network.getInstance();

    @Override
    public void getPlants(final APIHelper.Callback<GetPlantResponse> callback) {
        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        String link = Config.GET_PLANTS + Utils.decode(accessToken);
        network.executeGet(link, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                if (response.equalsIgnoreCase("[]")) {
                    callback.onSuccess(null);
                    return;
                }

                Gson gson = new Gson();
                List<Plant> plants = gson.fromJson(response, new TypeToken<List<Plant>>() {
                }.getType());
                GetPlantResponse getPlantResponse = new GetPlantResponse();
                getPlantResponse.plants = plants;
                callback.onSuccess(getPlantResponse);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void getPlantByID(long id, final APIHelper.Callback<Plant> callback) {
        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        String link = Config.GET_PLANT_BY_ID + Utils.decode(accessToken) + "&id=" + Utils.encode(String.valueOf(id));
        network.executeGet(link, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                if (response.equalsIgnoreCase("[]")) {
                    callback.onSuccess(null);
                    return;
                }

                Gson gson = new Gson();
                Plant plant = gson.fromJson(response, Plant.class);
                GetPlantResponse getPlantResponse = new GetPlantResponse();
                callback.onSuccess(plant);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public String getPlanImageByID(long id) {
//        if (plantList == null) {
//            return null;
//        }
//
//        for (Plant p : plantList) {
//            if (p.plant_id == id) {
//                Gson gson = new Gson();
//                List<String> images = gson.fromJson(p.plant_photos, new TypeToken<List<String>>(){}.getType());
//                return  images.get(0);
//            }
//        }
        return null;
    }

    @Override
    public void getPlantListByOffset(int offset, final APIHelper.Callback<GetPlantBasicResponse> callback) {
        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        String link = Config.GET_PLANTS + Utils.decode(accessToken) + "&version=v2&page=" + Utils.encode(String.valueOf(offset));
        network.executeGet(link, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                if (response.equalsIgnoreCase("[]")) {
                    callback.onSuccess(null);
                    return;
                }

                Gson gson = new Gson();
                List<PlantBasic> plants = gson.fromJson(response, new TypeToken<List<PlantBasic>>() {
                }.getType());
                GetPlantBasicResponse getPlantResponse = new GetPlantBasicResponse();
                getPlantResponse.plants = plants;
                //plantList = plants;
                callback.onSuccess(getPlantResponse);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void searchPlant(int page, String lang, String keyword, final APIHelper.Callback<GetPlantBasicResponse> callback) {
        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        StringBuilder builder = new StringBuilder();
        builder.append(Config.GET_PLANTS);
        builder.append(Utils.decode(accessToken));
        builder.append("&version=v2");
        builder.append("&page=").append(Utils.encode(String.valueOf(page)));
        if (null != keyword && !keyword.isEmpty()) {
            builder.append("&language=").append(Utils.encode(lang));
            builder.append("&query=").append(Utils.encode(keyword));
        }
        network.executeGet(builder.toString(), new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                if (response.equalsIgnoreCase("[]")) {
                    callback.onSuccess(null);
                    return;
                }

                Gson gson = new Gson();
                List<PlantBasic> plants = gson.fromJson(response, new TypeToken<List<PlantBasic>>() {
                }.getType());
                GetPlantBasicResponse getPlantResponse = new GetPlantBasicResponse();
                getPlantResponse.plants = plants;
                //plantList = plants;
                callback.onSuccess(getPlantResponse);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public List<Plant> getPlantList() {
        return null;
        //return plantList;
    }
}
