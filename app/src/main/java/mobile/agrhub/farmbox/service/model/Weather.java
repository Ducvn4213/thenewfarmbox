package mobile.agrhub.farmbox.service.model;

import com.google.gson.annotations.SerializedName;

public class Weather {
    @SerializedName("currently")
    public WeatherInfo data;

    public Weather() {
    }
}
