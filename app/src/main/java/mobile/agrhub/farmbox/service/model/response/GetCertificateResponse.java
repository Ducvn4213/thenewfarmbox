package mobile.agrhub.farmbox.service.model.response;

import java.util.List;

import mobile.agrhub.farmbox.service.model.GapCertificate;

public class GetCertificateResponse {
    public List<GapCertificate> certificates;

    public GetCertificateResponse() {
    }
}
