package mobile.agrhub.farmbox.service.model.response;

import java.util.List;

import mobile.agrhub.farmbox.service.model.Product;

public class GetProductResponse {
    public List<Product> products;

    public GetProductResponse() {
    }
}
