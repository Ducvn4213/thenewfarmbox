package mobile.agrhub.farmbox.service.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddDeviceResponse {
    @SerializedName("device_id")
    public long device_id;
    @SerializedName("device_name")
    public long device_name;
    @SerializedName("controllers")
    public List<Controller> controllers;


    public AddDeviceResponse() {
    }
}
