package mobile.agrhub.farmbox.service.model.response;

import java.util.List;

import mobile.agrhub.farmbox.service.model.SensorChartPointItem;

public class ChartDataResponse {
    public List<SensorChartPointItem> data;
    public long percent_liquid_a;
    public long percent_liquid_b;
    public long percent_liquid_c;
    public long percent_liquid_d;
    public long factory_id;

    public ChartDataResponse() {
    }
}
