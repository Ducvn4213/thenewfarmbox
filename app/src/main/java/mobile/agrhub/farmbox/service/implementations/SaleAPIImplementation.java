package mobile.agrhub.farmbox.service.implementations;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.interfaces.SaleAPI;
import mobile.agrhub.farmbox.service.model.Cart;
import mobile.agrhub.farmbox.service.model.FarmCart;
import mobile.agrhub.farmbox.service.model.FarmOrder;
import mobile.agrhub.farmbox.service.model.FarmProduct;
import mobile.agrhub.farmbox.service.model.response.GetFarmOrderResponse;
import mobile.agrhub.farmbox.service.model.response.GetFarmProductResponse;
import mobile.agrhub.farmbox.service.network.Config;
import mobile.agrhub.farmbox.service.network.Network;
import mobile.agrhub.farmbox.service.network.Param;
import mobile.agrhub.farmbox.utils.Utils;

public class SaleAPIImplementation implements SaleAPI {
    Network network = Network.getInstance();

    @Override
    public void createProduct(String name, List<String> avatarList, float weight, int unit, long price, int discount, String description, String fullDescription, final APIHelper.Callback<FarmProduct> callback) {
        Gson gson = new Gson();

        String[] mediaUrl;
        if (avatarList == null || avatarList.size() <= 0) {
            mediaUrl = new String[]{};
        } else {
            mediaUrl = avatarList.toArray(new String[0]);;
        }

        List<Param> params = new ArrayList<>();
        params.add(new Param("product_name", name));
        params.add(new Param("product_avatar", gson.toJson(mediaUrl)));
        params.add(new Param("product_weight", String.valueOf(weight)));
        params.add(new Param("product_unit", String.valueOf(unit)));
        params.add(new Param("product_price", String.valueOf(price)));
        params.add(new Param("product_discount", String.valueOf(discount)));
        params.add(new Param("product_description", description));
        params.add(new Param("product_full_description", fullDescription));

        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        String link = Config.CREATE_PRODUCT + Utils.decode(accessToken);
        network.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                Gson gson = new Gson();
                FarmProduct product = gson.fromJson(response, FarmProduct.class);
                callback.onSuccess(product);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void updateProduct(long id, String name, List<String> avatarList, long price, int discount, String description, String fullDescription, final APIHelper.Callback<FarmProduct> callback) {
        Gson gson = new Gson();

        String[] mediaUrl;
        if (avatarList == null || avatarList.size() <= 0) {
            mediaUrl = new String[]{};
        } else {
            mediaUrl = avatarList.toArray(new String[0]);;
        }

        List<Param> params = new ArrayList<>();
        params.add(new Param("product_id", String.valueOf(id)));
        params.add(new Param("product_name", name));
        params.add(new Param("product_avatar", gson.toJson(mediaUrl)));
        params.add(new Param("product_price", String.valueOf(price)));
        params.add(new Param("product_discount", String.valueOf(discount)));
        params.add(new Param("product_description", description));
        params.add(new Param("product_full_description", fullDescription));

        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        String link = Config.UPDATE_PRODUCT + Utils.decode(accessToken);
        network.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                Gson gson = new Gson();
                FarmProduct product = gson.fromJson(response, FarmProduct.class);
                callback.onSuccess(product);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void getProducts(int page, long userId, boolean verbose, final APIHelper.Callback<GetFarmProductResponse> callback) {
        String accessToken = FarmBoxAppController.getInstance().getAccessToken();

        StringBuilder builder = new StringBuilder();
        builder.append(Config.GET_PRODUCTS);
        builder.append(Utils.decode(accessToken));
        builder.append("&user_id=").append(Utils.encode(userId));
        builder.append("&page=").append(Utils.encode(page + ""));
        builder.append("&verbose=").append(Utils.encode(verbose));

        network.executeGet(builder.toString(), new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                if (response.equalsIgnoreCase("[]")) {
                    callback.onSuccess(null);
                    return;
                }

                GetFarmProductResponse getResponse = new GetFarmProductResponse();

                Gson gson = new Gson();
                List<FarmProduct> products = gson.fromJson(response, new TypeToken<List<FarmProduct>>() {
                }.getType());
                getResponse.farmProducts = products;
                callback.onSuccess(getResponse);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void getProduct(long id, final APIHelper.Callback<FarmProduct> callback) {
        String accessToken = FarmBoxAppController.getInstance().getAccessToken();

        StringBuilder builder = new StringBuilder();
        builder.append(Config.GET_PRODUCT);
        builder.append(Utils.decode(accessToken));
        builder.append("&product_id=").append(Utils.encode(id + ""));

        network.executeGet(builder.toString(), new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                if (response.equalsIgnoreCase("[]")) {
                    callback.onSuccess(null);
                    return;
                }

                Gson gson = new Gson();
                FarmProduct product = gson.fromJson(response, FarmProduct.class);
                callback.onSuccess(product);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void updateProductField(long id, String field, String val, APIHelper.Callback<String> callback) {

    }

    @Override
    public void createOrder(long customerId, List<FarmCart> carts, final APIHelper.Callback<FarmOrder> callback) {
        Gson gson = new Gson();

        List<Cart> postCartData = new ArrayList<>();
        Cart cartItem;
        for (FarmCart item : carts) {
            cartItem = new Cart(item.cartProduct.productId, item.cartItemQuantity);
            postCartData.add(cartItem);
        }

        List<Param> params = new ArrayList<>();
        params.add(new Param("customer_id", String.valueOf(customerId)));
        params.add(new Param("cart", gson.toJson(postCartData)));
        params.add(new Param("currency", "VND"));

        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        String link = Config.CREATE_ORDER + Utils.decode(accessToken);
        network.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                Gson gson = new Gson();
                FarmOrder product = gson.fromJson(response, FarmOrder.class);
                callback.onSuccess(product);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void getOrders(int page, Long userId, Long customerId, Integer state, final APIHelper.Callback<GetFarmOrderResponse> callback) {
        String accessToken = FarmBoxAppController.getInstance().getAccessToken();

        StringBuilder builder = new StringBuilder();
        builder.append(Config.GET_ORDERS);
        builder.append(Utils.decode(accessToken));
        builder.append("&page=").append(Utils.encode(page));
        if (userId != null) {
            builder.append("&user_id=").append(Utils.encode(userId));
        }
        if (customerId != null) {
            builder.append("&customer_id=").append(Utils.encode(customerId));
        }
        if (state != null) {
            builder.append("&order_state=").append(Utils.encode(state));
        }

        network.executeGet(builder.toString(), new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                if (response.equalsIgnoreCase("[]")) {
                    callback.onSuccess(null);
                    return;
                }

                GetFarmOrderResponse getResponse = new GetFarmOrderResponse();

                Gson gson = new Gson();
                List<FarmOrder> orders = gson.fromJson(response, new TypeToken<List<FarmOrder>>() {
                }.getType());
                getResponse.orders = orders;
                callback.onSuccess(getResponse);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void getOrder(long id, final APIHelper.Callback<FarmOrder> callback) {
        String accessToken = FarmBoxAppController.getInstance().getAccessToken();

        StringBuilder builder = new StringBuilder();
        builder.append(Config.GET_ORDER);
        builder.append(Utils.decode(accessToken));
        builder.append("&id=").append(Utils.encode(id));

        network.executeGet(builder.toString(), new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                if (response.equalsIgnoreCase("[]")) {
                    callback.onSuccess(null);
                    return;
                }

                Gson gson = new Gson();
                FarmOrder data = gson.fromJson(response, FarmOrder.class);
                callback.onSuccess(data);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void updateOrderField(long id, String field, String val, final APIHelper.Callback<FarmOrder> callback) {
        List<Param> params = new ArrayList<>();
        params.add(new Param("order_id", String.valueOf(id)));
        params.add(new Param("order_param", field));
        params.add(new Param("order_value", val));

        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        String link = Config.UPDATE_ORDER + Utils.decode(accessToken);
        network.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                Gson gson = new Gson();
                FarmOrder order = gson.fromJson(response, FarmOrder.class);
                callback.onSuccess(order);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void updateCartField(long id, String field, String val, final APIHelper.Callback<FarmCart> callback) {
        List<Param> params = new ArrayList<>();
        params.add(new Param("cart_id", String.valueOf(id)));
        params.add(new Param("cart_param", field));
        params.add(new Param("cart_value", val));

        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        String link = Config.UPDATE_CART + Utils.decode(accessToken);
        network.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                Gson gson = new Gson();
                FarmCart data = gson.fromJson(response, FarmCart.class);
                callback.onSuccess(data);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void createTrace(long cartId, String qrCode, final APIHelper.Callback<String> callback) {
        List<Param> params = new ArrayList<>();
        params.add(new Param("cart_id", String.valueOf(cartId)));
        params.add(new Param("qr_code", qrCode));

        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        String link = Config.CREATE_TRACE_WITH_QR_CODE + Utils.decode(accessToken);
        network.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                callback.onSuccess(response);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void createTrace(long cartId, String fromSerial, String toSerial, final APIHelper.Callback<String> callback) {
        List<Param> params = new ArrayList<>();
        params.add(new Param("cart_id", String.valueOf(cartId)));
        params.add(new Param("qr_sequence_start", fromSerial));
        params.add(new Param("qr_sequence_end", toSerial));
        params.add(new Param("qr_sequence_exclude", ""));

        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        String link = Config.CREATE_TRACE_WITH_SERIAL + Utils.decode(accessToken);
        network.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                callback.onSuccess(response);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }
}
