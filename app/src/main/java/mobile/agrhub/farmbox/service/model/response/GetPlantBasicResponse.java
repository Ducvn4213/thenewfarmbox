package mobile.agrhub.farmbox.service.model.response;

import java.util.List;

import mobile.agrhub.farmbox.service.model.PlantBasic;

public class GetPlantBasicResponse {
    public List<PlantBasic> plants;

    public GetPlantBasicResponse() {
    }
}
