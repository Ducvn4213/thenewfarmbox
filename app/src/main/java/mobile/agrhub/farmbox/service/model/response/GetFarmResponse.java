package mobile.agrhub.farmbox.service.model.response;

import java.util.List;

import mobile.agrhub.farmbox.service.model.Farm;

public class GetFarmResponse {
    public List<Farm> farms;

    public GetFarmResponse() {
    }
}
