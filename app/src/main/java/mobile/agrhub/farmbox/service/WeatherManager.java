package mobile.agrhub.farmbox.service;

import java.util.Date;

import mobile.agrhub.farmbox.service.model.Weather;

public class WeatherManager {
    private Weather weatherInfo;
    private boolean isLoadingWeather;
    private long weatherInfoUpdateDate;

    public WeatherManager() {
        this.isLoadingWeather = false;
        this.weatherInfoUpdateDate = -1;
    }

    public Weather getWeatherInfo() {
        return weatherInfo;
    }

    public void setWeatherInfo(Weather weatherInfo) {
        this.weatherInfo = weatherInfo;
        this.isLoadingWeather = false;
        this.weatherInfoUpdateDate = (new Date()).getTime();
    }

    public boolean isLoadingWeather() {
        return isLoadingWeather;
    }

    public void setLoadingWeather(boolean loadingWeather) {
        isLoadingWeather = loadingWeather;
    }

    public boolean needLoadWeather() {
        if (this.isLoadingWeather) {
            return false;
        } else {
            if (this.weatherInfo == null) {
                return true;
            } else if (this.weatherInfoUpdateDate == -1) {
                return true;
            } else {
                long currentTime = (new Date()).getTime();
                int diffInSecond = (int) ((currentTime - this.weatherInfoUpdateDate) / (1000 * 60));
                if (diffInSecond >= 60) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    public void clean() {
        this.weatherInfo = null;
        this.isLoadingWeather = false;
        this.weatherInfoUpdateDate = -1;
    }
}
