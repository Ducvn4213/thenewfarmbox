package mobile.agrhub.farmbox.service.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Camera implements Serializable {
    @SerializedName("camera_code")
    private String cameraCode;
    @SerializedName("camera_factory_id")
    private int cameraFactoryId;
    @SerializedName("camera_name")
    private String cameraName;
    @SerializedName("camera_registered_date")
    private long cameraRegisteredDate;
    @SerializedName("camera_stream_uri")
    private String cameraStreamUri;
    @SerializedName("camera_thumbnail")
    private String cameraThumbnail;
    @SerializedName("camera_updated_date")
    private long cameraUpdatedDate;
    @SerializedName("device_id")
    private long deviceId;
    @SerializedName("camera_id")
    private long cameraId;

    public Camera() {
    }

    public String getCameraCode() {
        return cameraCode;
    }

    public void setCameraCode(String cameraCode) {
        this.cameraCode = cameraCode;
    }

    public int getCameraFactoryId() {
        return cameraFactoryId;
    }

    public void setCameraFactoryId(int cameraFactoryId) {
        this.cameraFactoryId = cameraFactoryId;
    }

    public String getCameraName() {
        return cameraName;
    }

    public void setCameraName(String cameraName) {
        this.cameraName = cameraName;
    }

    public long getCameraRegisteredDate() {
        return cameraRegisteredDate;
    }

    public void setCameraRegisteredDate(long cameraRegisteredDate) {
        this.cameraRegisteredDate = cameraRegisteredDate;
    }

    public String getCameraStreamUri() {
        return cameraStreamUri;
    }

    public void setCameraStreamUri(String cameraStreamUri) {
        this.cameraStreamUri = cameraStreamUri;
    }

    public long getCameraUpdatedDate() {
        return cameraUpdatedDate;
    }

    public void setCameraUpdatedDate(long cameraUpdatedDate) {
        this.cameraUpdatedDate = cameraUpdatedDate;
    }

    public long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(long deviceId) {
        this.deviceId = deviceId;
    }

    public long getCameraId() {
        return cameraId;
    }

    public void setCameraId(long cameraId) {
        this.cameraId = cameraId;
    }

    public String getCameraThumbnail() {
        return cameraThumbnail;
    }

    public void setCameraThumbnail(String cameraThumbnail) {
        this.cameraThumbnail = cameraThumbnail;
    }
}
