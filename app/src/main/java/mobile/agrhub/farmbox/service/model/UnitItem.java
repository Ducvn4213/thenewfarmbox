package mobile.agrhub.farmbox.service.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UnitItem implements Serializable {
    public static String TEMPERATURE_DOT_C_VALUE = "°C";
    public static String TEMPERATURE_C_VALUE = "C";
    public static String TEMPERATURE_F_VALUE = "F";
    public static String NUTRITION_MILLISIEMENS_VALUE = "mS/cm";
    public static String NUTRITION_PARTS_PER_MILLION_VALUE = "ppm";

    private String key;
    private String title;
    private String value;
    private List<UnitOptionItem> options;

    public UnitItem() {

    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<UnitOptionItem> getOptions() {
        return options;
    }

    public void setOptions(List<UnitOptionItem> options) {
        this.options = options;
    }

    public void add(UnitOptionItem item) {
        if (this.options == null) {
            this.options = new ArrayList<>();
        }
        this.options.add(item);
    }
}