package mobile.agrhub.farmbox.service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mobile.agrhub.farmbox.service.model.Cart;
import mobile.agrhub.farmbox.service.model.Product;
import mobile.agrhub.farmbox.utils.Utils;

public class CartManager {
    private List<Product> productList;
    private Map<String, Integer> productAmount;

    public CartManager() {
        this.productList = new ArrayList<Product>();
        this.productAmount = new HashMap<String, Integer>();
    }

    public int getTotalAmount() {
        int amount = 0;
        for (Integer val : this.productAmount.values()) {
            amount += val;
        }
        return amount;
    }

    public int getAmountOfProduct(long productId) {
        String productIdStr = String.valueOf(productId);
        if (this.productAmount.containsKey(productIdStr)) {
            return this.productAmount.get(productIdStr);
        }
        return 0;
    }

    public long getTotalOriginPrice() {
        long total = 0;

        long productPrice;
        String productIdStr;
        for (Product product : this.productList) {
            productIdStr = String.valueOf(product.productId);

            productPrice = Utils.getLongFromMultiLanguageString(product.productPrice);

            total += (productPrice * this.productAmount.get(productIdStr));
        }

        return total;
    }

    public long getTotalDiscountPrice() {
        long total = 0;

        long productPrice;
        long discountPrice;
        String productIdStr;
        for (Product product : this.productList) {
            productIdStr = String.valueOf(product.productId);

            productPrice = Utils.getLongFromMultiLanguageString(product.productPrice);
            discountPrice = Utils.getLongFromMultiLanguageString(product.productDiscount);
            discountPrice = productPrice - Math.round((productPrice * discountPrice) / 100.0);

            total += (discountPrice * this.productAmount.get(productIdStr));
        }

        return total;
    }

    public void addProduct(Product product, int amount) {
        String productIdStr = String.valueOf(product.productId);
        if (productAmount.containsKey(productIdStr)) {
            Integer currentAmount = productAmount.get(productIdStr);
            currentAmount += amount;

            productAmount.put(productIdStr, currentAmount);
        } else {
            productList.add(product);

            productAmount.put(productIdStr, Integer.valueOf(amount));
        }
    }

    public int getCountProduct() {
        return productList.size();
    }

    public Product getProductAtIndex(int idx) {
        if (idx < productList.size()) {
            return productList.get(idx);
        }
        return null;
    }

    public void plusProduct(String productIdStr) {
        if (productAmount.containsKey(productIdStr)) {
            Integer currentAmount = productAmount.get(productIdStr);
            currentAmount += 1;

            productAmount.put(productIdStr, currentAmount);
        }
    }

    public void minusProduct(String productIdStr) {
        if (productAmount.containsKey(productIdStr)) {
            Integer currentAmount = productAmount.get(productIdStr);
            if (currentAmount > 0) {
                currentAmount -= 1;
            }

            productAmount.put(productIdStr, currentAmount);
        }
    }

    public String getCartJson() {
        List<Cart> cartList = new ArrayList<>();
        Cart cart;

        Integer amount;
        for (String productIdStr : this.productAmount.keySet()) {
            amount = this.productAmount.get(productIdStr);
            if (amount > 0) {
                cart = new Cart(Long.parseLong(productIdStr), amount);
                cartList.add(cart);
            }
        }

        Gson gson = new Gson();
        return gson.toJson(cartList, new TypeToken<List<Cart>>() {
        }.getType());
    }

    public void clearAll() {
        this.productList.clear();
        this.productAmount.clear();
    }
}
