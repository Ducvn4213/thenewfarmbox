package mobile.agrhub.farmbox.service;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.UtilAPI;

public class FarmBoxFirebaseInstanceIdService extends FirebaseInstanceIdService {
    private static final String TAG = "FirebaseIDService";

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        try {
            UtilAPI utilAPI = APIHelper.getUtilAPI();
            utilAPI.subscribeNotification(refreshedToken, new APIHelper.Callback<String>() {
                @Override
                public void onSuccess(String data) {
                    Log.d(TAG, data);
                }

                @Override
                public void onFail(int errorCode, String error) {
                    Log.d(TAG, error);
                }
            });
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }
    }


}
