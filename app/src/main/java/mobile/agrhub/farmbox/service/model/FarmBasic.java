package mobile.agrhub.farmbox.service.model;

import com.google.gson.annotations.SerializedName;

public class FarmBasic {
    @SerializedName("farm_id")
    public long farm_id;
    @SerializedName("farm_name")
    public String farm_name;
    @SerializedName("plant_image")
    public String plant_image;
    @SerializedName("user_id")
    public long user_id;
    @SerializedName("is_active")
    public boolean is_active;

    public FarmBasic() {
    }
}

