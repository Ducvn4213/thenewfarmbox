package mobile.agrhub.farmbox.service.model;

import com.google.gson.annotations.SerializedName;

public class GapCertificate {
    public static final int STATE_REVIEWING = 0;
    public static final int STATE_REJECTED = 1;
    public static final int STATE_REWORK = 2;
    public static final int STATE_APPROVED = 3;

    @SerializedName("farm_id")
    public long farmId;
    @SerializedName("gap_certificate_name")
    public String gapCertificateName;
    @SerializedName("gap_certificate_address")
    public String gapCertificateAddress;
    @SerializedName("gap_certificate_expired_date")
    public long gapCertificateExpiredDate;
    @SerializedName("gap_certificate_issuer")
    public String gapCertificateIssuer;
    @SerializedName("gap_certificate_no")
    public String gapCertificateNo;
    @SerializedName("gap_certificate_owner")
    public String gapCertificateOwner;
    @SerializedName("gap_certificate_product")
    public String gapCertificateProduct;
    @SerializedName("gap_certificate_registered_date")
    public long gapCertificateRegisteredDate;
    @SerializedName("gap_certificate_state")
    public int gapCertificateState;
    @SerializedName("gap_certificate_updated_date")
    public long gapCertificateUpdatedDate;
    @SerializedName("gap_certificate_valid_date")
    public long gapCertificateValidDate;
    @SerializedName("gap_certificate_id")
    public long gapCertificateId;
    @SerializedName("is_active")
    public boolean isActive;
    @SerializedName("gap_certificate_medias")
    public String[] gapCertificateMedia;
    @SerializedName("user_id")
    public long userId;


    public GapCertificate() {
    }
}

