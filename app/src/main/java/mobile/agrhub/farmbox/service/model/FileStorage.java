package mobile.agrhub.farmbox.service.model;

import com.google.gson.annotations.SerializedName;

public class FileStorage {
    public static final int UNKNOWN = 0;
    public static final int BINARY = 1;
    public static final int JPG = 2;
    public static final int JPEG = 3;
    public static final int PNG = 4;
    public static final int BMP = 5;
    public static final int GIF = 6;
    public static final int MP4 = 7;

    @SerializedName("file_extension")
    private String fileExtension;

    @SerializedName("file_name")
    private String fileName;

    @SerializedName("file_size")
    private long fileSize;

    @SerializedName("file_type")
    private int fileType;

    @SerializedName("file_updated_date")
    private long fileUpdatedDate;

    @SerializedName("file_url")
    private String fileUrl;

    @SerializedName("file_id")
    private long fileId;

    @SerializedName("user_id")
    private long userId;

    public FileStorage() {
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public int getFileType() {
        return fileType;
    }

    public void setFileType(int fileType) {
        this.fileType = fileType;
    }

    public long getFileUpdatedDate() {
        return fileUpdatedDate;
    }

    public void setFileUpdatedDate(long fileUpdatedDate) {
        this.fileUpdatedDate = fileUpdatedDate;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public long getFileId() {
        return fileId;
    }

    public void setFileId(long fileId) {
        this.fileId = fileId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
}
