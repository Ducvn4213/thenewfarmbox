package mobile.agrhub.farmbox.service.model;

import com.google.gson.annotations.SerializedName;

public class MultiLanguageObject {
    @SerializedName("vi")
    public String vi;
    @SerializedName("en")
    public String en;

    public MultiLanguageObject() {
    }
}
