package mobile.agrhub.farmbox.service.interfaces;

import android.content.Context;
import android.graphics.Bitmap;

import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.model.User;
import mobile.agrhub.farmbox.service.model.response.VerifyCodeResponse;

public interface UserAPI {
    void updateProfile(String userPhone, String email, String firstName, String lastName, String birth, String gender, String address, APIHelper.Callback<User> callback);

    void updateField(String userPhone, String fieldName, String value, APIHelper.Callback<User> callback);

    void updateAvatar(Bitmap newAvatar, APIHelper.Callback<String> callback);

    void verifyCode(Context context, String phone, String code, APIHelper.Callback<VerifyCodeResponse> callback);

    void getUserInfo(Context context, String token, APIHelper.Callback<User> callback);
}
