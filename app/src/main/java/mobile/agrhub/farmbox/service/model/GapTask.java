package mobile.agrhub.farmbox.service.model;

import com.google.gson.annotations.SerializedName;

public class GapTask {
    @SerializedName("gap_task_id")
    public long gapTaskId;
    @SerializedName("gap_task_title")
    public String gapTaskTitle;
    @SerializedName("gap_task_caption")
    public String gapTaskCaption;
    @SerializedName("is_active")
    public boolean isActive;
    @SerializedName("gap_task_registered_date")
    public long gapTaskRegisteredDate;
    @SerializedName("gap_task_updated_date")
    public long gapTaskUpdatedDate;

    public boolean isChecked = false;

    public GapTask() {
    }
}

