package mobile.agrhub.farmbox.service.implementations;

import android.content.Context;
import android.graphics.Bitmap;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.interfaces.UserAPI;
import mobile.agrhub.farmbox.service.model.User;
import mobile.agrhub.farmbox.service.model.response.VerifyCodeResponse;
import mobile.agrhub.farmbox.service.network.Config;
import mobile.agrhub.farmbox.service.network.Network;
import mobile.agrhub.farmbox.service.network.Param;
import mobile.agrhub.farmbox.utils.Utils;

public class UserAPIImplementation implements UserAPI {
    Network network = Network.getInstance();

    @Override
    public void updateProfile(String userPhone, String email, String firstName, String lastName, String birth, String gender, String address, final APIHelper.Callback<User> callback) {
        FarmBoxAppController app = FarmBoxAppController.getInstance();
        String accessToken = app.getAccessToken();

        List<Param> params = new ArrayList<>();
        params.add(new Param("user_phone", userPhone));
        params.add(new Param("user_first_name", firstName));
        params.add(new Param("user_last_name", lastName));
        params.add(new Param("user_birth_year", birth));
        params.add(new Param("user_gender", gender));
        params.add(new Param("user_email", email));
        if (address != null) {
            params.add(new Param("user_address", address));
        }


        String link = Config.UPDATE_PROFILE + Utils.decode(accessToken);
        network.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                Gson gson = new Gson();
                try {
                    User user = gson.fromJson(response, User.class);

                    callback.onSuccess(user);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    callback.onFail(Network.FATAL_ERROR, "the response is invalid format");
                }
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void updateField(String userPhone, String fieldName, String value, final APIHelper.Callback<User> callback) {
        String accessToken = FarmBoxAppController.getInstance().getAccessToken();

        List<Param> params = new ArrayList<>();
        params.add(new Param("user_phone", userPhone));
        params.add(new Param("user_param", fieldName));
        params.add(new Param("user_value", value));

        String link = Config.UPDATE_PROFILE + Utils.decode(accessToken);
        network.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                Gson gson = new Gson();
                try {
                    User user = gson.fromJson(response, User.class);
                    callback.onSuccess(user);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    callback.onFail(Network.FATAL_ERROR, "the response is invalid format");
                }
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void updateAvatar(Bitmap newAvatar, final APIHelper.Callback<String> callback) {
        Utils.ConvertBitmapToStringTask convertBitmapToStringTask = new Utils.ConvertBitmapToStringTask();
        convertBitmapToStringTask.setBitmap(newAvatar);
        convertBitmapToStringTask.setCallback(new Utils.ConvertBitmapToStringCallback() {
            @Override
            public void onSuccess(String bitmapString) {
                doUpdateAvatar(bitmapString, callback);
            }

            @Override
            public void onFail() {
                callback.onFail(Network.FATAL_ERROR, "The input image is not valid");
            }
        });
        convertBitmapToStringTask.execute();
    }

    @Override
    public void verifyCode(final Context context, String phone, String code, final APIHelper.Callback<VerifyCodeResponse> callback) {
        List<Param> params = new ArrayList<>();
        params.add(new Param("phone", phone));
        params.add(new Param("passcode", code));
        params.add(new Param("user_latitude", "0"));
        params.add(new Param("user_longitude", "0"));

        network.execute(Config.PASS_CODE_VERIFY, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                Gson gson = new Gson();
                try {
                    VerifyCodeResponse verifyCodeResponse = gson.fromJson(response, VerifyCodeResponse.class);

                    callback.onSuccess(verifyCodeResponse);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    callback.onFail(Network.FATAL_ERROR, ex.getMessage());
                }
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void getUserInfo(final Context context, final String token, final APIHelper.Callback<User> callback) {
        String link = Config.GET_USER + Utils.decode(token) + "&id=" + token;

        network.executeGet(link, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                Gson gson = new Gson();
                try {
                    User user = gson.fromJson(response, User.class);

                    callback.onSuccess(user);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    callback.onFail(Network.FATAL_ERROR, ex.getMessage());
                }
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    private void doUpdateAvatar(final String bitmapString, final APIHelper.Callback<String> callback) {
        User user = FarmBoxAppController.getInstance().getUser();
        String accessToken = FarmBoxAppController.getInstance().getAccessToken();

        List<Param> params = new ArrayList<>();
        params.add(new Param("user_phone", user.user_phone));
        params.add(new Param("user_avatar", bitmapString));

        String link = Config.UPDATE_AVATAR + Utils.decode(accessToken);
        network.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                callback.onSuccess(bitmapString);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }
}
