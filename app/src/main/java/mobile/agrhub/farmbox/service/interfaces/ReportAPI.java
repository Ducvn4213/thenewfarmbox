package mobile.agrhub.farmbox.service.interfaces;

import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.model.FarmReport;
import mobile.agrhub.farmbox.service.model.FarmReportList;

public interface ReportAPI {
    void getByFarm(long farmId, APIHelper.Callback<FarmReportList> callback);
    void generateFarmReport(long farmId, int granularity, long startDate
            , long endDate, int format, String lang, APIHelper.Callback<FarmReport> callback);
}
