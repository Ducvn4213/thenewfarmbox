package mobile.agrhub.farmbox.service.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lecong on 3/27/18.
 */

public class KeyValue {
    @SerializedName("id")
    public long id;
    @SerializedName("value")
    public String value;

    public KeyValue() {

    }

    public KeyValue(long id, String val) {
        this.id = id;
        this.value = val;
    }
}
