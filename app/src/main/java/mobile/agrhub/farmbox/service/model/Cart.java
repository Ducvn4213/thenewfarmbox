package mobile.agrhub.farmbox.service.model;

import com.google.gson.annotations.SerializedName;

public class Cart {
    @SerializedName("product_id")
    public long productId;

    @SerializedName("product_quantity")
    public int productQuantity;

    public Cart() {
    }

    public Cart(long productId, int productQuantity) {
        this.productId = productId;
        this.productQuantity = productQuantity;
    }
}
