package mobile.agrhub.farmbox.service.model;

import java.io.Serializable;
import java.util.List;

public class FarmReportList implements Serializable {

    private List<FarmReport> farmReportList;

    public List<FarmReport> getFarmReportList() {
        return farmReportList;
    }

    public void setFarmReportList(List<FarmReport> farmReportList) {
        this.farmReportList = farmReportList;
    }
}
