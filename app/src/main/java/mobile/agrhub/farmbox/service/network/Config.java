package mobile.agrhub.farmbox.service.network;

public class Config {
    public static final String USER_TOKEN_KEY = "USER_TOKEN_KEY";
    public static final String AVATAR_KEY = "AVATAR_KEY";

    private static final String HOST = "https://farmapi.agrhub.com";
//    private static final String HOST = "https://4-dot-farm-care.appspot.com";

    public static final String PASS_CODE_GET = HOST + "/user/passcode/get";
    public static final String PASS_CODE_VERIFY = HOST + "/user/passcode/verify";
    public static final String GET_USER = HOST + "/user/get?access_token=";

    public static final String UPDATE_PROFILE = HOST + "/user/edit?access_token=";
    public static final String UPDATE_AVATAR = HOST + "/user/updateAvatar?access_token=";
    public static final String SAVE_SETTING = HOST + "/profile/editFactory?access_token=";

    public static final String GET_FARM = HOST + "/farm/getFarmByUser?access_token=";
    public static final String GET_FARM_BY_ID = HOST + "/farm/get?access_token=";
    public static final String GET_PLANTS = HOST + "/plant/gets?access_token=";
    public static final String GET_PLANT_BY_ID = HOST + "/plant/get?access_token=";
    public static final String GET_SENSOR = HOST + "/sensor/get/series?access_token=";
    public static final String GET_NATION = HOST + "/nation/gets";
    public static final String GET_SETTING = HOST + "/profile/getFactoryByFarm?access_token=";
    public static final String GET_FARM_SETTING = HOST + "/profile/getFactoryByFarm?version=v3&access_token=";
    public static final String GET_FARM_SESSION_SETTING = HOST + "/profile/getFactory?version=v2&access_token=";
//    public static final String GET_FARM_SETTING = "https://4-dot-farm-care.appspot.com/profile/getFactory?version=v2&access_token=";

    public static final String ADD_SEASON = HOST + "/season/set?access_token=";
    public static final String ADD_FARM = HOST + "/farm/set?access_token=";
    public static final String ADD_DEVICE = HOST + "/device/activeDevice?access_token=";
    public static final String ADD_CONTROLLER = HOST + "/controller/edit?access_token=";
    public static final String EDIT_CONTROLLER = HOST + "/controller/edit?access_token=";
    public static final String ADD_CONTAINER = HOST + "/farm/activeContainer?access_token=";

    public static final String EDIT_FARM = HOST + "/farm/edit?access_token=";
    public static final String EDIT_DEVICE_NAME = HOST + "/device/edit?access_token=";

    public static final String ACTIVATE_CONTAINER = HOST + "/farm/activeContainer?access_token=";
    public static final String TOGGLE_CONTROLLER = HOST + "/controller/toggle?access_token=";

    public static final String FILE_API_UPLOAD_IMAGE = HOST + "/file/upload/image?access_token=";
    public static final String FILE_API_UPLOAD_VIDEO = HOST + "/file/upload/video?access_token=";

    public static final String GAP_API_PATH_FILTER = HOST + "/gapdiary/getByFilter";
    public static final String GAP_API_PATH_SET = HOST + "/gapdiary/set?access_token=";
    public static final String GAP_TASK_API_GETS = HOST + "/gaptask/gets?access_token=";

    public static final String PRODUCT_CATEGORY_API_GETS = HOST + "/farmbox/category/gets?access_token=";
    public static final String PRODUCT_API_GETS = HOST + "/farmbox/product/gets?access_token=";

    public static final String ORDER_API_POST = HOST + "/order/farmbox/mobilestore?access_token=";

    /**
     * Diary API
     */
    public static final String DIARY_API_PATH_FILTER = HOST + "/gapdiary/getByFilter";
    public static final String DIARY_API_PATH_SET = HOST + "/gapdiary/set?access_token=";

    /**
     * Notification
     */
    public static final String SUBSCRIBE_NOTIFICATION = HOST + "/cloudmessage/subscribeNotification?access_token=";
    public static final String UNSUBSCRIBE_NOTIFICATION = HOST + "/cloudmessage/unsubscribeNotification";

    /**
     * Device API
     */
    public static final String EDIT_DEVICE = HOST + "/device/edit?access_token=";

    /**
     * Report API
     */
    public static final String GET_REPORT = HOST + "/report/getByFarm?access_token=";
    public static final String GENERATE_REPORT = HOST + "/report/farm?access_token=";

    /**
     * Sale Product API
     */
    public static final String CREATE_PRODUCT = HOST + "/sale/product/set?access_token=";
    public static final String GET_PRODUCTS = HOST + "/sale/product/gets?access_token=";
    public static final String GET_PRODUCT = HOST + "/sale/product/get?access_token=";
    public static final String UPDATE_PRODUCT = HOST + "/sale/product/edit?access_token=";

    /**
     * Order API
     */
    public static final String CREATE_ORDER = HOST + "/sale/order/set?access_token=";
    public static final String GET_ORDERS = HOST + "/sale/order/gets?access_token=";
    public static final String GET_ORDER = HOST + "/sale/order/get?access_token=";
    public static final String UPDATE_ORDER = HOST + "/sale/order/edit?access_token=";
    public static final String UPDATE_CART = HOST + "/sale/order/cart/edit?access_token=";

    /**
     * GAP Certificate API
     */
    public static final String CREATE_CERTIFICATE = HOST + "/gapcertificate/set?access_token=";
    public static final String GET_CERTIFICATES = HOST + "/gapcertificate/getByUser?access_token=";
    public static final String GET_CERTIFICATE = HOST + "/gapcertificate/get?access_token=";
    public static final String UPDATE_CERTIFICATE = HOST + "/gapcertificate/update?access_token=";

    /**
     * Trace API
     */
    public static final String CREATE_TRACE_WITH_QR_CODE = HOST + "/trace/cart/set?access_token=";
    public static final String CREATE_TRACE_WITH_SERIAL = HOST + "/trace/cart/setBatch?access_token=";
}
