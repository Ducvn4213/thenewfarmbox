package mobile.agrhub.farmbox.service.implementations;

import mobile.agrhub.farmbox.service.interfaces.DeviceAPI;
import mobile.agrhub.farmbox.service.interfaces.FarmAPI;
import mobile.agrhub.farmbox.service.interfaces.GapAPI;
import mobile.agrhub.farmbox.service.interfaces.PlantAPI;
import mobile.agrhub.farmbox.service.interfaces.ReportAPI;
import mobile.agrhub.farmbox.service.interfaces.SaleAPI;
import mobile.agrhub.farmbox.service.interfaces.StoreAPI;
import mobile.agrhub.farmbox.service.interfaces.UserAPI;
import mobile.agrhub.farmbox.service.interfaces.UtilAPI;

public class APIHelper {
    public interface Callback<E> {
        void onSuccess(E data);

        void onFail(int errorCode, String error);
    }

    private static FarmAPI farmAPI;
    private static PlantAPI plantAPI;
    private static UserAPI userAPI;
    private static UtilAPI utilAPI;
    private static GapAPI gapAPI;
    private static StoreAPI storeAPI;
    private static DeviceAPI deviceAPI;
    private static ReportAPI reportAPI;
    private static SaleAPI saleAPI;

    private static void initFarmAPI() {
        farmAPI = new FarmAPIImplementation();
    }

    private static void initPlantAPI() {
        plantAPI = new PlantAPIImplementation();
    }

    private static void initUserAPI() {
        userAPI = new UserAPIImplementation();
    }

    private static void initUtilAPI() {
        utilAPI = new UtilAPIImplementation();
    }

    private static void initGapAPI() {
        gapAPI = new GapAPIImplementation();
    }

    private static void initStoreAPI() {
        storeAPI = new StoreAPIImplementation();
    }

    private static void initDeviceAPI() {
        deviceAPI = new DeviceAPIImplementation();
    }

    private static void initReportAPI() {
        reportAPI = new ReportAPIImplementation();
    }

    private static void initSaleAPI() {
        saleAPI = new SaleAPIImplementation();
    }


    public static FarmAPI getFarmAPI() {
        if (farmAPI == null) {
            initFarmAPI();
        }

        return farmAPI;
    }

    public static PlantAPI getPlantAPI() {
        if (plantAPI == null) {
            initPlantAPI();
        }

        return plantAPI;
    }

    public static UserAPI getUserAPI() {
        if (userAPI == null) {
            initUserAPI();
        }

        return userAPI;
    }

    public static UtilAPI getUtilAPI() {
        if (utilAPI == null) {
            initUtilAPI();
        }

        return utilAPI;
    }

    public static GapAPI getGapAPI() {
        if (gapAPI == null) {
            initGapAPI();
        }

        return gapAPI;
    }

    public static StoreAPI getStoreAPI() {
        if (storeAPI == null) {
            initStoreAPI();
        }

        return storeAPI;
    }

    public static DeviceAPI getDeviceAPI() {
        if (deviceAPI == null) {
            initDeviceAPI();
        }

        return deviceAPI;
    }

    public static ReportAPI getReportAPI() {
        if (reportAPI == null) {
            initReportAPI();
        }

        return reportAPI;
    }

    public static SaleAPI getSaleAPI() {
        if (saleAPI == null) {
            initSaleAPI();
        }

        return saleAPI;
    }
}
