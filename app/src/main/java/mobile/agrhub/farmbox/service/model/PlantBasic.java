package mobile.agrhub.farmbox.service.model;

import com.google.gson.annotations.SerializedName;

public class PlantBasic {
    @SerializedName("plant_id")
    public long plant_id;
    @SerializedName("plant_photo")
    public String plant_photo;
    @SerializedName("plant_name")
    public String plant_name;
    @SerializedName("plant_scientific_name")
    public String plant_scientific_name;
    @SerializedName("is_active")
    public Boolean is_active;

    public PlantBasic() {
    }
}
