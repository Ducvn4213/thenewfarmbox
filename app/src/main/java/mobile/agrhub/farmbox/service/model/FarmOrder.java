package mobile.agrhub.farmbox.service.model;

import android.content.Context;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.utils.Utils;

public class FarmOrder {
    public static final int STATE_UNKNOWN = -2;
    public static final int STATE_CANCEL = -1;
    public static final int STATE_NEW = 0;
    public static final int STATE_PROCESS = 1;
    public static final int STATE_DELIVERY = 2;
    public static final int STATE_RETURN = 3;
    public static final int STATE_FINISH = 4;

    @SerializedName("order_id")
    public long orderId;
    @SerializedName("order_amount")
    public int orderAmount;
    @SerializedName("order_products")
    public String orderProducts;
    @SerializedName("order_currency")
    public String orderCurrency;
    @SerializedName("order_note")
    public String orderNote;
    @SerializedName("order_number")
    public String orderNumber;
    @SerializedName("order_registered_date")
    public long orderRegisteredDate;
    @SerializedName("order_state")
    public int orderState;
    @SerializedName("order_updated_date")
    public long orderUpdatedDate;
    @SerializedName("user_id")
    public long userId;
    @SerializedName("customer_id")
    public long customerId;
    @SerializedName("carts")
    public List<FarmCart> carts;


    public FarmOrder() {
    }

    public String displayOrderNumber() {
        return "#" + orderNumber;
    }

    public String displayStatus(Context context) {
        switch (orderState) {
            case STATE_CANCEL:
                return context.getString(R.string.order_cancel);
            case STATE_NEW:
                return context.getString(R.string.order_new);
            case STATE_PROCESS:
                return context.getString(R.string.order_process);
            case STATE_DELIVERY:
                return context.getString(R.string.order_delivery);
            case STATE_RETURN:
                return context.getString(R.string.order_return);
            case STATE_FINISH:
                return context.getString(R.string.order_finish);
            default:
                return "";
        }
    }

    public String displayOrderDate() {
        return Utils.getDate(orderRegisteredDate, "yyyy/MM/dd HH:mm");
    }

    public String displayUpdateDate() {
        return Utils.getDate(orderUpdatedDate, "yyyy/MM/dd HH:mm");
    }

    public int totalItem() {
        int total = 0;
        if (carts != null && carts.size() > 0) {
            for (FarmCart item : carts) {
                total += item.cartItemQuantity;
            }
        }
        return total;
    }

    public String displayTotalItem() {
        return Utils.i2s(totalItem());
    }

    public long subTotalPrice() {
        long total = 0;
        if (carts != null && carts.size() > 0) {
            for (FarmCart item : carts) {
                if (item.cartProduct != null) {
                    total += (item.cartItemQuantity * item.cartProduct.price());
                }
            }
        }
        return total;
    }

    public String displaySubTotalPrice() {
        return Utils.formatPriceVND(subTotalPrice());
    }

    public boolean canControl() {
        return orderState == STATE_NEW || orderState == STATE_PROCESS || orderState == STATE_DELIVERY || orderState == STATE_RETURN;
    }
}

