package mobile.agrhub.farmbox.service.model;

import java.io.Serializable;

import mobile.agrhub.farmbox.utils.Utils;

public class SerialPackage implements Serializable {
    public static final int TYPE_CODE = 0;
    public static final int TYPE_SERIAL_SINGLE = 1;
    public static final int TYPE_SERIAL_FROM_TO = 2;

    public int type;
    public String code;
    public String fromSerial;
    public String toSerial;
    public int quantity;

    public SerialPackage() {

    }

    public SerialPackage(String code) {
        this.type = TYPE_CODE;
        this.code = code;
        this.quantity = 1;
    }

    public SerialPackage(String from, String to) {
        this.type = TYPE_SERIAL_FROM_TO;
        this.fromSerial = from;
        this.toSerial = to;
        this.quantity = Utils.sizeOfSerialNumber(from, to);
    }

    public SerialPackage(String from, String to, int quantity) {
        this.type = TYPE_SERIAL_FROM_TO;
        this.fromSerial = from;
        this.toSerial = to;
        this.quantity = quantity;
    }
}