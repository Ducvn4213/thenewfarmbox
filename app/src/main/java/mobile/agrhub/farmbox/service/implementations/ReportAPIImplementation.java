package mobile.agrhub.farmbox.service.implementations;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.interfaces.ReportAPI;
import mobile.agrhub.farmbox.service.model.FarmReport;
import mobile.agrhub.farmbox.service.model.FarmReportList;
import mobile.agrhub.farmbox.service.network.Config;
import mobile.agrhub.farmbox.service.network.Network;
import mobile.agrhub.farmbox.utils.Utils;

public class ReportAPIImplementation implements ReportAPI {
    Network network = Network.getInstance();

    @Override
    public void getByFarm(long farmId, final APIHelper.Callback<FarmReportList> callback) {
        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        String link = Config.GET_REPORT + Utils.decode(accessToken);
        link = link + "&id=" + Utils.encode(String.valueOf(farmId));
        network.executeGet(link, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                if (response.equalsIgnoreCase("[]")) {
                    callback.onSuccess(null);
                    return;
                }

                Gson gson = new Gson();
                List<FarmReport> farmReportList = gson.fromJson(response, new TypeToken<List<FarmReport>>() {
                }.getType());
                FarmReportList responseData = new FarmReportList();
                responseData.setFarmReportList(farmReportList);
                callback.onSuccess(responseData);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void generateFarmReport(long farmId, int granularity, long startDate
            , long endDate, int format, String lang
            , final APIHelper.Callback<FarmReport> callback) {
        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        StringBuilder builder = new StringBuilder();
        builder.append(Config.GENERATE_REPORT).append(Utils.decode(accessToken));
        builder.append("&id=").append(Utils.encode(String.valueOf(farmId)));
        builder.append("&granularity=").append(Utils.encode(String.valueOf(granularity)));
        builder.append("&start_date=").append(Utils.encode(String.valueOf(startDate)));
        builder.append("&end_date=").append(Utils.encode(String.valueOf(endDate)));
        builder.append("&interval=").append(Utils.encode("1"));
        builder.append("&format=").append(Utils.encode(String.valueOf(format)));
        builder.append("&lang=").append(lang);
        network.executeGet(builder.toString(), new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                if (response.equalsIgnoreCase("[]")) {
                    callback.onSuccess(null);
                    return;
                }

                Gson gson = new Gson();
                FarmReport farmReport = gson.fromJson(response, FarmReport.class);
                callback.onSuccess(farmReport);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }
}
