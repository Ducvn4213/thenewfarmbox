package mobile.agrhub.farmbox.service.model;

import android.content.Context;

import com.google.gson.annotations.SerializedName;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.utils.Constants;
import mobile.agrhub.farmbox.utils.Utils;

public class Sensor {
    public static final int UNKNOWN_SENSOR = 0;
    public static final int LIGHT_SENSOR = 1;
    public static final int AIR_TEMPERATURE_SENSOR = 2;
    public static final int AIR_HUMIDITY_SENSOR = 3;
    public static final int SOIL_TEMPERATURE_SENSOR = 4;
    public static final int SOIL_HUMIDITY_SENSOR = 5;
    public static final int SOIL_EC_SENSOR = 6;
    public static final int SOIL_PH_SENSOR = 7;
    public static final int WATER_TEMPERATURE_SENSOR = 8;
    public static final int WATER_EC_SENSOR = 9;
    public static final int WATER_PH_SENSOR = 10;
    public static final int WATER_ORP_SENSOR = 11;
    public static final int BATTERY_SENSOR = 12;
    public static final int CO2_SENSOR = 13;
    public static final int WATER_LEVEL_SENSOR = 14;
    public static final int WATER_LEAK_SENSOR = 15;
    public static final int ERROR_SENSOR = 16;
    public static final int UV_SENSOR = 17;
    public static final int PRESSURE_SENSOR = 18;
    public static final int SOUND_SENSOR = 19;
    public static final int VOC_SENSOR = 20;
    public static final int MAGNETIC_SENSOR = 21;

    @SerializedName("sensor_id")
    public long sensor_id;
    @SerializedName("sensor_type")
    public long sensor_type;
    @SerializedName("sensor_value")
    public double sensor_value;
    @SerializedName("is_error")
    public Boolean is_error;
    @SerializedName("sensor_registered_date")
    public long sensor_registered_date;
    @SerializedName("sensor_updated_date")
    public long sensor_updated_date;

    public Sensor() {
    }

    public String displayValue() {
        FarmBoxAppController app = FarmBoxAppController.getInstance();
        double valFloat = this.sensor_value;
        long tmpLongVal;
        switch ((int)this.sensor_type) {
            case AIR_TEMPERATURE_SENSOR:
            case SOIL_TEMPERATURE_SENSOR:
            case WATER_TEMPERATURE_SENSOR:
                tmpLongVal = Math.round(this.sensor_value);
                tmpLongVal = app.getTemperatureValue(tmpLongVal, UnitItem.TEMPERATURE_C_VALUE);
                return String.valueOf(tmpLongVal);
            case SOIL_EC_SENSOR:
                valFloat = app.getNutritionValue(valFloat, UnitItem.NUTRITION_PARTS_PER_MILLION_VALUE);
                return Utils.f2s(valFloat);
            case WATER_EC_SENSOR:
                valFloat = app.getNutritionValue(valFloat, UnitItem.NUTRITION_MILLISIEMENS_VALUE);
                return Utils.f2s(valFloat);
            default:
                return Utils.f2s(valFloat);
        }
    }

    public String displayUnit(int deviceType) {
        FarmBoxAppController app = FarmBoxAppController.getInstance();
        switch ((int)this.sensor_type) {
            case AIR_TEMPERATURE_SENSOR:
            case SOIL_TEMPERATURE_SENSOR:
            case WATER_TEMPERATURE_SENSOR:
                return app.getTemperatureUnit();
            case LIGHT_SENSOR:
                return Constants.DEF_LIGHT_SENSOR_UNIT;
            case AIR_HUMIDITY_SENSOR:
                return Constants.DEF_HUMIDITY_SENSOR_UNIT;
            case SOIL_HUMIDITY_SENSOR:
                return Constants.DEF_HUMIDITY_SENSOR_UNIT;
            case SOIL_EC_SENSOR:
            case WATER_EC_SENSOR:
                return app.getNutritionUnit();
            case SOIL_PH_SENSOR:
                return Constants.DEF_PH_SENSOR_UNIT;
            case WATER_PH_SENSOR:
                return Constants.DEF_PH_SENSOR_UNIT;
            case WATER_ORP_SENSOR:
                return "";
            case BATTERY_SENSOR:
                return Constants.DEF_HUMIDITY_SENSOR_UNIT;
            case CO2_SENSOR:
                return Constants.DEF_CO2_SENSOR_UNIT;
            case WATER_LEVEL_SENSOR:
                if (deviceType == Device.TYPE_SENSE_WATER_LEVEL) {
                    return Constants.DEF_WATER_LEVEL_SENSOR_CM_UNIT;
                } else {
                    return Constants.DEF_WATER_LEVEL_SENSOR_UNIT;
                }
            case WATER_LEAK_SENSOR:
                return "";
            case ERROR_SENSOR:
                return "";
            case UV_SENSOR:
                return Constants.DEF_UV_SENSOR_UNIT;
            case PRESSURE_SENSOR:
                return Constants.DEF_PRESSURE_SENSOR_UNIT;
            case SOUND_SENSOR:
                return Constants.DEF_SOUND_SENSOR_UNIT;
            case VOC_SENSOR:
                return Constants.DEF_VOC_SENSOR_UNIT;
            case MAGNETIC_SENSOR:
                return Constants.DEF_MAGNETIC_SENSOR_UNIT;
            default:
                return "";
        }
    }

    public String shortName(Context context) {
        FarmBoxAppController app = FarmBoxAppController.getInstance();
        switch ((int)this.sensor_type) {
            case LIGHT_SENSOR:
                return context.getString(R.string.sensor_short_name_light);
            case AIR_TEMPERATURE_SENSOR:
                return context.getString(R.string.sensor_short_name_air_temp);
            case AIR_HUMIDITY_SENSOR:
                return context.getString(R.string.sensor_short_name_air_humi);
            case SOIL_TEMPERATURE_SENSOR:
                return context.getString(R.string.sensor_short_name_soil_temp);
            case SOIL_HUMIDITY_SENSOR:
                return context.getString(R.string.sensor_short_name_soil_humi);
            case SOIL_EC_SENSOR:
            case WATER_EC_SENSOR:
                if (app.isNutritionPPMUnit()) {
                    return context.getString(R.string.sensor_short_name_nutrition_tds);
                } else {
                    return context.getString(R.string.sensor_short_name_nutrition_ec);
                }
            case SOIL_PH_SENSOR:
                return context.getString(R.string.sensor_short_name_soil_ph);
            case WATER_TEMPERATURE_SENSOR:
                return context.getString(R.string.sensor_short_name_water_temp);
            case WATER_PH_SENSOR:
                return context.getString(R.string.sensor_short_name_water_ph);
            case WATER_ORP_SENSOR:
                return context.getString(R.string.sensor_short_name_water_orp);
            case BATTERY_SENSOR:
                return context.getString(R.string.sensor_short_name_battery);
            case CO2_SENSOR:
                return context.getString(R.string.sensor_short_name_co2);
            case WATER_LEVEL_SENSOR:
                return context.getString(R.string.sensor_short_name_water_level);
            case WATER_LEAK_SENSOR:
                return context.getString(R.string.sensor_short_name_water_leak);
            case ERROR_SENSOR:
                return context.getString(R.string.sensor_short_name_error);
            case UV_SENSOR:
                return context.getString(R.string.sensor_short_name_uv);
            case PRESSURE_SENSOR:
                return context.getString(R.string.sensor_short_name_pressure);
            case SOUND_SENSOR:
                return context.getString(R.string.sensor_short_name_sound);
            case VOC_SENSOR:
                return context.getString(R.string.sensor_short_name_voc);
            case MAGNETIC_SENSOR:
                return context.getString(R.string.sensor_short_name_magnetic);
            default:
                return context.getString(R.string.sensor_short_name_error);
        }
    }

    public static String displayName(Context context, int sensorType) {
        FarmBoxAppController app = FarmBoxAppController.getInstance();
        switch (sensorType) {
            case LIGHT_SENSOR:
                return context.getString(R.string.sensor_light_name);
            case AIR_TEMPERATURE_SENSOR:
                return context.getString(R.string.sensor_air_temp_name);
            case AIR_HUMIDITY_SENSOR:
                return context.getString(R.string.sensor_air_humi_name);
            case SOIL_TEMPERATURE_SENSOR:
                return context.getString(R.string.sensor_soil_temp_name);
            case SOIL_HUMIDITY_SENSOR:
                return context.getString(R.string.sensor_soil_humi_name);
            case SOIL_EC_SENSOR:
                if (app.isNutritionPPMUnit()) {
                    return context.getString(R.string.sensor_soil_nutrition_tds);
                } else {
                    return context.getString(R.string.sensor_soil_nutrition_ec);
                }
            case SOIL_PH_SENSOR:
                return context.getString(R.string.sensor_soil_ph_name);
            case WATER_TEMPERATURE_SENSOR:
                return context.getString(R.string.sensor_water_temp_name);
            case WATER_EC_SENSOR:
                if (app.isNutritionPPMUnit()) {
                    return context.getString(R.string.sensor_water_nutrition_tds);
                } else {
                    return context.getString(R.string.sensor_water_nutrition_ec);
                }
            case WATER_PH_SENSOR:
                return context.getString(R.string.sensor_water_ph_name);
            case WATER_ORP_SENSOR:
                return context.getString(R.string.sensor_water_orp_name);
            case BATTERY_SENSOR:
                return context.getString(R.string.sensor_battery_name);
            case CO2_SENSOR:
                return context.getString(R.string.sensor_co2_name);
            case WATER_LEVEL_SENSOR:
                return context.getString(R.string.sensor_water_level_name);
            case WATER_LEAK_SENSOR:
                return context.getString(R.string.sensor_water_leak_name);
            case ERROR_SENSOR:
                return context.getString(R.string.sensor_error_name);
            case UV_SENSOR:
                return context.getString(R.string.sensor_uv_name);
            case PRESSURE_SENSOR:
                return context.getString(R.string.sensor_pressure_name);
            case SOUND_SENSOR:
                return context.getString(R.string.sensor_sound_name);
            case VOC_SENSOR:
                return context.getString(R.string.sensor_voc_name);
            case MAGNETIC_SENSOR:
                return context.getString(R.string.sensor_magnetic_name);
            default:
                return context.getString(R.string.sensor_error_name);
        }
    }
}
