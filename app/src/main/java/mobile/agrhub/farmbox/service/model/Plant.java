package mobile.agrhub.farmbox.service.model;

import com.google.gson.annotations.SerializedName;

public class Plant {
    @SerializedName("plant_id")
    public long plant_id;
    @SerializedName("plant_photos")
    public String plant_photos;
    @SerializedName("plant_name")
    public String plant_name;
    @SerializedName("plant_scientific_name")
    public String plant_scientific_name;
    @SerializedName("plant_genus")
    public String plant_genus;
    @SerializedName("plant_species")
    public String plant_species;
    @SerializedName("plant_variety")
    public String plant_variety;
    @SerializedName("plant_description")
    public String plant_description;
    @SerializedName("plant_interesting_fact")
    public String plant_interesting_fact;
    @SerializedName("plant_type")
    public String plant_type;
    @SerializedName("plant_lifetime")
    public String plant_lifetime;
    @SerializedName("plant_bloom_color")
    public String plant_bloom_color;
    @SerializedName("plant_leaf_color")
    public String plant_leaf_color;
    @SerializedName("plant_bloom_season")
    public String plant_bloom_season;
    @SerializedName("plant_shape")
    public String plant_shape;
    @SerializedName("plant_height_range")
    public String plant_height_range;
    @SerializedName("plant_spread_range")
    public String plant_spread_range;
    @SerializedName("plant_watering")
    public long plant_watering;
    @SerializedName("plant_sunlight")
    public long plant_sunlight;
    @SerializedName("plant_temperatures")
    public String plant_temperatures;
    @SerializedName("plant_fertilizer")
    public long plant_fertilizer;
    @SerializedName("plant_special_feature")
    public String plant_special_feature;
    @SerializedName("plant_planting")
    public String plant_planting;
    @SerializedName("plant_growth")
    public String plant_growth;
    @SerializedName("plant_blooming")
    public String plant_blooming;
    @SerializedName("plant_soil_irrigation")
    public String plant_soil_irrigation;
    @SerializedName("plant_pruning")
    public String plant_pruning;
    @SerializedName("plant_pest")
    public String plant_pest;
    @SerializedName("plant_growth_climate")
    public String plant_growth_climate;
    @SerializedName("is_active")
    public Boolean is_active;
    @SerializedName("plant_registered_date")
    public long plant_registered_date;
    @SerializedName("plant_updated_date")
    public long plant_updated_date;

    public Plant() {
    }

    public Plant clone() {
        Plant plant = new Plant();
        plant.plant_id = this.plant_id;
        plant.plant_photos = this.plant_photos;
        plant.plant_name = this.plant_name;
        plant.plant_scientific_name = this.plant_scientific_name;
        plant.plant_genus = this.plant_genus;
        plant.plant_species = this.plant_species;
        plant.plant_variety = this.plant_variety;
        plant.plant_description = this.plant_description;
        plant.plant_interesting_fact = this.plant_interesting_fact;
        plant.plant_type = this.plant_type;
        plant.plant_lifetime = this.plant_lifetime;
        plant.plant_bloom_color = this.plant_bloom_color;
        plant.plant_leaf_color = this.plant_leaf_color;
        plant.plant_bloom_season = this.plant_bloom_season;
        plant.plant_shape = this.plant_shape;
        plant.plant_height_range = this.plant_height_range;
        plant.plant_spread_range = this.plant_spread_range;
        plant.plant_watering = this.plant_watering;
        plant.plant_sunlight = this.plant_sunlight;
        plant.plant_temperatures = this.plant_temperatures;
        plant.plant_fertilizer = this.plant_fertilizer;
        plant.plant_special_feature = this.plant_special_feature;
        plant.plant_planting = this.plant_planting;
        plant.plant_growth = this.plant_growth;
        plant.plant_blooming = this.plant_blooming;
        plant.plant_soil_irrigation = this.plant_soil_irrigation;
        plant.plant_pruning = this.plant_pruning;
        plant.plant_pest = this.plant_pest;
        plant.plant_growth_climate = this.plant_growth_climate;
        plant.is_active = this.is_active;
        plant.plant_registered_date = this.plant_registered_date;
        plant.plant_updated_date = this.plant_updated_date;

        return plant;
    }
}
