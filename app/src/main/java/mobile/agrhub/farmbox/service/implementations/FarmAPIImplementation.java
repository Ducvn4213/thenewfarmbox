package mobile.agrhub.farmbox.service.implementations;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.interfaces.FarmAPI;
import mobile.agrhub.farmbox.service.model.Farm;
import mobile.agrhub.farmbox.service.model.FarmBasic;
import mobile.agrhub.farmbox.service.model.FarmSetting;
import mobile.agrhub.farmbox.service.model.response.GetFarmBasicResponse;
import mobile.agrhub.farmbox.service.model.response.GetFarmResponse;
import mobile.agrhub.farmbox.service.network.Config;
import mobile.agrhub.farmbox.service.network.Network;
import mobile.agrhub.farmbox.service.network.Param;
import mobile.agrhub.farmbox.utils.Utils;

public class FarmAPIImplementation implements FarmAPI {
    Network network = Network.getInstance();

    @Override
    public void getAllFarmBasic(boolean isFull, final APIHelper.Callback<GetFarmBasicResponse> callback) {
        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        String link = Config.GET_FARM + Utils.decode(accessToken) + "&id=" + accessToken + "&version=v2";
        if (isFull) {
            link += "&verbose=true";
        }

        network.executeGet(link, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                if (response.equalsIgnoreCase("[]")) {
                    callback.onSuccess(null);
                    return;
                }

                Gson gson = new Gson();
                List<FarmBasic> farmBasics = gson.fromJson(response, new TypeToken<List<FarmBasic>>() {
                }.getType());

                GetFarmBasicResponse getFarmBasicResponse = new GetFarmBasicResponse();
                getFarmBasicResponse.farms = farmBasics;
                callback.onSuccess(getFarmBasicResponse);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void getFarmByID(long id, final APIHelper.Callback<Farm> callback) {
        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        String link = Config.GET_FARM_BY_ID + Utils.decode(accessToken) + "&id=" + Utils.encode(String.valueOf(id));

        network.executeGet(link, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                Gson gson = new Gson();
                Farm farm = gson.fromJson(response, Farm.class);

                callback.onSuccess(farm);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void getFarm(final APIHelper.Callback<GetFarmResponse> callback) {
        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        String link = Config.GET_FARM + Utils.decode(accessToken) + "&id=" + accessToken;
        network.executeGet(link, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                if (response.equalsIgnoreCase("[]")) {
                    callback.onSuccess(null);
                    return;
                }

                Gson gson = new Gson();
                List<Farm> farms = gson.fromJson(response, new TypeToken<List<Farm>>() {
                }.getType());

                List<Farm> reFarms = new ArrayList<Farm>();
                for (Farm f : farms) {
                    reFarms.add(f);
                }

                GetFarmResponse getFarmResponse = new GetFarmResponse();
                getFarmResponse.farms = reFarms;
                callback.onSuccess(getFarmResponse);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void addNewFarm(String name, String address, String acreage, String type, String lat, String lon, final APIHelper.Callback<Farm> callback) {
        List<Param> params = new ArrayList<>();
        params.add(new Param("farm_name", name));
        params.add(new Param("farm_address", address));
        params.add(new Param("farm_acreage", acreage));
        params.add(new Param("farm_type", type));
        params.add(new Param("farm_latitude", lat));
        params.add(new Param("farm_longitude", lon));

        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        String link = Config.ADD_FARM + Utils.decode(accessToken);
        network.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                Gson gson = new Gson();
                Farm farm = gson.fromJson(response, Farm.class);
                callback.onSuccess(farm);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void editFarmField(String farmId, String field, String val, final APIHelper.Callback<String> callback) {
        List<Param> params = new ArrayList<>();
        params.add(new Param("farm_id", farmId));
        params.add(new Param("farm_param", field));
        params.add(new Param("farm_value", val));

        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        String link = Config.EDIT_FARM + Utils.decode(accessToken);
        network.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                callback.onSuccess(response);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void editFarm(String name, String address, String acreage, String lat, String lon, final APIHelper.Callback<String> callback) {
        FarmBoxAppController app = FarmBoxAppController.getInstance();

        List<Param> params = new ArrayList<>();
        params.add(new Param("farm_id", String.valueOf(app.getSelectedFarmId())));
        params.add(new Param("farm_name", name));
        params.add(new Param("farm_address", address));
        params.add(new Param("farm_acreage", acreage));
        params.add(new Param("farm_latitude", lat));
        params.add(new Param("farm_longitude", lon));

        String accessToken = app.getAccessToken();
        String link = Config.EDIT_FARM + Utils.decode(accessToken);
        network.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                callback.onSuccess(response);
            }


            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void addSeason(String farmID, String plantID, String date, String age, final APIHelper.Callback<String> callback) {
        List<Param> params = new ArrayList<>();
        params.add(new Param("plant_id", plantID));
        params.add(new Param("farm_id", farmID));
        params.add(new Param("season_start_date", date));
        params.add(new Param("season_age_of_plant", age));

        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        String link = Config.ADD_SEASON + Utils.decode(accessToken);
        network.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                callback.onSuccess(response);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void addContainer(String id, String lat, String lon, final APIHelper.Callback<String> callback) {
        List<Param> params = new ArrayList<>();
        params.add(new Param("farm_id", id));
        params.add(new Param("farm_latitude", lat));
        params.add(new Param("farm_longitude", lon));

        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        String link = Config.ADD_CONTAINER + Utils.decode(accessToken);
        network.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                callback.onSuccess(response);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void getFarmSettings(String farmID, final APIHelper.Callback<FarmSetting> callback) {
        FarmBoxAppController app = FarmBoxAppController.getInstance();
        String langParam = "&lang=" + app.getLanguage();
        String farmIDURL = "&id=" + Utils.encode(farmID);
        String accessToken = app.getAccessToken();
        String link = Config.GET_FARM_SETTING + Utils.decode(accessToken) + farmIDURL + langParam;

        network.executeGet(link, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                if (response.trim().isEmpty() || response.equalsIgnoreCase("[]")) {
                    callback.onSuccess(null);
                    return;
                }
                Gson gson = new Gson();
                FarmSetting farmSetting = gson.fromJson(response, FarmSetting.class);
                callback.onSuccess(farmSetting);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void getFarmSessionSettings(String factoryId, final APIHelper.Callback<FarmSetting> callback) {
        FarmBoxAppController app = FarmBoxAppController.getInstance();
        String langParam = "&lang=" + app.getLanguage();
        String farmIDURL = "&id=" + Utils.encode(factoryId);
        String accessToken = app.getAccessToken();
        String link = Config.GET_FARM_SESSION_SETTING + Utils.decode(accessToken) + farmIDURL + langParam;

        network.executeGet(link, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                if (response.trim().isEmpty() || response.equalsIgnoreCase("[]")) {
                    callback.onSuccess(null);
                    return;
                }
                Gson gson = new Gson();
                FarmSetting farmSetting = gson.fromJson(response, FarmSetting.class);
                callback.onSuccess(farmSetting);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }

    @Override
    public void saveFarmSetting(String factoryID, String farmID, List<Param> params, final APIHelper.Callback<String> callback) {
        params.add(new Param("factory_id", factoryID));
        params.add(new Param("farm_id", farmID));

        String accessToken = FarmBoxAppController.getInstance().getAccessToken();
        String link = Config.SAVE_SETTING + Utils.decode(accessToken);
        network.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                callback.onSuccess(response);
            }

            @Override
            public void onFail(int errorCode, String error) {
                callback.onFail(errorCode, error);
            }
        });
    }
}
