package mobile.agrhub.farmbox.service.model;

import android.content.Context;

import com.google.gson.annotations.SerializedName;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.utils.Utils;

public class Setting {
    @SerializedName("factory_id")
    public long factory_id;
    @SerializedName("farm_id")
    public long farm_id;

    @SerializedName("factory_co2_min")
    public long factory_co2_min = -1;
    @SerializedName("factory_co2_max")
    public long factory_co2_max = -1;

    public String getCO2Value() {
        if (factory_co2_min <= -1 || factory_co2_max <= -1) {
            return "---/---";
        }

        return factory_co2_min + "/" + factory_co2_max;
    }

    @SerializedName("factory_light_min")
    public long factory_light_min = -1;
    @SerializedName("factory_light_max")
    public long factory_light_max = -1;

    public String getLightValue() {
        if (factory_light_min <= -1 || factory_light_max <= -1) {
            return "---/---";
        }

        return factory_light_min + "/" + factory_light_max;
    }

    @SerializedName("factory_air_humidity_min")
    public long factory_air_humidity_min = -1;
    @SerializedName("factory_air_humidity_max")
    public long factory_air_humidity_max = -1;

    public String getAirHumiValue() {
        if (factory_air_humidity_min <= -1 || factory_air_humidity_max <= -1) {
            return "---/---";
        }

        return factory_air_humidity_min + "/" + factory_air_humidity_max;
    }

    @SerializedName("factory_soil_humidity_min")
    public long factory_soil_humidity_min = -1;
    @SerializedName("factory_soil_humidity_max")
    public long factory_soil_humidity_max = -1;

    public String getSoilHumiValue() {
        if (factory_soil_humidity_min <= -1 || factory_soil_humidity_max <= -1) {
            return "---/---";
        }

        return factory_soil_humidity_min + "/" + factory_soil_humidity_max;
    }

    @SerializedName("factory_ph_min")
    public double factory_ph_min = -1;
    @SerializedName("factory_ph_max")
    public double factory_ph_max = -1;

    public String getPHValue() {
        if (factory_ph_min <= -1 || factory_ph_max <= -1) {
            return "---/---";
        }

        return factory_ph_min + "/" + factory_ph_max;
    }

    @SerializedName("factory_ec_min")
    public double factory_ec_min = -1;
    @SerializedName("factory_ec_max")
    public double factory_ec_max = -1;

    public String getECValue(long farmType) {
        if (factory_ec_min <= -1 || factory_ec_max <= -1) {
            return "---/---";
        }

        String fromFormat = UnitItem.NUTRITION_PARTS_PER_MILLION_VALUE;
        if (farmType == 4 || farmType == 7) {
            fromFormat = UnitItem.NUTRITION_MILLISIEMENS_VALUE;
        }

        FarmBoxAppController app = FarmBoxAppController.getInstance();
        double min = app.getNutritionValue(factory_ec_min, fromFormat);
        double max = app.getNutritionValue(factory_ec_max, fromFormat);

        return Utils.f2s(min) + "/" + Utils.f2s(max);
    }

    @SerializedName("factory_water_level_min")
    public long factory_water_level_min = -1;
    @SerializedName("factory_water_level_max")
    public long factory_water_level_max = -1;

    public String getWaterLevelValue() {
        if (factory_water_level_min <= -1 || factory_water_level_max <= -1) {
            return "---/---";
        }

        return factory_water_level_min + "/" + factory_water_level_max;
    }

    @SerializedName("factory_ec_formula_a")
    public long factory_ec_formula_a = -1;
    @SerializedName("factory_ec_formula_b")
    public long factory_ec_formula_b = -1;
    @SerializedName("factory_ec_formula_c")
    public long factory_ec_formula_c = -1;

    public String getECFormula() {
        if (factory_ec_formula_a <= -1 || factory_ec_formula_b <= -1 || factory_ec_formula_c <= -1) {
            return "--(A) - --(B) - --(C)";
        }

        return factory_ec_formula_a + "(A) - " + factory_ec_formula_b + "(B) - " + factory_ec_formula_c + "(C)";
    }

    @SerializedName("factory_washing_mode_timer")
    public String factory_washing_mode_timer;

    @SerializedName("factory_water_pump_started_time")
    public long factory_water_pump_started_time = -1;
    @SerializedName("factory_water_pump_stopped_time")
    public long factory_water_pump_stopped_time = -1;

    public String getWaterPumpTime() {
        if (factory_water_pump_started_time <= -1 || factory_water_pump_stopped_time <= -1) {
            return "--:-- -> --:--";
        }

        return Utils.getTime(factory_water_pump_started_time) + " -> " + Utils.getTime(factory_water_pump_stopped_time);
    }

    @SerializedName("factory_oxygen_pump_started_time")
    public long factory_oxygen_pump_started_time = -1;
    @SerializedName("factory_oxygen_pump_stopped_time")
    public long factory_oxygen_pump_stopped_time = -1;

    public String getOxygenPumpTime() {
        if (factory_oxygen_pump_started_time <= -1 || factory_oxygen_pump_stopped_time <= -1) {
            return "--:-- -> --:--";
        }

        return Utils.getTime(factory_oxygen_pump_started_time) + " -> " + Utils.getTime(factory_oxygen_pump_stopped_time);
    }

    @SerializedName("factory_ac_started_time")
    public long factory_ac_started_time = -1;
    @SerializedName("factory_ac_stopped_time")
    public long factory_ac_stopped_time = -1;

    public String getACTime() {
        if (factory_ac_started_time <= -1 || factory_ac_stopped_time <= -1) {
            return "--:-- -> --:--";
        }

        return Utils.getTime(factory_ac_started_time) + " -> " + Utils.getTime(factory_ac_stopped_time);
    }

    @SerializedName("factory_lamp_started_time")
    public long factory_lamp_started_time = -1;
    @SerializedName("factory_lamp_stopped_time")
    public long factory_lamp_stopped_time = -1;

    public String getLampTime() {
        if (factory_lamp_started_time <= -1 || factory_lamp_stopped_time <= -1) {
            return "--:-- -> --:--";
        }

        return Utils.getTime(factory_lamp_started_time) + " -> " + Utils.getTime(factory_lamp_stopped_time);
    }

    @SerializedName("factory_fan_started_time")
    public long factory_fan_started_time = -1;
    @SerializedName("factory_fan_stopped_time")
    public long factory_fan_stopped_time = -1;

    public String getFanTime() {
        if (factory_fan_started_time <= -1 || factory_fan_stopped_time <= -1) {
            return "--:-- -> --:--";
        }

        return Utils.getTime(factory_fan_started_time) + " -> " + Utils.getTime(factory_fan_stopped_time);
    }

    @SerializedName("factory_misting_pump_started_time")
    public long factory_misting_pump_started_time = -1;
    @SerializedName("factory_misting_pump_stopped_time")
    public long factory_misting_pump_stopped_time = -1;

    public String getMistingPumpTime() {
        if (factory_misting_pump_started_time <= -1 || factory_misting_pump_stopped_time <= -1) {
            return "--:-- -> --:--";
        }

        return Utils.getTime(factory_misting_pump_started_time) + " -> " + Utils.getTime(factory_misting_pump_stopped_time);
    }

    @SerializedName("factory_temperature_level_min")
    public long factory_temperature_level_min = -1;
    @SerializedName("factory_temperature_level_max")
    public long factory_temperature_level_max = -1;

    public String getTempValue() {
        if (factory_temperature_level_min <= -1 || factory_temperature_level_max <= -1) {
            return "---/---";
        }

        FarmBoxAppController app = FarmBoxAppController.getInstance();
        long min = app.getTemperatureValue(factory_temperature_level_min, UnitItem.TEMPERATURE_C_VALUE);
        long max = app.getTemperatureValue(factory_temperature_level_max, UnitItem.TEMPERATURE_C_VALUE);
        return min + "/" + max;
    }

    @SerializedName("factory_registered_date")
    public long factory_registered_date;

    @SerializedName("factory_updated_date")
    public long factory_updated_date;

    @SerializedName("factory_liquid_level_a")
    public long factory_liquid_level_a = -1;
    @SerializedName("factory_liquid_level_b")
    public long factory_liquid_level_b = -1;
    @SerializedName("factory_liquid_level_c")
    public long factory_liquid_level_c = -1;
    @SerializedName("factory_liquid_level_d")
    public long factory_liquid_level_d = -1;

    public String getLiquidLevelValue() {
        return "";
    }

    //new setting item
    @SerializedName("factory_water_volume")
    public int factory_water_volume = -1;

    public String getWaterVolumeValue() {
        if (factory_water_volume == -1) {
            return "---";
        }
        return factory_water_volume + "";
    }

    @SerializedName("factory_liquid_volume")
    public int factory_liquid_volume = -1;

    public String getLiquidVolumeValue() {
        if (factory_liquid_volume == -1) {
            return "---";
        }
        return factory_liquid_volume + "";
    }

    @SerializedName("factory_tank_height")
    public int factory_tank_height = -1;

    public String getTankHeightValue() {
        if (factory_tank_height == -1) {
            return "---";
        }
        return factory_tank_height + "";
    }

    @SerializedName("factory_sensor_height")
    public int factory_sensor_height = -1;

    public String getSensorHeightValue() {
        if (factory_sensor_height == -1) {
            return "---";
        }
        return factory_sensor_height + "";
    }

    @SerializedName("factory_control_mode")
    public int factoryControlMode;

    @SerializedName("factory_water_pump_freq")
    public int factoryWaterPumpFreq;

    @SerializedName("factory_water_pump_off_freq")
    public int factoryWaterPumpOffFreq;

    public String getWaterPumpFreqValue(Context context) {
        String onTime = String.valueOf(this.factoryWaterPumpFreq);
        if (this.factoryWaterPumpFreq < 0) {
            onTime = "---";
        }
        String offTime = String.valueOf(this.factoryWaterPumpOffFreq);
        if (this.factoryWaterPumpOffFreq < 0) {
            offTime = "---";
        }
        return context.getString(R.string.setting_water_pump_freq_format, onTime, offTime);
    }

    @SerializedName("factory_oxygen_pump_freq")
    public int factoryOxygenPumpFreq;

    @SerializedName("factory_oxygen_pump_off_freq")
    public int factoryOxygenPumpOffFreq;

    public String getOxygenPumpFreqValue(Context context) {
        String onTime = String.valueOf(this.factoryOxygenPumpFreq);
        if (this.factoryOxygenPumpFreq < 0) {
            onTime = "---";
        }
        String offTime = String.valueOf(this.factoryOxygenPumpOffFreq);
        if (this.factoryOxygenPumpOffFreq < 0) {
            offTime = "---";
        }
        return context.getString(R.string.setting_oxygen_pump_freq_format, onTime, offTime);
    }

    public Setting() {
    }
}
