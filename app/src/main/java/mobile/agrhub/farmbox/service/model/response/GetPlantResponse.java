package mobile.agrhub.farmbox.service.model.response;

import java.util.List;

import mobile.agrhub.farmbox.service.model.Plant;

public class GetPlantResponse {
    public List<Plant> plants;

    public GetPlantResponse() {
    }
}
