package mobile.agrhub.farmbox.service.model.response;

import java.util.List;

import mobile.agrhub.farmbox.service.model.Diary;

public class GetDiariesResponse {
    public List<Diary> diaries;

    public GetDiariesResponse() {
    }
}
