package mobile.agrhub.farmbox.service.model.response;

import java.util.List;

import mobile.agrhub.farmbox.service.model.ProductCategory;

public class GetProductCategoryResponse {
    public List<ProductCategory> productCategories;

    public GetProductCategoryResponse() {
    }
}
