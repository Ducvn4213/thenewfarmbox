package mobile.agrhub.farmbox.service.model;

import com.google.gson.annotations.SerializedName;

public class Nation {
    @SerializedName("nation_id")
    public long nation_id;
    @SerializedName("nation_name")
    public String nation_name;
    @SerializedName("nation_code")
    public String nation_code;
    @SerializedName("phone_code")
    public String phone_code;
    @SerializedName("is_active")
    public Boolean is_active;
    @SerializedName("nation_registered_date")
    public long nation_registered_date;
    @SerializedName("nation_updated_date")
    public long nation_updated_date;

    public Nation() {
    }
}
