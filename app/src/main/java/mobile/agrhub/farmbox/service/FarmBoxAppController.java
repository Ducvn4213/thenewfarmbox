package mobile.agrhub.farmbox.service;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.List;
import java.util.Locale;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.Farm;
import mobile.agrhub.farmbox.service.model.FarmBasic;
import mobile.agrhub.farmbox.service.model.Product;
import mobile.agrhub.farmbox.service.model.UnitItem;
import mobile.agrhub.farmbox.service.model.User;
import mobile.agrhub.farmbox.utils.Constants;
import mobile.agrhub.farmbox.utils.Utils;

public class FarmBoxAppController extends Application {
    private static final String TAG = "AppController";

    private static FarmBoxAppController instance;

    private String language;
    private User user;
    private String accessToken;
    private CartManager cart;
    private WeatherManager weatherManager;
    private boolean needLoadFarmList;
    private List<FarmBasic> farmList;
    private int selectedFarmIndex;
    private Farm selectedFarm;
    private boolean isDemoMode;
    private boolean needReloadFarmSetting;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        cart = new CartManager();
        weatherManager = new WeatherManager();
        isDemoMode = false;
        needReloadFarmSetting = false;

        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();

        // Create global configuration and initialize ImageLoader with this config
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(defaultOptions)
                .build();

        ImageLoader.getInstance().init(config);
    }

    public void setLanguage(String lang) {
        language = lang;

        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);

        Utils.savePreference(this, "FARM_BOX_LANG", lang);
    }

    public static FarmBoxAppController getInstance() {
        return instance;
    }

    public String getLanguage() {
        if (language == null || language.isEmpty()) {
            language = Utils.getPreference(this, "FARM_BOX_LANG");
            if (language == null || language.isEmpty()) {
                language = Locale.getDefault().getLanguage();
                if (!"vi".equals(language)) {
                    language = "en";
                }
            }
        }
        return language;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user, String accessToken) {
        this.user = user;
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public int getTotalAmount() {
        return this.cart.getTotalAmount();
    }

    public int getAmountOfProduct(long productId) {
        return this.cart.getAmountOfProduct(productId);
    }

    public long getTotalOriginPrice() {
        return cart.getTotalOriginPrice();
    }

    public long getTotalDiscountPrice() {
        return cart.getTotalDiscountPrice();
    }

    public void addProduct(Product product, int amount) {
        this.cart.addProduct(product, amount);
    }

    public int getCountProduct() {
        return cart.getCountProduct();
    }

    public Product getProductAtIndex(int idx) {
        return cart.getProductAtIndex(idx);
    }

    public void plusProduct(String productIdStr) {
        this.cart.plusProduct(productIdStr);
    }

    public void minusProduct(String productIdStr) {
        this.cart.minusProduct(productIdStr);
    }

    public String getCartJson() {
        return this.cart.getCartJson();
    }

    public void clearAll() {
        this.cart.clearAll();
    }

    public boolean isDemoMode() {
        return isDemoMode;
    }

    public void setDemoMode(boolean demoMode) {
        isDemoMode = demoMode;
    }

    public WeatherManager getWeatherManager() {
        return weatherManager;
    }

    public void setWeatherManager(WeatherManager weatherManager) {
        this.weatherManager = weatherManager;
    }

    public boolean isNeedLoadFarmList() {
        return needLoadFarmList;
    }

    public void setNeedLoadFarmList(boolean needLoadFarmList) {
        this.needLoadFarmList = needLoadFarmList;
    }

    public List<FarmBasic> getFarmList() {
        return farmList;
    }

    public void setFarmList(List<FarmBasic> farmList) {
        this.farmList = farmList;
    }

    public int getSelectedFarmIndex() {
        return selectedFarmIndex;
    }

    public void setSelectedFarmIndex(int selectedFarmIndex) {
        this.selectedFarmIndex = selectedFarmIndex;
    }

    public Farm getSelectedFarm() {
        return selectedFarm;
    }

    public void setSelectedFarm(Farm selectedFarm) {
        this.selectedFarm = selectedFarm;
    }

    public long getSelectedFarmId() {
        if (selectedFarm != null) {
            return selectedFarm.farm_id;
        }
        return -1;
    }

    public String getSelectedFarmName() {
        if (selectedFarm != null) {
            return selectedFarm.farm_name;
        }
        return "";
    }

    public String getFarmName(long farmId) {
        if (farmList != null && farmList.size() > 0) {
            for (FarmBasic farm : farmList) {
                if (farmId == farm.farm_id) {
                    return farm.farm_name;
                }
            }
        }
        return "";
    }

    public void logout() {
        isDemoMode = false;
        user = null;
        accessToken = null;
        cart.clearAll();
        weatherManager.clean();
        if (null != farmList) {
            farmList.clear();
        }
        selectedFarmIndex = 0;
        selectedFarm = null;
    }

    public User loadSettings(Context context, User user) {
        if (user != null) {
            String temperatureUnit = Utils.getPreference(context, Constants.SETTING_TEMPERATURE_UNIT_KEY + user.user_phone);
            if (temperatureUnit == null || temperatureUnit.isEmpty()) {
                Utils.savePreference(context, Constants.SETTING_TEMPERATURE_UNIT_KEY + user.user_phone, UnitItem.TEMPERATURE_C_VALUE);
                temperatureUnit = UnitItem.TEMPERATURE_C_VALUE;
            }
            user.setTemperatureUnit(temperatureUnit);

            String nutritionUnit = Utils.getPreference(context, Constants.SETTING_NUTRITION_UNIT_KEY + user.user_phone);
            if (nutritionUnit == null || nutritionUnit.isEmpty()) {
                Utils.savePreference(context, Constants.SETTING_NUTRITION_UNIT_KEY + user.user_phone, UnitItem.NUTRITION_MILLISIEMENS_VALUE);
                nutritionUnit = UnitItem.NUTRITION_MILLISIEMENS_VALUE;
            }
            user.setNutritionUnit(nutritionUnit);
        }
        return user;
    }

    public boolean isTemperatureFUnit() {
        return this.user != null && UnitItem.TEMPERATURE_F_VALUE.equals(this.user.getTemperatureUnit());
    }

    public String getTemperatureUnit() {
        if (this.isTemperatureFUnit()) {
            return this.getString(R.string.temperature_unit_f);
        }
        return this.getString(R.string.temperature_unit_c);
    }

    public long getTemperatureValue(long temperature, String fromUnit) {
        if (this.isTemperatureFUnit()) {
            if (UnitItem.TEMPERATURE_F_VALUE.equals(fromUnit)) {
                return temperature;
            } else {
                return Utils.c2f(temperature);
            }
        } else {
            if (UnitItem.TEMPERATURE_C_VALUE.equals(fromUnit)) {
                return temperature;
            } else {
                return Utils.f2c(temperature);
            }
        }
    }

    public long parseTemperatureValue(long temperature, String toUnit) {
        if (this.isTemperatureFUnit()) {
            if (UnitItem.TEMPERATURE_F_VALUE.equals(toUnit)) {
                return temperature;
            } else {
                return Utils.f2c(temperature);
            }
        } else {
            if (UnitItem.TEMPERATURE_C_VALUE.equals(toUnit)) {
                return temperature;
            } else {
                return Utils.c2f(temperature);
            }
        }
    }

    public boolean isNutritionPPMUnit() {
        return this.user != null && UnitItem.NUTRITION_PARTS_PER_MILLION_VALUE.equals(this.user.getNutritionUnit());
    }

    public String getNutritionUnit() {
        if (this.isNutritionPPMUnit()) {
            return this.getString(R.string.nutrition_unit_ppm);
        }
        return this.getString(R.string.nutrition_unit_ms_cm);
    }

    public double getNutritionValue(double nutrition, String fromUnit) {
        if (this.isNutritionPPMUnit()) {
            if (UnitItem.NUTRITION_PARTS_PER_MILLION_VALUE.equals(fromUnit)) {
                return nutrition;
            } else {
                return Utils.ms2ppm(nutrition);
            }
        } else {
            if (UnitItem.NUTRITION_MILLISIEMENS_VALUE.equals(fromUnit)) {
                return nutrition;
            } else {
                return Utils.ppm2ms(nutrition);
            }
        }
    }

    public double parseNutritionValue(double nutrition, String toUnit) {
        if (this.isNutritionPPMUnit()) {
            if (UnitItem.NUTRITION_PARTS_PER_MILLION_VALUE.equals(toUnit)) {
                return nutrition;
            } else {
                return Utils.ppm2ms(nutrition);
            }
        } else {
            if (UnitItem.NUTRITION_MILLISIEMENS_VALUE.equals(toUnit)) {
                return nutrition;
            } else {
                return Utils.ms2ppm(nutrition);
            }
        }
    }

    public boolean isNeedReloadFarmSetting() {
        return needReloadFarmSetting;
    }

    public void setNeedReloadFarmSetting(boolean needReloadFarmSetting) {
        this.needReloadFarmSetting = needReloadFarmSetting;
    }
}
