package mobile.agrhub.farmbox.dev_hub;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.activities.TabActivity;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.implementations.APIHelper;
import mobile.agrhub.farmbox.service.interfaces.UserAPI;
import mobile.agrhub.farmbox.service.model.User;
import mobile.agrhub.farmbox.utils.BaseActivity;
import mobile.agrhub.farmbox.utils.Utils;

public class DevHub extends BaseActivity {

    LinearLayout farmAMinh;
    LinearLayout farmCuChi;
    LinearLayout farmBenTre;
    LinearLayout farmQuan1;
    LinearLayout farmCanTho;
    LinearLayout farmThaiNguyenUniversity;
    LinearLayout farmCongLN;
    LinearLayout otherToken;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_develop_page);

        bindingControls();
        setupControlEvents();
    }

    private void bindingControls() {
        farmAMinh = (LinearLayout) findViewById(R.id.ll_farm_a_minh);
        farmCuChi = (LinearLayout) findViewById(R.id.ll_farm_cu_chi);
        farmBenTre = (LinearLayout) findViewById(R.id.ll_farm_ben_tre);
        farmQuan1 = (LinearLayout) findViewById(R.id.ll_farm_quan_1);
        farmCanTho = (LinearLayout) findViewById(R.id.ll_farm_can_tho);
        farmThaiNguyenUniversity = (LinearLayout) findViewById(R.id.ll_farm_thai_nguyen_university);
        farmCongLN = (LinearLayout) findViewById(R.id.ll_farm_cong_ln);
        otherToken = (LinearLayout) findViewById(R.id.ll_other_token);
    }

    private void setupControlEvents() {
        farmAMinh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginWith("ODZjNzI2NmE0ODkzODM3Mjc2NzFhYmM4YzMwOTdmMTA=");
            }
        });

        farmCuChi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginWith("N2QzYTg4YzUyZjc2NmZiYmNiZTc3YjZjMjdiYmU5ZTA=");
            }
        });

        farmBenTre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginWith("M2NhYWFkOGQ2NTg1YTYyZDZjNWQwMWU4ZDMxNzA0YmY=");
            }
        });

        farmQuan1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginWith("ZjQ4ZGMxMWVkMmY4ZGNjNmJlMmM5ZTdhYzJmZjI4N2Q=");
            }
        });

        farmCanTho.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginWith("ZDkxM2Y1ZmZiMTE3YmJmODM4NDc1YTdkOTU4NDE5ZmQ=");
            }
        });

        farmThaiNguyenUniversity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginWith("MGZkZDBjYmNhZGI5NmVjNDc5NTlmMWY1M2ZjOGNkNjA=");
            }
        });

        farmCongLN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginWith("OGEyYjg4NTBlYTQzYmYzNWE0Mjg3YTJiZjA4N2M0NmI=");
            }
        });

        otherToken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showInputTokenDialog();
            }
        });
    }

    private void showInputTokenDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppTheme_Dialog_Alert);
        builder.setTitle("Enter the token");

        // Set up the input
        final EditText tokenInput = new EditText(this);

        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        tokenInput.setInputType(InputType.TYPE_CLASS_TEXT);
        tokenInput.setGravity(Gravity.CENTER);
        builder.setView(tokenInput);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String token = Utils.encode(tokenInput.getText().toString().trim());
                loginWith(token);
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void loginWith(final String token) {
        showLoading();
        UserAPI userAPI = APIHelper.getUserAPI();
        userAPI.getUserInfo(this, token, new APIHelper.Callback<User>() {
            @Override
            public void onSuccess(User user) {
                FarmBoxAppController app = FarmBoxAppController.getInstance();
                user = app.loadSettings(DevHub.this, user);
                app.setUser(user, token);
                app.setNeedLoadFarmList(true);

                DevHub.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        Intent intent = new Intent(DevHub.this, TabActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                });
            }

            @Override
            public void onFail(final int errorCode, final String error) {
                DevHub.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        handleError(errorCode, error);
                    }
                });
            }
        });
    }
}
