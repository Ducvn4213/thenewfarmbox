package mobile.agrhub.farmbox.utils;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.network.Network;

public class BaseFragment extends Fragment {

    @Override
    public void onResume() {
        super.onResume();
        Utils.hideKeyboard(getActivity());
    }

    protected BaseActivity.GetCurrentLocationCallback getCurrentLocationCallback;
    private ProgressDialog mProgressDialog;
    private int retryCount = 0;

    protected void showLoading() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getContext());
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage(getString(R.string.progress_bar_message));
        }

        mProgressDialog.show();
    }

    protected void hideLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    protected boolean isLoadingShowing() {
        return (mProgressDialog != null && mProgressDialog.isShowing());
    }

    protected void showErrorDialog(int messageResource) {
        Utils.showErrorDialog(getContext(), getString(messageResource));
    }

    protected void showErrorDialog(String message) {
        Utils.showErrorDialog(getContext(), message);
    }

    protected void showDialog(String title, String message) {
        Utils.showDialog(getContext(), title, message);
    }

    protected void getCurrentLocationAndContinue() {
        checkLocationPermission();
    }

    private void checkLocationPermission() {
        List<String> permissions = new ArrayList<>();
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        } else {
            doGetCurrentLocation();
            return;
        }

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        } else {
            doGetCurrentLocation();
            return;
        }

        String[] perArr = new String[permissions.size()];
        perArr = permissions.toArray(perArr);
        ActivityCompat.requestPermissions(getActivity(),
                perArr,
                Utils.PERMISSIONS_LOCATION_REQUEST);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Utils.PERMISSIONS_LOCATION_REQUEST) {
            if (grantResults.length > 0) {
                boolean isGrant = false;
                for (int gr : grantResults) {
                    if (gr == PackageManager.PERMISSION_GRANTED) {
                        isGrant = true;
                        break;
                    }
                }

                if (isGrant) {
                    doGetCurrentLocation();
                } else {
                    showErrorDialog(getString(R.string.dialog_message_missing_location_permission));
                    getCurrentLocationCallback.afterGetCurrentLocationFail();
                }
            } else {
                showErrorDialog(getString(R.string.dialog_message_missing_location_permission));
                getCurrentLocationCallback.afterGetCurrentLocationFail();
            }
        }
    }

    private void doGetCurrentLocation() {
        if (retryCount > 5) {
            getCurrentLocationCallback.afterGotCurrentLocation(0, 0);
            return;
        }
        retryCount++;

        TrackerGPS mTrackerGPS = new TrackerGPS(getContext());
        if (mTrackerGPS.canGetLocation()) {

            double longitude = mTrackerGPS.getLongitude();
            double latitude = mTrackerGPS.getLatitude();

            if (longitude == 0 && latitude == 0) {
                doGetCurrentLocation();
                return;
            }

            getCurrentLocationCallback.afterGotCurrentLocation(latitude, longitude);
        } else {
            mTrackerGPS.showSettingsAlert();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (null != mProgressDialog) {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
            mProgressDialog = null;
        }
    }

    protected void handleError(int errorCode, String message) {
        if (errorCode == Network.DEMO_MODE_ERROR) {
            Utils.showDialog(getContext(), getString(R.string.demo_mode), getString(R.string.demo_mode_message));
        } else {
            Utils.showErrorDialog(getContext(), getString(R.string.fatal_error_message));
        }
    }
}
