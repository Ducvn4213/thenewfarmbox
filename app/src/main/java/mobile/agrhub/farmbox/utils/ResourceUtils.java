package mobile.agrhub.farmbox.utils;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.model.Farm;

public class ResourceUtils {
    public static final long UNKNOWN_DEVICE_NAME = 0l;
    public static final long BROADLINK_SP3_SMART_PLUG = 1l;
    public static final long SONOFF_SMART_PLUG = 2l;
    public static final long XIAOMI_SMART_PLUG = 3l;
    public static final long XIAOMI_PLANT_CARE_SENSOR = 4l;
    public static final long AXEC_AIR_SENSOR = 5l;
    public static final long TI_TAG_SENSOR = 6l;
    public static final long FIOT_SMART_TANK_CONTROLLER = 7l;
    public static final long BROADLINK_RM3_SMART_REMOTE = 8l;
    public static final long AGRHUB_SENSE_HUB = 9l;
    public static final long VIETTEL_CAMERA = 10l;
    public static final long AGRHUB_SENSE_HUB_PRO = 11l;
    public static final long NRF51822_SENSOR = 12l;
    public static final long VGV_CAMERA = 13l;
    public static final long KAMOER_DRIPPING_PRO = 18l;
    public static final long ARIATEC_HYDROPONIC_CONTROLLER = 21l;
    public static final long STORAGE_CONTROLLER = 22l;
    public static final long TYPE_SENSE_PLUG = 23;

    private static String DEF_LIGHT_SENSOR_NAME = "";
    private static String DEF_AIR_TEMPERATURE_SENSOR_NAME = "";
    private static String DEF_AIR_HUMIDITY_SENSOR_NAME = "";
    private static String DEF_SOIL_TEMPERATURE_SENSOR_NAME = "";
    private static String DEF_SOIL_HUMIDITY_SENSOR_NAME = "";
    private static String DEF_SOIL_EC_SENSOR_NAME = "";
    private static String DEF_SOIL_PH_SENSOR_NAME = "";
    private static String DEF_WATER_TEMPERATURE_SENSOR_NAME = "";
    private static String DEF_WATER_EC_SENSOR_NAME = "";
    private static String DEF_WATER_PH_SENSOR_NAME = "";
    private static String DEF_WATER_ORP_SENSOR_NAME = "";
    private static String DEF_BATTERY_SENSOR_NAME = "";
    private static String DEF_CO2_SENSOR_NAME = "";
    private static String DEF_WATER_LEVEL_SENSOR_NAME = "";
    private static String DEF_WATER_LEAK_SENSOR_NAME = "";
    private static String DEF_ERROR_SENSOR_NAME = "";

    private static HashMap<String, Integer> plantTypes;
    private static HashMap<String, Integer> plantLifeCycles;
    private static HashMap<String, Integer> plantColors;
    private static HashMap<String, Integer> plantSeasons;
    private static HashMap<String, Integer> plantShapes;

    public static int getFarmTypeName(long type) {
        switch ((int) type) {
            case Farm.GENERAL:
                return R.string.farm_general;
            case Farm.FAMILY:
                return R.string.farm_family;
            case Farm.MUSHROOM:
                return R.string.farm_mushroom;
            case Farm.GREEN_HOUSE:
                return R.string.farm_green;
            case Farm.HYDROPONIC:
                return R.string.farm_hydroponic;
            case Farm.AQUAPONIC:
                return R.string.farm_aquaponic;
            case Farm.FIELD:
                return R.string.farm_field;
            case Farm.CONTAINER:
                return R.string.farm_container;
            case Farm.STORAGE:
                return R.string.farm_storage;
            case Farm.CATTLE_HOUSE:
                return R.string.farm_cattle_house;
        }

        return R.string.farm_family;
    }

    public static int getFarmImageResourceByType(long type) {
        switch ((int) type) {
            case 1:
                return R.drawable.add_farm_home_background;
            case 2:
                return R.drawable.add_farm_mushroom_background;
            case 3:
                return R.drawable.add_farm_greenhouse_background;
            case 4:
                return R.drawable.add_farm_hydroponic_background;
            case 5:
                return R.drawable.add_farm_hydroponic_background;
            case 6:
                return R.drawable.add_farm_home_background;
            case 7:
                return R.drawable.add_farm_mushroom_background;
        }

        return R.drawable.add_farm_home_background;
    }

    public static int getControllerNameByType(long type) {
        switch ((int) type) {
            case 1:
                return R.string.controller_lamp;
            case 2:
                return R.string.controller_water_pump;
            case 3:
                return R.string.controller_misting_pump;
            case 4:
                return R.string.controller_fan;
            case 5:
                return R.string.controller_air_conditioner;
            case 6:
                return R.string.controller_co2;
            case 7:
                return R.string.controller_dosing_pump;
            case 8:
                return R.string.controller_oxygen_pump;
            case 9:
                return R.string.controller_valve_input;
            case 10:
                return R.string.controller_valve_output;
            case 11:
                return R.string.controller_washing_mode;
            default:
                return R.string.controller_unassigned;
        }
    }

    private static void configPlantTypes() {
        if (plantTypes == null) {
            plantTypes = new HashMap<>();
            plantTypes.put("GR", R.string.plant_type_grass);
            plantTypes.put("IP", R.string.plant_type_interior);
            plantTypes.put("SH", R.string.plant_type_shrub);
            plantTypes.put("TR", R.string.plant_type_tree);
            plantTypes.put("VI", R.string.plant_type_vine);
            plantTypes.put("ED", R.string.plant_type_edible);
        }
    }

    private static void configPlantLifeCycles() {
        if (plantLifeCycles == null) {
            plantLifeCycles = new HashMap<>();
            plantLifeCycles.put("AN", R.string.plant_life_time_annual);
            plantLifeCycles.put("BA", R.string.plant_life_time_biennial);
            plantLifeCycles.put("PE", R.string.plant_life_time_perennial);
        }
    }

    private static void configPlantColors() {
        if (plantColors == null) {
            plantColors = new HashMap<>();
            plantColors.put("BK", R.string.plant_element_color_black);
            plantColors.put("BL", R.string.plant_element_color_blue);
            plantColors.put("GR", R.string.plant_element_color_green);
            plantColors.put("OR", R.string.plant_element_color_orange);
            plantColors.put("PI", R.string.plant_element_color_pink);
            plantColors.put("PU", R.string.plant_element_color_purple);
            plantColors.put("RE", R.string.plant_element_color_red);
            plantColors.put("WH", R.string.plant_element_color_white);
            plantColors.put("YE", R.string.plant_element_color_yellow);
        }
    }

    private static void configPlantSeasons() {
        if (plantSeasons == null) {
            plantSeasons = new HashMap<>();
            plantSeasons.put("EE", R.string.plant_season_spring);
            plantSeasons.put("ME", R.string.plant_season_spring);
            plantSeasons.put("LE", R.string.plant_season_spring);
            plantSeasons.put("ES", R.string.plant_season_summer);
            plantSeasons.put("MS", R.string.plant_season_summer);
            plantSeasons.put("LS", R.string.plant_season_summer);
            plantSeasons.put("EF", R.string.plant_season_fall);
            plantSeasons.put("MF", R.string.plant_season_fall);
            plantSeasons.put("LF", R.string.plant_season_fall);
            plantSeasons.put("4", R.string.plant_season_winter);
        }
    }

    private static void configPlantShapes() {
        if (plantShapes == null) {
            plantShapes = new HashMap<>();
            plantShapes.put("IR", R.string.plant_shape_irregular);
            plantShapes.put("PY", R.string.plant_shape_pyramidal);
            plantShapes.put("RO", R.string.plant_shape_rounded);
            plantShapes.put("SP", R.string.plant_shape_speading);
            plantShapes.put("UP", R.string.plant_shape_upright);
        }
    }

    public static String getPlantTypeName(Context context, String input) {
        configPlantTypes();

        List<String> inputJSON = new Gson().fromJson(input, new TypeToken<List<String>>() {
        }.getType());
        String output = "";

        for (String item : inputJSON) {
            try {
                int value = plantTypes.get(item);
                output += ", " + context.getString(value);
            } catch (Exception ex) {
                ex.printStackTrace();

                output += ", " + context.getString(R.string.plant_type_shrub);
            }

        }

        if (!output.isEmpty()) {
            output = output.substring(2);
        } else {
            output = "-----";
        }

        return output;
    }

    public static String getPlantLifeCycleName(Context context, String input) {
        configPlantLifeCycles();

        List<String> inputJSON = new Gson().fromJson(input, new TypeToken<List<String>>() {
        }.getType());
        String output = "";

        for (String item : inputJSON) {
            try {
                int value = plantLifeCycles.get(item);
                output += ", " + context.getString(value);
            } catch (Exception ex) {
                ex.printStackTrace();

                output += ", " + context.getString(R.string.plant_life_time_annual);
            }
        }

        if (!output.isEmpty()) {
            output = output.substring(2);
        } else {
            output = "-----";
        }

        return output;
    }

    public static String getPlantElementColorName(Context context, String input) {
        configPlantColors();

        List<String> inputJSON = new Gson().fromJson(input, new TypeToken<List<String>>() {
        }.getType());
        String output = "";

        for (String item : inputJSON) {
            try {
                int value = plantColors.get(item);
                output += ", " + context.getString(value);
            } catch (Exception ex) {
                ex.printStackTrace();

                output += ", " + context.getString(R.string.plant_element_color_black);
            }
        }

        if (!output.isEmpty()) {
            output = output.substring(2);
        } else {
            output = "-----";
        }

        return output;
    }

    public static String getPlantShapeName(Context context, String input) {
        configPlantShapes();

        List<String> inputJSON = new Gson().fromJson(input, new TypeToken<List<String>>() {
        }.getType());
        String output = "";

        for (String item : inputJSON) {
            try {
                int value = plantShapes.get(item);
                output += ", " + context.getString(value);
            } catch (Exception ex) {
                ex.printStackTrace();

                output += ", " + context.getString(R.string.plant_shape_irregular);
            }
        }

        if (!output.isEmpty()) {
            output = output.substring(2);
        } else {
            output = "-----";
        }

        return output;
    }

    public static String getPlantSeasonName(Context context, String input) {
        configPlantSeasons();

        List<String> inputJSON = new Gson().fromJson(input, new TypeToken<List<String>>() {
        }.getType());
        String output = "";

        for (String item : inputJSON) {
            try {
                int value = plantSeasons.get(item);
                output += ", " + context.getString(value);
            } catch (Exception ex) {
                ex.printStackTrace();

                output += ", " + context.getString(R.string.plant_season_spring);
            }
        }

        if (!output.isEmpty()) {
            output = output.substring(2);
        } else {
            output = "-----";
        }

        return output;
    }
}
