package mobile.agrhub.farmbox.utils;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.network.Network;

public class BaseActivity extends AppCompatActivity {

    public interface GetCurrentLocationCallback {
        void afterGotCurrentLocation(double lat, double lon);

        void afterGetCurrentLocationFail();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Utils.hideKeyboard(BaseActivity.this);
    }

    protected GetCurrentLocationCallback getCurrentLocationCallback;
    private ProgressDialog progressDialog;
    private int retryCount = 0;

    protected void showLoading() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(BaseActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage(getString(R.string.progress_bar_message));
        }

        progressDialog.show();
    }

    protected void hideLoading() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.hide();
        }
    }

    protected void showErrorDialog(int stringResult) {
        showErrorDialog(getString(stringResult));
    }

    protected void showErrorDialog(String message) {
        Utils.showErrorDialog(BaseActivity.this, message);
    }

    protected void showDialog(String title, String message) {
        Utils.showDialog(BaseActivity.this, title, message);
    }

    protected void setGetCurrentLocationCallback(GetCurrentLocationCallback callback) {
        this.getCurrentLocationCallback = callback;
    }

    protected void getCurrentLocationAndContinue() {
        if (getCurrentLocationCallback != null) {
            checkLocationPermission();
        }
    }

    private void checkLocationPermission() {
        List<String> permissions = new ArrayList<>();
        if (ContextCompat.checkSelfPermission(BaseActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        } else {
            doGetCurrentLocation();
            return;
        }

        if (ContextCompat.checkSelfPermission(BaseActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        } else {
            doGetCurrentLocation();
            return;
        }

        String[] perArr = new String[permissions.size()];
        perArr = permissions.toArray(perArr);
        ActivityCompat.requestPermissions(BaseActivity.this,
                perArr,
                Utils.PERMISSIONS_LOCATION_REQUEST);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Utils.PERMISSIONS_LOCATION_REQUEST) {
            if (grantResults.length > 0) {
                boolean isGrant = false;
                for (int gr : grantResults) {
                    if (gr == PackageManager.PERMISSION_GRANTED) {
                        isGrant = true;
                        break;
                    }
                }

                if (isGrant) {
                    doGetCurrentLocation();
                } else {
                    showErrorDialog(getString(R.string.dialog_message_missing_location_permission));
                    getCurrentLocationCallback.afterGetCurrentLocationFail();
                }
            } else {
                showErrorDialog(getString(R.string.dialog_message_missing_location_permission));
                getCurrentLocationCallback.afterGetCurrentLocationFail();
            }
        }
    }

    private void doGetCurrentLocation() {
        if (retryCount > 5) {
            getCurrentLocationCallback.afterGotCurrentLocation(0, 0);
            return;
        }
        retryCount++;

        TrackerGPS mTrackerGPS = new TrackerGPS(BaseActivity.this);
        if (mTrackerGPS.canGetLocation()) {

            double longitude = mTrackerGPS.getLongitude();
            double latitude = mTrackerGPS.getLatitude();

            if (longitude == 0 && latitude == 0) {
                doGetCurrentLocation();
                return;
            }

            getCurrentLocationCallback.afterGotCurrentLocation(latitude, longitude);
        } else {
            mTrackerGPS.showSettingsAlert();
        }
    }

    protected void handleOnFarmBoxNotInit(boolean closeAfter) {
        Utils.SimpleCallback closeAfterHandler = null;
        if (closeAfter) {
            closeAfterHandler = new Utils.SimpleCallback() {
                @Override
                public void onDone() {
                    BaseActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            BaseActivity.this.finish();
                        }
                    });
                }
            };
        }
        Utils.showFarmBoxNotInitMessage(BaseActivity.this, closeAfterHandler);
    }

    @Override
    protected void onDestroy() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = null;
        }
        super.onDestroy();
    }

    protected void handleError(int errorCode, String message) {
        if (errorCode == Network.DEMO_MODE_ERROR) {
            Utils.showDialog(this, getString(R.string.demo_mode), getString(R.string.demo_mode_message));
        } else {
            Utils.showErrorDialog(this, getString(R.string.fatal_error_message));
        }
    }
}
