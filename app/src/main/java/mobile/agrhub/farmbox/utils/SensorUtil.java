package mobile.agrhub.farmbox.utils;

import java.util.List;

import mobile.agrhub.farmbox.service.model.Sensor;

public class SensorUtil {

    public static String getBatteryValue(List<Sensor> sensorList) {
        for (Sensor s : sensorList) {
            if (s.sensor_type == 12) {
                return (int) s.sensor_value + "%";
            }
        }

        return "0%";
    }
}
