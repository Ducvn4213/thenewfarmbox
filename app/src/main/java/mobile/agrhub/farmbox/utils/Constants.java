package mobile.agrhub.farmbox.utils;

public class Constants {
    public static final String CAMERA_INFO_KEY = "CAMERA_INFO_KEY";
    public static final String NOTIFICATION_SETTING_KEY = "NOTIFICATION_SETTING_KEY";
    public static final String FARM_ID_KEY = "FARM_ID_KEY";
    public static final String FACTORY_ID_KEY = "FACTORY_ID_KEY";
    public static final String SETTING_VALUE_KEY = "SETTING_VALUE_KEY";
    public static final String SETTING_INFO_KEY = "SETTING_INFO_KEY";
    public static final String SETTING_POSITION_KEY = "SETTING_POSITION_KEY";
    public static final String SCREEN_TITLE_KEY = "SCREEN_TITLE_KEY";
    public static final String DEVICE_INFO_KEY = "DEVICE_INFO_KEY";
    public static final String OPTION_LIST_KEY = "OPTION_LIST_KEY";
    public static final String SELECTED_OPTION_KEY = "SELECTED_OPTION_KEY";
    public static final String FACTORY_CONTROL_MODE_KEY = "factory_control_mode";
    public static final String SETTING_TEMPERATURE_UNIT_KEY = "SETTING_TEMPERATURE_UNIT_";
    public static final String SETTING_NUTRITION_UNIT_KEY = "SETTING_NUTRITION_UNIT_";
    public static final String FARM_PRODUCT_INFO_KEY = "FARM_PRODUCT_INFO_KEY";
    public static final String GAP_CERTIFICATE_INFO_KEY = "GAP_CERTIFICATE_INFO_KEY";
    public static final String ORDER_CART_LIST_KEY = "ORDER_CART_LIST_KEY";
    public static final String ORDER_INFO_KEY = "ORDER_INFO_KEY";
    public static final String ORDER_ID_KEY = "ORDER_ID_KEY";
    public static final String CART_INFO_KEY = "CART_INFO_KEY";
    public static final String SERIAL_INFO_LIST_KEY = "SERIAL_INFO_LIST_KEY";

    public static final String FIELD_IS_ACTIVE = "is_active";
    public static final String FIELD_FACTORY_START_TIME = "factory_start_time";
    public static final String FIELD_CONTROLLER_TYPE = "controller_type";
    public static final String FIELD_ORDER_STATE = "order_state";
    public static final String FIELD_FARM_ID = "farm_id";

    public static String DEF_LIGHT_SENSOR_UNIT = "lux";
    public static String DEF_HUMIDITY_SENSOR_UNIT = "%";
    public static String DEF_PH_SENSOR_UNIT = "pH";
    public static String DEF_CO2_SENSOR_UNIT = "ppm";
    public static String DEF_WATER_LEVEL_SENSOR_UNIT = "%";
    public static String DEF_WATER_LEVEL_SENSOR_CM_UNIT = "cm";
    public static String DEF_UV_SENSOR_UNIT = "uV";
    public static String DEF_PRESSURE_SENSOR_UNIT = "hPa";
    public static String DEF_SOUND_SENSOR_UNIT = "dB";
    public static String DEF_VOC_SENSOR_UNIT = "ppb";
    public static String DEF_MAGNETIC_SENSOR_UNIT = "mT";
}
