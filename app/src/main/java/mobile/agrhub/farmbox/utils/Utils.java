package mobile.agrhub.farmbox.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.google.gson.Gson;
import com.squareup.picasso.Transformation;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import mobile.agrhub.farmbox.R;
import mobile.agrhub.farmbox.service.FarmBoxAppController;
import mobile.agrhub.farmbox.service.model.MultiLanguageObject;

import static android.content.Context.MODE_PRIVATE;

public class Utils {

    public static final int DiaryType_UTM = 0;
    public static final int DiaryType_Hourly = 1;
    public static final int DiaryType_Daily = 2;
    public static final int DiaryType_Weekly = 3;
    public static final int DiaryType_Monthly = 4;


    private static final String PLANT_FACTORY_PREFS_NAME = "PLANT_FACTORY_PREFS_NAME";
    private static final String PREFS_KEY_FIRST_USING_APP = "PLANT_FACTORY_PREFS_NAME";

    public static final int PERMISSIONS_LOCATION_REQUEST = 8760;
    public static final int PERMISSIONS_CAMERA_REQUEST = 8761;

    private static final int[] COLOR_ARRAY = {
            Color.argb(255, 46, 204, 113),
            Color.argb(255, 52, 152, 219),
            Color.argb(255, 155, 89, 182),
            Color.argb(255, 26, 188, 156),
            Color.argb(255, 52, 73, 94)};

    public static interface SimpleCallback {
        void onDone();
    }

    public static int getRandomColor(int index) {
        int theIndex = index % COLOR_ARRAY.length;
        return COLOR_ARRAY[theIndex];
    }

    public static String getMarginTimeString(Context context, long theTime) {
        long curDateMS = System.currentTimeMillis();
        long diff = curDateMS - theTime;

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = diff / daysInMilli;
        //diff = diff * daysInMilli;

        long elapsedHours = diff / hoursInMilli;
        //diff = diff * hoursInMilli;

        long elapsedMinutes = diff / minutesInMilli;
        //diff = diff % minutesInMilli;

        long elapsedSeconds = diff / secondsInMilli;


        if (elapsedDays > 0) {
            return elapsedDays + " " + context.getString(R.string.updated_time_text_day);
        }

        if (elapsedHours > 0) {
            return elapsedHours + " " + context.getString(R.string.updated_time_text_hour);
        }

        if (elapsedMinutes > 0) {
            return elapsedMinutes + " " + context.getString(R.string.updated_time_text_minute);
        } else {
            return elapsedSeconds + " " + context.getString(R.string.updated_time_text_second);
        }
    }

    public static String getValueFromMultiLanguageString(String data) {
        Gson gson = new Gson();
        MultiLanguageObject multiLanguageObject = gson.fromJson(data, MultiLanguageObject.class);

        if ("vi".equals(FarmBoxAppController.getInstance().getLanguage())) {
            try {
                String val = URLDecoder.decode(multiLanguageObject.vi, "UTF-8");
                if (val == null || val.isEmpty()) {
                    return URLDecoder.decode(multiLanguageObject.en, "UTF-8");
                }
                return val;
            } catch (UnsupportedEncodingException e) {
                return multiLanguageObject.vi;
            }
        }

        try {
            return URLDecoder.decode(multiLanguageObject.en, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return multiLanguageObject.en;
        }
    }

    public static long getLongFromMultiLanguageString(String data) {
        String strVal = Utils.getValueFromMultiLanguageString(data);
        return Long.valueOf(strVal);
    }

    public static class CircleTransform implements Transformation {
        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());

            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }

            Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap,
                    BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);

            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);

            squaredBitmap.recycle();
            return bitmap;
        }

        @Override
        public String key() {
            return "circle";
        }
    }

    public static void savePreference(Context context, String key, String data) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PLANT_FACTORY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString(key, data);
        editor.commit();
    }

    public static boolean existPreference(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences(PLANT_FACTORY_PREFS_NAME, MODE_PRIVATE);
        return prefs.contains(key);
    }

    public static String getPreference(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences(PLANT_FACTORY_PREFS_NAME, MODE_PRIVATE);
        String restoredText = prefs.getString(key, null);
        return restoredText;
    }

    public static void savePreference(Context context, String key, long data) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PLANT_FACTORY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putLong(key, data);
        editor.commit();
    }

    public static void savePreference(Context context, String key, int data) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PLANT_FACTORY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putInt(key, data);
        editor.commit();
    }

    public static long getLongPreference(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences(PLANT_FACTORY_PREFS_NAME, MODE_PRIVATE);
        long restoredText = prefs.getLong(key, 0);
        return restoredText;
    }

    public static int getIntPreference(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences(PLANT_FACTORY_PREFS_NAME, MODE_PRIVATE);
        int restoredText = prefs.getInt(key, 0);
        return restoredText;
    }

    public static Boolean isFirstUsingApp(Context context) {
        String keyCheck = getPreference(context, PREFS_KEY_FIRST_USING_APP);
        return keyCheck == null;
    }

    public static void showErrorDialog(final Context context, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(context, R.style.AppTheme_Dialog_Alert)
                .setTitle(R.string.button_error)
                .setMessage(message)
                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    public static void showDialog(final Context context, String title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(context, R.style.AppTheme_Dialog_Alert)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface arg0) {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(context.getResources().getColor(R.color.colorPrimary));
                }
            });

            dialog.show();
        }
    }

    public static void showFarmBoxNotInitMessage(Context context, final SimpleCallback callback) {
        final AlertDialog dialog = new AlertDialog.Builder(context, R.style.AppTheme_Dialog_Alert)
                .setTitle(R.string.button_error)
                .setMessage(R.string.dialog_message_farmbox_not_init)
                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        if (callback != null) callback.onDone();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    public static Bitmap getCircleBitmap(Bitmap bitmap) {
        int theSize = Math.min(bitmap.getWidth(), bitmap.getHeight());
        final Bitmap output = Bitmap.createBitmap(theSize,
                theSize, Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, theSize, theSize);
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        //bitmap.recycle();

        return output;
    }

    public static void hideKeyboard(@NonNull Activity activity) {
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String encode(String source) {
        byte[] data = source.getBytes(StandardCharsets.UTF_8);
        String returnValue = Base64.encodeToString(data, Base64.DEFAULT);
        return returnValue;
    }

    public static String encode(boolean val) {
        return Utils.encode(val ? "true" : "false");
    }

    public static String encode(int val) {
        return Utils.encode(String.valueOf(val));
    }

    public static String encode(long val) {
        return Utils.encode(String.valueOf(val));
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String decode(String source) {
        byte[] data = Base64.decode(source, Base64.DEFAULT);
        String returnValue = new String(data, StandardCharsets.UTF_8);
        return returnValue;
    }

    public interface ConvertBitmapToStringCallback {
        void onSuccess(String bitmapString);

        void onFail();
    }

    private static String bitMapToString(Bitmap bitmap) {
        //Bitmap scaleBitmap = bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, 0);
        temp = temp.replace("\n", "").replace("\r", "");
        return temp;
    }

    public static class ConvertBitmapToStringTask extends AsyncTask<String, Void, String> {

        private Bitmap mBitmap;
        private ConvertBitmapToStringCallback mCallback;

        public void setBitmap(Bitmap bitmap) {
            this.mBitmap = bitmap;
        }

        public void setCallback(ConvertBitmapToStringCallback callback) {
            this.mCallback = callback;
        }

        protected String doInBackground(String... bitmap) {
            return bitMapToString(this.mBitmap);
        }

        protected void onPostExecute(String result) {
            if (mCallback != null) {
                mCallback.onSuccess(result);
            }
        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static <T> T getField(Class targetClass, Object instance, String fieldName) {
        try {
            Field field = targetClass.getDeclaredField(fieldName);
            field.setAccessible(true);
            return (T) field.get(instance);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public static void setRecycleViewHeightBasedOnChildren(Context context, RecyclerView listView, int itemHeight) {
        RecyclerView.Adapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getItemCount(); i++) {
            totalHeight += itemHeight;
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = (int) getPixelFromDP((float) totalHeight, context);
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public static String getStringFromCron(Context context, String input) {
        try {
            String[] crons = input.split(" ");

            int m = Integer.parseInt(crons[0]);
            int h = Integer.parseInt(crons[1]);
            String d = crons[4];

            String results = "";

            if (h < 10) {
                results += "0" + h;
            } else {
                results += h;
            }

            results += ":";

            if (m < 10) {
                results += "0" + m;
            } else {
                results += m;
            }

            results += " " + context.getString(R.string.at) + " ";

            switch (d) {
                case "*":
                    results += context.getString(R.string.cron_time_every_day_week);
                    break;
                case "1":
                    results += context.getString(R.string.cron_time_monday);
                    break;
                case "2":
                    results += context.getString(R.string.cron_time_tuesday);
                    break;
                case "3":
                    results += context.getString(R.string.cron_time_wednesday);
                    break;
                case "4":
                    results += context.getString(R.string.cron_time_thursday);
                    break;
                case "5":
                    results += context.getString(R.string.cron_time_friday);
                    break;
                case "6":
                    results += context.getString(R.string.cron_time_saturday);
                    break;
                case "7":
                    results += context.getString(R.string.cron_time_sunday);
                    break;
                default:
                    break;
            }

            return results;
        } catch (Exception e) {
            e.printStackTrace();
            return "------";
        }
    }

    public static String getDate(long milliSeconds, String dateFormat) {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static String getDate(int year, int month, int date, String dateFormat) {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, date);
        return formatter.format(calendar.getTime());
    }

    public static String getTime(long data) {
        long h = data / 60;
        long m = data % 60;

        return correctTimeUnit(h) + ":" + correctTimeUnit(m);
    }

    public static String correctTimeUnit(long input) {
        if (input < 10) {
            return "0" + input;
        }

        return input + "";
    }

    public static float getPixelFromDP(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    public static String formatNumber(long val, String pattern) {
        DecimalFormat format = new DecimalFormat(pattern);
        return format.format(val);
    }

    public static String formatPrice(long val, String forceLang) {
        if (forceLang == null || forceLang.isEmpty()) {
            forceLang = FarmBoxAppController.getInstance().getLanguage();
        }
        if ("vi".equals(forceLang)) {
            return Utils.l2s(val) + "đ";
        } else {
            return '$' +Utils.l2s(val);
        }
    }

    public static String formatPrice(long val) {
        return Utils.formatPrice(val, null);
    }

    public static String formatPriceVND(long val) {
        return Utils.formatPrice(val, "vi");
    }

    public static String getUserCountry(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            String simCountry = telephonyManager.getSimCountryIso();
            if (simCountry != null && simCountry.length() == 2) {
                return simCountry.toLowerCase(Locale.US);
            } else {
                if (telephonyManager.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) {
                    String networkCountry = telephonyManager.getNetworkCountryIso();
                    if (networkCountry != null && networkCountry.length() == 2) {
                        return networkCountry.toLowerCase(Locale.US);
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public static boolean isDemoMode() {
        FarmBoxAppController app = FarmBoxAppController.getInstance();
        if (app == null) {
            return false;
        } else {
            if (app.isDemoMode()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public static long c2f(long c) {
        return Math.round((c * 9 / 5.0) + 32);
    }

    public static long f2c(long f) {
        return Math.round((f - 32) * (5 / 9.0));
    }

    public static double ms2ppm(double ms) {
        return ms * 500;
    }

    public static double ppm2ms(double ppm) {
        return ppm / 500;
    }

    public static String f2s(double val) {
        return Utils.f2s(val, false);
    }

    public static String f2s(double val, boolean isDataVal) {
        String formatStr = "#,##0.#";
        if (isDataVal) {
            formatStr = "0.#";
        }
        DecimalFormat decimalFormat = new DecimalFormat(formatStr, DecimalFormatSymbols.getInstance(Locale.US));
        return decimalFormat.format(val);
    }

    public static String l2s(long val) {
        return Utils.l2s(val, false);
    }

    public static String l2s(long val, boolean isDataVal) {
        String formatStr = "#,##0";
        if (isDataVal) {
            formatStr = "0";
        }
        DecimalFormat decimalFormat = new DecimalFormat(formatStr, DecimalFormatSymbols.getInstance(Locale.US));
        return decimalFormat.format(val);
    }

    public static String i2s(int val) {
        return Utils.i2s(val, false);
    }

    public static String i2s(int val, boolean isDataVal) {
        String formatStr = "#,##0";
        if (isDataVal) {
            formatStr = "0";
        }
        DecimalFormat decimalFormat = new DecimalFormat(formatStr, DecimalFormatSymbols.getInstance(Locale.US));
        return decimalFormat.format(val);
    }

    public static long minutesBetween(Calendar startDate, Calendar endDate) {
        long end = endDate.getTimeInMillis();
        long start = startDate.getTimeInMillis();
        return Math.abs(end - start) / (1000 * 60);
    }

    public static int str2Int(String str) throws ParseException {
        Number num = Utils.numberFormat().parse(str.replace(",", ""));
        return num.intValue();
    }

    public static long str2Long(String str) throws ParseException {
        Number num = Utils.numberFormat().parse(str.replace(",", ""));
        return num.longValue();
    }

    public static float str2Float(String str) throws ParseException {
        Number num = Utils.numberFormat().parse(str.replace(",", ""));
        return num.floatValue();
    }

    public static double str2Double(String str) throws ParseException {
        Number num = Utils.numberFormat().parse(str.replace(",", ""));
        return num.doubleValue();
    }

    public static NumberFormat numberFormat() {
        return NumberFormat.getInstance(Locale.US);
    }

    public static String join(String separator, String[] arr) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < arr.length; i++) {
            if (i > 0) {
                builder.append(separator);
            }
            builder.append(arr[i]);
        }
        return builder.toString();
    }

    public static long serialNumber(String serial) {
        long val = -1;
        if (serial.contains("-")) {
            String[] arr = serial.split("-");
            try {
                val = str2Long(arr[arr.length - 1]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return val;
    }

    public static String toSerialNumber(String serial, int quantity) {
        if (serial.contains("-")) {
            String[] arr = serial.split("-");
            long val = -1;
            try {
                val = str2Long(arr[arr.length - 1]);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (val >= 0) {
                long to = val + (quantity - 1);
                String formatStr = "%0" + arr[arr.length - 1].length() + "d";
                arr[arr.length - 1] = String.format(formatStr, to);

                return Utils.join("-", arr);
            }
        }
        return "";
    }

    public static int sizeOfSerialNumber(String fromSerial, String toSerial) {
        int val = 0;
        long from = Utils.serialNumber(fromSerial);
        long to = Utils.serialNumber(toSerial);
        if (from >=0 && to >= 0 && from <= to) {
            val = (int)((to - from) + 1);
        }
        return val;
    }

    public static void doVibrator(Context context) {
        Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(250, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(250);
        }
    }
}
